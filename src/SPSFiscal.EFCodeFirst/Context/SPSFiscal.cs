﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SPSFiscal.EFCodeFirst.TypeConfiguration;
using SPSFiscal.EFCodeFirst.TypeConfiguration.Dime;
using SPSFiscal.Model;
using SPSFiscal.Model.Dime;

namespace SPSFiscal.EFCodeFirst.Context
{
   internal class SPSFiscal : DbContext
    {
        public IDbSet<DimeTabelaMunicipiosModel> TabelaMunicipios { get; set; } //OK
        public IDbSet<DimeTabelaCodAtividadeModel> TabelaCodAtividade { get; set; } //OK
        public IDbSet<DimeContabilistaModel> Contabilista { get; set; } //OK
        public IDbSet<DimeContribuinteModel> Contribuinte { get; set; } //OK
        public IDbSet<DimeContribuinteDeclaracaoModel> ContribuinteDeclaracao { get; set; } //OK
        public IDbSet<DimeDeclaracaoModel> Declaracao { get; set; } //OK
        public IDbSet<DimeTipoRegistroModel> TipoRegistro { get; set; } //OK
        public IDbSet<DimeRegistroDeclaracaoModel> RegistroDeclaracao { get; set; } //OK
        public IDbSet<DimeDadosQuadro01Model> DadosQuadro01 { get; set; } //OK
        public IDbSet<DimeDadosQuadro02Model> DadosQuadro02 { get; set; } //OK
        public IDbSet<DimeTabelaQuadro03Model> TabelaQuadro03 { get; set; } //OK
        public IDbSet<DimeDadosQuadro03Model> DadosQuadro03 { get; set; } //OK
        public IDbSet<DimeTabelaQuadro04Model> TabelaQuadro04 { get; set; } //OK
        public IDbSet<DimeDadosQuadro04Model> DadosQuadro04 { get; set; } //OK
        public IDbSet<DimeTabelaQuadro05Model> TabelaQuadro05 { get; set; } //OK
        public IDbSet<DimeDadosQuadro05Model> DadosQuadro05 { get; set; } //OK
        public IDbSet<DimeTabelaQuadro09Model> TabelaQuadro09 { get; set; } //OK
        public IDbSet<DimeDadosQuadro09Model> DadosQuadro09 { get; set; } //OK
        public IDbSet<DimeTabelaQuadro11Model> TabelaQuadro11 { get; set; } //OK
        public IDbSet<DimeDadosQuadro11Model> DadosQuadro11 { get; set; } //OK
        public IDbSet<DimeDadosQuadro12Model> DadosQuadro12 { get; set; } //OK
        public IDbSet<DimeTabelaQuadro14Model> TabelaQuadro14 { get; set; } //OK
        public IDbSet<DimeDadosQuadro14Model> DadosQuadro14 { get; set; } //OK
        public IDbSet<DimeDadosQuadro46Model> DadosQuadro46 { get; set; } //OK
        public IDbSet<DimeDadosQuadro48Model> DadosQuadro48 { get; set; }
        public IDbSet<DimeDadosQuadro49Model> DadosQuadro49 { get; set; }
        public IDbSet<DimeDadosQuadro50Model> DadosQuadro50 { get; set; }
        public IDbSet<DimeTabelaQuadro80Model> TabelaQuadro80 { get; set; }
        public IDbSet<DimeDadosQuadro80Model> DadosQuadro80 { get; set; }
        public IDbSet<DimeTabelaQuadro81Model> TabelaQuadro81 { get; set; }
        public IDbSet<DimeDadosQuadro81Model> DadosQuadro81 { get; set; }
        public IDbSet<DimeTabelaQuadro82Model> TabelaQuadro82 { get; set; }
        public IDbSet<DimeDadosQuadro82Model> DadosQuadro82 { get; set; }
        public IDbSet<DimeTabelaQuadro83Model> TabelaQuadro83 { get; set; }
        public IDbSet<DimeDadosQuadro83Model> DadosQuadro83 { get; set; }
        public IDbSet<DimeTabelaQuadro84Model> TabelaQuadro84 { get; set; }
        public IDbSet<DimeDadosQuadro84Model> DadosQuadro84 { get; set; }
        public IDbSet<DimeTabelaQuadro90Model> TabelaQuadro90 { get; set; }
        public IDbSet<DimeDadosQuadro90Model> DadosQuadro90 { get; set; }
        public IDbSet<DimeTabelaQuadro91Model> TabelaQuadro91 { get; set; }
        public IDbSet<DimeDadosQuadro91Model> DadosQuadro91 { get; set; }
        public IDbSet<DimeTabelaQuadro92Model> TabelaQuadro92 { get; set; }
        public IDbSet<DimeDadosQuadro92Model> DadosQuadro92 { get; set; }
        public IDbSet<DimeTabelaQuadro93Model> TabelaQuadro93 { get; set; }
        public IDbSet<DimeDadosQuadro93Model> DadosQuadro93 { get; set; }
        public IDbSet<DimeTabelaQuadro94Model> TabelaQuadro94 { get; set; }
        public IDbSet<DimeDadosQuadro94Model> DadosQuadro94 { get; set; }
        public IDbSet<DimePeriodoApuracaoModel> DimePeriodoApuracao { get; set; }

        public IDbSet<DimeDebitosApuracaoModel> DimeDebitosApuracaoModel { get; set; }
        public IDbSet<DimeEntradasP1Model> EntradasP1 { get; set; }
        public IDbSet<DimeSaidasP2Model> SaidasP2 { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new TabelaMunicipiosTypeConfiguration());
            modelBuilder.Configurations.Add(new TabelaCodAtividadeTypeConfiguration());
            modelBuilder.Configurations.Add(new ContabilistaTypeConfiguration());
            modelBuilder.Configurations.Add(new ContribuinteTypeConfiguration());
            modelBuilder.Configurations.Add(new DeclaracaoTypeConfiguration());
            modelBuilder.Configurations.Add(new ContribuinteDeclaracaoTypeConfiguration());
            modelBuilder.Configurations.Add(new TipoRegistroTypeConfiguration());
            modelBuilder.Configurations.Add(new RegistroDeclaracaoTypeConfiguration());
            modelBuilder.Configurations.Add(new DadosQuadro01TypeConfiguration());
            modelBuilder.Configurations.Add(new DadosQuadro02TypeConfiguration());
            modelBuilder.Configurations.Add(new TabelaQuadro03TypeConfiguration());
            modelBuilder.Configurations.Add(new DadosQuadro03TypeConfiguration());
            modelBuilder.Configurations.Add(new TabelaQuadro04TypeConfiguration());
            modelBuilder.Configurations.Add(new DadosQuadro04TypeConfiguration());
            modelBuilder.Configurations.Add(new TabelaQuadro05TypeConfiguration());
            modelBuilder.Configurations.Add(new DadosQuadro05TypeConfiguration());
            modelBuilder.Configurations.Add(new TabelaQuadro09TypeConfiguration());
            modelBuilder.Configurations.Add(new DadosQuadro09TypeConfiguration());
            modelBuilder.Configurations.Add(new TabelaQuadro11TypeConfiguration());
            modelBuilder.Configurations.Add(new DadosQuadro11TypeConfiguration());
            modelBuilder.Configurations.Add(new DadosQuadro12TypeConfiguration());
            modelBuilder.Configurations.Add(new TabelaQuadro14TypeConfiguration());
            modelBuilder.Configurations.Add(new DadosQuadro14TypeConfiguration());
            modelBuilder.Configurations.Add(new DadosQuadro46TypeConfiguration());
            modelBuilder.Configurations.Add(new DadosQuadro48TypeConfiguration());
            modelBuilder.Configurations.Add(new DadosQuadro49TypeConfiguration());
            modelBuilder.Configurations.Add(new DadosQuadro50TypeConfiguration());
            modelBuilder.Configurations.Add(new TabelaQuadro80TypeConfiguration());
            modelBuilder.Configurations.Add(new DadosQuadro80TypeConfiguration());
            modelBuilder.Configurations.Add(new TabelaQuadro81TypeConfiguration());
            modelBuilder.Configurations.Add(new DadosQuadro81TypeConfiguration());
            modelBuilder.Configurations.Add(new TabelaQuadro82TypeConfiguration());
            modelBuilder.Configurations.Add(new DadosQuadro82TypeConfiguration());
            modelBuilder.Configurations.Add(new TabelaQuadro83TypeConfiguration());
            modelBuilder.Configurations.Add(new DadosQuadro83TypeConfiguration());
            modelBuilder.Configurations.Add(new TabelaQuadro84TypeConfiguration());
            modelBuilder.Configurations.Add(new DadosQuadro84TypeConfiguration());
            modelBuilder.Configurations.Add(new TabelaQuadro90TypeConfiguration());
            modelBuilder.Configurations.Add(new DadosQuadro90TypeConfiguration());
            modelBuilder.Configurations.Add(new TabelaQuadro91TypeConfiguration());
            modelBuilder.Configurations.Add(new DadosQuadro91TypeConfiguration());
            modelBuilder.Configurations.Add(new TabelaQuadro92TypeConfiguration());
            modelBuilder.Configurations.Add(new DadosQuadro92TypeConfiguration());
            modelBuilder.Configurations.Add(new TabelaQuadro93TypeConfiguration());
            modelBuilder.Configurations.Add(new DadosQuadro93TypeConfiguration());
            modelBuilder.Configurations.Add(new TabelaQuadro94TypeConfiguration());
            modelBuilder.Configurations.Add(new DadosQuadro94TypeConfiguration());
            modelBuilder.Configurations.Add(new PeriodoApuracaoTypeConfiguration());
            modelBuilder.Configurations.Add(new DebitosApuracaoTypeConfiguration());
            modelBuilder.Configurations.Add(new EntradaP1TypeConfiguration());
            modelBuilder.Configurations.Add(new SaidasP2TypeConfiguration());

            modelBuilder.Properties<decimal>().Configure(p => p.HasPrecision(15, 2));
        }

    }
}
