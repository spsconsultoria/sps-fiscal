﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPSFiscal.EFCodeFirst
{
    public static class Migrate
    {
        public static void ExecuteMigration()
        {
            try
            {
                System.Console.Read();
                string path = System.AppDomain.CurrentDomain.BaseDirectory.ToString();
                var update = new Process
                {
                    StartInfo =
                    {
                        FileName = path + @"migrate.exe",
                        UseShellExecute = true,
                        Arguments = @"SPSFiscal.EFCodeFirst.dll /startUpDirectory:" + path +
                                    "/startUpConfigurationFile:" + path + @"\SPSFiscal.Addon.exe.Config",
                        RedirectStandardOutput = false
                    }
                };
                update.Start();
            }
            catch (Exception e)
            {
                System.Console.WriteLine(e);
                throw;
            }
        }
    }
}
