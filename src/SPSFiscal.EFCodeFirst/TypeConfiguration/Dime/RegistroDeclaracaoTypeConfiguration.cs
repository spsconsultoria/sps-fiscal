﻿using System.ComponentModel.DataAnnotations.Schema;
using SPSFiscal.Common.Entity;
using SPSFiscal.Model;

namespace SPSFiscal.EFCodeFirst.TypeConfiguration.Dime
{
    public class RegistroDeclaracaoTypeConfiguration : SPSFiscalEntityAbstractConfig<DimeRegistroDeclaracaoModel>
    {
        protected override void ConfigurarNomeTabela()
        {
            ToTable("RegistroDeclaracao", "dime");
        }

        protected override void ConfigurarCamposTabela()
        {
            Property(p => p.IdRegistroDeclaracao).IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(p => p.IdDeclaracao).IsRequired();

            Property(p => p.TipoRegistro).IsRequired();
        }

        protected override void ConfigurarChavePrimaria()
        {
            HasKey(p => p.IdRegistroDeclaracao);
        }

        protected override void ConfigurarChavesEstrangeiras()
        {
            //HasOptional(p => p.Declaracao)
            //   .WithMany()
            //   .HasForeignKey(p => p.IdDeclaracao);

            //HasOptional(p => p.TipoRegistroV)
            //   .WithMany()
            //   .HasForeignKey(p => p.TipoRegistro);
        }
    }
}