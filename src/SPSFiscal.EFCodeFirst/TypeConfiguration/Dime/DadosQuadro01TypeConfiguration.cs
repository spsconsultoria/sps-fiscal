﻿using System.ComponentModel.DataAnnotations.Schema;
using SPSFiscal.Common.Entity;
using SPSFiscal.Model;

namespace SPSFiscal.EFCodeFirst.TypeConfiguration.Dime
{
    public class DadosQuadro01TypeConfiguration : SPSFiscalEntityAbstractConfig<DimeDadosQuadro01Model>
    {
        protected override void ConfigurarNomeTabela()
        {
            ToTable("DadosQuadro01", "dime");
        }

        protected override void ConfigurarCamposTabela()
        {
            Property(p => p.IdLancamento).IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(p => p.IdDeclaracao).IsRequired();

            Property(p => p.CFOP).HasMaxLength(5);

            Property(p => p.Vl_Contabil);

            Property(p => p.Vl_Base_Calc);

            Property(p => p.Vl_Imp_Cred);

            Property(p => p.Vl_Isentas_Nao_Trib);

            Property(p => p.Vl_Outras);

            Property(p => p.Vl_Base_Calc_IR);

            Property(p => p.Vl_IR);

            Property(p => p.Vl_Difal);
        }

        protected override void ConfigurarChavePrimaria()
        {
            HasKey(p => p.IdLancamento);
        }

        protected override void ConfigurarChavesEstrangeiras()
        {
            //HasRequired(p => p.Declaracao)
            //    .WithMany(p => p.DadosQuadro01)
            //    .HasForeignKey(p => p.IdDeclaracao);
        }
    }
}