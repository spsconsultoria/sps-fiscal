﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using SPSFiscal.Common.Entity;
using SPSFiscal.Model;

namespace SPSFiscal.EFCodeFirst.TypeConfiguration.Dime
{
    public class DadosQuadro49TypeConfiguration : SPSFiscalEntityAbstractConfig<DimeDadosQuadro49Model>
    {
        protected override void ConfigurarNomeTabela()
        {
            
            ToTable("DadosQuadro49", "dime");
        }

        protected override void ConfigurarCamposTabela()
        {
            Property(p => p.IdLancamento).IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(p => p.IdDeclaracao).IsRequired();

            Property(p => p.Sigla_Estado).HasMaxLength(2);

            Property(p => p.Vl_Contabil);

            Property(p => p.Vl_Base_Calc);

            Property(p => p.Vl_Outras);

            Property(p => p.Vl_ST);

            Property(p => p.Vl_ST_Outros);
        }

        protected override void ConfigurarChavePrimaria()
        {
            HasKey(p => p.IdLancamento);
        }

        protected override void ConfigurarChavesEstrangeiras()
        {
            HasRequired(p => p.Declaracao)
                .WithMany(p => p.DadosQuadro49)
                .HasForeignKey(p => p.IdDeclaracao);
        }
    }
}