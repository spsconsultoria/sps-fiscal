﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SPSFiscal.Common.Entity;
using SPSFiscal.Model;

namespace SPSFiscal.EFCodeFirst.TypeConfiguration.Dime
{
    class SaidasP2TypeConfiguration : SPSFiscalEntityAbstractConfig<DimeSaidasP2Model>
    {
        protected override void ConfigurarNomeTabela()
        {
            ToTable("SaidasP2", "dime");
        }

        protected override void ConfigurarCamposTabela()
        {
            Property(p => p.IdSaida).IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(p => p.IdPeriodo).IsRequired();

            Property(p => p.BD).HasMaxLength(50);

            Property(p => p.Filial).HasMaxLength(5);

            Property(p => p.TPDoc).HasMaxLength(5);

            Property(p => p.Serial).HasMaxLength(30);

            Property(p => p.Especie).HasMaxLength(30);

            Property(p => p.CFOP).HasMaxLength(5);

            Property(p => p.Vl_Contabil);

            Property(p => p.Vl_Base_Calc);

            Property(p => p.Aliq);

            Property(p => p.Vl_Imp_Debit);

            Property(p => p.Vl_Isentas_Nao_Trib);

            Property(p => p.Vl_Outras);

            Property(p => p.Vl_Base_ST);

            Property(p => p.Vl_ST);

            Property(p => p.Vl_Base_Calc_IR);

            Property(p => p.Vl_IR);

            Property(p => p.Dia);
        }

        protected override void ConfigurarChavePrimaria()
        {
            HasKey(p => p.IdSaida);
        }

        protected override void ConfigurarChavesEstrangeiras()
        {
            //HasRequired(p => p.PeriodoApuracao).WithMany(p => p.SaidasP2).HasForeignKey(p => p.IdPeriodo);
        }
    }
}
