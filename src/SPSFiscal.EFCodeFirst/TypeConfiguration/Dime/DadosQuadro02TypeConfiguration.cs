﻿using System.ComponentModel.DataAnnotations.Schema;
using SPSFiscal.Common.Entity;
using SPSFiscal.Model;

namespace SPSFiscal.EFCodeFirst.TypeConfiguration.Dime
{
    public class DadosQuadro02TypeConfiguration : SPSFiscalEntityAbstractConfig<DimeDadosQuadro02Model>
    {
        protected override void ConfigurarNomeTabela()
        {
            ToTable("DadosQuadro02", "dime");
        }

        protected override void ConfigurarCamposTabela()
        {
            Property(p => p.IdLancamento).IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(p => p.IdDeclaracao).IsRequired();

            Property(p => p.CFOP).HasMaxLength(5);

            Property(p => p.Vl_Contabil);

            Property(p => p.Vl_Imp_Debit);

            Property(p => p.Vl_Base_Calc);

            Property(p => p.Vl_Isentas_Nao_Trib);

            Property(p => p.Vl_Outras);

            Property(p => p.Vl_Base_Calc_IR);

            Property(p => p.Vl_IR);
        }

        protected override void ConfigurarChavePrimaria()
        {
            HasKey(p => p.IdLancamento);
        }

        protected override void ConfigurarChavesEstrangeiras()
        {
            HasRequired(p => p.Declaracao)
               .WithMany(p => p.DadosQuadro02)
               .HasForeignKey(p => p.IdDeclaracao);
        }
    }
}