﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SPSFiscal.Common.Entity;
using SPSFiscal.Model;

namespace SPSFiscal.EFCodeFirst.TypeConfiguration.Dime
{
    class TabelaQuadro84TypeConfiguration : SPSFiscalEntityAbstractConfig<DimeTabelaQuadro84Model>
    {
        protected override void ConfigurarNomeTabela()
        {
            ToTable("TabelaQuadro84", "dime");
        }

        protected override void ConfigurarCamposTabela()
        {
            Property(p => p.NumeroItem).IsRequired().HasMaxLength(3);

            Property(p => p.Descricao).HasMaxLength(200);

            Property(p => p.TipoItem).HasMaxLength(2);
        }

        protected override void ConfigurarChavePrimaria()
        {
            HasKey(p => p.NumeroItem);
        }

        protected override void ConfigurarChavesEstrangeiras()
        {
            HasOptional(p => p.DadosQuadro84).WithOptionalPrincipal();
        }
    }
}
