﻿using System.ComponentModel.DataAnnotations.Schema;
using SPSFiscal.Common.Entity;
using SPSFiscal.Model;

namespace SPSFiscal.EFCodeFirst.TypeConfiguration.Dime
{
    public class DadosQuadro11TypeConfiguration : SPSFiscalEntityAbstractConfig<DimeDadosQuadro11Model>
    {
        protected override void ConfigurarNomeTabela()
        {
            ToTable("DadosQuadro11", "dime");
        }

        protected override void ConfigurarCamposTabela()
        {
            Property(p => p.IdDadosQuadro11).IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(p => p.ID).IsOptional();

            Property(p => p.NumeroItem).HasMaxLength(3).IsRequired();

            Property(p => p.IdDeclaracao).IsRequired();

            Property(p => p.Valor);
        }

        protected override void ConfigurarChavePrimaria()
        {
            HasKey(p => p.IdDadosQuadro11);
            //HasKey(p => p.IdDeclaracao);
        }

        protected override void ConfigurarChavesEstrangeiras()
        {
            //    HasRequired(p => p.Declaracao)
            //        .WithMany(p => p.DadosQuadro11)
            //        .HasForeignKey(p => p.IdDeclaracao);

            //    HasRequired(p => p.TabelaQuadro11)
            //        .WithMany(p => p.DadosQuadro11)
            //        .HasForeignKey(p => p.NumeroItem);
        }
    }
}