﻿using System.ComponentModel.DataAnnotations.Schema;
using SPSFiscal.Common.Entity;
using SPSFiscal.Model;

namespace SPSFiscal.EFCodeFirst.TypeConfiguration.Dime
{
    public class DadosQuadro04TypeConfiguration : SPSFiscalEntityAbstractConfig<DimeDadosQuadro04Model>
    {
        protected override void ConfigurarNomeTabela()
        {
            ToTable("DadosQuadro04", "dime");
        }

        protected override void ConfigurarCamposTabela()
        {
            Property(p => p.IdDadosQuadro4).IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(p => p.NumeroItem).HasMaxLength(3).IsRequired();

            Property(p => p.IdDeclaracao).IsRequired();

            Property(p => p.Valor);
        }

        protected override void ConfigurarChavePrimaria()
        {
            HasKey(p => p.IdDadosQuadro4);
            //HasKey(p => p.IdDeclaracao);
        }

        protected override void ConfigurarChavesEstrangeiras()
        {
            //HasRequired(p => p.Declaracao)
            //    .WithMany(p => p.DadosQuadro04)
            //    .HasForeignKey(p => p.IdDeclaracao);

            //HasRequired(p => p.TabelaQuadro04)
            //    .WithMany(p => p.DadosQuadro04)
            //    .HasForeignKey(p => p.NumeroItem);
        }
    }
}