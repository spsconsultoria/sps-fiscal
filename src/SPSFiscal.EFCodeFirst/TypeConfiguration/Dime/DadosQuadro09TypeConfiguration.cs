﻿using System.ComponentModel.DataAnnotations.Schema;
using SPSFiscal.Common.Entity;
using SPSFiscal.Model;

namespace SPSFiscal.EFCodeFirst.TypeConfiguration.Dime
{
    public class DadosQuadro09TypeConfiguration : SPSFiscalEntityAbstractConfig<DimeDadosQuadro09Model>
    {
        protected override void ConfigurarNomeTabela()
        {
            ToTable("DadosQuadro09", "dime");
        }

        protected override void ConfigurarCamposTabela()
        {
            Property(p => p.IdDadosQuadro9).IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(p => p.NumeroItem).HasMaxLength(3).IsRequired();

            Property(p => p.IdDeclaracao).IsRequired();

            Property(p => p.Valor);
        }

        protected override void ConfigurarChavePrimaria()
        {
            HasKey(p => p.IdDadosQuadro9);
            //HasKey(p => p.IdDeclaracao);
        }

        protected override void ConfigurarChavesEstrangeiras()
        {
            //HasRequired(p => p.Declaracao)
            //    .WithMany(p => p.DadosQuadro09)
            //    .HasForeignKey(p => p.IdDeclaracao);

            //HasRequired(p => p.TabelaQuadro09)
            //    .WithMany(p => p.DadosQuadro09)
            //    .HasForeignKey(p => p.NumeroItem);
        }
    }
}