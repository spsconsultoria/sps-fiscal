﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SPSFiscal.Common.Entity;
using SPSFiscal.Model;

namespace SPSFiscal.EFCodeFirst.TypeConfiguration.Dime
{
    class DadosQuadro48TypeConfiguration : SPSFiscalEntityAbstractConfig<DimeDadosQuadro48Model>
    {
        protected override void ConfigurarNomeTabela()
        {
            ToTable("DadosQuadro48", "dime");
        }

        protected override void ConfigurarCamposTabela()
        {
            Property(p => p.IdLancamento).IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(p => p.IdDeclaracao).IsRequired();

            Property(p => p.Cod_Municipio).HasMaxLength(5);

            Property(p => p.Vl_Perc_Ad);

            Property(p => p.Cod_Tipo_Atv).HasMaxLength(3);
        }

        protected override void ConfigurarChavePrimaria()
        {
            HasKey(p => p.IdLancamento);
        }

        protected override void ConfigurarChavesEstrangeiras()
        {
            HasRequired(p => p.Declaracao)
                .WithMany(p => p.DadosQuadro48)
                .HasForeignKey(p => p.IdDeclaracao);
        }
    }
}
