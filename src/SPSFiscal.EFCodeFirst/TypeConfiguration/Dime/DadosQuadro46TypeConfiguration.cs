﻿using System.ComponentModel.DataAnnotations.Schema;
using SPSFiscal.Common.Entity;
using SPSFiscal.Model;

namespace SPSFiscal.EFCodeFirst.TypeConfiguration.Dime
{
    public class DadosQuadro46TypeConfiguration : SPSFiscalEntityAbstractConfig<DimeDadosQuadro46Model>
    {
        protected override void ConfigurarNomeTabela()
        {
            ToTable("DadosQuadro46", "dime");
        }

        protected override void ConfigurarCamposTabela()
        {
            Property(p => p.IdLancamento).IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(p => p.IdDeclaracao).IsRequired();

            Property(p => p.Seq_Reg).IsRequired();

            Property(p => p.Cod_Identif).HasMaxLength(15);

            Property(p => p.Vl_Cred_Apur);

            Property(p => p.Ind_Origem).HasMaxLength(2);
        }

        protected override void ConfigurarChavePrimaria()
        {
            HasKey(p => p.IdLancamento);
        }

        protected override void ConfigurarChavesEstrangeiras()
        {
            HasRequired(p => p.Declaracao)
                .WithMany(p => p.DadosQuadro46)
                .HasForeignKey(p => p.IdDeclaracao);
        }
    }
}