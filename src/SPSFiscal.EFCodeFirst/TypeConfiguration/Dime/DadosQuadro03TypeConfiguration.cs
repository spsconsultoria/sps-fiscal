﻿using System.ComponentModel.DataAnnotations.Schema;
using SPSFiscal.Common.Entity;
using SPSFiscal.Model;

namespace SPSFiscal.EFCodeFirst.TypeConfiguration.Dime
{
    public class DadosQuadro03TypeConfiguration : SPSFiscalEntityAbstractConfig<DimeDadosQuadro03Model>
    {
        protected override void ConfigurarNomeTabela()
        {
            ToTable("DadosQuadro03", "dime");
        }

        protected override void ConfigurarCamposTabela()
        {
            Property(p => p.IdDadosQuadro3).IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(p => p.NumeroItem).HasMaxLength(3).IsRequired();

            Property(p => p.IdDeclaracao).IsRequired();

            Property(p => p.Valor);
        }

        protected override void ConfigurarChavePrimaria()
        {
            HasKey(p => p.IdDadosQuadro3);
            //HasKey(p => p.IdDeclaracao);
        }

        protected override void ConfigurarChavesEstrangeiras()
        {
            //HasRequired(p => p.Declaracao)
            //    .WithMany()
            //    .HasForeignKey(p => p.IdDeclaracao);

            //HasRequired(p => p.TabelaQuadro03)
            //    .WithMany()
            //    .HasForeignKey(p => p.NumeroItem);
        }
    }
}