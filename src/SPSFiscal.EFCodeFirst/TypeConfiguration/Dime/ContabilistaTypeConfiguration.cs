﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SPSFiscal.Common.Entity;
using SPSFiscal.Model;

namespace SPSFiscal.EFCodeFirst.TypeConfiguration
{
    class ContabilistaTypeConfiguration: SPSFiscalEntityAbstractConfig<DimeContabilistaModel>
    {
        protected override void ConfigurarNomeTabela()
        {
            ToTable("Contabilista", "dime");
        }

        protected override void ConfigurarCamposTabela()
        {
            Property(p => p.IdContabilista)//Campo
                .IsRequired()// É OBRIGATÓRIO
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)//AUTO INCREMENTO
                .HasColumnName("IdContabilista");//NOME DO CAMPO

            Property(p => p.CPF)
                .HasMaxLength(18);

            Property(p => p.NomeContabilista)
                .HasMaxLength(50);

            Property(p => p.Excluido)
                .HasMaxLength(1);
        }

        protected override void ConfigurarChavePrimaria()
        {
            HasKey(pk => pk.IdContabilista);
        }

        protected override void ConfigurarChavesEstrangeiras()
        {
            HasMany(p => p.Contribuinte).WithRequired(p => p.Contabilista).HasForeignKey(p => p.IdContabilista);
        }
    }
}
