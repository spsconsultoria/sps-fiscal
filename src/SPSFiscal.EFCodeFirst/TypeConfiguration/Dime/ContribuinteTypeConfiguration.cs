﻿using System.ComponentModel.DataAnnotations.Schema;
using SPSFiscal.Common.Entity;
using SPSFiscal.Model;

namespace SPSFiscal.EFCodeFirst.TypeConfiguration.Dime
{
    public class ContribuinteTypeConfiguration : SPSFiscalEntityAbstractConfig<DimeContribuinteModel>
    {
        protected override void ConfigurarNomeTabela()
        {
            ToTable("Contribuinte", "dime");
        }

        protected override void ConfigurarCamposTabela()
        {
            Property(p => p.IdContribuinte).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
                .IsRequired().HasColumnName("IdContribuinte");

            Property(p => p.IdContabilista);

            Property(p => p.NomeRazaoSocial).HasMaxLength(50);

            Property(p => p.CNPJ).HasMaxLength(18);

            Property(p => p.NroInscricao).HasMaxLength(20);

            Property(p => p.TipoDeclaracao).HasMaxLength(1);

            Property(p => p.RegimeApuracao).HasMaxLength(1);

            Property(p => p.PorteEmpresa).HasMaxLength(1);

            Property(p => p.ApuracaoConsolidada).HasMaxLength(1);

            Property(p => p.ApuracaoCentralizada).HasMaxLength(1);

            Property(p => p.TemCreditosPresumido).HasMaxLength(1);

            Property(p => p.TemCredIncentFiscais).HasMaxLength(1);

            Property(p => p.SubstitutoTributario).HasMaxLength(1);

            Property(p => p.TemEscritaContabil).HasMaxLength(1);

            Property(p => p.QtdeTrabAtividade);

            Property(p => p.ErpIdEmpresa).HasMaxLength(10);

            Property(p => p.ErpBaseDados).HasMaxLength(50);

            Property(p => p.Excluido).HasMaxLength(1);
        }

        protected override void ConfigurarChavePrimaria()
        {
            HasKey(p => p.IdContribuinte);
        }

        protected override void ConfigurarChavesEstrangeiras()
        {
            HasMany(p => p.ContribuinteDeclaracao)
                .WithRequired(p => p.Contribuinte)
                .HasForeignKey(p => p.IdContribuinte);

            HasRequired(p => p.Contabilista)
                .WithMany(p => p.Contribuinte)
                .HasForeignKey(fk => fk.IdContabilista);

            //HasRequired(p => p.Declaracao)
            //    .WithMany(p => p.Contribuinte)
            //    .HasForeignKey(fk => fk.IdContribuinte);
        }
    }
}