﻿using System.Security;
using SPSFiscal.Common.Entity;
using SPSFiscal.Model;

namespace SPSFiscal.EFCodeFirst.TypeConfiguration.Dime
{
    public class TipoRegistroTypeConfiguration : SPSFiscalEntityAbstractConfig<DimeTipoRegistroModel>
    {
        protected override void ConfigurarNomeTabela()
        {
            ToTable("TipoRegistro", "dime");
        }

        protected override void ConfigurarCamposTabela()
        {
            Property(p => p.Quadro).IsRequired().HasMaxLength(5);

            Property(p => p.TipoRegistro).IsRequired();

            Property(p => p.Descricao).HasMaxLength(100);
        }

        protected override void ConfigurarChavePrimaria()
        {
            HasKey(p => p.TipoRegistro);
        }

        protected override void ConfigurarChavesEstrangeiras()
        {
            HasOptional(p => p.RegistroDeclaracao).WithOptionalPrincipal();
        }
    }
}