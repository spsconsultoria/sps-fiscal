﻿using System.ComponentModel.DataAnnotations.Schema;
using SPSFiscal.Common.Entity;
using SPSFiscal.Model;

namespace SPSFiscal.EFCodeFirst.TypeConfiguration.Dime
{
    public class DadosQuadro14TypeConfiguration : SPSFiscalEntityAbstractConfig<DimeDadosQuadro14Model>
    {
        protected override void ConfigurarNomeTabela()
        {
            ToTable("DadosQuadro14", "dime");
        }

        protected override void ConfigurarCamposTabela()
        {
            Property(p => p.IdDadosQuadro14).IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(p => p.NumeroItem).HasMaxLength(3).IsRequired();

            Property(p => p.IdDeclaracao).IsRequired();

            Property(p => p.Valor);
        }

        protected override void ConfigurarChavePrimaria()
        {
            HasKey(p => p.IdDadosQuadro14);
            //HasKey(p => p.IdDeclaracao);
        }

        protected override void ConfigurarChavesEstrangeiras()
        {
            
        }
    }
}