﻿using System.ComponentModel.DataAnnotations.Schema;
using SPSFiscal.Common.Entity;
using SPSFiscal.Model;

namespace SPSFiscal.EFCodeFirst.TypeConfiguration.Dime
{
    public class DadosQuadro91TypeConfiguration : SPSFiscalEntityAbstractConfig<DimeDadosQuadro91Model>
    {
        protected override void ConfigurarNomeTabela()
        {
            ToTable("DadosQuadro91", "dime");
        }

        protected override void ConfigurarCamposTabela()
        {
            Property(p => p.IdDadosQuadro91).IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(p => p.NumeroItem).IsRequired().HasMaxLength(3);

            Property(p => p.IdDeclaracao).IsRequired();

            Property(p => p.Valor).IsRequired();
        }

        protected override void ConfigurarChavePrimaria()
        {
            HasKey(p => p.IdDadosQuadro91);
            //HasKey(p => p.IdDeclaracao);
        }

        protected override void ConfigurarChavesEstrangeiras()
        {
            
        }
    }
}