﻿using System.ComponentModel.DataAnnotations.Schema;
using SPSFiscal.Common.Entity;
using SPSFiscal.Model;

namespace SPSFiscal.EFCodeFirst.TypeConfiguration.Dime
{
    public class DadosQuadro12TypeConfiguration : SPSFiscalEntityAbstractConfig<DimeDadosQuadro12Model>
    {
        protected override void ConfigurarNomeTabela()
        {
            ToTable("DadosQuadro12", "dime");
        }

        protected override void ConfigurarCamposTabela()
        {
            Property(p => p.IdLancamento).IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(p => p.IdDeclaracao).IsRequired();

            Property(p => p.Ind_Org_Recol).HasMaxLength(2);

            Property(p => p.Cod_Class_Vcto).HasMaxLength(5);

            Property(p => p.Dt_Vcto_Recol);

            Property(p => p.Vl_Recol);

            Property(p => p.Cod_Class_Vcto).HasMaxLength(5);

            Property(p => p.Nro_Acordo).HasMaxLength(15);
        }

        protected override void ConfigurarChavePrimaria()
        {
            HasKey(p => p.IdLancamento);
        }

        protected override void ConfigurarChavesEstrangeiras()
        {
            //HasRequired(p => p.Declaracao)
            //    .WithMany(p => p.DadosQuadro12)
            //    .HasForeignKey(p => p.IdDeclaracao);
        }
    }
}