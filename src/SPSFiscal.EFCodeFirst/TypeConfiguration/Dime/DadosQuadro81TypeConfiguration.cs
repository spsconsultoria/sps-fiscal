﻿using System.ComponentModel.DataAnnotations.Schema;
using SPSFiscal.Common.Entity;
using SPSFiscal.Model;

namespace SPSFiscal.EFCodeFirst.TypeConfiguration.Dime
{
    public class DadosQuadro81TypeConfiguration : SPSFiscalEntityAbstractConfig<DimeDadosQuadro81Model>
    {
        protected override void ConfigurarNomeTabela()
        {
            ToTable("DadosQuadro81", "dime");
        }

        protected override void ConfigurarCamposTabela()
        {
            Property(p => p.IdDadosQuadro81).IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(p => p.NumeroItem).IsRequired().HasMaxLength(3);

            Property(p => p.IdDeclaracao).IsRequired();

            Property(p => p.Valor).IsRequired();
        }

        protected override void ConfigurarChavePrimaria()
        {
            HasKey(p => p.IdDadosQuadro81);
            //HasKey(p => p.IdDeclaracao);
        }

        protected override void ConfigurarChavesEstrangeiras()
        {
            
        }
    }
}