﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using SPSFiscal.Common.Entity;
using SPSFiscal.Model;

namespace SPSFiscal.EFCodeFirst.TypeConfiguration.Dime
{
    public class DeclaracaoTypeConfiguration : SPSFiscalEntityAbstractConfig<DimeDeclaracaoModel>
    {
        protected override void ConfigurarNomeTabela()
        {
            ToTable("Declaracao", "dime");
        }

        protected override void ConfigurarCamposTabela()
        {
            Property(p => p.IdDeclaracao).IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(p => p.IdContribuinte).IsRequired();

            Property(p => p.DtInicio);

            Property(p => p.DtTermino);

            Property(p => p.NomeContabilista).HasMaxLength(50);

            Property(p => p.ContabilistaCPF).HasMaxLength(15);

            Property(p => p.NomeContribuinte).HasMaxLength(50);

            Property(p => p.NroInscricao).HasMaxLength(20);

            Property(p => p.ContribuinteCNPJ).HasMaxLength(18);

            Property(p => p.PeriodoReferencia).HasMaxLength(8);

            Property(p => p.TipoDeclaracao).HasMaxLength(1);

            Property(p => p.RegimeApuracao).HasMaxLength(1);

            Property(p => p.PorteEmpresa).HasMaxLength(1);

            Property(p => p.ApuracaoConsolidada).HasMaxLength(1);

            Property(p => p.ApuracaoCentralizada).HasMaxLength(1);

            Property(p => p.TransCredPeriodo).HasMaxLength(1);

            Property(p => p.TemCreditosPresumido).HasMaxLength(1);

            Property(p => p.TemCredIncentFiscais).HasMaxLength(1);

            Property(p => p.Movimento).HasMaxLength(1);

            Property(p => p.SubstitutoTributario).HasMaxLength(1);

            Property(p => p.TemEscritaContabil).HasMaxLength(1);

            Property(p => p.QtdeTrabAtividade);

            Property(p => p.ErpBaseDados).HasMaxLength(50);

            Property(p => p.Excluido).HasMaxLength(1);
    }

        protected override void ConfigurarChavePrimaria()
        {
            HasKey(p => p.IdDeclaracao);
        }

        protected override void ConfigurarChavesEstrangeiras()
        {
            HasOptional(p => p.DadosQuadro01).WithOptionalPrincipal();
            HasOptional(p => p.DadosQuadro03).WithOptionalPrincipal();
            HasOptional(p => p.DadosQuadro04).WithOptionalPrincipal();
            HasOptional(p => p.DadosQuadro05).WithOptionalPrincipal();
            HasOptional(p => p.DadosQuadro09).WithOptionalPrincipal();
            HasOptional(p => p.DadosQuadro11).WithOptionalPrincipal();
            HasOptional(p => p.DadosQuadro12).WithOptionalPrincipal();
            HasOptional(p => p.DadosQuadro14).WithOptionalPrincipal();
            HasOptional(p => p.DadosQuadro80).WithOptionalPrincipal();
            HasOptional(p => p.DadosQuadro81).WithOptionalPrincipal();
            HasOptional(p => p.DadosQuadro82).WithOptionalPrincipal();
            HasOptional(p => p.DadosQuadro83).WithOptionalPrincipal();
            HasOptional(p => p.DadosQuadro84).WithOptionalPrincipal();
            HasOptional(p => p.DadosQuadro90).WithOptionalPrincipal();
            HasOptional(p => p.DadosQuadro91).WithOptionalPrincipal();
            HasOptional(p => p.DadosQuadro92).WithOptionalPrincipal();
            HasOptional(p => p.DadosQuadro93).WithOptionalPrincipal();
            HasOptional(p => p.DadosQuadro94).WithOptionalPrincipal();
            HasOptional(p => p.RegistroDeclaracao).WithOptionalPrincipal();
            HasOptional(p => p.Contribuinte).WithOptionalPrincipal();
        }
    }
}