﻿using System.ComponentModel.DataAnnotations.Schema;
using SPSFiscal.Common.Entity;
using SPSFiscal.Model;

namespace SPSFiscal.EFCodeFirst.TypeConfiguration.Dime
{
    public class DadosQuadro84TypeConfiguration : SPSFiscalEntityAbstractConfig<DimeDadosQuadro84Model>
    {
        protected override void ConfigurarNomeTabela()
        {
            ToTable("DadosQuadro84", "dime");
        }

        protected override void ConfigurarCamposTabela()
        {
            Property(p => p.IdDadosQuadro84).IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(p => p.NumeroItem).IsRequired().HasMaxLength(3);

            Property(p => p.IdDeclaracao).IsRequired();

            Property(p => p.Valor).IsRequired();
        }

        protected override void ConfigurarChavePrimaria()
        {
            HasKey(p => p.IdDadosQuadro84);
            //HasKey(p => p.IdDeclaracao);
        }

        protected override void ConfigurarChavesEstrangeiras()
        {
            //HasRequired(p => p.Declaracao).WithMany(p => p.DadosQuadro84).HasForeignKey(p => p.IdDeclaracao);
        }
    }
}