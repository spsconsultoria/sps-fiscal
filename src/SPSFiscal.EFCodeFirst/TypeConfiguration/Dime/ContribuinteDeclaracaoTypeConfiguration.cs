﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SPSFiscal.Common.Entity;
using SPSFiscal.Model;

namespace SPSFiscal.EFCodeFirst.TypeConfiguration.Dime
{
    class ContribuinteDeclaracaoTypeConfiguration: SPSFiscalEntityAbstractConfig<DimeContribuinteDeclaracaoModel>
    {
        protected override void ConfigurarNomeTabela()
        {
            ToTable("ContribuinteDeclaracao", "dime");
        }

        protected override void ConfigurarCamposTabela()
        {
            Property(p => p.IdDeclaracao).IsRequired();

            Property(p => p.IdContribuinte).IsRequired();

            Property(p => p.PeriodoReferencia).HasMaxLength(8);

            Property(p => p.DtCriacao);

            Property(p => p.HrCriacao).HasMaxLength(8);
        }

        protected override void ConfigurarChavePrimaria()
        {
            HasKey(p => p.IdDeclaracao);
        }

        protected override void ConfigurarChavesEstrangeiras()
        {
            HasRequired(p => p.Contribuinte)
                .WithMany(p => p.ContribuinteDeclaracao)
                .HasForeignKey(p => p.IdContribuinte);
        }
    }
}
