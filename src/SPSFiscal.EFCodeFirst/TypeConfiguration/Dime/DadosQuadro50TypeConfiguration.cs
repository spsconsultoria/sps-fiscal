﻿using System.ComponentModel.DataAnnotations.Schema;
using SPSFiscal.Common.Entity;
using SPSFiscal.Model;

namespace SPSFiscal.EFCodeFirst.TypeConfiguration.Dime
{
    public class DadosQuadro50TypeConfiguration : SPSFiscalEntityAbstractConfig<DimeDadosQuadro50Model>
    {
        protected override void ConfigurarNomeTabela()
        {
            ToTable("dadosQuadro50", "dime");
        }

        protected override void ConfigurarCamposTabela()
        {
            Property(p => p.IdLancamento).IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(p => p.IdLancamento).IsRequired();

            Property(p => p.Sigla_Estado).HasMaxLength(2);

            Property(p => p.Vl_Contabil_Nao_Contrib);

            Property(p => p.Vl_Contabil_Contrib);

            Property(p => p.Vl_Base_Calc_Nao_Contrib);

            Property(p => p.Vl_Base_Calc_Contrib);

            Property(p => p.Vl_Outras);

            Property(p => p.Vl_ST);
        }

        protected override void ConfigurarChavePrimaria()
        {
            HasKey(p => p.IdLancamento);
        }

        protected override void ConfigurarChavesEstrangeiras()
        {
            HasRequired(p => p.Declaracao).WithMany(p => p.DadosQuadro50).HasForeignKey(p => p.IdDeclaracao);
        }
    }
}