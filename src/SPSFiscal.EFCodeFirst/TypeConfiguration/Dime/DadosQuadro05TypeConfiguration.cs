﻿using System.ComponentModel.DataAnnotations.Schema;
using SPSFiscal.Common.Entity;
using SPSFiscal.Model;

namespace SPSFiscal.EFCodeFirst.TypeConfiguration.Dime
{
    public class DadosQuadro05TypeConfiguration : SPSFiscalEntityAbstractConfig<DimeDadosQuadro05Model>
    {
        protected override void ConfigurarNomeTabela()
        {
            ToTable("DadosQuadro05", "dime");
        }

        protected override void ConfigurarCamposTabela()
        {
            Property(p => p.IdDadosQuadro5).IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(p => p.NumeroItem).HasMaxLength(3).IsRequired();

            Property(p => p.IdDeclaracao).IsRequired();

            Property(p => p.Valor);
        }

        protected override void ConfigurarChavePrimaria()
        {
            HasKey(p => p.IdDadosQuadro5);
            //HasKey(p => p.IdDeclaracao);
        }

        protected override void ConfigurarChavesEstrangeiras()
        {
            //HasRequired(p => p.Declaracao)
            //    .WithMany(p => p.DadosQuadro05)
            //    .HasForeignKey(p => p.IdDeclaracao);

            //HasRequired(p => p.TabelaQuadro05)
            //    .WithMany(p => p.DadosQuadro05)
            //    .HasForeignKey(p => p.NumeroItem);
        }
    }
}