﻿using SPSFiscal.Common.Entity;
using SPSFiscal.Model;

namespace SPSFiscal.EFCodeFirst.TypeConfiguration.Dime
{
    public class TabelaQuadro11TypeConfiguration: SPSFiscalEntityAbstractConfig<DimeTabelaQuadro11Model>
    {
        protected override void ConfigurarNomeTabela()
        {
            ToTable("TabelaQuadro11", "dime");
        }

        protected override void ConfigurarCamposTabela()
        {
            Property(p => p.NumeroItem).IsRequired().HasMaxLength(3);

            Property(p => p.Descricao).HasMaxLength(200);

            Property(p => p.TipoItem).HasMaxLength(5);
        }

        protected override void ConfigurarChavePrimaria()
        {
            HasKey(p => p.NumeroItem);
        }

        protected override void ConfigurarChavesEstrangeiras()
        {
            HasOptional(p => p.DadosQuadro11).WithOptionalPrincipal();
        }
    }
}