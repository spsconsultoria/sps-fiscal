﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SPSFiscal.Common.Entity;
using SPSFiscal.Model;

namespace SPSFiscal.EFCodeFirst.TypeConfiguration.Dime
{
    class TabelaCodAtividadeTypeConfiguration : SPSFiscalEntityAbstractConfig<DimeTabelaCodAtividadeModel>
    {
        protected override void ConfigurarNomeTabela()
        {
            ToTable("TabelaCodAtividade", "dime");
        }

        protected override void ConfigurarCamposTabela()
        {
            Property(p => p.Codigo).HasMaxLength(3).IsRequired();

            Property(p => p.Descricao).HasMaxLength(300);
        }

        protected override void ConfigurarChavePrimaria()
        {
            HasKey(p => p.Codigo);
        }

        protected override void ConfigurarChavesEstrangeiras()
        {
            
        }
    }
}
