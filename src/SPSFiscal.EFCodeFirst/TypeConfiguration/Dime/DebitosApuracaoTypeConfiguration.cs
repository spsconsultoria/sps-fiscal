﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SPSFiscal.Common.Entity;
using SPSFiscal.Model;

namespace SPSFiscal.EFCodeFirst.TypeConfiguration.Dime
{
    class DebitosApuracaoTypeConfiguration : SPSFiscalEntityAbstractConfig<DimeDebitosApuracaoModel>
    {
        protected override void ConfigurarNomeTabela()
        {
            ToTable("DebitosApuracao", "dime");
        }

        protected override void ConfigurarCamposTabela()
        {
            Property(p => p.IdPeriodo).IsRequired();

            Property(p => p.NumeroItem).IsRequired().HasMaxLength(3);

            Property(p => p.Vl_Ajuste);
        }

        protected override void ConfigurarChavePrimaria()
        {
            HasKey(p => p.IdPeriodo);
            HasKey(p => p.NumeroItem);
        }

        protected override void ConfigurarChavesEstrangeiras()
        {
            //HasRequired(p => p.PeriodoApuracao).WithMany(p => p.DebitosApuracao).HasForeignKey(p => p.IdPeriodo);

            //HasRequired(p => p.TabelaDados04).WithMany(p => p.DebitosApuracao).HasForeignKey(p => p.NumeroItem);
        }
    }
}
