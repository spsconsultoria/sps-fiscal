﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SPSFiscal.Common.Entity;
using SPSFiscal.Model.Dime;

namespace SPSFiscal.EFCodeFirst.TypeConfiguration.Dime
{
    class PeriodoApuracaoTypeConfiguration : SPSFiscalEntityAbstractConfig<DimePeriodoApuracaoModel>
    {
        protected override void ConfigurarNomeTabela()
        {
            ToTable("PeriodoApuracao", "dime");
        }

        protected override void ConfigurarCamposTabela()
        {
            Property(p => p.IdPeriodo).IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(p => p.DB).IsRequired().HasMaxLength(50);

            Property(p => p.Filial).IsRequired().HasMaxLength(5);

            Property(p => p.TipoReg).IsRequired().HasMaxLength(20);

            Property(p => p.IdDeclaracao).IsRequired();

            Property(p => p.IdContribuinte).IsRequired();

            Property(p => p.Periodo).IsRequired().HasMaxLength(8);

            Property(p => p.Usuario).IsRequired().HasMaxLength(30);
        }

        protected override void ConfigurarChavePrimaria()
        {
            HasKey(p => p.IdPeriodo);
        }

        protected override void ConfigurarChavesEstrangeiras()
        {
            HasOptional(p => p.DebitosApuracao).WithOptionalPrincipal();
            HasOptional(p => p.EntradaP1).WithOptionalPrincipal();
            HasOptional(p => p.SaidasP2).WithOptionalPrincipal();

            HasOptional(p => p.Contribuinte).WithOptionalPrincipal();
        }
    }
}
