﻿using SPSFiscal.Common.Entity;
using SPSFiscal.Model;

namespace SPSFiscal.EFCodeFirst.TypeConfiguration.Dime
{
    public class TabelaMunicipiosTypeConfiguration : SPSFiscalEntityAbstractConfig<DimeTabelaMunicipiosModel>
    {
        protected override void ConfigurarNomeTabela()
        {
            ToTable("TabelaMunicipios", "dime");
        }

        protected override void ConfigurarCamposTabela()
        {
            Property(p => p.Codigo).HasMaxLength(5).IsRequired();

            Property(p => p.NomeMunicipio).HasMaxLength(200);
        }

        protected override void ConfigurarChavePrimaria()
        {
            HasKey(p => p.Codigo);
        }

        protected override void ConfigurarChavesEstrangeiras()
        {
            
        }
    }
}