﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SPSFiscal.EFCodeFirst.Migrations;
using System.Configuration;

namespace SPSFiscal.EFCodeFirst
{
    public static class Help
    {
        public static string InsertDataBaseName(string query)
        {
            string sql = string.Empty;

            string[] split = query.Split(' ');

            foreach (var itens in split)
            {
                sql += " ";
                if (itens.Contains("[dime]"))
                    sql += string.Concat("[SPS_Fiscal].", itens);
                else
                    sql += itens;
            }
            return sql;
        }

        public static void Migration()
        {
            string fileName = "SPSFiscal.Addon.exe.config";
            DataSet ds = new DataSet();
            ds.ReadXml(fileName);
            int table = Convert.ToInt32(ds.Tables.Count);
            string connectionString = ds.Tables[3].Rows[0][1].ToString();
            ds.Dispose();

            var configuration = new Migrations.Configuration();
            configuration.TargetDatabase = new DbConnectionInfo(
                connectionString, "System.Data.SqlClient");

            var migrator = new DbMigrator(configuration);
            migrator.Update();
        }
    }
}
