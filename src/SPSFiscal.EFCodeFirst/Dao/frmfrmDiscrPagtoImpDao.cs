﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SAPbouiCOM;
using SPSFiscal.Model;
using SPSFiscal = SPSFiscal.EFCodeFirst.Context.SPSFiscal;

namespace SPSFiscal.EFCodeFirst.Dao
{
    public static class frmfrmDiscrPagtoImpDao
    {
        public static void CarregaGrid(ref SAPbouiCOM.Form oForm, DimeDadosQuadro12Model model)
        {
            try
            {
                using (var db = new Context.SPSFiscal())
                {
                    var query = (from q in db.DadosQuadro12
                                 where q.IdDeclaracao == model.IdDeclaracao
                                 select new
                                 {
                                     q.IdLancamento,
                                     q.IdDeclaracao,
                                     q.Ind_Org_Recol,
                                     q.Cod_Receita,
                                     q.Cod_Class_Vcto,
                                     q.Dt_Vcto_Recol,
                                     q.Vl_Recol,
                                     q.Nro_Acordo
                                 }).ToList();

                    if (oForm.DataSources.DataTables.Item("grd").Columns.Count == 0)
                    {
                        oForm.DataSources.DataTables.Item("grd").Columns.Add("#", BoFieldsType.ft_AlphaNumeric, 200);
                        oForm.DataSources.DataTables.Item("grd").Columns.Add("IdDeclaracao", BoFieldsType.ft_AlphaNumeric, 200);
                        oForm.DataSources.DataTables.Item("grd").Columns.Add("Origem", BoFieldsType.ft_AlphaNumeric, 200);
                        oForm.DataSources.DataTables.Item("grd").Columns.Add("Código Receita", BoFieldsType.ft_AlphaNumeric, 200);
                        oForm.DataSources.DataTables.Item("grd").Columns.Add("Classe de Vencimento", BoFieldsType.ft_AlphaNumeric, 200);
                        oForm.DataSources.DataTables.Item("grd").Columns.Add("Data de Vencimento", BoFieldsType.ft_Date, 200);
                        oForm.DataSources.DataTables.Item("grd").Columns.Add("Valor", BoFieldsType.ft_Float, 200);
                        oForm.DataSources.DataTables.Item("grd").Columns.Add("Numero de Acordo", BoFieldsType.ft_AlphaNumeric, 200);
                    }

                    int linha = 0;
                    foreach (var itens in query)
                    {
                        oForm.DataSources.DataTables.Item("grd").Rows.Add();
                        oForm.DataSources.DataTables.Item("grd").SetValue("#", linha, itens.IdLancamento);
                        oForm.DataSources.DataTables.Item("grd").SetValue("IdDeclaracao", linha, itens.IdDeclaracao);
                        oForm.DataSources.DataTables.Item("grd").SetValue("Origem", linha, itens.Ind_Org_Recol);
                        oForm.DataSources.DataTables.Item("grd").SetValue("Código Receita", linha, itens.Cod_Receita);
                        oForm.DataSources.DataTables.Item("grd").SetValue("Classe de Vencimento", linha, itens.Cod_Class_Vcto);
                        oForm.DataSources.DataTables.Item("grd").SetValue("Data de Vencimento", linha, itens.Dt_Vcto_Recol);
                        oForm.DataSources.DataTables.Item("grd").SetValue("Valor", linha, Convert.ToDouble(itens.Vl_Recol.ToString(CultureInfo.InvariantCulture)));
                        oForm.DataSources.DataTables.Item("grd").SetValue("Numero de Acordo", linha, itens.Nro_Acordo);
                        linha++;
                    }
                    
                    ((Grid)oForm.Items.Item("grd").Specific).Columns.Item(1).Visible = false;
                    (((SAPbouiCOM.EditTextColumn)(((Grid)oForm.Items.Item("grd").Specific).Columns.Item("#")))).LinkedObjectType = "BOY_IPT";
                    ((Grid)oForm.Items.Item("grd").Specific).AutoResizeColumns();
                    ((Grid)oForm.Items.Item("grd").Specific).SelectionMode = BoMatrixSelect.ms_Auto;
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public static void Delete(int Id)
        {
            try
            {
                using (var db = new Context.SPSFiscal())
                {
                    var query = (from q in db.DadosQuadro12
                                 where q.IdLancamento == Id
                                 select q).Single();
                    db.DadosQuadro12.Remove(query);
                    db.SaveChanges();
                }
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }
        }
    }
}
