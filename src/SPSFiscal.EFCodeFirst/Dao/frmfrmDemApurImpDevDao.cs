﻿using SAPbouiCOM;
using SPSFiscal.Model;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPSFiscal.EFCodeFirst.Dao
{
    public static class frmfrmDemApurImpDevDao
    {
        public static void CarregaGrid(ref SAPbouiCOM.Form oForm, DimeDadosQuadro14Model model)
        {
            try
            {
                using (var db = new Context.SPSFiscal())
                {
                    var query = (from q in db.DadosQuadro14
                                 join x in db.TabelaQuadro14 on q.NumeroItem equals x.NumeroItem
                                 where q.IdDeclaracao == model.IdDeclaracao 
                                 orderby q.NumeroItem ascending
                                 select new
                                 {
                                     Item = x.TipoItem,
                                     Numero = q.NumeroItem,
                                     Descrição = x.Descricao,
                                     Valor = q.Valor
                                 }).ToList();

                    if (oForm.DataSources.DataTables.Item("grd").Columns.Count == 0)
                    {
                        oForm.DataSources.DataTables.Item("grd").Columns.Add("Item", BoFieldsType.ft_AlphaNumeric, 250);
                        oForm.DataSources.DataTables.Item("grd").Columns.Add("Numero", BoFieldsType.ft_AlphaNumeric, 200);
                        oForm.DataSources.DataTables.Item("grd").Columns.Add("Descrição", BoFieldsType.ft_AlphaNumeric, 250);
                        oForm.DataSources.DataTables.Item("grd").Columns.Add("Valor", BoFieldsType.ft_Float, 200);
                    }

                    int linha = 0;
                    string item = string.Empty;

                    query = query.OrderBy(p => p.Numero).ToList();

                    foreach (var itens in query)
                    {
                        switch (itens.Item)
                        {
                            case "1":
                                item = "1 - Apuração do Débito pela Apropriação do Crédito Presumido";
                                break;
                            case "2":
                                item = "2 - Pagamentos Antecipados";
                                break;
                            case "3":
                                item = "3 - Imposto a Recolher ou Saldo Credor para o Mês Seguinte";
                                break;
                            default:
                                break;
                        }
                        oForm.DataSources.DataTables.Item("grd").Rows.Add();
                        oForm.DataSources.DataTables.Item("grd").SetValue("Item", linha, item);
                        oForm.DataSources.DataTables.Item("grd").SetValue("Numero", linha, itens.Numero);
                        oForm.DataSources.DataTables.Item("grd").SetValue("Descrição", linha, itens.Descrição);
                        oForm.DataSources.DataTables.Item("grd").SetValue("Valor", linha, Convert.ToDouble(itens.Valor.ToString(CultureInfo.InvariantCulture)));
                        linha++;
                    }
                    //oForm.Items.Item("grd").Enabled = true;
                    ((Grid)oForm.Items.Item("grd").Specific).Columns.Item("Item").Editable = false;
                    ((Grid)oForm.Items.Item("grd").Specific).Columns.Item("Numero").Editable = false;
                    ((Grid)oForm.Items.Item("grd").Specific).Columns.Item("Descrição").Editable = false;
                    ((Grid)oForm.Items.Item("grd").Specific).Columns.Item("Valor").Editable = true;
                    ((Grid)oForm.Items.Item("grd").Specific).CollapseLevel = 1;
                    ((Grid)oForm.Items.Item("grd").Specific).AutoResizeColumns();
                    ((Grid)oForm.Items.Item("grd").Specific).SelectionMode = BoMatrixSelect.ms_Auto;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static void Update(decimal vl1, decimal vl2,int idDeclaracao)
        {
            try
            {
                using (var db = new Context.SPSFiscal())
                {
                    var query = (from q in db.DadosQuadro14
                                 where q.NumeroItem == "045" && q.IdDeclaracao == idDeclaracao
                                 select q).SingleOrDefault();
                    query.Valor = vl1 / 100;
                    db.SaveChanges();

                    query = (from q in db.DadosQuadro14
                             where q.NumeroItem == "050" && q.IdDeclaracao == idDeclaracao
                             select q).SingleOrDefault();
                    query.Valor = vl2 / 100;
                    db.SaveChanges();                    
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}
