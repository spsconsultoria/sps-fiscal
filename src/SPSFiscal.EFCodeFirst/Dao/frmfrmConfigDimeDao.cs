﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using SAPbouiCOM;
using SPSFiscal = SPSFiscal.EFCodeFirst.Context.SPSFiscal;

namespace SPSFiscal.EFCodeFirst.Dao
{
    public static class frmfrmConfigDimeDao
    {
        public static void CarregaGrid(ref Form oForm)
        {
            try
            {
                using (var db = new Context.SPSFiscal())
                {
                    var query = (from q in db.Contabilista.Where(p => p.Excluido == "N")
                                select new
                                {
                                    q.IdContabilista,
                                    q.CPF,
                                    q.NomeContabilista
                                }).ToString();

                    oForm.DataSources.DataTables.Item("grdContab").ExecuteQuery(Help.InsertDataBaseName(query
                        .Replace("AS [IdContabilista]","AS [#]")
                        .Replace("AS [NomeContabilista]", "AS [Nome Contabilista]")));
                    (((SAPbouiCOM.EditTextColumn)(((Grid)oForm.Items.Item("grdContab").Specific).Columns.Item("#")))).LinkedObjectType = "BOY_IPT";
                    ((Grid)oForm.Items.Item("grdContab").Specific).AutoResizeColumns();
                    ((Grid)oForm.Items.Item("grdContab").Specific).SelectionMode = BoMatrixSelect.ms_Auto;
                }
               
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static void CarregaGridContribuinte(ref Form oForm)
        {
            try
            {
                using (var db = new Context.SPSFiscal())
                {
                    var query = (from q in db.Contribuinte
                                where q.Excluido == "N"
                                select q).ToString();
                    oForm.DataSources.DataTables.Item("grdContri").ExecuteQuery(Help.InsertDataBaseName(query
                        .Replace("AS [IdContribuinte]", "AS [#]")
                        .Replace("AS [NomeRazaoSocial]", "AS [Nome ou Razão Social]")
                        .Replace("AS [RegimeApuracao]", "AS [Regime Apuração]")
                        .Replace("AS [SubstitutoTributario]", "AS [Substituto Tributário]")));
                    ((Grid)oForm.Items.Item("grdContri").Specific).Columns.Item("IdContabilista").Visible = false;
                    ((Grid)oForm.Items.Item("grdContri").Specific).Columns.Item("CNPJ").Visible = false;
                    ((Grid)oForm.Items.Item("grdContri").Specific).Columns.Item("NroInscricao").Visible = false;
                    ((Grid)oForm.Items.Item("grdContri").Specific).Columns.Item("TipoDeclaracao").Visible = false;
                    ((Grid)oForm.Items.Item("grdContri").Specific).Columns.Item("PorteEmpresa").Visible = false;
                    ((Grid)oForm.Items.Item("grdContri").Specific).Columns.Item("ApuracaoConsolidada").Visible = false;
                    ((Grid)oForm.Items.Item("grdContri").Specific).Columns.Item("ApuracaoCentralizada").Visible = false;
                    ((Grid)oForm.Items.Item("grdContri").Specific).Columns.Item("TemCreditosPresumido").Visible = false;
                    ((Grid)oForm.Items.Item("grdContri").Specific).Columns.Item("TemCredIncentFiscais").Visible = false;
                    ((Grid)oForm.Items.Item("grdContri").Specific).Columns.Item("TemEscritaContabil").Visible = false;
                    ((Grid)oForm.Items.Item("grdContri").Specific).Columns.Item("QtdeTrabAtividade").Visible = false;
                    ((Grid)oForm.Items.Item("grdContri").Specific).Columns.Item("ErpIdEmpresa").Visible = false;
                    ((Grid)oForm.Items.Item("grdContri").Specific).Columns.Item("ErpBaseDados").Visible = false;
                    ((Grid)oForm.Items.Item("grdContri").Specific).Columns.Item("Excluido").Visible = false;
                    ((Grid)oForm.Items.Item("grdContri").Specific).Columns.Item("DimePeriodoApuracaoModel_IdPeriodo").Visible = false;

                    (((SAPbouiCOM.EditTextColumn)(((Grid)oForm.Items.Item("grdContri").Specific).Columns.Item("#")))).LinkedObjectType = "BOY_IPT";
                    ((Grid)oForm.Items.Item("grdContri").Specific).AutoResizeColumns();
                    ((Grid)oForm.Items.Item("grdContri").Specific).SelectionMode = BoMatrixSelect.ms_Auto;
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

    }
}
