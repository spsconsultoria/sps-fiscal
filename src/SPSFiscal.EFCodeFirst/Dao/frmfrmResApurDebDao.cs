﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SAPbouiCOM;
using SPSFiscal.Model;

namespace SPSFiscal.EFCodeFirst.Dao
{
    public static class frmfrmResApurDebDao
    {
        public static void CarregaGrid(ref SAPbouiCOM.Form oForm, string IdDeclaracao)
        {
            try
            {
                string sql = string.Format(@"
SELECT 'Débitos Gerais' AS 'Item'
,T0.[NumeroItem]
,T0.[Descricao]
,T1.[Valor]
FROM [SPS_Fiscal].[dime].[TabelaQuadro04] T0
INNER JOIN [SPS_Fiscal].[dime].[DadosQuadro04] T1 ON T0.NumeroItem = T1.NumeroItem
WHERE T0.TipoItem = 1 AND T1.IdDeclaracao = {0}

UNION ALL

SELECT 'Estorno' AS 'Item'
,T0.[NumeroItem]
,T0.[Descricao]
,T1.[Valor]
FROM [SPS_Fiscal].[dime].[TabelaQuadro04] T0
INNER JOIN [SPS_Fiscal].[dime].[DadosQuadro04] T1 ON T0.NumeroItem = T1.NumeroItem
WHERE T0.TipoItem = 2 AND T1.IdDeclaracao = {0}

UNION ALL

SELECT 'Outros Débitos' AS 'Item'
,T0.[NumeroItem]
,T0.[Descricao]
,T1.[Valor]
FROM [SPS_Fiscal].[dime].[TabelaQuadro04] T0
INNER JOIN [SPS_Fiscal].[dime].[DadosQuadro04] T1 ON T0.NumeroItem = T1.NumeroItem
WHERE T0.TipoItem = 3 AND T1.IdDeclaracao = {0}", IdDeclaracao);
                oForm.DataSources.DataTables.Item("grd").ExecuteQuery(sql);
                ((Grid)oForm.Items.Item("grd").Specific).CollapseLevel = 1;
                ((Grid)oForm.Items.Item("grd").Specific).AutoResizeColumns();
                ((Grid)oForm.Items.Item("grd").Specific).SelectionMode = BoMatrixSelect.ms_Auto;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
