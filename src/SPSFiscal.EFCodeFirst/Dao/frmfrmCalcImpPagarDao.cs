﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SAPbouiCOM;
using SPSFiscal.Model;
using SPSFiscal = SPSFiscal.EFCodeFirst.Context.SPSFiscal;

namespace SPSFiscal.EFCodeFirst.Dao
{
    public static class frmfrmCalcImpPagarDao
    {
        public static void CarregaDados(ref Form oForm, DimeDadosQuadro09Model model)
        {
            try
            {
                #region SQL QUERY
                string sql = string.Format(@"SELECT '1 - Totalização de Débitos' as 'Item'
	  ,T0.[NumeroItem] as 'Número'
      ,T0.[Descricao]
	  ,T1.[Valor]
INTO #TEMP
  FROM [SPS_Fiscal].[dime].[TabelaQuadro09] T0
  INNER JOIN [SPS_Fiscal].[dime].[DadosQuadro09] T1 ON T0.NumeroItem = T1.NumeroItem
  WHERE T0.TipoItem = '1' AND T1.IdDeclaracao = {0}

  UNION ALL

SELECT '2 - Totalização de Créditos' as 'Item'
	  ,T0.[NumeroItem] as 'Número'
      ,T0.[Descricao]
	  ,T1.[Valor]
  FROM [SPS_Fiscal].[dime].[TabelaQuadro09] T0
  INNER JOIN [SPS_Fiscal].[dime].[DadosQuadro09] T1 ON T0.NumeroItem = T1.NumeroItem
  WHERE T0.TipoItem = '2' AND T1.IdDeclaracao = {0}

  UNION ALL

SELECT '3 - Ajustes da apuração decendial e Antecipações' as 'Item'
	  ,T0.[NumeroItem] as 'Número'
      ,T0.[Descricao]
	  ,T1.[Valor]
  FROM [SPS_Fiscal].[dime].[TabelaQuadro09] T0
  INNER JOIN [SPS_Fiscal].[dime].[DadosQuadro09] T1 ON T0.NumeroItem = T1.NumeroItem
  WHERE T0.TipoItem = '3' AND T1.IdDeclaracao = {0}

  UNION ALL

SELECT '4 - Apuração do Saldo Devedor' as 'Item'
	  ,T0.[NumeroItem] as 'Número'
      ,T0.[Descricao]
	  ,T1.[Valor]
  FROM [SPS_Fiscal].[dime].[TabelaQuadro09] T0
  INNER JOIN [SPS_Fiscal].[dime].[DadosQuadro09] T1 ON T0.NumeroItem = T1.NumeroItem
  WHERE T0.TipoItem = '4' AND T1.IdDeclaracao = {0}

  UNION ALL

SELECT '5 - Apuração do Saldo Credor' as 'Item' 
	  ,T0.[NumeroItem] as 'Número'
      ,T0.[Descricao]
	  ,T1.[Valor]
  FROM [SPS_Fiscal].[dime].[TabelaQuadro09] T0
  INNER JOIN [SPS_Fiscal].[dime].[DadosQuadro09] T1 ON T0.NumeroItem = T1.NumeroItem
  WHERE T0.TipoItem = '5' AND T1.IdDeclaracao = {0}

  UNION ALL

SELECT '6 - Discriminação do saldo credor para o mês seguiinte' as 'Item' 
	  ,T0.[NumeroItem] as 'Número'
      ,T0.[Descricao]
	  ,T1.[Valor]
  FROM [SPS_Fiscal].[dime].[TabelaQuadro09] T0
  INNER JOIN [SPS_Fiscal].[dime].[DadosQuadro09] T1 ON T0.NumeroItem = T1.NumeroItem
  WHERE T0.TipoItem = '6' AND T1.IdDeclaracao = {0}
ORDER BY Número ASC

declare @TotalDeb as decimal(18,2) = (select SUM(Valor) FROM #TEMP  WHERE Número IN (010,011,020,030) group by Item)
declare @TotalCred as decimal(18,2) = (select SUM(Valor) FROM #TEMP  WHERE Número IN (050,060,070,075) group by Item)
declare @AjusteApur as decimal(18,2) = (select SUM(Valor) FROM #TEMP  WHERE Número IN (090,100,105,110) group by Item)
declare @ApurDev as decimal(18,2) = (select SUM(Valor) FROM #TEMP  WHERE Número IN (120,130) group by Item)
declare @Discriminacao as decimal(18,2) = (select SUM(Valor) FROM #TEMP  WHERE Número IN (160,170,180) group by Item)

select item
,Número
,Descricao
,CASE Número 
WHEN '040' THEN @TotalDeb
WHEN '080' THEN @TotalCred
WHEN '110' THEN @AjusteApur
WHEN '999' THEN @ApurDev
WHEN '190' THEN @Discriminacao
ELSE Valor 
END AS 'Valor'
from #TEMP
drop table #TEMP", model.IdDeclaracao);
                #endregion

                oForm.DataSources.DataTables.Item("grd").ExecuteQuery(sql);
                ((Grid)oForm.Items.Item("grd").Specific).CollapseLevel = 1;
                ((Grid)oForm.Items.Item("grd").Specific).AutoResizeColumns();
                ((Grid)oForm.Items.Item("grd").Specific).SelectionMode = BoMatrixSelect.ms_Auto;

                sql = string.Format(@"SELECT T0.[NumeroItem]
                                                ,T0.[Descricao]
	                                            ,T1.[Valor]
                                            FROM [SPS_Fiscal].[dime].[TabelaQuadro09] T0
                                            INNER JOIN [SPS_Fiscal].[dime].[DadosQuadro09] T1 ON T0.NumeroItem = T1.NumeroItem
                                            WHERE T0.NumeroItem in (010,011,020,030,050,060,090,100,105,130,150,160,170,180,190) AND T1.IdDeclaracao = {0}", model.IdDeclaracao);
                oForm.DataSources.DataTables.Item("grd2").ExecuteQuery(sql);
                ((Grid)oForm.Items.Item("grd2").Specific).Columns.Item("NumeroItem").Editable = false;
                ((Grid)oForm.Items.Item("grd2").Specific).Columns.Item("Descricao").Editable = false;
                ((Grid)oForm.Items.Item("grd2").Specific).Columns.Item("Valor").Editable = true;
                ((Grid)oForm.Items.Item("grd2").Specific).AutoResizeColumns();
                ((Grid)oForm.Items.Item("grd2").Specific).SelectionMode = BoMatrixSelect.ms_Auto;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public static void Update(DimeDadosQuadro09Model model)
        {
            try
            {
                using (var db =new Context.SPSFiscal())
                {
                    var query = (from q in db.DadosQuadro09
                                 where q.NumeroItem == model.NumeroItem
                                 select q).Single();
                    query.Valor = model.Valor;
                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}
