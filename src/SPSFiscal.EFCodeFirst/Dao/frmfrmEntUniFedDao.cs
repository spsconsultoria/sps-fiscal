﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SAPbouiCOM;
using SPSFiscal.Model;
using SPSFiscal = SPSFiscal.EFCodeFirst.Context.SPSFiscal;

namespace SPSFiscal.EFCodeFirst.Dao
{
    public static class frmfrmEntUniFedDao
    {
        public static void CarregaGrid(ref Form oForm, DimeDadosQuadro49Model model)
        {
            try
            {
                using (var db = new Context.SPSFiscal())
                {
                    var query = (from q in db.DadosQuadro49
                                 where q.IdDeclaracao == model.IdDeclaracao
                                 select new
                                 {
                                     q.IdLancamento,
                                     q.IdDeclaracao,
                                     q.Sigla_Estado,
                                     q.Vl_Contabil,
                                     q.Vl_Base_Calc,
                                     q.Vl_Outras,
                                     q.Vl_ST,
                                     q.Vl_ST_Outros
                                 }).ToList();

                    if (oForm.DataSources.DataTables.Item("grd").Columns.Count == 0)
                    {
                        oForm.DataSources.DataTables.Item("grd").Columns.Add("#", BoFieldsType.ft_AlphaNumeric, 200);
                        oForm.DataSources.DataTables.Item("grd").Columns.Add("IdDeclaracao", BoFieldsType.ft_AlphaNumeric, 200);
                        oForm.DataSources.DataTables.Item("grd").Columns.Add("UF", BoFieldsType.ft_AlphaNumeric, 200);
                        oForm.DataSources.DataTables.Item("grd").Columns.Add("Valor Contábil", BoFieldsType.ft_Price, 200);
                        oForm.DataSources.DataTables.Item("grd").Columns.Add("Base Cálculo", BoFieldsType.ft_Price, 200);
                        oForm.DataSources.DataTables.Item("grd").Columns.Add("Outras", BoFieldsType.ft_Price, 200);
                        oForm.DataSources.DataTables.Item("grd").Columns.Add("Petróleo/Energia Elétrica", BoFieldsType.ft_Price, 200);
                        oForm.DataSources.DataTables.Item("grd").Columns.Add("Outros Produtos", BoFieldsType.ft_Price, 200);
                    }

                    int linha = 0;
                    foreach (var itens in query)
                    {
                        oForm.DataSources.DataTables.Item("grd").Rows.Add();
                        oForm.DataSources.DataTables.Item("grd").SetValue("#", linha, itens.IdLancamento);
                        oForm.DataSources.DataTables.Item("grd").SetValue("IdDeclaracao", linha, itens.IdDeclaracao);
                        oForm.DataSources.DataTables.Item("grd").SetValue("UF", linha, itens.Sigla_Estado);
                        oForm.DataSources.DataTables.Item("grd").SetValue("Valor Contábil", linha, Convert.ToDouble(itens.Vl_Contabil));
                        oForm.DataSources.DataTables.Item("grd").SetValue("Base Cálculo", linha, Convert.ToDouble(itens.Vl_Outras));
                        oForm.DataSources.DataTables.Item("grd").SetValue("Outras", linha, Convert.ToDouble(itens.Vl_Outras));
                        oForm.DataSources.DataTables.Item("grd").SetValue("Petróleo/Energia Elétrica", linha, Convert.ToDouble(itens.Vl_ST));
                        oForm.DataSources.DataTables.Item("grd").SetValue("Outros Produtos", linha, Convert.ToDouble(itens.Vl_ST_Outros));
                        linha++;
                    }

                    ((Grid)oForm.Items.Item("grd").Specific).Columns.Item("IdDeclaracao").Visible = false;
                    (((SAPbouiCOM.EditTextColumn)(((Grid)oForm.Items.Item("grd").Specific).Columns.Item("#")))).LinkedObjectType = "BOY_IPT";
                    ((Grid)oForm.Items.Item("grd").Specific).AutoResizeColumns();
                    ((Grid)oForm.Items.Item("grd").Specific).SelectionMode = BoMatrixSelect.ms_Auto;
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public static void Delete(int ID)
        {
            try
            {
                using (var db = new Context.SPSFiscal())
                {
                    var query = (from q in db.DadosQuadro49
                                 where q.IdLancamento == ID
                                 select q).Single();
                    db.DadosQuadro49.Remove(query);
                    db.SaveChanges();
                    
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}
