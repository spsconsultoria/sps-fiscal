﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SPSFiscal.Model;
using SPSFiscal = SPSFiscal.EFCodeFirst.Context.SPSFiscal;

namespace SPSFiscal.EFCodeFirst.Dao
{
    public static class frmfrmDimeValorDao
    {
        public static void Update(DimeDadosQuadro04Model model)
        {
            try
            {
                using (var db = new Context.SPSFiscal())
                {
                    var query = (from q in db.DadosQuadro04
                                 where q.NumeroItem == model.NumeroItem
                                 select q).Single();
                    query.Valor = model.Valor;
                    db.SaveChanges();

                    decimal valor = (from p in db.DadosQuadro04.Where(g => g.NumeroItem != "990")
                                 select p).Sum(p => p.Valor);

                    query = (from q in db.DadosQuadro04
                             where q.NumeroItem == "990"
                             select q).Single();

                    query.Valor = valor;
                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}
