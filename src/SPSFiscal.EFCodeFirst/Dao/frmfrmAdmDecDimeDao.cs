﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using SAPbouiCOM;
using SPSFiscal.EFCodeFirst.Context;
using SPSFiscal.Model;
using SPSFiscal = SPSFiscal.EFCodeFirst.Context.SPSFiscal;

namespace SPSFiscal.EFCodeFirst.Dao
{
    public static class frmfrmAdmDecDimeDao
    {
        public static void CarregaDados(DimeDeclaracaoModel model, ref Form oForm, string erpBaseDados)
        {
            try
            {
                using (var db = new Context.SPSFiscal())
                {
                    string contribuinte = ((ComboBox)oForm.Items.Item("contri").Specific).Selected.Description;
                    DateTime dtInicio = Convert.ToDateTime(oForm.DataSources.UserDataSources.Item("dtDe").Value);
                    DateTime dtFim = Convert.ToDateTime(oForm.DataSources.UserDataSources.Item("dtAte").Value);
                    Grid oGrid = (Grid)oForm.Items.Item("grd").Specific;

                    var query = (from q in db.Declaracao
                                 where q.NomeContribuinte == contribuinte && q.ErpBaseDados == erpBaseDados
                                 && q.Excluido == "N"
                                 && q.DtInicio >= dtInicio
                                 && q.DtTermino <= dtFim
                                 select q).ToList();

                    if (oForm.DataSources.DataTables.Item("grd").Columns.Count == 0)
                    {
                        oForm.DataSources.DataTables.Item("grd").Columns.Add("#", BoFieldsType.ft_Text, 255);
                        oForm.DataSources.DataTables.Item("grd").Columns.Add("DtInicio", BoFieldsType.ft_Date, 255);
                        oForm.DataSources.DataTables.Item("grd").Columns.Add("DtTermino", BoFieldsType.ft_Date, 255);
                        oForm.DataSources.DataTables.Item("grd").Columns.Add("NomeContabilista", BoFieldsType.ft_Text, 255);
                        oForm.DataSources.DataTables.Item("grd").Columns.Add("ContabilistaCPF", BoFieldsType.ft_Text, 255);
                        oForm.DataSources.DataTables.Item("grd").Columns.Add("Nome do Contribuinte", BoFieldsType.ft_Text, 255);
                        oForm.DataSources.DataTables.Item("grd").Columns.Add("NroInscricao", BoFieldsType.ft_Text, 255);
                        oForm.DataSources.DataTables.Item("grd").Columns.Add("CNPJ do Contribuinte", BoFieldsType.ft_Text, 255);
                        oForm.DataSources.DataTables.Item("grd").Columns.Add("Periodo da Declaração", BoFieldsType.ft_Text, 255);
                        oForm.DataSources.DataTables.Item("grd").Columns.Add("Tipo da Declaração", BoFieldsType.ft_Text, 255);
                        oForm.DataSources.DataTables.Item("grd").Columns.Add("RegimeApuracao", BoFieldsType.ft_Text, 255);
                        oForm.DataSources.DataTables.Item("grd").Columns.Add("PorteEmpresa", BoFieldsType.ft_Text, 255);
                        oForm.DataSources.DataTables.Item("grd").Columns.Add("ApuracaoConsolidada", BoFieldsType.ft_Text, 255);
                        oForm.DataSources.DataTables.Item("grd").Columns.Add("ApuracaoCentralizada", BoFieldsType.ft_Text, 255);
                        oForm.DataSources.DataTables.Item("grd").Columns.Add("TransCredPeriodo", BoFieldsType.ft_Text, 255);
                        oForm.DataSources.DataTables.Item("grd").Columns.Add("TemCreditosPresumido", BoFieldsType.ft_Text, 255);
                        oForm.DataSources.DataTables.Item("grd").Columns.Add("TemCredIncentFiscais", BoFieldsType.ft_Text, 255);
                        oForm.DataSources.DataTables.Item("grd").Columns.Add("Movimento", BoFieldsType.ft_Text, 255);
                        oForm.DataSources.DataTables.Item("grd").Columns.Add("SubstitutoTributario", BoFieldsType.ft_Text, 255);
                        oForm.DataSources.DataTables.Item("grd").Columns.Add("TemEscritaContabil", BoFieldsType.ft_Text, 255);
                        oForm.DataSources.DataTables.Item("grd").Columns.Add("QtdeTrabAtividade", BoFieldsType.ft_Text, 255);
                        oForm.DataSources.DataTables.Item("grd").Columns.Add("Excluido", BoFieldsType.ft_Text, 255);
                        oForm.DataSources.DataTables.Item("grd").Columns.Add("tpMov", BoFieldsType.ft_Text, 255);
                    }
                    int linhas = 0;
                    foreach (var itens in query)
                    {
                        oForm.DataSources.DataTables.Item("grd").Rows.Add(1);
                        oForm.DataSources.DataTables.Item("grd").SetValue("#", linhas, itens.IdDeclaracao);
                        oForm.DataSources.DataTables.Item("grd").SetValue("DtInicio", linhas, itens.DtInicio);
                        oForm.DataSources.DataTables.Item("grd").SetValue("DtTermino", linhas, itens.DtTermino);
                        oForm.DataSources.DataTables.Item("grd").SetValue("NomeContabilista", linhas, itens.NomeContabilista);
                        oForm.DataSources.DataTables.Item("grd").SetValue("ContabilistaCPF", linhas, itens.ContabilistaCPF);
                        oForm.DataSources.DataTables.Item("grd").SetValue("Nome do Contribuinte", linhas, itens.NomeContribuinte);
                        oForm.DataSources.DataTables.Item("grd").SetValue("NroInscricao", linhas, itens.NroInscricao);
                        oForm.DataSources.DataTables.Item("grd").SetValue("CNPJ do Contribuinte", linhas, itens.ContribuinteCNPJ);
                        oForm.DataSources.DataTables.Item("grd").SetValue("Periodo da Declaração", linhas, itens.PeriodoReferencia);
                        oForm.DataSources.DataTables.Item("grd").SetValue("Tipo da Declaração", linhas, itens.TipoDeclaracao);
                        oForm.DataSources.DataTables.Item("grd").SetValue("RegimeApuracao", linhas, itens.RegimeApuracao);
                        oForm.DataSources.DataTables.Item("grd").SetValue("PorteEmpresa", linhas, itens.PorteEmpresa);
                        oForm.DataSources.DataTables.Item("grd").SetValue("ApuracaoConsolidada", linhas, itens.ApuracaoConsolidada);
                        oForm.DataSources.DataTables.Item("grd").SetValue("ApuracaoCentralizada", linhas, itens.ApuracaoCentralizada);
                        oForm.DataSources.DataTables.Item("grd").SetValue("TransCredPeriodo", linhas, itens.TransCredPeriodo);
                        oForm.DataSources.DataTables.Item("grd").SetValue("TemCreditosPresumido", linhas, itens.TemCreditosPresumido);
                        oForm.DataSources.DataTables.Item("grd").SetValue("TemCredIncentFiscais", linhas, itens.TemCredIncentFiscais);
                        oForm.DataSources.DataTables.Item("grd").SetValue("Movimento", linhas, string.IsNullOrEmpty(itens.Movimento) ? string.Empty : itens.Movimento);
                        oForm.DataSources.DataTables.Item("grd").SetValue("SubstitutoTributario", linhas, itens.SubstitutoTributario);
                        oForm.DataSources.DataTables.Item("grd").SetValue("TemEscritaContabil", linhas, itens.TemEscritaContabil);
                        oForm.DataSources.DataTables.Item("grd").SetValue("QtdeTrabAtividade", linhas, itens.QtdeTrabAtividade);
                        oForm.DataSources.DataTables.Item("grd").SetValue("Excluido", linhas, itens.Excluido);
                        oForm.DataSources.DataTables.Item("grd").SetValue("tpMov", linhas, string.IsNullOrEmpty(itens.Movimento) ? string.Empty : itens.Movimento);
                        linhas++;
                    }
                    
                    oGrid.Columns.Item("DtInicio").Visible = false;
                    oGrid.Columns.Item("DtTermino").Visible = false;
                    oGrid.Columns.Item("NomeContabilista").Visible = false;
                    oGrid.Columns.Item("ContabilistaCPF").Visible = false;
                    oGrid.Columns.Item("NroInscricao").Visible = false;
                    oGrid.Columns.Item("RegimeApuracao").Visible = false;
                    oGrid.Columns.Item("PorteEmpresa").Visible = false;
                    oGrid.Columns.Item("ApuracaoConsolidada").Visible = false;
                    oGrid.Columns.Item("ApuracaoCentralizada").Visible = false;
                    oGrid.Columns.Item("TransCredPeriodo").Visible = false;
                    oGrid.Columns.Item("TemCreditosPresumido").Visible = false;
                    oGrid.Columns.Item("TemCredIncentFiscais").Visible = false;
                    oGrid.Columns.Item("Movimento").Visible = false;
                    oGrid.Columns.Item("SubstitutoTributario").Visible = false;
                    oGrid.Columns.Item("TemEscritaContabil").Visible = false;
                    oGrid.Columns.Item("QtdeTrabAtividade").Visible = false;
                    oGrid.Columns.Item("Excluido").Visible = false;
                    oGrid.Columns.Item("tpMov").Visible = false;
                    (((SAPbouiCOM.EditTextColumn)(((Grid)oForm.Items.Item("grd").Specific).Columns.Item("#")))).LinkedObjectType = "BOY_IPT";
                    ((Grid)oForm.Items.Item("grd").Specific).AutoResizeColumns();
                    ((Grid)oForm.Items.Item("grd").Specific).SelectionMode = BoMatrixSelect.ms_Auto;
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        public static void Delete(DimeDeclaracaoModel model)
        {
            try
            {
                using (var db = new Context.SPSFiscal())
                {
                    var query = (from q in db.Declaracao where q.IdDeclaracao == model.IdDeclaracao select q).SingleOrDefault();
                    db.Declaracao.Remove(query);
                    //db.Entry(model).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}
