﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SAPbouiCOM;
using SPSFiscal.EFCodeFirst.TypeConfiguration.Dime;
using SPSFiscal.Model;
using SPSFiscal.Model.Dime;
using SPSFiscal = SPSFiscal.EFCodeFirst.Context.SPSFiscal;

namespace SPSFiscal.EFCodeFirst.Dao
{
    public static class frmfrmCadDeclarDimeDao
    {
        public static void SaveDeclaracao(DimeDeclaracaoModel model)
        {
            try
            {
                using (var db = new Context.SPSFiscal())
                {
                    db.Declaracao.Add(model);
                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        public static void UpdateDeclaracao(DimeDeclaracaoModel model)
        {
            try
            {
                using (var db = new Context.SPSFiscal())
                {
                    var query = (from q in db.Declaracao
                                 where q.IdDeclaracao == model.IdDeclaracao
                                 select q).Single();
                    query.TipoDeclaracao = model.TipoDeclaracao;
                    query.RegimeApuracao = model.RegimeApuracao;
                    query.SubstitutoTributario = model.SubstitutoTributario;
                    query.TemEscritaContabil = model.TemEscritaContabil;
                    query.QtdeTrabAtividade = model.QtdeTrabAtividade;
                    query.PorteEmpresa = model.PorteEmpresa;
                    query.ApuracaoCentralizada = model.ApuracaoCentralizada;
                    query.TransCredPeriodo = model.TransCredPeriodo;
                    query.TemCreditosPresumido = model.TemCreditosPresumido;
                    query.TemCredIncentFiscais = model.TemCredIncentFiscais;
                    query.NomeContabilista = model.NomeContabilista;
                    query.ContabilistaCPF = model.ContabilistaCPF;
                    query.NroInscricao = model.NroInscricao;
                    query.Movimento = model.Movimento;
                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        public static int IdDeclaracao()
        {

            try
            {
                using (var db = new Context.SPSFiscal())
                {
                    var query = (from q in db.Declaracao.OrderByDescending(x => x.IdDeclaracao) select q).FirstOrDefault();
                    //var query = (from q in db.Declaracao.Max(p => p.IdDeclaracao.ToString()) select q).SingleOrDefault();
                    //int cod = 0;
                    //query = (from q in query orderby q ascending select q).ToList();
                    //foreach (var item in query)
                    //    cod = item;

                    return Convert.ToInt32(query.IdDeclaracao.ToString());
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        public static int IdContribuinte(string contri)
        {

            try
            {
                using (var db = new Context.SPSFiscal())
                {
                    int Id = Convert.ToInt32(contri);
                    var query = (from q in db.Contribuinte
                                 where q.NomeRazaoSocial == contri || q.IdContribuinte == Id
                                 select q.IdContribuinte).Single();

                    return Convert.ToInt32(query.ToString());
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        public static void SavePeriodoApuracao(DimePeriodoApuracaoModel model)
        {
            try
            {
                using (var db = new Context.SPSFiscal())
                {
                    db.DimePeriodoApuracao.Add(model);
                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        public static void SaveRegistroDeclaracao(DimeRegistroDeclaracaoModel model)
        {
            try
            {
                using (var db = new Context.SPSFiscal())
                {
                    db.RegistroDeclaracao.Add(model);
                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        public static List<DimeTipoRegistroModel> TipoRegistroList()
        {
            var lista = new List<DimeTipoRegistroModel>();

            try
            {
                using (var db = new Context.SPSFiscal())
                {
                    var query = (from q in db.TipoRegistro select q).ToArray();

                    foreach (var itens in query)
                    {
                        lista.Add(new DimeTipoRegistroModel()
                        {
                            TipoRegistro = itens.TipoRegistro,
                            Descricao = itens.Descricao

                        });
                    }
                }
                return lista;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        public static void SaveContribuinteDeclaracao(DimeContribuinteDeclaracaoModel model)
        {
            try
            {
                using (var db = new Context.SPSFiscal())
                {
                    db.ContribuinteDeclaracao.Add(model);
                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        public static void CarregaGridQuadros(ref SAPbouiCOM.Form oForm, DimeRegistroDeclaracaoModel model)
        {
            try
            {
                using (var db = new Context.SPSFiscal())
                {
                    var tpReg = new string[] { };
                    var query = (from q in db.RegistroDeclaracao
                                 join t in db.TipoRegistro on q.TipoRegistro equals t.TipoRegistro
                                 where q.IdDeclaracao == model.IdDeclaracao
                                 && t.TipoRegistro <= 12
                                 orderby q.TipoRegistro
                                 select new
                                 {
                                     q.IdDeclaracao,
                                     t.Quadro,
                                     t.TipoRegistro,
                                     t.Descricao
                                 }).ToList();
                    if (oForm.DataSources.DataTables.Item("grdQ").Columns.Count == 0)
                    {
                        oForm.DataSources.DataTables.Item("grdQ").Columns.Add("IdDeclaracao", BoFieldsType.ft_Text, 254);
                        oForm.DataSources.DataTables.Item("grdQ").Columns.Add("Quadro", BoFieldsType.ft_Text, 254);
                        oForm.DataSources.DataTables.Item("grdQ").Columns.Add("Tipo Registro", BoFieldsType.ft_Text, 254);
                        oForm.DataSources.DataTables.Item("grdQ").Columns.Add("Descrição", BoFieldsType.ft_Text, 254);
                    }

                    int linha = 0;
                    foreach (var itens in query)
                    {
                        oForm.DataSources.DataTables.Item("grdQ").Rows.Add();
                        oForm.DataSources.DataTables.Item("grdQ").SetValue("IdDeclaracao", linha, itens.IdDeclaracao);
                        oForm.DataSources.DataTables.Item("grdQ").SetValue("Quadro", linha, itens.Quadro);
                        oForm.DataSources.DataTables.Item("grdQ").SetValue("Tipo Registro", linha, itens.TipoRegistro);
                        oForm.DataSources.DataTables.Item("grdQ").SetValue("Descrição", linha, itens.Descricao);
                        linha++;
                    }

                    (((SAPbouiCOM.EditTextColumn)(((Grid)oForm.Items.Item("grdQ").Specific).Columns.Item("Quadro")))).LinkedObjectType = "BOY_IPT";
                    ((Grid)oForm.Items.Item("grdQ").Specific).Columns.Item("IdDeclaracao").Visible = false;
                    ((Grid)oForm.Items.Item("grdQ").Specific).AutoResizeColumns();
                    ((Grid)oForm.Items.Item("grdQ").Specific).SelectionMode = BoMatrixSelect.ms_Auto;
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        public static void CarregaGridResumoAnual(ref SAPbouiCOM.Form oForm, DimeRegistroDeclaracaoModel model)
        {
            try
            {
                using (var db = new Context.SPSFiscal())
                {
                    var query = (from q in db.RegistroDeclaracao
                                 join t in db.TipoRegistro on q.TipoRegistro equals t.TipoRegistro
                                 where q.IdDeclaracao == model.IdDeclaracao
                                       && t.TipoRegistro > 12
                                 orderby q.TipoRegistro
                                 select new
                                 {
                                     q.IdDeclaracao,
                                     t.Quadro,
                                     q.TipoRegistro,
                                     t.Descricao
                                 }).ToList();

                    if (oForm.DataSources.DataTables.Item("GrdRA").Columns.Count == 0)
                    {
                        oForm.DataSources.DataTables.Item("GrdRA").Columns.Add("IdDeclaracao", BoFieldsType.ft_Text, 254);
                        oForm.DataSources.DataTables.Item("GrdRA").Columns.Add("Quadro", BoFieldsType.ft_Text, 254);
                        oForm.DataSources.DataTables.Item("GrdRA").Columns.Add("Tipo Registro", BoFieldsType.ft_Text, 254);
                        oForm.DataSources.DataTables.Item("GrdRA").Columns.Add("Descrição", BoFieldsType.ft_Text, 254);
                    }

                    int linha = 0;
                    foreach (var itens in query)
                    {
                        oForm.DataSources.DataTables.Item("GrdRA").Rows.Add();
                        oForm.DataSources.DataTables.Item("GrdRA").SetValue("IdDeclaracao", linha, itens.IdDeclaracao);
                        oForm.DataSources.DataTables.Item("GrdRA").SetValue("Quadro", linha, itens.Quadro);
                        oForm.DataSources.DataTables.Item("GrdRA").SetValue("Tipo Registro", linha, itens.TipoRegistro);
                        oForm.DataSources.DataTables.Item("GrdRA").SetValue("Descrição", linha, itens.Descricao);
                        linha++;
                    }
                    (((SAPbouiCOM.EditTextColumn)(((Grid)oForm.Items.Item("GrdRA").Specific).Columns.Item("Quadro")))).LinkedObjectType = "BOY_IPT";
                    ((Grid)oForm.Items.Item("GrdRA").Specific).Columns.Item("IdDeclaracao").Visible = false;
                    ((Grid)oForm.Items.Item("GrdRA").Specific).AutoResizeColumns();
                    ((Grid)oForm.Items.Item("GrdRA").Specific).SelectionMode = BoMatrixSelect.ms_Auto;
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        public static bool ValidaPeriodo(string filial, string periodo)
        {
            bool retorno = false;

            using (var db = new Context.SPSFiscal())
            {
                var query = (from q in db.Declaracao where q.PeriodoReferencia == periodo && q.NomeContribuinte == filial select q).ToList();

                if (query.Count > 0)
                    retorno = true;
            }

            return retorno;
        }
    }
}
