﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SAPbouiCOM;
using SPSFiscal.Model;
using SPSFiscal = SPSFiscal.EFCodeFirst.Context.SPSFiscal;

namespace SPSFiscal.EFCodeFirst.Dao
{
    public static class frmfrmCadContribDao
    {
        public static void Save(DimeContribuinteModel model)
        {
            try
            {
                using (var db = new Context.SPSFiscal())
                {
                    db.Contribuinte.Add(model);
                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        public static void Update(DimeContribuinteModel model)
        {
            try
            {
                using (var db = new Context.SPSFiscal())
                {
                    db.Entry(model).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        public static void Delete(DimeContribuinteModel model)
        {
            try
            {
                using (var db = new Context.SPSFiscal())
                {
                    db.Entry(model).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        public static string GetFilial(string empresa, int idContabilista)
        {
            try
            {
                using (var db = new Context.SPSFiscal())
                {
                    string retorno = string.Empty;

                    var query = (from q in db.Contribuinte
                                 where q.ErpIdEmpresa == empresa
                                 && q.IdContabilista == idContabilista
                                 && q.Excluido == "N"
                                 select new { q.ErpIdEmpresa }).ToList();
                    if (query.Count > 0)
                    {
                        foreach (var itens in query)
                            retorno = itens.ErpIdEmpresa;
                    }

                    return retorno;
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        public static List<DimeContribuinteModel> CarregaUmContribuinte(int idContribuinte)
        {
            using (var db = new Context.SPSFiscal())
            {
                var query = from q in db.Contribuinte
                             where q.IdContribuinte == idContribuinte
                             join t in db.Contabilista on q.IdContabilista equals t.IdContabilista
                             select new
                             {
                                 q.IdContribuinte,
                                 q.IdContabilista,
                                 t.NomeContabilista,
                                 t.CPF,
                                 q.NomeRazaoSocial,
                                 q.CNPJ,
                                 q.NroInscricao,
                                 q.TipoDeclaracao,
                                 q.RegimeApuracao,
                                 q.PorteEmpresa,
                                 q.ApuracaoConsolidada,
                                 q.ApuracaoCentralizada,
                                 q.TemCreditosPresumido,
                                 q.TemCredIncentFiscais,
                                 q.SubstitutoTributario,
                                 q.TemEscritaContabil,
                                 q.QtdeTrabAtividade,
                                 q.ErpIdEmpresa,
                                 q.ErpBaseDados,
                                 q.Excluido
                             };

                List<DimeContribuinteModel> lista = new List<DimeContribuinteModel>();

                foreach (var itens in query.ToList())
                {
                    lista.Add(new DimeContribuinteModel()
                    {
                        IdContribuinte = itens.IdContribuinte,
                        IdContabilista = itens.IdContabilista,
                        //NomeContabilista = itens.NomeContabilista,
                        //CPF = itens.CPF,
                        NomeRazaoSocial = itens.NomeRazaoSocial,
                        CNPJ = itens.CNPJ,
                        NroInscricao = itens.NroInscricao,
                        TipoDeclaracao = itens.TipoDeclaracao,
                        RegimeApuracao = itens.RegimeApuracao,
                        PorteEmpresa = itens.PorteEmpresa,
                        ApuracaoConsolidada = itens.ApuracaoConsolidada,
                        ApuracaoCentralizada = itens.ApuracaoCentralizada,
                        TemCreditosPresumido = itens.TemCreditosPresumido,
                        TemCredIncentFiscais = itens.TemCredIncentFiscais,
                        SubstitutoTributario = itens.SubstitutoTributario,
                        TemEscritaContabil = itens.TemEscritaContabil,
                        QtdeTrabAtividade = itens.QtdeTrabAtividade,
                        ErpIdEmpresa = itens.ErpIdEmpresa,
                        ErpBaseDados = itens.ErpBaseDados,
                        Excluido = itens.Excluido
                    });
                }
                return lista;
            }
        }
        public static List<DimeContabilistaModel> CarregaUmContabilista(int idContribuinte)
        {
            using (var db = new Context.SPSFiscal())
            {
                var query = from q in db.Contribuinte
                    where q.IdContribuinte == idContribuinte
                    join t in db.Contabilista on q.IdContabilista equals t.IdContabilista
                    select new
                    {
                        t.NomeContabilista,
                        t.CPF
                    };

                List<DimeContabilistaModel> lista = new List<DimeContabilistaModel>();

                foreach (var itens in query.ToList())
                {
                    lista.Add(new DimeContabilistaModel()
                    {
                        NomeContabilista = itens.NomeContabilista,
                        CPF = itens.CPF,
                    });
                }
                return lista;
            }
        }
    }
}
