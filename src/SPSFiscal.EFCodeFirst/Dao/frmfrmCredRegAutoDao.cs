﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SAPbouiCOM;
using SPSFiscal.Model;
using SPSFiscal = SPSFiscal.EFCodeFirst.Context.SPSFiscal;

namespace SPSFiscal.EFCodeFirst.Dao
{
    public static class frmfrmCredRegAutoDao
    {
        public static void CarregaGrid(ref Form oForm, DimeDadosQuadro46Model model)
        {
            try
            {
                using (var db = new Context.SPSFiscal())
                {
                    var query = db.DadosQuadro46
                        .Where(p => p.IdDeclaracao == model.IdDeclaracao)
                        .Select(q => new
                        {
                            q.IdLancamento,
                            q.IdDeclaracao,
                            q.Seq_Reg,
                            q.Cod_Identif,
                            q.Vl_Cred_Apur,
                            q,
                            range = (q.Ind_Origem == "1"
                                ? "AUC - Cred.Receb.transf/Compens.Sald.Dev.Prop."
                                : q.Ind_Origem == "14"
                                    ? "Demonstr. de Créd. Informado Previamente - DCIP"
                                    : q.Ind_Origem == "16"
                                        ? "DCIP Créd. Imposto Retido"
                                        : q.Ind_Origem == "17"
                                            ? "DCIP Créd. Transferível Relativo à Exportação"
                                            : q.Ind_Origem == "18"
                                                ? "DCIP Créd. Transferível à Saída Insenta"
                                                : "DCIP Créd. Transferível Relativo à Saída Isenta (Diferidas)")
                        })
                        .GroupBy(@t => @t.range, @t => @t.q).SingleOrDefault();

                    if (oForm.DataSources.DataTables.Item("grd").Columns.Count == 0)
                    {
                        oForm.DataSources.DataTables.Item("grd").Columns.Add("#", BoFieldsType.ft_AlphaNumeric, 200);
                        oForm.DataSources.DataTables.Item("grd").Columns.Add("IdDeclaracao", BoFieldsType.ft_AlphaNumeric, 200);
                        oForm.DataSources.DataTables.Item("grd").Columns.Add("Seq.", BoFieldsType.ft_AlphaNumeric, 200);
                        oForm.DataSources.DataTables.Item("grd").Columns.Add("Identif. do Regime ou da Autoriz. Especial", BoFieldsType.ft_AlphaNumeric, 200);
                        oForm.DataSources.DataTables.Item("grd").Columns.Add("Valor", BoFieldsType.ft_Price, 200);
                        oForm.DataSources.DataTables.Item("grd").Columns.Add("Origem de Crédito", BoFieldsType.ft_AlphaNumeric, 200);
                    }

                    int linhas = 0;
                    if (query != null)
                        foreach (var itens in query)
                        {
                            oForm.DataSources.DataTables.Item("grd").Rows.Add();
                            oForm.DataSources.DataTables.Item("grd").SetValue("#", linhas, itens.IdLancamento);
                            oForm.DataSources.DataTables.Item("grd").SetValue("IdDeclaracao", linhas, itens.IdDeclaracao);
                            oForm.DataSources.DataTables.Item("grd").SetValue("Seq.", linhas, itens.Seq_Reg);
                            //oForm.DataSources.DataTables.Item("grd").SetValue("Identif. do Regime ou da Autoriz. Especial", linhas, itens.Cod_Identif);
                            oForm.DataSources.DataTables.Item("grd").SetValue("Valor", linhas, Convert.ToDouble(itens.Vl_Cred_Apur));
                            oForm.DataSources.DataTables.Item("grd").SetValue("Origem de Crédito", linhas, itens.Ind_Origem);
                            linhas++;
                        }
                    ((Grid)oForm.Items.Item("grd").Specific).Columns.Item("IdDeclaracao").Visible = false;
                    (((SAPbouiCOM.EditTextColumn)(((Grid)oForm.Items.Item("grd").Specific).Columns.Item("#")))).LinkedObjectType = "BOY_IPT";
                    ((Grid)oForm.Items.Item("grd").Specific).AutoResizeColumns();
                    ((Grid)oForm.Items.Item("grd").Specific).SelectionMode = BoMatrixSelect.ms_Auto;
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public static void Delete(DimeDadosQuadro46Model model)
        {
            try
            {
                using (var db = new Context.SPSFiscal())
                {
                    var query = from q in db.DadosQuadro46
                                where q.IdLancamento == model.IdLancamento
                                select q;
                    db.DadosQuadro46.Remove(query.Single());
                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}
