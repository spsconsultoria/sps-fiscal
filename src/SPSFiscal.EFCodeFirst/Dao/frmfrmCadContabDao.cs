﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SPSFiscal.Model;

namespace SPSFiscal.EFCodeFirst.Dao
{
    public static class frmfrmCadContabDao
    {
        public static void Save(DimeContabilistaModel model)
        {
            try
            {
                using (var db = new Context.SPSFiscal())
                {
                    if (model.IdContabilista == 0)
                        db.Contabilista.Add(model);
                    else
                        db.Entry(model).State = EntityState.Modified;

                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public static void Delete(DimeContabilistaModel model)
        {
            try
            {
                using (var db = new Context.SPSFiscal())
                {
                    db.Entry(model).State = EntityState.Deleted;
                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}
