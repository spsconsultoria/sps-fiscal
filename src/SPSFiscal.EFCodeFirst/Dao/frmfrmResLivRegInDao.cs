﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SAPbouiCOM;
using SPSFiscal.Model;
using SPSFiscal = SPSFiscal.EFCodeFirst.Context.SPSFiscal;

namespace SPSFiscal.EFCodeFirst.Dao
{
    public static class frmfrmResLivRegInDao
    {
        public static void CarregaGridQuadro80(ref SAPbouiCOM.Form oForm, string idDeclaracao)
        {
            try
            {
                string sql = string.Format(@"DECLARE @LINHAS int = case when (select count(*) from [SPS_Fiscal].[dime].[DadosQuadro80] where IdDeclaracao = {0}) <> 0 
                                            Then (select count(*) from [SPS_Fiscal].[dime].[DadosQuadro80] where IdDeclaracao = {0}) else 0 end

                                            IF @LINHAS = 0
                                            INSERT INTO [SPS_Fiscal].[dime].[DadosQuadro80] (NumeroItem, IdDeclaracao, Valor)
                                            select [NumeroItem], {0},0.0 from [SPS_Fiscal].[dime].[TabelaQuadro80]

                                            SELECT T0.[NumeroItem]
                                                ,T0.[Descricao]
	                                            ,T1.[Valor]
                                            FROM [SPS_Fiscal].[dime].[TabelaQuadro80] T0
                                            left JOIN [SPS_Fiscal].[dime].[DadosQuadro80] T1 ON T0.NumeroItem = T1.NumeroItem
                                            WHERE T0.NumeroItem in (010,020,030) AND T1.IdDeclaracao = {0}", idDeclaracao);
                oForm.DataSources.DataTables.Item("grd").ExecuteQuery(sql);
                ((Grid)oForm.Items.Item("grd").Specific).AutoResizeColumns();
                ((Grid)oForm.Items.Item("grd").Specific).Columns.Item("NumeroItem").Editable = false;
                ((Grid)oForm.Items.Item("grd").Specific).Columns.Item("Descricao").Editable = false;
                ((Grid)oForm.Items.Item("grd").Specific).Columns.Item("Valor").Editable = true;
                ((Grid)oForm.Items.Item("grd").Specific).SelectionMode = BoMatrixSelect.ms_Auto;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public static void CarregaGridQuadro81(ref SAPbouiCOM.Form oForm, string idDeclaracao)
        {
            try
            {
                string sql = string.Format(@"DECLARE @LINHAS int = case when (select count(*) from [SPS_Fiscal].[dime].[DadosQuadro81] where IdDeclaracao = {0}) <> 0 
                                            Then (select count(*) from [SPS_Fiscal].[dime].[DadosQuadro81] where IdDeclaracao = {0}) else 0 end

                                            IF @LINHAS = 0
                                            INSERT INTO [SPS_Fiscal].[dime].[DadosQuadro81] (NumeroItem, IdDeclaracao, Valor)
                                            select [NumeroItem], {0},0.0 from [SPS_Fiscal].[dime].[TabelaQuadro81]

                                            SELECT T0.[NumeroItem]
                                                ,T0.[Descricao]
	                                            ,T1.[Valor]
                                            FROM [SPS_Fiscal].[dime].[TabelaQuadro81] T0
                                            left JOIN [SPS_Fiscal].[dime].[DadosQuadro81] T1 ON T0.NumeroItem = T1.NumeroItem
                                            WHERE T1.IdDeclaracao = {0}", idDeclaracao);
                oForm.DataSources.DataTables.Item("grd").ExecuteQuery(sql);
                ((Grid)oForm.Items.Item("grd").Specific).AutoResizeColumns();
                ((Grid)oForm.Items.Item("grd").Specific).Columns.Item("NumeroItem").Editable = false;
                ((Grid)oForm.Items.Item("grd").Specific).Columns.Item("Descricao").Editable = false;
                ((Grid)oForm.Items.Item("grd").Specific).Columns.Item("Valor").Editable = true;
                ((Grid)oForm.Items.Item("grd").Specific).SelectionMode = BoMatrixSelect.ms_Auto;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public static void CarregaGridQuadro82(ref SAPbouiCOM.Form oForm, string idDeclaracao)
        {
            try
            {
                string sql = string.Format(@"DECLARE @LINHAS int = case when (select count(*) from [SPS_Fiscal].[dime].[DadosQuadro82] where IdDeclaracao = {0}) <> 0 
                                            Then (select count(*) from [SPS_Fiscal].[dime].[DadosQuadro82] where IdDeclaracao = {0}) else 0 end

                                            IF @LINHAS = 0
                                            INSERT INTO [SPS_Fiscal].[dime].[DadosQuadro82] (NumeroItem, IdDeclaracao, Valor)
                                            select [NumeroItem], {0},0.0 from [SPS_Fiscal].[dime].[TabelaQuadro82]

                                            SELECT T0.[NumeroItem]
                                                ,T0.[Descricao]
	                                            ,T1.[Valor]
                                            FROM [SPS_Fiscal].[dime].[TabelaQuadro82] T0
                                            left JOIN [SPS_Fiscal].[dime].[DadosQuadro82] T1 ON T0.NumeroItem = T1.NumeroItem
                                            WHERE T1.IdDeclaracao = {0}", idDeclaracao);
                oForm.DataSources.DataTables.Item("grd").ExecuteQuery(sql);
                ((Grid)oForm.Items.Item("grd").Specific).AutoResizeColumns();
                ((Grid)oForm.Items.Item("grd").Specific).Columns.Item("NumeroItem").Editable = false;
                ((Grid)oForm.Items.Item("grd").Specific).Columns.Item("Descricao").Editable = false;
                ((Grid)oForm.Items.Item("grd").Specific).Columns.Item("Valor").Editable = true;
                ((Grid)oForm.Items.Item("grd").Specific).SelectionMode = BoMatrixSelect.ms_Auto;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public static void CarregaGridQuadro83(ref SAPbouiCOM.Form oForm, string idDeclaracao)
        {
            try
            {
                string sql = string.Format(@"DECLARE @LINHAS int = case when (select count(*) from [SPS_Fiscal].[dime].[DadosQuadro83] where IdDeclaracao = {0}) <> 0 
                                            Then (select count(*) from [SPS_Fiscal].[dime].[DadosQuadro83] where IdDeclaracao = {0}) else 0 end

                                            IF @LINHAS = 0
                                            INSERT INTO [SPS_Fiscal].[dime].[DadosQuadro83] (NumeroItem, IdDeclaracao, Valor)
                                            select [NumeroItem], {0},0.0 from [SPS_Fiscal].[dime].[TabelaQuadro83]

                                            SELECT T0.[NumeroItem]
                                                ,T0.[Descricao]
	                                            ,T1.[Valor]
                                            FROM [SPS_Fiscal].[dime].[TabelaQuadro83] T0
                                            left JOIN [SPS_Fiscal].[dime].[DadosQuadro83] T1 ON T0.NumeroItem = T1.NumeroItem
                                            WHERE T1.IdDeclaracao = {0}", idDeclaracao);
                oForm.DataSources.DataTables.Item("grd").ExecuteQuery(sql);
                ((Grid)oForm.Items.Item("grd").Specific).AutoResizeColumns();
                ((Grid)oForm.Items.Item("grd").Specific).Columns.Item("NumeroItem").Editable = false;
                ((Grid)oForm.Items.Item("grd").Specific).Columns.Item("Descricao").Editable = false;
                ((Grid)oForm.Items.Item("grd").Specific).Columns.Item("Valor").Editable = true;
                ((Grid)oForm.Items.Item("grd").Specific).SelectionMode = BoMatrixSelect.ms_Auto;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public static void CarregaGridQuadro84(ref SAPbouiCOM.Form oForm, string idDeclaracao)
        {
            try
            {
                string sql = string.Format(@"DECLARE @LINHAS int = case when (select count(*) from [SPS_Fiscal].[dime].[DadosQuadro84] where IdDeclaracao = {0}) <> 0 
                                            Then (select count(*) from [SPS_Fiscal].[dime].[DadosQuadro84] where IdDeclaracao = {0}) else 0 end

                                            IF @LINHAS = 0
                                            INSERT INTO [SPS_Fiscal].[dime].[DadosQuadro84] (NumeroItem, IdDeclaracao, Valor)
                                            select [NumeroItem], {0},0.0 from [SPS_Fiscal].[dime].[TabelaQuadro84]

                                            SELECT T0.[NumeroItem]
                                                ,T0.[Descricao]
	                                            ,T1.[Valor]
                                            FROM [SPS_Fiscal].[dime].[TabelaQuadro84] T0
                                            left JOIN [SPS_Fiscal].[dime].[DadosQuadro84] T1 ON T0.NumeroItem = T1.NumeroItem
                                            WHERE T1.IdDeclaracao = {0}", idDeclaracao);
                oForm.DataSources.DataTables.Item("grd").ExecuteQuery(sql);
                ((Grid)oForm.Items.Item("grd").Specific).AutoResizeColumns();
                ((Grid)oForm.Items.Item("grd").Specific).Columns.Item("NumeroItem").Editable = false;
                ((Grid)oForm.Items.Item("grd").Specific).Columns.Item("Descricao").Editable = false;
                ((Grid)oForm.Items.Item("grd").Specific).Columns.Item("Valor").Editable = true;
                ((Grid)oForm.Items.Item("grd").Specific).SelectionMode = BoMatrixSelect.ms_Auto;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public static void CarregaGridQuadro90(ref SAPbouiCOM.Form oForm, string idDeclaracao)
        {
            try
            {
                string sql = string.Format(@"DECLARE @LINHAS int = case when (select count(*) from [SPS_Fiscal].[dime].[DadosQuadro90] where IdDeclaracao = {0}) <> 0 
                                            Then (select count(*) from [SPS_Fiscal].[dime].[DadosQuadro90] where IdDeclaracao = {0}) else 0 end

                                            IF @LINHAS = 0
                                            INSERT INTO [SPS_Fiscal].[dime].[DadosQuadro90] (NumeroItem, IdDeclaracao, Valor)
                                            select [NumeroItem], {0},0.0 from [SPS_Fiscal].[dime].[TabelaQuadro90]

                                            SELECT T0.[NumeroItem]
                                                ,T0.[Descricao]
	                                            ,T1.[Valor]
                                            FROM [SPS_Fiscal].[dime].[TabelaQuadro90] T0
                                            left JOIN [SPS_Fiscal].[dime].[DadosQuadro90] T1 ON T0.NumeroItem = T1.NumeroItem
                                            WHERE T1.IdDeclaracao = {0}", idDeclaracao);
                oForm.DataSources.DataTables.Item("grd").ExecuteQuery(sql);
                ((Grid)oForm.Items.Item("grd").Specific).AutoResizeColumns();
                ((Grid)oForm.Items.Item("grd").Specific).Columns.Item("NumeroItem").Editable = false;
                ((Grid)oForm.Items.Item("grd").Specific).Columns.Item("Descricao").Editable = false;
                ((Grid)oForm.Items.Item("grd").Specific).Columns.Item("Valor").Editable = true;
                ((Grid)oForm.Items.Item("grd").Specific).SelectionMode = BoMatrixSelect.ms_Auto;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public static void CarregaGridQuadro91(ref SAPbouiCOM.Form oForm, string idDeclaracao)
        {
            try
            {
                string sql = string.Format(@"DECLARE @LINHAS int = case when (select count(*) from [SPS_Fiscal].[dime].[DadosQuadro91] where IdDeclaracao = {0}) <> 0 
                                            Then (select count(*) from [SPS_Fiscal].[dime].[DadosQuadro91] where IdDeclaracao = {0}) else 0 end

                                            IF @LINHAS = 0
                                            INSERT INTO [SPS_Fiscal].[dime].[DadosQuadro91] (NumeroItem, IdDeclaracao, Valor)
                                            select [NumeroItem], {0},0.0 from [SPS_Fiscal].[dime].[TabelaQuadro91]

                                            SELECT T0.[NumeroItem]
                                                ,T0.[Descricao]
	                                            ,T1.[Valor]
                                            FROM [SPS_Fiscal].[dime].[TabelaQuadro91] T0
                                            left JOIN [SPS_Fiscal].[dime].[DadosQuadro91] T1 ON T0.NumeroItem = T1.NumeroItem
                                            WHERE T1.IdDeclaracao = {0}", idDeclaracao);
                oForm.DataSources.DataTables.Item("grd").ExecuteQuery(sql);
                ((Grid)oForm.Items.Item("grd").Specific).AutoResizeColumns();
                ((Grid)oForm.Items.Item("grd").Specific).Columns.Item("NumeroItem").Editable = false;
                ((Grid)oForm.Items.Item("grd").Specific).Columns.Item("Descricao").Editable = false;
                ((Grid)oForm.Items.Item("grd").Specific).Columns.Item("Valor").Editable = true;
                ((Grid)oForm.Items.Item("grd").Specific).SelectionMode = BoMatrixSelect.ms_Auto;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public static void CarregaGridQuadro92(ref SAPbouiCOM.Form oForm, string idDeclaracao)
        {
            try
            {
                string sql = string.Format(@"DECLARE @LINHAS int = case when (select count(*) from [SPS_Fiscal].[dime].[DadosQuadro92] where IdDeclaracao = {0}) <> 0 
                                            Then (select count(*) from [SPS_Fiscal].[dime].[DadosQuadro92] where IdDeclaracao = {0}) else 0 end

                                            IF @LINHAS = 0
                                            INSERT INTO [SPS_Fiscal].[dime].[DadosQuadro92] (NumeroItem, IdDeclaracao, Valor)
                                            select [NumeroItem], {0},0.0 from [SPS_Fiscal].[dime].[TabelaQuadro92]

                                            SELECT T0.[NumeroItem]
                                                ,T0.[Descricao]
	                                            ,T1.[Valor]
                                            FROM [SPS_Fiscal].[dime].[TabelaQuadro92] T0
                                            left JOIN [SPS_Fiscal].[dime].[DadosQuadro92] T1 ON T0.NumeroItem = T1.NumeroItem
                                            WHERE T1.IdDeclaracao = {0}", idDeclaracao);
                oForm.DataSources.DataTables.Item("grd").ExecuteQuery(sql);
                ((Grid)oForm.Items.Item("grd").Specific).AutoResizeColumns();
                ((Grid)oForm.Items.Item("grd").Specific).Columns.Item("NumeroItem").Editable = false;
                ((Grid)oForm.Items.Item("grd").Specific).Columns.Item("Descricao").Editable = false;
                ((Grid)oForm.Items.Item("grd").Specific).Columns.Item("Valor").Editable = true;
                ((Grid)oForm.Items.Item("grd").Specific).SelectionMode = BoMatrixSelect.ms_Auto;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public static void CarregaGridQuadro93(ref SAPbouiCOM.Form oForm, string idDeclaracao)
        {
            try
            {
                string sql = string.Format(@"DECLARE @LINHAS int = case when (select count(*) from [SPS_Fiscal].[dime].[DadosQuadro93] where IdDeclaracao = {0}) <> 0 
                                            Then (select count(*) from [SPS_Fiscal].[dime].[DadosQuadro93] where IdDeclaracao = {0}) else 0 end

                                            IF @LINHAS = 0
                                            INSERT INTO [SPS_Fiscal].[dime].[DadosQuadro93] (NumeroItem, IdDeclaracao, Valor)
                                            select [NumeroItem], {0},0.0 from [SPS_Fiscal].[dime].[TabelaQuadro93]

                                            SELECT T0.[NumeroItem]
                                                ,T0.[Descricao]
	                                            ,T1.[Valor]
                                            FROM [SPS_Fiscal].[dime].[TabelaQuadro93] T0
                                            left JOIN [SPS_Fiscal].[dime].[DadosQuadro93] T1 ON T0.NumeroItem = T1.NumeroItem
                                            WHERE T1.IdDeclaracao = {0}", idDeclaracao);
                oForm.DataSources.DataTables.Item("grd").ExecuteQuery(sql);
                ((Grid)oForm.Items.Item("grd").Specific).AutoResizeColumns();
                ((Grid)oForm.Items.Item("grd").Specific).Columns.Item("NumeroItem").Editable = false;
                ((Grid)oForm.Items.Item("grd").Specific).Columns.Item("Descricao").Editable = false;
                ((Grid)oForm.Items.Item("grd").Specific).Columns.Item("Valor").Editable = true;
                ((Grid)oForm.Items.Item("grd").Specific).SelectionMode = BoMatrixSelect.ms_Auto;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public static void CarregaGridQuadro94(ref SAPbouiCOM.Form oForm, string idDeclaracao)
        {
            try
            {
                string sql = string.Format(@"DECLARE @LINHAS int = case when (select count(*) from [SPS_Fiscal].[dime].[DadosQuadro94] where IdDeclaracao = {0}) <> 0 
                                            Then (select count(*) from [SPS_Fiscal].[dime].[DadosQuadro94] where IdDeclaracao = {0}) else 0 end

                                            IF @LINHAS = 0
                                            INSERT INTO [SPS_Fiscal].[dime].[DadosQuadro94] (NumeroItem, IdDeclaracao, Valor)
                                            select [NumeroItem], {0},0.0 from [SPS_Fiscal].[dime].[TabelaQuadro94]

                                            SELECT T0.[NumeroItem]
                                                ,T0.[Descricao]
	                                            ,T1.[Valor]
                                            FROM [SPS_Fiscal].[dime].[TabelaQuadro94] T0
                                            left JOIN [SPS_Fiscal].[dime].[DadosQuadro94] T1 ON T0.NumeroItem = T1.NumeroItem
                                            WHERE T1.IdDeclaracao = {0}", idDeclaracao);
                oForm.DataSources.DataTables.Item("grd").ExecuteQuery(sql);
                ((Grid)oForm.Items.Item("grd").Specific).AutoResizeColumns();
                ((Grid)oForm.Items.Item("grd").Specific).Columns.Item("NumeroItem").Editable = false;
                ((Grid)oForm.Items.Item("grd").Specific).Columns.Item("Descricao").Editable = false;
                ((Grid)oForm.Items.Item("grd").Specific).Columns.Item("Valor").Editable = true;
                ((Grid)oForm.Items.Item("grd").Specific).SelectionMode = BoMatrixSelect.ms_Auto;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static void SaveDadosQuadro80(DimeDadosQuadro80Model model)
        {
            try
            {
                using (var db = new Context.SPSFiscal())
                {
                    var query = (from q in db.DadosQuadro80
                        where q.NumeroItem == model.NumeroItem
                        select q).Single();
                    query.Valor = model.Valor;
                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        public static void SaveDadosQuadro81(DimeDadosQuadro81Model model)
        {
            try
            {
                using (var db = new Context.SPSFiscal())
                {
                    var query = (from q in db.DadosQuadro81
                        where q.NumeroItem == model.NumeroItem
                        select q).Single();
                    query.Valor = model.Valor;
                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        public static void SaveDadosQuadro82(DimeDadosQuadro82Model model)
        {
            try
            {
                using (var db = new Context.SPSFiscal())
                {
                    var query = (from q in db.DadosQuadro82
                        where q.NumeroItem == model.NumeroItem
                        select q).Single();
                    query.Valor = model.Valor;
                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        public static void SaveDadosQuadro83(DimeDadosQuadro83Model model)
        {
            try
            {
                using (var db = new Context.SPSFiscal())
                {
                    var query = (from q in db.DadosQuadro84
                        where q.NumeroItem == model.NumeroItem
                        select q).Single();
                    query.Valor = model.Valor;
                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        public static void SaveDadosQuadro84(DimeDadosQuadro84Model model)
        {
            try
            {
                using (var db = new Context.SPSFiscal())
                {
                    var query = (from q in db.DadosQuadro84
                        where q.NumeroItem == model.NumeroItem
                        select q).Single();
                    query.Valor = model.Valor;
                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        public static void SaveDadosQuadro90(DimeDadosQuadro90Model model)
        {
            try
            {
                using (var db = new Context.SPSFiscal())
                {
                    var query = (from q in db.DadosQuadro90
                        where q.NumeroItem == model.NumeroItem
                        select q).Single();
                    query.Valor = model.Valor;
                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        public static void SaveDadosQuadro91(DimeDadosQuadro91Model model)
        {
            try
            {
                using (var db = new Context.SPSFiscal())
                {
                    var query = (from q in db.DadosQuadro91
                        where q.NumeroItem == model.NumeroItem
                        select q).Single();
                    query.Valor = model.Valor;
                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        public static void SaveDadosQuadro92(DimeDadosQuadro92Model model)
        {
            try
            {
                using (var db = new Context.SPSFiscal())
                {
                    var query = (from q in db.DadosQuadro92
                        where q.NumeroItem == model.NumeroItem
                        select q).Single();
                    query.Valor = model.Valor;
                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        public static void SaveDadosQuadro93(DimeDadosQuadro93Model model)
        {
            try
            {
                using (var db = new Context.SPSFiscal())
                {
                    var query = (from q in db.DadosQuadro93
                        where q.NumeroItem == model.NumeroItem
                        select q).Single();
                    query.Valor = model.Valor;
                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        public static void SaveDadosQuadro94(DimeDadosQuadro94Model model)
        {
            try
            {
                using (var db = new Context.SPSFiscal())
                {
                    var query = (from q in db.DadosQuadro94
                        where q.NumeroItem == model.NumeroItem
                        select q).Single();
                    query.Valor = model.Valor;
                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}
