﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SPSFiscal.Model;
using SPSFiscal = SPSFiscal.EFCodeFirst.Context.SPSFiscal;

namespace SPSFiscal.EFCodeFirst.Dao
{
    public static class frmfrmLancCredDao
    {
        public static void Save(DimeDadosQuadro46Model model)
        {
            try
            {
                using (var db = new Context.SPSFiscal())
                {
                    db.DadosQuadro46.Add(model);
                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        public static void Update(DimeDadosQuadro46Model model)
        {
            try
            {
                using (var db = new Context.SPSFiscal())
                {
                    var query = (from q in db.DadosQuadro46 where q.IdLancamento == model.IdLancamento select q).Single();
                    query.Cod_Identif = model.Cod_Identif;
                    query.Vl_Cred_Apur = model.Vl_Cred_Apur;
                    query.Ind_Origem = model.Ind_Origem;
                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        public static void UpdateOrigem1(DimeDadosQuadro46Model model)
        {
            try
            {
                using (var db = new Context.SPSFiscal())
                {
                    decimal total = (from q in db.DadosQuadro46
                        where q.Ind_Origem == "1" && q.IdDeclaracao == model.IdDeclaracao
                        select q).Sum(p => p.Vl_Cred_Apur);

                    var query = (from q in db.DadosQuadro09
                        where q.NumeroItem == "070" && q.IdDeclaracao == model.IdDeclaracao
                        select q).Single();
                    query.Valor = total;
                    db.SaveChanges();

                    var nuemroItens = new[] { "050", "060", "070", "075", "076" };

                     total = (from q in db.DadosQuadro09
                        where nuemroItens.Contains(q.NumeroItem) && q.IdDeclaracao == model.IdDeclaracao
                        select q).Sum(p => p.Valor);

                    query = (from q in db.DadosQuadro09
                        where q.NumeroItem == "080" && q.IdDeclaracao == model.IdDeclaracao
                        select q).Single();
                    query.Valor = total;
                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        public static void UpdateOrigem14(DimeDadosQuadro46Model model)
        {
            try
            {
                using (var db = new Context.SPSFiscal())
                {
                    decimal total = (from q in db.DadosQuadro46
                        where q.Ind_Origem == "14" && q.IdDeclaracao == model.IdDeclaracao
                        select q).Sum(p => p.Vl_Cred_Apur);

                    var query = (from q in db.DadosQuadro09
                        where q.NumeroItem == "075" && q.IdDeclaracao == model.IdDeclaracao
                        select q).Single();
                    query.Valor = total;
                    db.SaveChanges();

                    var nuemroItens = new[] { "050", "060", "070", "075", "076" };

                    total = (from q in db.DadosQuadro09
                        where nuemroItens.Contains(q.NumeroItem) && q.IdDeclaracao == model.IdDeclaracao
                        select q).Sum(p => p.Valor);

                    query = (from q in db.DadosQuadro09
                        where q.NumeroItem == "080" && q.IdDeclaracao == model.IdDeclaracao
                        select q).Single();
                    query.Valor = total;
                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        public static bool Existe(string origem)
        {
            try
            {
                using (var db = new Context.SPSFiscal())
                {
                    var query = (from q in db.DadosQuadro46
                                 where q.Ind_Origem == origem
                                 select q).ToList();

                    if (query.Count > 0)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }


    }
}
