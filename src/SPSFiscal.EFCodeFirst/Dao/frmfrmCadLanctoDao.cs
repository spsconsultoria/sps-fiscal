﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SPSFiscal.Model;
using SPSFiscal = SPSFiscal.EFCodeFirst.Context.SPSFiscal;

namespace SPSFiscal.EFCodeFirst.Dao
{
    public static class frmfrmCadLanctoDao
    {
        public static void Save(DimeDadosQuadro12Model model)
        {
            try
            {
                using (var db = new Context.SPSFiscal())
                {
                    db.DadosQuadro12.Add(model);
                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public static void Update(DimeDadosQuadro12Model model)
        {
            try
            {
                using (var db = new Context.SPSFiscal())
                {
                    db.Entry(model).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}
