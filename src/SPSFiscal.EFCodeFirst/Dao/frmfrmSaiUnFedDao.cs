﻿using System;
using System.Linq;
using SAPbouiCOM;
using SPSFiscal.Model;
using SPSFiscal = SPSFiscal.EFCodeFirst.Context.SPSFiscal;

namespace SPSFiscal.EFCodeFirst.Dao
{
    public static class frmfrmSaiUnFedDao
    {
        public static void CarregaGrid(ref SAPbouiCOM.Form oForm , DimeDadosQuadro50Model model)
        {
            try
            {
                using (var db = new Context.SPSFiscal())
                {
                    var query = (from q in db.DadosQuadro50
                                where q.IdDeclaracao == model.IdDeclaracao
                                select new
                                {
                                    q.IdLancamento,
                                    q.IdDeclaracao,
                                    q.Sigla_Estado,
                                    q.Vl_Contabil_Nao_Contrib,
                                    q.Vl_Contabil_Contrib,
                                    q.Vl_Base_Calc_Nao_Contrib,
                                    q.Vl_Base_Calc_Contrib,
                                    q.Vl_Outras,
                                    q.Vl_ST
                                }).ToList();

                    if (oForm.DataSources.DataTables.Item("grd").Columns.Count == 0)
                    {
                        oForm.DataSources.DataTables.Item("grd").Columns.Add("#", BoFieldsType.ft_AlphaNumeric, 200);
                        oForm.DataSources.DataTables.Item("grd").Columns.Add("IdDeclaracao", BoFieldsType.ft_AlphaNumeric, 200);
                        oForm.DataSources.DataTables.Item("grd").Columns.Add("UF", BoFieldsType.ft_AlphaNumeric, 200);
                        oForm.DataSources.DataTables.Item("grd").Columns.Add("Vl. Cont. Não Contrib.", BoFieldsType.ft_Price, 200);
                        oForm.DataSources.DataTables.Item("grd").Columns.Add("Vl. Cont. Contrib.", BoFieldsType.ft_Price, 200);
                        oForm.DataSources.DataTables.Item("grd").Columns.Add("BC Não Contrib.", BoFieldsType.ft_Price, 200);
                        oForm.DataSources.DataTables.Item("grd").Columns.Add("BC Contrib.", BoFieldsType.ft_Price, 200);
                        oForm.DataSources.DataTables.Item("grd").Columns.Add("Outras", BoFieldsType.ft_Price, 200);
                        oForm.DataSources.DataTables.Item("grd").Columns.Add("ICMS Retido por Sub. Tributária", BoFieldsType.ft_Price, 200);
                    }

                    int linha = 0;
                    foreach (var itens in query)
                    {
                        oForm.DataSources.DataTables.Item("grd").Rows.Add();
                        oForm.DataSources.DataTables.Item("grd").SetValue("#", linha, itens.IdLancamento);
                        oForm.DataSources.DataTables.Item("grd").SetValue("IdDeclaracao", linha, itens.IdDeclaracao);
                        oForm.DataSources.DataTables.Item("grd").SetValue("UF", linha, itens.Sigla_Estado);
                        oForm.DataSources.DataTables.Item("grd").SetValue("Vl. Cont. Não Contrib.", linha, Convert.ToDouble(itens.Vl_Contabil_Nao_Contrib));
                        oForm.DataSources.DataTables.Item("grd").SetValue("Vl. Cont. Contrib.", linha, Convert.ToDouble(itens.Vl_Contabil_Contrib));
                        oForm.DataSources.DataTables.Item("grd").SetValue("BC Não Contrib.", linha, Convert.ToDouble(itens.Vl_Base_Calc_Nao_Contrib));
                        oForm.DataSources.DataTables.Item("grd").SetValue("BC Contrib.", linha, Convert.ToDouble(itens.Vl_Base_Calc_Contrib));
                        oForm.DataSources.DataTables.Item("grd").SetValue("Outras", linha, Convert.ToDouble(itens.Vl_Outras));
                        oForm.DataSources.DataTables.Item("grd").SetValue("ICMS Retido por Sub. Tributária", linha, Convert.ToDouble(itens.Vl_ST));
                        linha++;
                    }
                    
                    ((Grid)oForm.Items.Item("grd").Specific).Columns.Item("IdDeclaracao").Visible = false;
                    (((SAPbouiCOM.EditTextColumn)(((Grid)oForm.Items.Item("grd").Specific).Columns.Item("#")))).LinkedObjectType = "BOY_IPT";
                    ((Grid)oForm.Items.Item("grd").Specific).AutoResizeColumns();
                    ((Grid)oForm.Items.Item("grd").Specific).SelectionMode = BoMatrixSelect.ms_Auto;
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static void Delete(int id)
        {
            try
            {
                using (var db = new Context.SPSFiscal())
                {
                    var query = (from q in db.DadosQuadro50 where q.IdLancamento == id select q).Single();
                    db.DadosQuadro50.Remove(query);
                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}