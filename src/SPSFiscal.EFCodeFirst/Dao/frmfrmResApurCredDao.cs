﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SAPbouiCOM;
using SPSFiscal.Model;

namespace SPSFiscal.EFCodeFirst.Dao
{
    public static class frmfrmResApurCredDao
    {
        public static void CarregaGrid(ref SAPbouiCOM.Form oForm, DimeDadosQuadro05Model model)
        {
            try
            {
                string sql = string.Format(@"
SELECT '1 - Transporte o saldo credor do mês anterior' AS 'Item'
,T0.[NumeroItem]
,T0.[Descricao]
,T1.[Valor]
INTO #TEMP
FROM [SPS_Fiscal].[dime].[TabelaQuadro05] T0
INNER JOIN [SPS_Fiscal].[dime].[DadosQuadro05] T1 ON T0.NumeroItem = T1.NumeroItem
WHERE T0.TipoItem = 1 AND T1.IdDeclaracao = {0}

UNION ALL

SELECT '2 - Créditos Gerais' AS 'Item'
,T0.[NumeroItem]
,T0.[Descricao]
,T1.[Valor]
FROM [SPS_Fiscal].[dime].[TabelaQuadro05] T0
INNER JOIN [SPS_Fiscal].[dime].[DadosQuadro05] T1 ON T0.NumeroItem = T1.NumeroItem
WHERE T0.TipoItem = 2 AND T1.IdDeclaracao = {0}

UNION ALL

SELECT '3 - Totalização' AS 'Item'
,T0.[NumeroItem]
,T0.[Descricao]
,T1.[Valor]
FROM [SPS_Fiscal].[dime].[TabelaQuadro05] T0
INNER JOIN [SPS_Fiscal].[dime].[DadosQuadro05] T1 ON T0.NumeroItem = T1.NumeroItem
WHERE T0.TipoItem = 3 AND T1.IdDeclaracao = {0}

declare @valor as decimal(18,2) = (select sum(Valor) from #TEMP)
select Item, NumeroItem AS 'Número', Descricao, Valor from #TEMP

UNION ALL

SELECT '4 - Totalização' AS 'Item'
, '990' AS 'Número'
, '(=) Subtotal de crédito => (transportar para o item 050 do quadro 09 - Cálculo do Imposto a Pagar ou Saldo Credor)' as 'Descricao'
, @valor as 'Valor'
order by NumeroItem asc
drop table #TEMP", model.IdDeclaracao);
                oForm.DataSources.DataTables.Item("grd").ExecuteQuery(sql);
                ((Grid)oForm.Items.Item("grd").Specific).CollapseLevel = 1;
                ((Grid)oForm.Items.Item("grd").Specific).AutoResizeColumns();
                ((Grid)oForm.Items.Item("grd").Specific).SelectionMode = BoMatrixSelect.ms_Auto;

                sql = string.Format(@"SELECT T0.[NumeroItem]
                                            ,T0.[Descricao]
	                                        ,T1.[Valor]
                                        FROM [SPS_Fiscal].[dime].[TabelaQuadro05] T0
                                        INNER JOIN [SPS_Fiscal].[dime].[DadosQuadro05] T1 ON T0.NumeroItem = T1.NumeroItem
                                        WHERE T0.NumeroItem in (030,040,045,050) AND T1.IdDeclaracao = {0}", model.IdDeclaracao);
                oForm.DataSources.DataTables.Item("grd2").ExecuteQuery(sql);
                ((Grid)oForm.Items.Item("grd2").Specific).AutoResizeColumns();
                ((Grid)oForm.Items.Item("grd2").Specific).Columns.Item("NumeroItem").Editable = false;
                ((Grid)oForm.Items.Item("grd2").Specific).Columns.Item("Descricao").Editable = false;
                ((Grid)oForm.Items.Item("grd2").Specific).Columns.Item("Valor").Editable = true;
                ((Grid)oForm.Items.Item("grd2").Specific).SelectionMode = BoMatrixSelect.ms_Auto;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static void Update(DimeDadosQuadro05Model model)
        {
            try
            {
                using (var db = new Context.SPSFiscal())
                {
                    var query = (from q in db.DadosQuadro05
                                 where q.NumeroItem == model.NumeroItem
                                 select q).SingleOrDefault();
                    query.Valor = model.Valor;
                    db.SaveChanges();
                } 
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}
