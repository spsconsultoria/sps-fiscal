﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SPSFiscal.Model;
using SPSFiscal = SPSFiscal.EFCodeFirst.Context.SPSFiscal;

namespace SPSFiscal.EFCodeFirst.Dao
{
    public static class frmfrmCadRateioDao
    {
        public static void Save(DimeDadosQuadro48Model model)
        {
            try
            {
                using (var db = new Context.SPSFiscal())
                {
                    db.DadosQuadro48.Add(model);
                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        public static void Update(DimeDadosQuadro48Model model)
        {
            try
            {
                using (var db = new Context.SPSFiscal())
                {
                    db.Entry(model).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        public static string GetNomeMunicipio(string vlCombo)
        {
            try
            {
                using (var db = new Context.SPSFiscal())
                {
                    var query = (from q in db.TabelaMunicipios
                                 where q.Codigo == vlCombo
                                 select q).Single();

                    return query.NomeMunicipio;
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        public static string GetDescricao(string vlCombo)
        {
            try
            {
                using (var db = new Context.SPSFiscal())
                {
                    var query = (from q in db.TabelaCodAtividade
                        where q.Codigo == vlCombo
                        select q).Single();

                    return query.Descricao;
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

    }
}
