﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SAPbouiCOM;
using SPSFiscal.Model;

namespace SPSFiscal.EFCodeFirst.Dao
{
    public static class frmfrmValFiscSaiDao
    {
        public static void CarregaGrid(ref SAPbouiCOM.Form oForm, DimeDadosQuadro02Model model)
        {
            try
            {
                using (var db = new Context.SPSFiscal())
                {
                    var query = (from q in db.DadosQuadro02
                                where q.IdDeclaracao == model.IdDeclaracao
                                select new
                                {
                                    q.IdLancamento,
                                    q.IdDeclaracao,
                                    q.CFOP,
                                    q.Vl_Contabil,
                                    q.Vl_Base_Calc,
                                    q.Vl_Imp_Debit,
                                    q.Vl_Isentas_Nao_Trib,
                                    q.Vl_Outras,
                                    q.Vl_Base_Calc_IR,
                                    q.Vl_IR
                                }).ToList();

                    if (oForm.DataSources.DataTables.Item("grd").Columns.Count == 0)
                    {
                        oForm.DataSources.DataTables.Item("grd").Columns.Add("IdLancamento", BoFieldsType.ft_AlphaNumeric, 200);
                        oForm.DataSources.DataTables.Item("grd").Columns.Add("IdDeclaracao", BoFieldsType.ft_AlphaNumeric, 200);
                        oForm.DataSources.DataTables.Item("grd").Columns.Add("CFOP", BoFieldsType.ft_AlphaNumeric, 200);
                        oForm.DataSources.DataTables.Item("grd").Columns.Add("Valor Contábil", BoFieldsType.ft_Price, 200);
                        oForm.DataSources.DataTables.Item("grd").Columns.Add("Base Cálculo", BoFieldsType.ft_Price, 200);
                        oForm.DataSources.DataTables.Item("grd").Columns.Add("imposto Debitado", BoFieldsType.ft_Price, 200);
                        oForm.DataSources.DataTables.Item("grd").Columns.Add("Isentas/Não Tributadas", BoFieldsType.ft_Price, 200);
                        oForm.DataSources.DataTables.Item("grd").Columns.Add("Outras", BoFieldsType.ft_Price, 200);
                        oForm.DataSources.DataTables.Item("grd").Columns.Add("BC.Imposto Retido", BoFieldsType.ft_Price, 200);
                        oForm.DataSources.DataTables.Item("grd").Columns.Add("Imposto Retido", BoFieldsType.ft_Price, 200);
                    }

                    int linha = 0;
                    foreach (var itens in query)
                    {
                        oForm.DataSources.DataTables.Item("grd").Rows.Add();
                        oForm.DataSources.DataTables.Item("grd").SetValue("IdLancamento", linha, itens.IdDeclaracao);
                        oForm.DataSources.DataTables.Item("grd").SetValue("IdDeclaracao", linha, itens.IdLancamento);
                        oForm.DataSources.DataTables.Item("grd").SetValue("CFOP", linha, itens.CFOP);
                        oForm.DataSources.DataTables.Item("grd").SetValue("Valor Contábil", linha, Convert.ToDouble(itens.Vl_Contabil));
                        oForm.DataSources.DataTables.Item("grd").SetValue("Base Cálculo", linha, Convert.ToDouble(itens.Vl_Base_Calc));
                        oForm.DataSources.DataTables.Item("grd").SetValue("imposto Debitado", linha, Convert.ToDouble(itens.Vl_Imp_Debit));
                        oForm.DataSources.DataTables.Item("grd").SetValue("Isentas/Não Tributadas", linha, Convert.ToDouble(itens.Vl_Isentas_Nao_Trib));
                        oForm.DataSources.DataTables.Item("grd").SetValue("Outras", linha, Convert.ToDouble(itens.Vl_Outras));
                        oForm.DataSources.DataTables.Item("grd").SetValue("BC.Imposto Retido", linha, Convert.ToDouble(itens.Vl_Base_Calc_IR));
                        oForm.DataSources.DataTables.Item("grd").SetValue("Imposto Retido", linha, Convert.ToDouble(itens.Vl_IR));
                        linha++;
                    }
                    
                    ((Grid)oForm.Items.Item("grd").Specific).Columns.Item(0).Visible = false;
                    ((Grid)oForm.Items.Item("grd").Specific).Columns.Item(1).Visible = false;
                    ((Grid)oForm.Items.Item("grd").Specific).AutoResizeColumns();
                    ((Grid)oForm.Items.Item("grd").Specific).SelectionMode = BoMatrixSelect.ms_Auto;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public static void Update(DimeDadosQuadro02Model model)
        {
            try
            {
                using (var db = new Context.SPSFiscal())
                {
                    var query = (from q in db.DadosQuadro02 where q.IdLancamento == model.IdLancamento select q).Single();
                    query.Vl_IR = model.Vl_IR;
                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}
