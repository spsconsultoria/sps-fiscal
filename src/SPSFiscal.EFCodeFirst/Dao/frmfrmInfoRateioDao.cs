﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SAPbouiCOM;
using SPSFiscal.Model;
using SPSFiscal = SPSFiscal.EFCodeFirst.Context.SPSFiscal;

namespace SPSFiscal.EFCodeFirst.Dao
{
    public static class frmfrmInfoRateioDao
    {
        public static void CarregaGrid(ref SAPbouiCOM.Form oForm, DimeDadosQuadro48Model model)
        {
            try
            {
                using (var db = new Context.SPSFiscal())
                {
                    var query = (from q in db.DadosQuadro48
                                 where q.IdDeclaracao == model.IdDeclaracao
                                 select new
                                 {
                                     q.IdLancamento,
                                     q.IdDeclaracao,
                                     q.Cod_Municipio,
                                     q.Vl_Perc_Ad,
                                     q.Cod_Tipo_Atv
                                 }).ToList();

                    if (oForm.DataSources.DataTables.Item("grd").Columns.Count == 0)
                    {
                        oForm.DataSources.DataTables.Item("grd").Columns.Add("#", BoFieldsType.ft_AlphaNumeric, 200);
                        oForm.DataSources.DataTables.Item("grd").Columns.Add("IdDeclaracao", BoFieldsType.ft_AlphaNumeric, 200);
                        oForm.DataSources.DataTables.Item("grd").Columns.Add("Cód. do Município de Santa Catarina", BoFieldsType.ft_AlphaNumeric, 200);
                        oForm.DataSources.DataTables.Item("grd").Columns.Add("Valor ou Percentual", BoFieldsType.ft_Price, 200);
                        oForm.DataSources.DataTables.Item("grd").Columns.Add("Cod. do Tipo de Atividade", BoFieldsType.ft_AlphaNumeric, 200);
                    }

                    int linha = 0;
                    foreach (var itens in query)
                    {
                        oForm.DataSources.DataTables.Item("grd").Rows.Add();
                        oForm.DataSources.DataTables.Item("grd").SetValue("#", linha, itens.IdLancamento);
                        oForm.DataSources.DataTables.Item("grd").SetValue("IdDeclaracao", linha, itens.IdDeclaracao);
                        oForm.DataSources.DataTables.Item("grd").SetValue("Cód. do Município de Santa Catarina", linha, itens.Cod_Municipio);
                        oForm.DataSources.DataTables.Item("grd").SetValue("Valor ou Percentual", linha, Convert.ToDouble(itens.Vl_Perc_Ad));
                        oForm.DataSources.DataTables.Item("grd").SetValue("Cod. do Tipo de Atividade", linha, itens.Cod_Tipo_Atv);
                        linha++;
                    }
                    
                    (((SAPbouiCOM.EditTextColumn)(((Grid)oForm.Items.Item("grd").Specific).Columns.Item("#")))).LinkedObjectType = "BOY_IPT";
                    ((Grid)oForm.Items.Item("grd").Specific).AutoResizeColumns();
                    ((Grid)oForm.Items.Item("grd").Specific).SelectionMode = BoMatrixSelect.ms_Auto;
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public static void Delete(int id)
        {
            try
            {
                using (var db = new Context.SPSFiscal())
                {
                    var query = (from q in db.DadosQuadro48
                        where q.IdDeclaracao == id
                        select q).Single();
                    db.DadosQuadro48.Remove(query);
                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public static List<DimeTabelaMunicipiosModel> GetComboTabelaMunicipios()
        {
            try
            {
                List <DimeTabelaMunicipiosModel> lista = new List<DimeTabelaMunicipiosModel>(); 
                using (var db = new Context.SPSFiscal())
                {
                    var query = (from q in db.TabelaMunicipios select q).ToArray();

                    foreach (var itens in query)
                    {
                        lista.Add(new DimeTabelaMunicipiosModel()
                        {
                            Codigo =  itens.Codigo,
                            NomeMunicipio = itens.NomeMunicipio
                        });
                    }
                }

                return lista;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public static List<DimeTabelaCodAtividadeModel> GetComboTabelaCodAtividade()
        {
            try
            {
                List<DimeTabelaCodAtividadeModel> lista = new List<DimeTabelaCodAtividadeModel>();
                using (var db = new Context.SPSFiscal())
                {
                    var query = (from q in db.TabelaCodAtividade select q).ToArray();

                    foreach (var itens in query)
                    {
                        lista.Add(new DimeTabelaCodAtividadeModel()
                        {
                            Codigo = itens.Codigo,
                            Descricao = itens.Descricao
                        });
                    }
                }

                return lista;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}
