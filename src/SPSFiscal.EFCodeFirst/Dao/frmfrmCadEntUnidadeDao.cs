﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SPSFiscal.Model;
using SPSFiscal = SPSFiscal.EFCodeFirst.Context.SPSFiscal;

namespace SPSFiscal.EFCodeFirst.Dao
{
    public static class frmfrmCadEntUnidadeDao
    {
        public static void Save(DimeDadosQuadro49Model model)
        {
            try
            {
                using (var db = new Context.SPSFiscal())
                {
                    db.DadosQuadro49.Add(model);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        
        public static void Update(DimeDadosQuadro49Model model)
        {
            try
            {
                using (var db = new Context.SPSFiscal())
                {
                    db.Entry(model).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
