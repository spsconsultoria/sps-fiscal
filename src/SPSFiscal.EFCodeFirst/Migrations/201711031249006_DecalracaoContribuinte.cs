namespace SPSFiscal.EFCodeFirst.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DecalracaoContribuinte : DbMigration
    {
        public override void Up()
        {
            AddColumn("dime.Contribuinte", "DimeDeclaracaoModel_IdDeclaracao", c => c.Int());
            AddColumn("dime.Declaracao", "IdContribuinte", c => c.Int(nullable: false));
            AddColumn("dime.Declaracao", "ErpBaseDados", c => c.String(maxLength: 50));
            AddColumn("dime.Declaracao", "DimeContribuinteModel_IdContribuinte", c => c.Int());
            CreateIndex("dime.Contribuinte", "DimeDeclaracaoModel_IdDeclaracao");
            CreateIndex("dime.Declaracao", "DimeContribuinteModel_IdContribuinte");
            AddForeignKey("dime.Contribuinte", "DimeDeclaracaoModel_IdDeclaracao", "dime.Declaracao", "IdDeclaracao");
            AddForeignKey("dime.Declaracao", "DimeContribuinteModel_IdContribuinte", "dime.Contribuinte", "IdContribuinte");
        }
        
        public override void Down()
        {
            DropForeignKey("dime.Declaracao", "DimeContribuinteModel_IdContribuinte", "dime.Contribuinte");
            DropForeignKey("dime.Contribuinte", "DimeDeclaracaoModel_IdDeclaracao", "dime.Declaracao");
            DropIndex("dime.Declaracao", new[] { "DimeContribuinteModel_IdContribuinte" });
            DropIndex("dime.Contribuinte", new[] { "DimeDeclaracaoModel_IdDeclaracao" });
            DropColumn("dime.Declaracao", "DimeContribuinteModel_IdContribuinte");
            DropColumn("dime.Declaracao", "ErpBaseDados");
            DropColumn("dime.Declaracao", "IdContribuinte");
            DropColumn("dime.Contribuinte", "DimeDeclaracaoModel_IdDeclaracao");
        }
    }
}
