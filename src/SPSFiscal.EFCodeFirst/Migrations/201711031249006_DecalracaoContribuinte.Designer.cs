// <auto-generated />
namespace SPSFiscal.EFCodeFirst.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class DecalracaoContribuinte : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(DecalracaoContribuinte));
        
        string IMigrationMetadata.Id
        {
            get { return "201711031249006_DecalracaoContribuinte"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
