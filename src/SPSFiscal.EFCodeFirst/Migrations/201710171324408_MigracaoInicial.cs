namespace SPSFiscal.EFCodeFirst.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MigracaoInicial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dime.Contabilista",
                c => new
                    {
                        IdContabilista = c.Int(nullable: false, identity: true),
                        CPF = c.String(maxLength: 18),
                        NomeContabilista = c.String(maxLength: 50),
                        Excluido = c.String(maxLength: 1),
                    })
                .PrimaryKey(t => t.IdContabilista);
            
            CreateTable(
                "dime.Contribuinte",
                c => new
                    {
                        IdContribuinte = c.Int(nullable: false, identity: true),
                        IdContabilista = c.Int(nullable: false),
                        NomeRazaoSocial = c.String(maxLength: 50),
                        CNPJ = c.String(maxLength: 18),
                        NroInscricao = c.String(maxLength: 20),
                        TipoDeclaracao = c.String(maxLength: 1),
                        RegimeApuracao = c.String(maxLength: 1),
                        PorteEmpresa = c.String(maxLength: 1),
                        ApuracaoConsolidada = c.String(maxLength: 1),
                        ApuracaoCentralizada = c.String(maxLength: 1),
                        TemCreditosPresumido = c.String(maxLength: 1),
                        TemCredIncentFiscais = c.String(maxLength: 1),
                        SubstitutoTributario = c.String(maxLength: 1),
                        TemEscritaContabil = c.String(maxLength: 1),
                        QtdeTrabAtividade = c.Int(nullable: false),
                        ErpIdEmpresa = c.String(maxLength: 10),
                        ErpBaseDados = c.String(maxLength: 50),
                        Excluido = c.String(maxLength: 1),
                        DimePeriodoApuracaoModel_IdPeriodo = c.Int(),
                    })
                .PrimaryKey(t => t.IdContribuinte)
                .ForeignKey("dime.PeriodoApuracao", t => t.DimePeriodoApuracaoModel_IdPeriodo)
                .ForeignKey("dime.Contabilista", t => t.IdContabilista, cascadeDelete: true)
                .Index(t => t.IdContabilista)
                .Index(t => t.DimePeriodoApuracaoModel_IdPeriodo);
            
            CreateTable(
                "dime.ContribuinteDeclaracao",
                c => new
                    {
                        IdDeclaracao = c.Int(nullable: false, identity: true),
                        IdContribuinte = c.Int(nullable: false),
                        PeriodoReferencia = c.String(maxLength: 8),
                        DtCriacao = c.DateTime(nullable: false),
                        HrCriacao = c.String(maxLength: 8),
                    })
                .PrimaryKey(t => t.IdDeclaracao)
                .ForeignKey("dime.Contribuinte", t => t.IdContribuinte, cascadeDelete: true)
                .Index(t => t.IdContribuinte);
            
            CreateTable(
                "dime.PeriodoApuracao",
                c => new
                    {
                        IdPeriodo = c.Int(nullable: false, identity: true),
                        DB = c.String(nullable: false, maxLength: 50),
                        Filial = c.String(nullable: false, maxLength: 5),
                        TipoReg = c.String(nullable: false, maxLength: 20),
                        IdDeclaracao = c.Int(nullable: false),
                        IdContribuinte = c.Int(nullable: false),
                        Periodo = c.String(nullable: false, maxLength: 8),
                        Usuario = c.String(nullable: false, maxLength: 30),
                        DimeContribuinteModel_IdContribuinte = c.Int(),
                    })
                .PrimaryKey(t => t.IdPeriodo)
                .ForeignKey("dime.Contribuinte", t => t.DimeContribuinteModel_IdContribuinte)
                .Index(t => t.DimeContribuinteModel_IdContribuinte);
            
            CreateTable(
                "dime.DebitosApuracao",
                c => new
                    {
                        NumeroItem = c.String(nullable: false, maxLength: 3),
                        IdPeriodo = c.Int(nullable: false),
                        Vl_Ajuste = c.Decimal(nullable: false, precision: 15, scale: 2),
                        PeriodoApuracao_IdPeriodo = c.Int(),
                        TabelaDados04_NumeroItem = c.String(maxLength: 3),
                        DimePeriodoApuracaoModel_IdPeriodo = c.Int(),
                    })
                .PrimaryKey(t => t.NumeroItem)
                .ForeignKey("dime.PeriodoApuracao", t => t.PeriodoApuracao_IdPeriodo)
                .ForeignKey("dime.TabelaQuadro04", t => t.TabelaDados04_NumeroItem)
                .ForeignKey("dime.PeriodoApuracao", t => t.DimePeriodoApuracaoModel_IdPeriodo)
                .Index(t => t.PeriodoApuracao_IdPeriodo)
                .Index(t => t.TabelaDados04_NumeroItem)
                .Index(t => t.DimePeriodoApuracaoModel_IdPeriodo);
            
            CreateTable(
                "dime.TabelaQuadro04",
                c => new
                    {
                        NumeroItem = c.String(nullable: false, maxLength: 3),
                        Descricao = c.String(maxLength: 200),
                        TipoItem = c.String(maxLength: 5),
                    })
                .PrimaryKey(t => t.NumeroItem);
            
            CreateTable(
                "dime.DadosQuadro04",
                c => new
                    {
                        IdDadosQuadro4 = c.Int(nullable: false, identity: true),
                        NumeroItem = c.String(nullable: false, maxLength: 3),
                        IdDeclaracao = c.Int(nullable: false),
                        Valor = c.Decimal(nullable: false, precision: 15, scale: 2),
                        DimeDeclaracaoModel_IdDeclaracao = c.Int(),
                        Declaracao_IdDeclaracao = c.Int(),
                        TabelaQuadro04_NumeroItem = c.String(maxLength: 3),
                        DimeTabelaQuadro04Model_NumeroItem = c.String(maxLength: 3),
                    })
                .PrimaryKey(t => t.IdDadosQuadro4)
                .ForeignKey("dime.Declaracao", t => t.DimeDeclaracaoModel_IdDeclaracao)
                .ForeignKey("dime.Declaracao", t => t.Declaracao_IdDeclaracao)
                .ForeignKey("dime.TabelaQuadro04", t => t.TabelaQuadro04_NumeroItem)
                .ForeignKey("dime.TabelaQuadro04", t => t.DimeTabelaQuadro04Model_NumeroItem)
                .Index(t => t.DimeDeclaracaoModel_IdDeclaracao)
                .Index(t => t.Declaracao_IdDeclaracao)
                .Index(t => t.TabelaQuadro04_NumeroItem)
                .Index(t => t.DimeTabelaQuadro04Model_NumeroItem);
            
            CreateTable(
                "dime.Declaracao",
                c => new
                    {
                        IdDeclaracao = c.Int(nullable: false, identity: true),
                        DtInicio = c.DateTime(nullable: false),
                        DtTermino = c.DateTime(nullable: false),
                        NomeContabilista = c.String(maxLength: 50),
                        ContabilistaCPF = c.String(maxLength: 15),
                        NomeContribuinte = c.String(maxLength: 50),
                        NroInscricao = c.String(maxLength: 20),
                        ContribuinteCNPJ = c.String(maxLength: 18),
                        PeriodoReferencia = c.String(maxLength: 8),
                        TipoDeclaracao = c.String(maxLength: 1),
                        RegimeApuracao = c.String(maxLength: 1),
                        PorteEmpresa = c.String(maxLength: 1),
                        ApuracaoConsolidada = c.String(maxLength: 1),
                        ApuracaoCentralizada = c.String(maxLength: 1),
                        TransCredPeriodo = c.String(maxLength: 1),
                        TemCreditosPresumido = c.String(maxLength: 1),
                        TemCredIncentFiscais = c.String(maxLength: 1),
                        Movimento = c.String(maxLength: 1),
                        SubstitutoTributario = c.String(maxLength: 1),
                        TemEscritaContabil = c.String(maxLength: 1),
                        QtdeTrabAtividade = c.Int(nullable: false),
                        Excluido = c.String(maxLength: 1),
                    })
                .PrimaryKey(t => t.IdDeclaracao);
            
            CreateTable(
                "dime.DadosQuadro01",
                c => new
                    {
                        IdLancamento = c.Int(nullable: false, identity: true),
                        IdDeclaracao = c.Int(nullable: false),
                        CFOP = c.String(maxLength: 5),
                        Vl_Contabil = c.Decimal(nullable: false, precision: 15, scale: 2),
                        Vl_Base_Calc = c.Decimal(nullable: false, precision: 15, scale: 2),
                        Vl_Imp_Cred = c.Decimal(nullable: false, precision: 15, scale: 2),
                        Vl_Isentas_Nao_Trib = c.Decimal(nullable: false, precision: 15, scale: 2),
                        Vl_Outras = c.Decimal(nullable: false, precision: 15, scale: 2),
                        Vl_Base_Calc_IR = c.Decimal(nullable: false, precision: 15, scale: 2),
                        Vl_IR = c.Decimal(nullable: false, precision: 15, scale: 2),
                        Vl_Difal = c.Decimal(nullable: false, precision: 15, scale: 2),
                        Declaracao_IdDeclaracao = c.Int(),
                        DimeDeclaracaoModel_IdDeclaracao = c.Int(),
                    })
                .PrimaryKey(t => t.IdLancamento)
                .ForeignKey("dime.Declaracao", t => t.Declaracao_IdDeclaracao)
                .ForeignKey("dime.Declaracao", t => t.DimeDeclaracaoModel_IdDeclaracao)
                .Index(t => t.Declaracao_IdDeclaracao)
                .Index(t => t.DimeDeclaracaoModel_IdDeclaracao);
            
            CreateTable(
                "dime.DadosQuadro02",
                c => new
                    {
                        IdLancamento = c.Int(nullable: false, identity: true),
                        IdDeclaracao = c.Int(nullable: false),
                        CFOP = c.String(maxLength: 5),
                        Vl_Contabil = c.Decimal(nullable: false, precision: 15, scale: 2),
                        Vl_Base_Calc = c.Decimal(nullable: false, precision: 15, scale: 2),
                        Vl_Imp_Debit = c.Decimal(nullable: false, precision: 15, scale: 2),
                        Vl_Isentas_Nao_Trib = c.Decimal(nullable: false, precision: 15, scale: 2),
                        Vl_Outras = c.Decimal(nullable: false, precision: 15, scale: 2),
                        Vl_Base_Calc_IR = c.Decimal(nullable: false, precision: 15, scale: 2),
                        Vl_IR = c.Decimal(nullable: false, precision: 15, scale: 2),
                    })
                .PrimaryKey(t => t.IdLancamento)
                .ForeignKey("dime.Declaracao", t => t.IdDeclaracao, cascadeDelete: true)
                .Index(t => t.IdDeclaracao);
            
            CreateTable(
                "dime.DadosQuadro03",
                c => new
                    {
                        IdDadosQuadro3 = c.Int(nullable: false, identity: true),
                        NumeroItem = c.String(nullable: false, maxLength: 3),
                        IdDeclaracao = c.Int(nullable: false),
                        Valor = c.Decimal(nullable: false, precision: 15, scale: 2),
                        Declaracao_IdDeclaracao = c.Int(),
                        DimeTabelaQuadro03Model_NumeroItem = c.String(maxLength: 3),
                        TabelaQuadro03_NumeroItem = c.String(maxLength: 3),
                        DimeDeclaracaoModel_IdDeclaracao = c.Int(),
                    })
                .PrimaryKey(t => t.IdDadosQuadro3)
                .ForeignKey("dime.Declaracao", t => t.Declaracao_IdDeclaracao)
                .ForeignKey("dime.TabelaQuadro03", t => t.DimeTabelaQuadro03Model_NumeroItem)
                .ForeignKey("dime.TabelaQuadro03", t => t.TabelaQuadro03_NumeroItem)
                .ForeignKey("dime.Declaracao", t => t.DimeDeclaracaoModel_IdDeclaracao)
                .Index(t => t.Declaracao_IdDeclaracao)
                .Index(t => t.DimeTabelaQuadro03Model_NumeroItem)
                .Index(t => t.TabelaQuadro03_NumeroItem)
                .Index(t => t.DimeDeclaracaoModel_IdDeclaracao);
            
            CreateTable(
                "dime.TabelaQuadro03",
                c => new
                    {
                        NumeroItem = c.String(nullable: false, maxLength: 3),
                        Descricao = c.String(maxLength: 200),
                        TipoItem = c.String(maxLength: 5),
                    })
                .PrimaryKey(t => t.NumeroItem);
            
            CreateTable(
                "dime.DadosQuadro05",
                c => new
                    {
                        IdDadosQuadro5 = c.Int(nullable: false, identity: true),
                        NumeroItem = c.String(nullable: false, maxLength: 3),
                        IdDeclaracao = c.Int(nullable: false),
                        Valor = c.Decimal(nullable: false, precision: 15, scale: 2),
                        Declaracao_IdDeclaracao = c.Int(),
                        DimeTabelaQuadro05Model_NumeroItem = c.String(maxLength: 3),
                        TabelaQuadro05_NumeroItem = c.String(maxLength: 3),
                        DimeDeclaracaoModel_IdDeclaracao = c.Int(),
                    })
                .PrimaryKey(t => t.IdDadosQuadro5)
                .ForeignKey("dime.Declaracao", t => t.Declaracao_IdDeclaracao)
                .ForeignKey("dime.TabelaQuadro05", t => t.DimeTabelaQuadro05Model_NumeroItem)
                .ForeignKey("dime.TabelaQuadro05", t => t.TabelaQuadro05_NumeroItem)
                .ForeignKey("dime.Declaracao", t => t.DimeDeclaracaoModel_IdDeclaracao)
                .Index(t => t.Declaracao_IdDeclaracao)
                .Index(t => t.DimeTabelaQuadro05Model_NumeroItem)
                .Index(t => t.TabelaQuadro05_NumeroItem)
                .Index(t => t.DimeDeclaracaoModel_IdDeclaracao);
            
            CreateTable(
                "dime.TabelaQuadro05",
                c => new
                    {
                        NumeroItem = c.String(nullable: false, maxLength: 3),
                        Descricao = c.String(maxLength: 200),
                        TipoItem = c.String(maxLength: 5),
                    })
                .PrimaryKey(t => t.NumeroItem);
            
            CreateTable(
                "dime.DadosQuadro09",
                c => new
                    {
                        IdDadosQuadro9 = c.Int(nullable: false, identity: true),
                        NumeroItem = c.String(nullable: false, maxLength: 3),
                        IdDeclaracao = c.Int(nullable: false),
                        Valor = c.Decimal(nullable: false, precision: 15, scale: 2),
                        Declaracao_IdDeclaracao = c.Int(),
                        DimeTabelaQuadro09Model_NumeroItem = c.String(maxLength: 3),
                        TabelaQuadro09_NumeroItem = c.String(maxLength: 3),
                        DimeDeclaracaoModel_IdDeclaracao = c.Int(),
                    })
                .PrimaryKey(t => t.IdDadosQuadro9)
                .ForeignKey("dime.Declaracao", t => t.Declaracao_IdDeclaracao)
                .ForeignKey("dime.TabelaQuadro09", t => t.DimeTabelaQuadro09Model_NumeroItem)
                .ForeignKey("dime.TabelaQuadro09", t => t.TabelaQuadro09_NumeroItem)
                .ForeignKey("dime.Declaracao", t => t.DimeDeclaracaoModel_IdDeclaracao)
                .Index(t => t.Declaracao_IdDeclaracao)
                .Index(t => t.DimeTabelaQuadro09Model_NumeroItem)
                .Index(t => t.TabelaQuadro09_NumeroItem)
                .Index(t => t.DimeDeclaracaoModel_IdDeclaracao);
            
            CreateTable(
                "dime.TabelaQuadro09",
                c => new
                    {
                        NumeroItem = c.String(nullable: false, maxLength: 3),
                        Descricao = c.String(maxLength: 200),
                        TipoItem = c.String(maxLength: 5),
                    })
                .PrimaryKey(t => t.NumeroItem);
            
            CreateTable(
                "dime.DadosQuadro11",
                c => new
                    {
                        IdDadosQuadro11 = c.Int(nullable: false, identity: true),
                        ID = c.Int(),
                        NumeroItem = c.String(nullable: false, maxLength: 3),
                        IdDeclaracao = c.Int(nullable: false),
                        Valor = c.Decimal(nullable: false, precision: 15, scale: 2),
                        Declaracao_IdDeclaracao = c.Int(),
                        DimeTabelaQuadro11Model_NumeroItem = c.String(maxLength: 3),
                        TabelaQuadro11_NumeroItem = c.String(maxLength: 3),
                        DimeDeclaracaoModel_IdDeclaracao = c.Int(),
                    })
                .PrimaryKey(t => t.IdDadosQuadro11)
                .ForeignKey("dime.Declaracao", t => t.Declaracao_IdDeclaracao)
                .ForeignKey("dime.TabelaQuadro11", t => t.DimeTabelaQuadro11Model_NumeroItem)
                .ForeignKey("dime.TabelaQuadro11", t => t.TabelaQuadro11_NumeroItem)
                .ForeignKey("dime.Declaracao", t => t.DimeDeclaracaoModel_IdDeclaracao)
                .Index(t => t.Declaracao_IdDeclaracao)
                .Index(t => t.DimeTabelaQuadro11Model_NumeroItem)
                .Index(t => t.TabelaQuadro11_NumeroItem)
                .Index(t => t.DimeDeclaracaoModel_IdDeclaracao);
            
            CreateTable(
                "dime.TabelaQuadro11",
                c => new
                    {
                        NumeroItem = c.String(nullable: false, maxLength: 3),
                        Descricao = c.String(maxLength: 200),
                        TipoItem = c.String(maxLength: 5),
                    })
                .PrimaryKey(t => t.NumeroItem);
            
            CreateTable(
                "dime.DadosQuadro12",
                c => new
                    {
                        IdLancamento = c.Int(nullable: false, identity: true),
                        IdDeclaracao = c.Int(nullable: false),
                        Ind_Org_Recol = c.String(maxLength: 2),
                        Cod_Receita = c.String(),
                        Dt_Vcto_Recol = c.DateTime(nullable: false),
                        Vl_Recol = c.Decimal(nullable: false, precision: 15, scale: 2),
                        Cod_Class_Vcto = c.String(maxLength: 5),
                        Nro_Acordo = c.String(maxLength: 15),
                        Declaracao_IdDeclaracao = c.Int(),
                        DimeDeclaracaoModel_IdDeclaracao = c.Int(),
                    })
                .PrimaryKey(t => t.IdLancamento)
                .ForeignKey("dime.Declaracao", t => t.Declaracao_IdDeclaracao)
                .ForeignKey("dime.Declaracao", t => t.DimeDeclaracaoModel_IdDeclaracao)
                .Index(t => t.Declaracao_IdDeclaracao)
                .Index(t => t.DimeDeclaracaoModel_IdDeclaracao);
            
            CreateTable(
                "dime.DadosQuadro14",
                c => new
                    {
                        IdDadosQuadro14 = c.Int(nullable: false, identity: true),
                        NumeroItem = c.String(nullable: false, maxLength: 3),
                        IdDeclaracao = c.Int(nullable: false),
                        Valor = c.Decimal(nullable: false, precision: 15, scale: 2),
                        Declaracao_IdDeclaracao = c.Int(),
                        DimeTabelaQuadro14Model_NumeroItem = c.String(maxLength: 3),
                        TabelaQuadro14_NumeroItem = c.String(maxLength: 3),
                        DimeDeclaracaoModel_IdDeclaracao = c.Int(),
                    })
                .PrimaryKey(t => t.IdDadosQuadro14)
                .ForeignKey("dime.Declaracao", t => t.Declaracao_IdDeclaracao)
                .ForeignKey("dime.TabelaQuadro14", t => t.DimeTabelaQuadro14Model_NumeroItem)
                .ForeignKey("dime.TabelaQuadro14", t => t.TabelaQuadro14_NumeroItem)
                .ForeignKey("dime.Declaracao", t => t.DimeDeclaracaoModel_IdDeclaracao)
                .Index(t => t.Declaracao_IdDeclaracao)
                .Index(t => t.DimeTabelaQuadro14Model_NumeroItem)
                .Index(t => t.TabelaQuadro14_NumeroItem)
                .Index(t => t.DimeDeclaracaoModel_IdDeclaracao);
            
            CreateTable(
                "dime.TabelaQuadro14",
                c => new
                    {
                        NumeroItem = c.String(nullable: false, maxLength: 3),
                        Descricao = c.String(maxLength: 250),
                        TipoItem = c.String(maxLength: 5),
                    })
                .PrimaryKey(t => t.NumeroItem);
            
            CreateTable(
                "dime.DadosQuadro46",
                c => new
                    {
                        IdLancamento = c.Int(nullable: false, identity: true),
                        IdDeclaracao = c.Int(nullable: false),
                        Seq_Reg = c.Int(nullable: false),
                        Cod_Identif = c.String(maxLength: 15),
                        Vl_Cred_Apur = c.Decimal(nullable: false, precision: 15, scale: 2),
                        Ind_Origem = c.String(maxLength: 2),
                    })
                .PrimaryKey(t => t.IdLancamento)
                .ForeignKey("dime.Declaracao", t => t.IdDeclaracao, cascadeDelete: true)
                .Index(t => t.IdDeclaracao);
            
            CreateTable(
                "dime.DadosQuadro48",
                c => new
                    {
                        IdLancamento = c.Int(nullable: false, identity: true),
                        IdDeclaracao = c.Int(nullable: false),
                        Cod_Municipio = c.String(maxLength: 5),
                        Vl_Perc_Ad = c.Decimal(nullable: false, precision: 15, scale: 2),
                        Cod_Tipo_Atv = c.String(maxLength: 3),
                    })
                .PrimaryKey(t => t.IdLancamento)
                .ForeignKey("dime.Declaracao", t => t.IdDeclaracao, cascadeDelete: true)
                .Index(t => t.IdDeclaracao);
            
            CreateTable(
                "dime.DadosQuadro49",
                c => new
                    {
                        IdLancamento = c.Int(nullable: false, identity: true),
                        IdDeclaracao = c.Int(nullable: false),
                        Sigla_Estado = c.String(maxLength: 2),
                        Vl_Contabil = c.Decimal(nullable: false, precision: 15, scale: 2),
                        Vl_Base_Calc = c.Decimal(nullable: false, precision: 15, scale: 2),
                        Vl_Outras = c.Decimal(nullable: false, precision: 15, scale: 2),
                        Vl_ST = c.Decimal(nullable: false, precision: 15, scale: 2),
                        Vl_ST_Outros = c.Decimal(nullable: false, precision: 15, scale: 2),
                    })
                .PrimaryKey(t => t.IdLancamento)
                .ForeignKey("dime.Declaracao", t => t.IdDeclaracao, cascadeDelete: true)
                .Index(t => t.IdDeclaracao);
            
            CreateTable(
                "dime.dadosQuadro50",
                c => new
                    {
                        IdLancamento = c.Int(nullable: false, identity: true),
                        IdDeclaracao = c.Int(nullable: false),
                        Sigla_Estado = c.String(maxLength: 2),
                        Vl_Contabil_Nao_Contrib = c.Decimal(nullable: false, precision: 15, scale: 2),
                        Vl_Contabil_Contrib = c.Decimal(nullable: false, precision: 15, scale: 2),
                        Vl_Base_Calc_Nao_Contrib = c.Decimal(nullable: false, precision: 15, scale: 2),
                        Vl_Base_Calc_Contrib = c.Decimal(nullable: false, precision: 15, scale: 2),
                        Vl_Outras = c.Decimal(nullable: false, precision: 15, scale: 2),
                        Vl_ST = c.Decimal(nullable: false, precision: 15, scale: 2),
                    })
                .PrimaryKey(t => t.IdLancamento)
                .ForeignKey("dime.Declaracao", t => t.IdDeclaracao, cascadeDelete: true)
                .Index(t => t.IdDeclaracao);
            
            CreateTable(
                "dime.DadosQuadro80",
                c => new
                    {
                        IdDadosQuadro80 = c.Int(nullable: false, identity: true),
                        NumeroItem = c.String(nullable: false, maxLength: 3),
                        IdDeclaracao = c.Int(nullable: false),
                        Valor = c.Decimal(nullable: false, precision: 15, scale: 2),
                        Declaracao_IdDeclaracao = c.Int(),
                        DimeDeclaracaoModel_IdDeclaracao = c.Int(),
                        DimeTabelaQuadro80Model_NumeroItem = c.String(maxLength: 3),
                    })
                .PrimaryKey(t => t.IdDadosQuadro80)
                .ForeignKey("dime.Declaracao", t => t.Declaracao_IdDeclaracao)
                .ForeignKey("dime.Declaracao", t => t.DimeDeclaracaoModel_IdDeclaracao)
                .ForeignKey("dime.TabelaQuadro80", t => t.DimeTabelaQuadro80Model_NumeroItem)
                .Index(t => t.Declaracao_IdDeclaracao)
                .Index(t => t.DimeDeclaracaoModel_IdDeclaracao)
                .Index(t => t.DimeTabelaQuadro80Model_NumeroItem);
            
            CreateTable(
                "dime.DadosQuadro81",
                c => new
                    {
                        IdDadosQuadro81 = c.Int(nullable: false, identity: true),
                        NumeroItem = c.String(nullable: false, maxLength: 3),
                        IdDeclaracao = c.Int(nullable: false),
                        Valor = c.Decimal(nullable: false, precision: 15, scale: 2),
                        Declaracao_IdDeclaracao = c.Int(),
                        DimeDeclaracaoModel_IdDeclaracao = c.Int(),
                        DimeTabelaQuadro81Model_NumeroItem = c.String(maxLength: 3),
                    })
                .PrimaryKey(t => t.IdDadosQuadro81)
                .ForeignKey("dime.Declaracao", t => t.Declaracao_IdDeclaracao)
                .ForeignKey("dime.Declaracao", t => t.DimeDeclaracaoModel_IdDeclaracao)
                .ForeignKey("dime.TabelaQuadro81", t => t.DimeTabelaQuadro81Model_NumeroItem)
                .Index(t => t.Declaracao_IdDeclaracao)
                .Index(t => t.DimeDeclaracaoModel_IdDeclaracao)
                .Index(t => t.DimeTabelaQuadro81Model_NumeroItem);
            
            CreateTable(
                "dime.DadosQuadro82",
                c => new
                    {
                        IdDadosQuadro82 = c.Int(nullable: false, identity: true),
                        NumeroItem = c.String(nullable: false, maxLength: 3),
                        IdDeclaracao = c.Int(nullable: false),
                        Valor = c.Decimal(nullable: false, precision: 15, scale: 2),
                        Declaracao_IdDeclaracao = c.Int(),
                        DimeDeclaracaoModel_IdDeclaracao = c.Int(),
                        DimeTabelaQuadro82Model_NumeroItem = c.String(maxLength: 3),
                    })
                .PrimaryKey(t => t.IdDadosQuadro82)
                .ForeignKey("dime.Declaracao", t => t.Declaracao_IdDeclaracao)
                .ForeignKey("dime.Declaracao", t => t.DimeDeclaracaoModel_IdDeclaracao)
                .ForeignKey("dime.TabelaQuadro82", t => t.DimeTabelaQuadro82Model_NumeroItem)
                .Index(t => t.Declaracao_IdDeclaracao)
                .Index(t => t.DimeDeclaracaoModel_IdDeclaracao)
                .Index(t => t.DimeTabelaQuadro82Model_NumeroItem);
            
            CreateTable(
                "dime.DadosQuadro83",
                c => new
                    {
                        IdDadosQuadro83 = c.Int(nullable: false, identity: true),
                        NumeroItem = c.String(nullable: false, maxLength: 3),
                        IdDeclaracao = c.Int(nullable: false),
                        Valor = c.Decimal(nullable: false, precision: 15, scale: 2),
                        Declaracao_IdDeclaracao = c.Int(),
                        DimeDeclaracaoModel_IdDeclaracao = c.Int(),
                        DimeTabelaQuadro83Model_NumeroItem = c.String(maxLength: 3),
                    })
                .PrimaryKey(t => t.IdDadosQuadro83)
                .ForeignKey("dime.Declaracao", t => t.Declaracao_IdDeclaracao)
                .ForeignKey("dime.Declaracao", t => t.DimeDeclaracaoModel_IdDeclaracao)
                .ForeignKey("dime.TabelaQuadro83", t => t.DimeTabelaQuadro83Model_NumeroItem)
                .Index(t => t.Declaracao_IdDeclaracao)
                .Index(t => t.DimeDeclaracaoModel_IdDeclaracao)
                .Index(t => t.DimeTabelaQuadro83Model_NumeroItem);
            
            CreateTable(
                "dime.DadosQuadro84",
                c => new
                    {
                        IdDadosQuadro84 = c.Int(nullable: false, identity: true),
                        NumeroItem = c.String(nullable: false, maxLength: 3),
                        IdDeclaracao = c.Int(nullable: false),
                        Valor = c.Decimal(nullable: false, precision: 15, scale: 2),
                        Declaracao_IdDeclaracao = c.Int(),
                        DimeTabelaQuadro84Model_NumeroItem = c.String(maxLength: 3),
                        TabelaQuadro84_NumeroItem = c.String(maxLength: 3),
                        DimeDeclaracaoModel_IdDeclaracao = c.Int(),
                    })
                .PrimaryKey(t => t.IdDadosQuadro84)
                .ForeignKey("dime.Declaracao", t => t.Declaracao_IdDeclaracao)
                .ForeignKey("dime.TabelaQuadro84", t => t.DimeTabelaQuadro84Model_NumeroItem)
                .ForeignKey("dime.TabelaQuadro84", t => t.TabelaQuadro84_NumeroItem)
                .ForeignKey("dime.Declaracao", t => t.DimeDeclaracaoModel_IdDeclaracao)
                .Index(t => t.Declaracao_IdDeclaracao)
                .Index(t => t.DimeTabelaQuadro84Model_NumeroItem)
                .Index(t => t.TabelaQuadro84_NumeroItem)
                .Index(t => t.DimeDeclaracaoModel_IdDeclaracao);
            
            CreateTable(
                "dime.TabelaQuadro84",
                c => new
                    {
                        NumeroItem = c.String(nullable: false, maxLength: 3),
                        Descricao = c.String(maxLength: 200),
                        TipoItem = c.String(maxLength: 2),
                    })
                .PrimaryKey(t => t.NumeroItem);
            
            CreateTable(
                "dime.DadosQuadro90",
                c => new
                    {
                        IdDadosQuadro90 = c.Int(nullable: false, identity: true),
                        NumeroItem = c.String(nullable: false, maxLength: 3),
                        IdDeclaracao = c.Int(nullable: false),
                        Valor = c.Decimal(nullable: false, precision: 15, scale: 2),
                        Declaracao_IdDeclaracao = c.Int(),
                        DimeDeclaracaoModel_IdDeclaracao = c.Int(),
                        DimeTabelaQuadro90Model_NumeroItem = c.String(maxLength: 3),
                    })
                .PrimaryKey(t => t.IdDadosQuadro90)
                .ForeignKey("dime.Declaracao", t => t.Declaracao_IdDeclaracao)
                .ForeignKey("dime.Declaracao", t => t.DimeDeclaracaoModel_IdDeclaracao)
                .ForeignKey("dime.TabelaQuadro90", t => t.DimeTabelaQuadro90Model_NumeroItem)
                .Index(t => t.Declaracao_IdDeclaracao)
                .Index(t => t.DimeDeclaracaoModel_IdDeclaracao)
                .Index(t => t.DimeTabelaQuadro90Model_NumeroItem);
            
            CreateTable(
                "dime.DadosQuadro91",
                c => new
                    {
                        IdDadosQuadro91 = c.Int(nullable: false, identity: true),
                        NumeroItem = c.String(nullable: false, maxLength: 3),
                        IdDeclaracao = c.Int(nullable: false),
                        Valor = c.Decimal(nullable: false, precision: 15, scale: 2),
                        Declaracao_IdDeclaracao = c.Int(),
                        DimeDeclaracaoModel_IdDeclaracao = c.Int(),
                        DimeTabelaQuadro91Model_NumeroItem = c.String(maxLength: 3),
                    })
                .PrimaryKey(t => t.IdDadosQuadro91)
                .ForeignKey("dime.Declaracao", t => t.Declaracao_IdDeclaracao)
                .ForeignKey("dime.Declaracao", t => t.DimeDeclaracaoModel_IdDeclaracao)
                .ForeignKey("dime.TabelaQuadro91", t => t.DimeTabelaQuadro91Model_NumeroItem)
                .Index(t => t.Declaracao_IdDeclaracao)
                .Index(t => t.DimeDeclaracaoModel_IdDeclaracao)
                .Index(t => t.DimeTabelaQuadro91Model_NumeroItem);
            
            CreateTable(
                "dime.DadosQuadro92",
                c => new
                    {
                        IdDadosQuadro92 = c.Int(nullable: false, identity: true),
                        NumeroItem = c.String(nullable: false, maxLength: 3),
                        IdDeclaracao = c.Int(nullable: false),
                        Valor = c.Decimal(nullable: false, precision: 15, scale: 2),
                        Declaracao_IdDeclaracao = c.Int(),
                        DimeDeclaracaoModel_IdDeclaracao = c.Int(),
                        DimeTabelaQuadro92Model_NumeroItem = c.String(maxLength: 3),
                    })
                .PrimaryKey(t => t.IdDadosQuadro92)
                .ForeignKey("dime.Declaracao", t => t.Declaracao_IdDeclaracao)
                .ForeignKey("dime.Declaracao", t => t.DimeDeclaracaoModel_IdDeclaracao)
                .ForeignKey("dime.TabelaQuadro92", t => t.DimeTabelaQuadro92Model_NumeroItem)
                .Index(t => t.Declaracao_IdDeclaracao)
                .Index(t => t.DimeDeclaracaoModel_IdDeclaracao)
                .Index(t => t.DimeTabelaQuadro92Model_NumeroItem);
            
            CreateTable(
                "dime.DadosQuadro93",
                c => new
                    {
                        IdDadosQuadro93 = c.Int(nullable: false, identity: true),
                        NumeroItem = c.String(nullable: false, maxLength: 3),
                        IdDeclaracao = c.Int(nullable: false),
                        Valor = c.Decimal(nullable: false, precision: 15, scale: 2),
                        Declaracao_IdDeclaracao = c.Int(),
                        DimeDeclaracaoModel_IdDeclaracao = c.Int(),
                        DimeTabelaQuadro93Model_NumeroItem = c.String(maxLength: 3),
                    })
                .PrimaryKey(t => t.IdDadosQuadro93)
                .ForeignKey("dime.Declaracao", t => t.Declaracao_IdDeclaracao)
                .ForeignKey("dime.Declaracao", t => t.DimeDeclaracaoModel_IdDeclaracao)
                .ForeignKey("dime.TabelaQuadro93", t => t.DimeTabelaQuadro93Model_NumeroItem)
                .Index(t => t.Declaracao_IdDeclaracao)
                .Index(t => t.DimeDeclaracaoModel_IdDeclaracao)
                .Index(t => t.DimeTabelaQuadro93Model_NumeroItem);
            
            CreateTable(
                "dime.DadosQuadro94",
                c => new
                    {
                        IdDadosQuadro94 = c.Int(nullable: false, identity: true),
                        NumeroItem = c.String(nullable: false, maxLength: 3),
                        IdDeclaracao = c.Int(nullable: false),
                        Valor = c.Decimal(nullable: false, precision: 15, scale: 2),
                        Declaracao_IdDeclaracao = c.Int(),
                        DimeDeclaracaoModel_IdDeclaracao = c.Int(),
                        DimeTabelaQuadro94Model_NumeroItem = c.String(maxLength: 3),
                    })
                .PrimaryKey(t => t.IdDadosQuadro94)
                .ForeignKey("dime.Declaracao", t => t.Declaracao_IdDeclaracao)
                .ForeignKey("dime.Declaracao", t => t.DimeDeclaracaoModel_IdDeclaracao)
                .ForeignKey("dime.TabelaQuadro94", t => t.DimeTabelaQuadro94Model_NumeroItem)
                .Index(t => t.Declaracao_IdDeclaracao)
                .Index(t => t.DimeDeclaracaoModel_IdDeclaracao)
                .Index(t => t.DimeTabelaQuadro94Model_NumeroItem);
            
            CreateTable(
                "dime.RegistroDeclaracao",
                c => new
                    {
                        IdRegistroDeclaracao = c.Int(nullable: false, identity: true),
                        IdDeclaracao = c.Int(nullable: false),
                        TipoRegistro = c.Int(nullable: false),
                        Declaracao_IdDeclaracao = c.Int(),
                        DimeTipoRegistroModel_TipoRegistro = c.Int(),
                        TipoRegistroV_TipoRegistro = c.Int(),
                        DimeDeclaracaoModel_IdDeclaracao = c.Int(),
                    })
                .PrimaryKey(t => t.IdRegistroDeclaracao)
                .ForeignKey("dime.Declaracao", t => t.Declaracao_IdDeclaracao)
                .ForeignKey("dime.TipoRegistro", t => t.DimeTipoRegistroModel_TipoRegistro)
                .ForeignKey("dime.TipoRegistro", t => t.TipoRegistroV_TipoRegistro)
                .ForeignKey("dime.Declaracao", t => t.DimeDeclaracaoModel_IdDeclaracao)
                .Index(t => t.Declaracao_IdDeclaracao)
                .Index(t => t.DimeTipoRegistroModel_TipoRegistro)
                .Index(t => t.TipoRegistroV_TipoRegistro)
                .Index(t => t.DimeDeclaracaoModel_IdDeclaracao);
            
            CreateTable(
                "dime.TipoRegistro",
                c => new
                    {
                        TipoRegistro = c.Int(nullable: false, identity: true),
                        Quadro = c.String(nullable: false, maxLength: 5),
                        Descricao = c.String(maxLength: 100),
                    })
                .PrimaryKey(t => t.TipoRegistro);
            
            CreateTable(
                "dime.EntradaP1",
                c => new
                    {
                        IdEntrada = c.Int(nullable: false, identity: true),
                        IdPeriodo = c.Int(nullable: false),
                        BD = c.String(maxLength: 50),
                        Filial = c.String(maxLength: 5),
                        TPDoc = c.String(maxLength: 5),
                        DataLanc = c.DateTime(nullable: false),
                        DataDoc = c.DateTime(nullable: false),
                        Serial = c.String(maxLength: 30),
                        Especie = c.String(maxLength: 30),
                        Estado = c.String(),
                        CFOP = c.String(maxLength: 5),
                        Vl_Contabil = c.Decimal(nullable: false, precision: 15, scale: 2),
                        Vl_Base_Calc = c.Decimal(nullable: false, precision: 15, scale: 2),
                        Aliq = c.Decimal(nullable: false, precision: 15, scale: 2),
                        Vl_Imp_Cred = c.Decimal(nullable: false, precision: 15, scale: 2),
                        Vl_Isentas_Nao_Trib = c.Decimal(nullable: false, precision: 15, scale: 2),
                        Vl_Outras = c.Decimal(nullable: false, precision: 15, scale: 2),
                        Vl_Base_ST = c.Decimal(nullable: false, precision: 15, scale: 2),
                        Vl_ST = c.Decimal(nullable: false, precision: 15, scale: 2),
                        Vl_Base_Calc_IR = c.Decimal(nullable: false, precision: 15, scale: 2),
                        Vl_IR = c.Decimal(nullable: false, precision: 15, scale: 2),
                        Vl_Difal = c.Decimal(nullable: false, precision: 15, scale: 2),
                        PeriodoApuracao_IdPeriodo = c.Int(),
                        DimePeriodoApuracaoModel_IdPeriodo = c.Int(),
                    })
                .PrimaryKey(t => t.IdEntrada)
                .ForeignKey("dime.PeriodoApuracao", t => t.PeriodoApuracao_IdPeriodo)
                .ForeignKey("dime.PeriodoApuracao", t => t.DimePeriodoApuracaoModel_IdPeriodo)
                .Index(t => t.PeriodoApuracao_IdPeriodo)
                .Index(t => t.DimePeriodoApuracaoModel_IdPeriodo);
            
            CreateTable(
                "dime.SaidasP2",
                c => new
                    {
                        IdSaida = c.Int(nullable: false, identity: true),
                        IdPeriodo = c.Int(nullable: false),
                        BD = c.String(maxLength: 50),
                        Filial = c.String(maxLength: 5),
                        TPDoc = c.String(maxLength: 5),
                        Especie = c.String(maxLength: 30),
                        Serial = c.String(maxLength: 30),
                        CFOP = c.String(maxLength: 5),
                        Vl_Contabil = c.Decimal(nullable: false, precision: 15, scale: 2),
                        Vl_Base_Calc = c.Decimal(nullable: false, precision: 15, scale: 2),
                        Aliq = c.Decimal(nullable: false, precision: 15, scale: 2),
                        Vl_Imp_Debit = c.Decimal(nullable: false, precision: 15, scale: 2),
                        Vl_Isentas_Nao_Trib = c.Decimal(nullable: false, precision: 15, scale: 2),
                        Vl_Outras = c.Decimal(nullable: false, precision: 15, scale: 2),
                        Vl_Base_ST = c.Decimal(nullable: false, precision: 15, scale: 2),
                        Vl_ST = c.Decimal(nullable: false, precision: 15, scale: 2),
                        Vl_Base_Calc_IR = c.Decimal(nullable: false, precision: 15, scale: 2),
                        Vl_IR = c.Decimal(nullable: false, precision: 15, scale: 2),
                        Dia = c.Int(nullable: false),
                        PeriodoApuracao_IdPeriodo = c.Int(),
                        DimePeriodoApuracaoModel_IdPeriodo = c.Int(),
                    })
                .PrimaryKey(t => t.IdSaida)
                .ForeignKey("dime.PeriodoApuracao", t => t.PeriodoApuracao_IdPeriodo)
                .ForeignKey("dime.PeriodoApuracao", t => t.DimePeriodoApuracaoModel_IdPeriodo)
                .Index(t => t.PeriodoApuracao_IdPeriodo)
                .Index(t => t.DimePeriodoApuracaoModel_IdPeriodo);
            
            CreateTable(
                "dime.TabelaCodAtividade",
                c => new
                    {
                        Codigo = c.String(nullable: false, maxLength: 3),
                        Descricao = c.String(maxLength: 300),
                    })
                .PrimaryKey(t => t.Codigo);
            
            CreateTable(
                "dime.TabelaMunicipios",
                c => new
                    {
                        Codigo = c.String(nullable: false, maxLength: 5),
                        NomeMunicipio = c.String(maxLength: 200),
                    })
                .PrimaryKey(t => t.Codigo);
            
            CreateTable(
                "dime.TabelaQuadro80",
                c => new
                    {
                        NumeroItem = c.String(nullable: false, maxLength: 3),
                        Descricao = c.String(maxLength: 200),
                        TipoItem = c.String(maxLength: 2),
                    })
                .PrimaryKey(t => t.NumeroItem);
            
            CreateTable(
                "dime.TabelaQuadro81",
                c => new
                    {
                        NumeroItem = c.String(nullable: false, maxLength: 3),
                        Descricao = c.String(maxLength: 200),
                        TipoItem = c.String(maxLength: 2),
                    })
                .PrimaryKey(t => t.NumeroItem);
            
            CreateTable(
                "dime.TabelaQuadro82",
                c => new
                    {
                        NumeroItem = c.String(nullable: false, maxLength: 3),
                        Descricao = c.String(maxLength: 200),
                        TipoItem = c.String(maxLength: 2),
                    })
                .PrimaryKey(t => t.NumeroItem);
            
            CreateTable(
                "dime.TabelaQuadro83",
                c => new
                    {
                        NumeroItem = c.String(nullable: false, maxLength: 3),
                        Descricao = c.String(maxLength: 200),
                        TipoItem = c.String(maxLength: 2),
                    })
                .PrimaryKey(t => t.NumeroItem);
            
            CreateTable(
                "dime.TabelaQuadro90",
                c => new
                    {
                        NumeroItem = c.String(nullable: false, maxLength: 3),
                        Descricao = c.String(maxLength: 200),
                        TipoItem = c.String(maxLength: 2),
                    })
                .PrimaryKey(t => t.NumeroItem);
            
            CreateTable(
                "dime.TabelaQuadro91",
                c => new
                    {
                        NumeroItem = c.String(nullable: false, maxLength: 3),
                        Descricao = c.String(maxLength: 200),
                        TipoItem = c.String(maxLength: 2),
                    })
                .PrimaryKey(t => t.NumeroItem);
            
            CreateTable(
                "dime.TabelaQuadro92",
                c => new
                    {
                        NumeroItem = c.String(nullable: false, maxLength: 3),
                        Descricao = c.String(maxLength: 200),
                        TipoItem = c.String(maxLength: 2),
                    })
                .PrimaryKey(t => t.NumeroItem);
            
            CreateTable(
                "dime.TabelaQuadro93",
                c => new
                    {
                        NumeroItem = c.String(nullable: false, maxLength: 3),
                        Descricao = c.String(maxLength: 200),
                        TipoItem = c.String(maxLength: 2),
                    })
                .PrimaryKey(t => t.NumeroItem);
            
            CreateTable(
                "dime.TabelaQuadro94",
                c => new
                    {
                        NumeroItem = c.String(nullable: false, maxLength: 3),
                        Descricao = c.String(maxLength: 200),
                        TipoItem = c.String(maxLength: 2),
                    })
                .PrimaryKey(t => t.NumeroItem);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dime.DadosQuadro94", "DimeTabelaQuadro94Model_NumeroItem", "dime.TabelaQuadro94");
            DropForeignKey("dime.DadosQuadro93", "DimeTabelaQuadro93Model_NumeroItem", "dime.TabelaQuadro93");
            DropForeignKey("dime.DadosQuadro92", "DimeTabelaQuadro92Model_NumeroItem", "dime.TabelaQuadro92");
            DropForeignKey("dime.DadosQuadro91", "DimeTabelaQuadro91Model_NumeroItem", "dime.TabelaQuadro91");
            DropForeignKey("dime.DadosQuadro90", "DimeTabelaQuadro90Model_NumeroItem", "dime.TabelaQuadro90");
            DropForeignKey("dime.DadosQuadro83", "DimeTabelaQuadro83Model_NumeroItem", "dime.TabelaQuadro83");
            DropForeignKey("dime.DadosQuadro82", "DimeTabelaQuadro82Model_NumeroItem", "dime.TabelaQuadro82");
            DropForeignKey("dime.DadosQuadro81", "DimeTabelaQuadro81Model_NumeroItem", "dime.TabelaQuadro81");
            DropForeignKey("dime.DadosQuadro80", "DimeTabelaQuadro80Model_NumeroItem", "dime.TabelaQuadro80");
            DropForeignKey("dime.Contribuinte", "IdContabilista", "dime.Contabilista");
            DropForeignKey("dime.PeriodoApuracao", "DimeContribuinteModel_IdContribuinte", "dime.Contribuinte");
            DropForeignKey("dime.SaidasP2", "DimePeriodoApuracaoModel_IdPeriodo", "dime.PeriodoApuracao");
            DropForeignKey("dime.SaidasP2", "PeriodoApuracao_IdPeriodo", "dime.PeriodoApuracao");
            DropForeignKey("dime.EntradaP1", "DimePeriodoApuracaoModel_IdPeriodo", "dime.PeriodoApuracao");
            DropForeignKey("dime.EntradaP1", "PeriodoApuracao_IdPeriodo", "dime.PeriodoApuracao");
            DropForeignKey("dime.DebitosApuracao", "DimePeriodoApuracaoModel_IdPeriodo", "dime.PeriodoApuracao");
            DropForeignKey("dime.DebitosApuracao", "TabelaDados04_NumeroItem", "dime.TabelaQuadro04");
            DropForeignKey("dime.DadosQuadro04", "DimeTabelaQuadro04Model_NumeroItem", "dime.TabelaQuadro04");
            DropForeignKey("dime.DadosQuadro04", "TabelaQuadro04_NumeroItem", "dime.TabelaQuadro04");
            DropForeignKey("dime.DadosQuadro04", "Declaracao_IdDeclaracao", "dime.Declaracao");
            DropForeignKey("dime.RegistroDeclaracao", "DimeDeclaracaoModel_IdDeclaracao", "dime.Declaracao");
            DropForeignKey("dime.RegistroDeclaracao", "TipoRegistroV_TipoRegistro", "dime.TipoRegistro");
            DropForeignKey("dime.RegistroDeclaracao", "DimeTipoRegistroModel_TipoRegistro", "dime.TipoRegistro");
            DropForeignKey("dime.RegistroDeclaracao", "Declaracao_IdDeclaracao", "dime.Declaracao");
            DropForeignKey("dime.DadosQuadro94", "DimeDeclaracaoModel_IdDeclaracao", "dime.Declaracao");
            DropForeignKey("dime.DadosQuadro94", "Declaracao_IdDeclaracao", "dime.Declaracao");
            DropForeignKey("dime.DadosQuadro93", "DimeDeclaracaoModel_IdDeclaracao", "dime.Declaracao");
            DropForeignKey("dime.DadosQuadro93", "Declaracao_IdDeclaracao", "dime.Declaracao");
            DropForeignKey("dime.DadosQuadro92", "DimeDeclaracaoModel_IdDeclaracao", "dime.Declaracao");
            DropForeignKey("dime.DadosQuadro92", "Declaracao_IdDeclaracao", "dime.Declaracao");
            DropForeignKey("dime.DadosQuadro91", "DimeDeclaracaoModel_IdDeclaracao", "dime.Declaracao");
            DropForeignKey("dime.DadosQuadro91", "Declaracao_IdDeclaracao", "dime.Declaracao");
            DropForeignKey("dime.DadosQuadro90", "DimeDeclaracaoModel_IdDeclaracao", "dime.Declaracao");
            DropForeignKey("dime.DadosQuadro90", "Declaracao_IdDeclaracao", "dime.Declaracao");
            DropForeignKey("dime.DadosQuadro84", "DimeDeclaracaoModel_IdDeclaracao", "dime.Declaracao");
            DropForeignKey("dime.DadosQuadro84", "TabelaQuadro84_NumeroItem", "dime.TabelaQuadro84");
            DropForeignKey("dime.DadosQuadro84", "DimeTabelaQuadro84Model_NumeroItem", "dime.TabelaQuadro84");
            DropForeignKey("dime.DadosQuadro84", "Declaracao_IdDeclaracao", "dime.Declaracao");
            DropForeignKey("dime.DadosQuadro83", "DimeDeclaracaoModel_IdDeclaracao", "dime.Declaracao");
            DropForeignKey("dime.DadosQuadro83", "Declaracao_IdDeclaracao", "dime.Declaracao");
            DropForeignKey("dime.DadosQuadro82", "DimeDeclaracaoModel_IdDeclaracao", "dime.Declaracao");
            DropForeignKey("dime.DadosQuadro82", "Declaracao_IdDeclaracao", "dime.Declaracao");
            DropForeignKey("dime.DadosQuadro81", "DimeDeclaracaoModel_IdDeclaracao", "dime.Declaracao");
            DropForeignKey("dime.DadosQuadro81", "Declaracao_IdDeclaracao", "dime.Declaracao");
            DropForeignKey("dime.DadosQuadro80", "DimeDeclaracaoModel_IdDeclaracao", "dime.Declaracao");
            DropForeignKey("dime.DadosQuadro80", "Declaracao_IdDeclaracao", "dime.Declaracao");
            DropForeignKey("dime.dadosQuadro50", "IdDeclaracao", "dime.Declaracao");
            DropForeignKey("dime.DadosQuadro49", "IdDeclaracao", "dime.Declaracao");
            DropForeignKey("dime.DadosQuadro48", "IdDeclaracao", "dime.Declaracao");
            DropForeignKey("dime.DadosQuadro46", "IdDeclaracao", "dime.Declaracao");
            DropForeignKey("dime.DadosQuadro14", "DimeDeclaracaoModel_IdDeclaracao", "dime.Declaracao");
            DropForeignKey("dime.DadosQuadro14", "TabelaQuadro14_NumeroItem", "dime.TabelaQuadro14");
            DropForeignKey("dime.DadosQuadro14", "DimeTabelaQuadro14Model_NumeroItem", "dime.TabelaQuadro14");
            DropForeignKey("dime.DadosQuadro14", "Declaracao_IdDeclaracao", "dime.Declaracao");
            DropForeignKey("dime.DadosQuadro12", "DimeDeclaracaoModel_IdDeclaracao", "dime.Declaracao");
            DropForeignKey("dime.DadosQuadro12", "Declaracao_IdDeclaracao", "dime.Declaracao");
            DropForeignKey("dime.DadosQuadro11", "DimeDeclaracaoModel_IdDeclaracao", "dime.Declaracao");
            DropForeignKey("dime.DadosQuadro11", "TabelaQuadro11_NumeroItem", "dime.TabelaQuadro11");
            DropForeignKey("dime.DadosQuadro11", "DimeTabelaQuadro11Model_NumeroItem", "dime.TabelaQuadro11");
            DropForeignKey("dime.DadosQuadro11", "Declaracao_IdDeclaracao", "dime.Declaracao");
            DropForeignKey("dime.DadosQuadro09", "DimeDeclaracaoModel_IdDeclaracao", "dime.Declaracao");
            DropForeignKey("dime.DadosQuadro09", "TabelaQuadro09_NumeroItem", "dime.TabelaQuadro09");
            DropForeignKey("dime.DadosQuadro09", "DimeTabelaQuadro09Model_NumeroItem", "dime.TabelaQuadro09");
            DropForeignKey("dime.DadosQuadro09", "Declaracao_IdDeclaracao", "dime.Declaracao");
            DropForeignKey("dime.DadosQuadro05", "DimeDeclaracaoModel_IdDeclaracao", "dime.Declaracao");
            DropForeignKey("dime.DadosQuadro05", "TabelaQuadro05_NumeroItem", "dime.TabelaQuadro05");
            DropForeignKey("dime.DadosQuadro05", "DimeTabelaQuadro05Model_NumeroItem", "dime.TabelaQuadro05");
            DropForeignKey("dime.DadosQuadro05", "Declaracao_IdDeclaracao", "dime.Declaracao");
            DropForeignKey("dime.DadosQuadro04", "DimeDeclaracaoModel_IdDeclaracao", "dime.Declaracao");
            DropForeignKey("dime.DadosQuadro03", "DimeDeclaracaoModel_IdDeclaracao", "dime.Declaracao");
            DropForeignKey("dime.DadosQuadro03", "TabelaQuadro03_NumeroItem", "dime.TabelaQuadro03");
            DropForeignKey("dime.DadosQuadro03", "DimeTabelaQuadro03Model_NumeroItem", "dime.TabelaQuadro03");
            DropForeignKey("dime.DadosQuadro03", "Declaracao_IdDeclaracao", "dime.Declaracao");
            DropForeignKey("dime.DadosQuadro02", "IdDeclaracao", "dime.Declaracao");
            DropForeignKey("dime.DadosQuadro01", "DimeDeclaracaoModel_IdDeclaracao", "dime.Declaracao");
            DropForeignKey("dime.DadosQuadro01", "Declaracao_IdDeclaracao", "dime.Declaracao");
            DropForeignKey("dime.DebitosApuracao", "PeriodoApuracao_IdPeriodo", "dime.PeriodoApuracao");
            DropForeignKey("dime.Contribuinte", "DimePeriodoApuracaoModel_IdPeriodo", "dime.PeriodoApuracao");
            DropForeignKey("dime.ContribuinteDeclaracao", "IdContribuinte", "dime.Contribuinte");
            DropIndex("dime.SaidasP2", new[] { "DimePeriodoApuracaoModel_IdPeriodo" });
            DropIndex("dime.SaidasP2", new[] { "PeriodoApuracao_IdPeriodo" });
            DropIndex("dime.EntradaP1", new[] { "DimePeriodoApuracaoModel_IdPeriodo" });
            DropIndex("dime.EntradaP1", new[] { "PeriodoApuracao_IdPeriodo" });
            DropIndex("dime.RegistroDeclaracao", new[] { "DimeDeclaracaoModel_IdDeclaracao" });
            DropIndex("dime.RegistroDeclaracao", new[] { "TipoRegistroV_TipoRegistro" });
            DropIndex("dime.RegistroDeclaracao", new[] { "DimeTipoRegistroModel_TipoRegistro" });
            DropIndex("dime.RegistroDeclaracao", new[] { "Declaracao_IdDeclaracao" });
            DropIndex("dime.DadosQuadro94", new[] { "DimeTabelaQuadro94Model_NumeroItem" });
            DropIndex("dime.DadosQuadro94", new[] { "DimeDeclaracaoModel_IdDeclaracao" });
            DropIndex("dime.DadosQuadro94", new[] { "Declaracao_IdDeclaracao" });
            DropIndex("dime.DadosQuadro93", new[] { "DimeTabelaQuadro93Model_NumeroItem" });
            DropIndex("dime.DadosQuadro93", new[] { "DimeDeclaracaoModel_IdDeclaracao" });
            DropIndex("dime.DadosQuadro93", new[] { "Declaracao_IdDeclaracao" });
            DropIndex("dime.DadosQuadro92", new[] { "DimeTabelaQuadro92Model_NumeroItem" });
            DropIndex("dime.DadosQuadro92", new[] { "DimeDeclaracaoModel_IdDeclaracao" });
            DropIndex("dime.DadosQuadro92", new[] { "Declaracao_IdDeclaracao" });
            DropIndex("dime.DadosQuadro91", new[] { "DimeTabelaQuadro91Model_NumeroItem" });
            DropIndex("dime.DadosQuadro91", new[] { "DimeDeclaracaoModel_IdDeclaracao" });
            DropIndex("dime.DadosQuadro91", new[] { "Declaracao_IdDeclaracao" });
            DropIndex("dime.DadosQuadro90", new[] { "DimeTabelaQuadro90Model_NumeroItem" });
            DropIndex("dime.DadosQuadro90", new[] { "DimeDeclaracaoModel_IdDeclaracao" });
            DropIndex("dime.DadosQuadro90", new[] { "Declaracao_IdDeclaracao" });
            DropIndex("dime.DadosQuadro84", new[] { "DimeDeclaracaoModel_IdDeclaracao" });
            DropIndex("dime.DadosQuadro84", new[] { "TabelaQuadro84_NumeroItem" });
            DropIndex("dime.DadosQuadro84", new[] { "DimeTabelaQuadro84Model_NumeroItem" });
            DropIndex("dime.DadosQuadro84", new[] { "Declaracao_IdDeclaracao" });
            DropIndex("dime.DadosQuadro83", new[] { "DimeTabelaQuadro83Model_NumeroItem" });
            DropIndex("dime.DadosQuadro83", new[] { "DimeDeclaracaoModel_IdDeclaracao" });
            DropIndex("dime.DadosQuadro83", new[] { "Declaracao_IdDeclaracao" });
            DropIndex("dime.DadosQuadro82", new[] { "DimeTabelaQuadro82Model_NumeroItem" });
            DropIndex("dime.DadosQuadro82", new[] { "DimeDeclaracaoModel_IdDeclaracao" });
            DropIndex("dime.DadosQuadro82", new[] { "Declaracao_IdDeclaracao" });
            DropIndex("dime.DadosQuadro81", new[] { "DimeTabelaQuadro81Model_NumeroItem" });
            DropIndex("dime.DadosQuadro81", new[] { "DimeDeclaracaoModel_IdDeclaracao" });
            DropIndex("dime.DadosQuadro81", new[] { "Declaracao_IdDeclaracao" });
            DropIndex("dime.DadosQuadro80", new[] { "DimeTabelaQuadro80Model_NumeroItem" });
            DropIndex("dime.DadosQuadro80", new[] { "DimeDeclaracaoModel_IdDeclaracao" });
            DropIndex("dime.DadosQuadro80", new[] { "Declaracao_IdDeclaracao" });
            DropIndex("dime.dadosQuadro50", new[] { "IdDeclaracao" });
            DropIndex("dime.DadosQuadro49", new[] { "IdDeclaracao" });
            DropIndex("dime.DadosQuadro48", new[] { "IdDeclaracao" });
            DropIndex("dime.DadosQuadro46", new[] { "IdDeclaracao" });
            DropIndex("dime.DadosQuadro14", new[] { "DimeDeclaracaoModel_IdDeclaracao" });
            DropIndex("dime.DadosQuadro14", new[] { "TabelaQuadro14_NumeroItem" });
            DropIndex("dime.DadosQuadro14", new[] { "DimeTabelaQuadro14Model_NumeroItem" });
            DropIndex("dime.DadosQuadro14", new[] { "Declaracao_IdDeclaracao" });
            DropIndex("dime.DadosQuadro12", new[] { "DimeDeclaracaoModel_IdDeclaracao" });
            DropIndex("dime.DadosQuadro12", new[] { "Declaracao_IdDeclaracao" });
            DropIndex("dime.DadosQuadro11", new[] { "DimeDeclaracaoModel_IdDeclaracao" });
            DropIndex("dime.DadosQuadro11", new[] { "TabelaQuadro11_NumeroItem" });
            DropIndex("dime.DadosQuadro11", new[] { "DimeTabelaQuadro11Model_NumeroItem" });
            DropIndex("dime.DadosQuadro11", new[] { "Declaracao_IdDeclaracao" });
            DropIndex("dime.DadosQuadro09", new[] { "DimeDeclaracaoModel_IdDeclaracao" });
            DropIndex("dime.DadosQuadro09", new[] { "TabelaQuadro09_NumeroItem" });
            DropIndex("dime.DadosQuadro09", new[] { "DimeTabelaQuadro09Model_NumeroItem" });
            DropIndex("dime.DadosQuadro09", new[] { "Declaracao_IdDeclaracao" });
            DropIndex("dime.DadosQuadro05", new[] { "DimeDeclaracaoModel_IdDeclaracao" });
            DropIndex("dime.DadosQuadro05", new[] { "TabelaQuadro05_NumeroItem" });
            DropIndex("dime.DadosQuadro05", new[] { "DimeTabelaQuadro05Model_NumeroItem" });
            DropIndex("dime.DadosQuadro05", new[] { "Declaracao_IdDeclaracao" });
            DropIndex("dime.DadosQuadro03", new[] { "DimeDeclaracaoModel_IdDeclaracao" });
            DropIndex("dime.DadosQuadro03", new[] { "TabelaQuadro03_NumeroItem" });
            DropIndex("dime.DadosQuadro03", new[] { "DimeTabelaQuadro03Model_NumeroItem" });
            DropIndex("dime.DadosQuadro03", new[] { "Declaracao_IdDeclaracao" });
            DropIndex("dime.DadosQuadro02", new[] { "IdDeclaracao" });
            DropIndex("dime.DadosQuadro01", new[] { "DimeDeclaracaoModel_IdDeclaracao" });
            DropIndex("dime.DadosQuadro01", new[] { "Declaracao_IdDeclaracao" });
            DropIndex("dime.DadosQuadro04", new[] { "DimeTabelaQuadro04Model_NumeroItem" });
            DropIndex("dime.DadosQuadro04", new[] { "TabelaQuadro04_NumeroItem" });
            DropIndex("dime.DadosQuadro04", new[] { "Declaracao_IdDeclaracao" });
            DropIndex("dime.DadosQuadro04", new[] { "DimeDeclaracaoModel_IdDeclaracao" });
            DropIndex("dime.DebitosApuracao", new[] { "DimePeriodoApuracaoModel_IdPeriodo" });
            DropIndex("dime.DebitosApuracao", new[] { "TabelaDados04_NumeroItem" });
            DropIndex("dime.DebitosApuracao", new[] { "PeriodoApuracao_IdPeriodo" });
            DropIndex("dime.PeriodoApuracao", new[] { "DimeContribuinteModel_IdContribuinte" });
            DropIndex("dime.ContribuinteDeclaracao", new[] { "IdContribuinte" });
            DropIndex("dime.Contribuinte", new[] { "DimePeriodoApuracaoModel_IdPeriodo" });
            DropIndex("dime.Contribuinte", new[] { "IdContabilista" });
            DropTable("dime.TabelaQuadro94");
            DropTable("dime.TabelaQuadro93");
            DropTable("dime.TabelaQuadro92");
            DropTable("dime.TabelaQuadro91");
            DropTable("dime.TabelaQuadro90");
            DropTable("dime.TabelaQuadro83");
            DropTable("dime.TabelaQuadro82");
            DropTable("dime.TabelaQuadro81");
            DropTable("dime.TabelaQuadro80");
            DropTable("dime.TabelaMunicipios");
            DropTable("dime.TabelaCodAtividade");
            DropTable("dime.SaidasP2");
            DropTable("dime.EntradaP1");
            DropTable("dime.TipoRegistro");
            DropTable("dime.RegistroDeclaracao");
            DropTable("dime.DadosQuadro94");
            DropTable("dime.DadosQuadro93");
            DropTable("dime.DadosQuadro92");
            DropTable("dime.DadosQuadro91");
            DropTable("dime.DadosQuadro90");
            DropTable("dime.TabelaQuadro84");
            DropTable("dime.DadosQuadro84");
            DropTable("dime.DadosQuadro83");
            DropTable("dime.DadosQuadro82");
            DropTable("dime.DadosQuadro81");
            DropTable("dime.DadosQuadro80");
            DropTable("dime.dadosQuadro50");
            DropTable("dime.DadosQuadro49");
            DropTable("dime.DadosQuadro48");
            DropTable("dime.DadosQuadro46");
            DropTable("dime.TabelaQuadro14");
            DropTable("dime.DadosQuadro14");
            DropTable("dime.DadosQuadro12");
            DropTable("dime.TabelaQuadro11");
            DropTable("dime.DadosQuadro11");
            DropTable("dime.TabelaQuadro09");
            DropTable("dime.DadosQuadro09");
            DropTable("dime.TabelaQuadro05");
            DropTable("dime.DadosQuadro05");
            DropTable("dime.TabelaQuadro03");
            DropTable("dime.DadosQuadro03");
            DropTable("dime.DadosQuadro02");
            DropTable("dime.DadosQuadro01");
            DropTable("dime.Declaracao");
            DropTable("dime.DadosQuadro04");
            DropTable("dime.TabelaQuadro04");
            DropTable("dime.DebitosApuracao");
            DropTable("dime.PeriodoApuracao");
            DropTable("dime.ContribuinteDeclaracao");
            DropTable("dime.Contribuinte");
            DropTable("dime.Contabilista");
        }
    }
}
