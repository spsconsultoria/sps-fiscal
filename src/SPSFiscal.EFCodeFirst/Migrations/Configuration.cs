namespace SPSFiscal.EFCodeFirst.Migrations
{
    using Model;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<SPSFiscal.EFCodeFirst.Context.SPSFiscal>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(SPSFiscal.EFCodeFirst.Context.SPSFiscal context)
        {
            //if (!System.Diagnostics.Debugger.IsAttached)
            //    System.Diagnostics.Debugger.Launch();
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //

            #region TABELA COD ATIVIDADE

            context.TabelaCodAtividade.AddOrUpdate(p => p.Codigo,
                new DimeTabelaCodAtividadeModel()
                {
                    Codigo = "1",
                    Descricao = "Prestador de servi�os de transportes"
                },
                new DimeTabelaCodAtividadeModel()
                {
                    Codigo = "2",
                    Descricao = "Prestador de servi�os de telecomunica��es"
                },
                new DimeTabelaCodAtividadeModel()
                {
                    Codigo = "3",
                    Descricao = "Fornecedor de energia el�trica ao consumidor independente, inclusive da parcela relativa � demanda contratada"
                },
                new DimeTabelaCodAtividadeModel()
                {
                    Codigo = "4",
                    Descricao = "Distribuidor de energia el�trica a consumidor pessoa f�sica ou jur�dica inclusive os fornecimentos a consumidor independente e demanda contratada"
                },
                new DimeTabelaCodAtividadeModel()
                {
                    Codigo = "5",
                    Descricao = "Fornecedor de g�s natural"
                },
                new DimeTabelaCodAtividadeModel()
                {
                    Codigo = "6",
                    Descricao = "Empresa que opere com o marketing direto"
                },
                new DimeTabelaCodAtividadeModel()
                {
                    Codigo = "7",
                    Descricao = "Dep�sito ou centro de distribui��o ou efetuar a entrega de mercadoria vendida por outro estabelecimento do mesmo titular"
                },
                new DimeTabelaCodAtividadeModel()
                {
                    Codigo = "8",
                    Descricao = "Fornecedor de alimentos preparados"
                },
                new DimeTabelaCodAtividadeModel()
                {
                    Codigo = "501",
                    Descricao = "For Detentor de TTD de obriga��o acess�ria autorizando a remessa de mercadorias ao varejo ou pronta entrega atrav�s de postos de abastecimento, situados no Estado"
                },
                new DimeTabelaCodAtividadeModel()
                {
                    Codigo = "502",
                    Descricao = "For Detentor de TTD de obriga��o acess�ria autorizando que nas opera��es de venda utilize demonstrativo auxiliar especifico para escritura��o do Livro de Sa�das"
                },
                new DimeTabelaCodAtividadeModel()
                {
                    Codigo = "503",
                    Descricao = "For Detentor de TTD de obriga��o acess�ria autorizando que o estabelecimento gerador de energia el�trica possua inscri��o �nica englobando v�rias PCHs"
                }
            );

            #endregion

            #region TABELA MUNICIPIOS

            context.TabelaMunicipios.AddOrUpdate(p => p.Codigo,
                new DimeTabelaMunicipiosModel() { Codigo = "8869", NomeMunicipio = "ALTO BELA VISTA" },
                new DimeTabelaMunicipiosModel() { Codigo = "8885", NomeMunicipio = "BALNE�RIO ARROIO DO SILVA" },
                new DimeTabelaMunicipiosModel() { Codigo = "8907", NomeMunicipio = "BALNE�RIO GAIVOTA" },
                new DimeTabelaMunicipiosModel() { Codigo = "8923", NomeMunicipio = "BANDEIRANTE" },
                new DimeTabelaMunicipiosModel() { Codigo = "8940", NomeMunicipio = "BARRA BONITA" },
                new DimeTabelaMunicipiosModel() { Codigo = "8966", NomeMunicipio = "BELA VISTA DO TOLDO" },
                new DimeTabelaMunicipiosModel() { Codigo = "8982", NomeMunicipio = "BOCAINA DO SUL" },
                new DimeTabelaMunicipiosModel() { Codigo = "9008", NomeMunicipio = "BOM JESUS" },
                new DimeTabelaMunicipiosModel() { Codigo = "9024", NomeMunicipio = "BOM JESUS DO OESTE" },
                new DimeTabelaMunicipiosModel() { Codigo = "9040", NomeMunicipio = "BRUN�POLIS" },
                new DimeTabelaMunicipiosModel() { Codigo = "9067", NomeMunicipio = "CAP�O ALTO" },
                new DimeTabelaMunicipiosModel() { Codigo = "9083", NomeMunicipio = "CHAPAD�O DO LAGEADO" },
                new DimeTabelaMunicipiosModel() { Codigo = "9105", NomeMunicipio = "CUNHATA�" },
                new DimeTabelaMunicipiosModel() { Codigo = "9121", NomeMunicipio = "ENTRE RIOS" },
                new DimeTabelaMunicipiosModel() { Codigo = "9148", NomeMunicipio = "ERMO" },
                new DimeTabelaMunicipiosModel() { Codigo = "9164", NomeMunicipio = "FLOR DO SERT�O" },
                new DimeTabelaMunicipiosModel() { Codigo = "9180", NomeMunicipio = "FREI ROG�RIO" },
                new DimeTabelaMunicipiosModel() { Codigo = "9202", NomeMunicipio = "IBIAM" },
                new DimeTabelaMunicipiosModel() { Codigo = "9229", NomeMunicipio = "IOMER�" },
                new DimeTabelaMunicipiosModel() { Codigo = "9245", NomeMunicipio = "JUPI�" },
                new DimeTabelaMunicipiosModel() { Codigo = "9261", NomeMunicipio = "LUZERNA" },
                new DimeTabelaMunicipiosModel() { Codigo = "9288", NomeMunicipio = "PAIAL" },
                new DimeTabelaMunicipiosModel() { Codigo = "9300", NomeMunicipio = "PAINEL" },
                new DimeTabelaMunicipiosModel() { Codigo = "9326", NomeMunicipio = "PALMEIRA" },
                new DimeTabelaMunicipiosModel() { Codigo = "9342", NomeMunicipio = "PRINCESA" },
                new DimeTabelaMunicipiosModel() { Codigo = "9369", NomeMunicipio = "SALTINHO" },
                new DimeTabelaMunicipiosModel() { Codigo = "9385", NomeMunicipio = "SANTA TEREZINHA DO PROGRESSO" },
                new DimeTabelaMunicipiosModel() { Codigo = "9407", NomeMunicipio = "SANTIAGO DO SUL" },
                new DimeTabelaMunicipiosModel() { Codigo = "9423", NomeMunicipio = "S�O BERNARDINO" },
                new DimeTabelaMunicipiosModel() { Codigo = "9440", NomeMunicipio = "S�O PEDRO DE ALC�NTARA" },
                new DimeTabelaMunicipiosModel() { Codigo = "9466", NomeMunicipio = "TIGRINHOS" },
                new DimeTabelaMunicipiosModel() { Codigo = "9482", NomeMunicipio = "TREVISO" },
                new DimeTabelaMunicipiosModel() { Codigo = "9504", NomeMunicipio = "ZORT�A" },
                new DimeTabelaMunicipiosModel() { Codigo = "9733", NomeMunicipio = "FORQUILHINHA" },
                new DimeTabelaMunicipiosModel() { Codigo = "11924", NomeMunicipio = "BALNE�RIO RINC�O" },
                new DimeTabelaMunicipiosModel() { Codigo = "11940", NomeMunicipio = "PESCARIA BRAVA" },
                new DimeTabelaMunicipiosModel() { Codigo = "55379", NomeMunicipio = "BOMBINHAS" },
                new DimeTabelaMunicipiosModel() { Codigo = "55395", NomeMunicipio = "MORRO GRANDE" },
                new DimeTabelaMunicipiosModel() { Codigo = "55417", NomeMunicipio = "PASSO DE TORRES" },
                new DimeTabelaMunicipiosModel() { Codigo = "55433", NomeMunicipio = "COCAL DO SUL" },
                new DimeTabelaMunicipiosModel() { Codigo = "55450", NomeMunicipio = "CAPIVARI DE BAIXO" },
                new DimeTabelaMunicipiosModel() { Codigo = "55476", NomeMunicipio = "SANG�O" },
                new DimeTabelaMunicipiosModel() { Codigo = "55492", NomeMunicipio = "BALNE�RIO BARRA DO SUL" },
                new DimeTabelaMunicipiosModel() { Codigo = "55514", NomeMunicipio = "S�O JO�O DO ITAPERIU" },
                new DimeTabelaMunicipiosModel() { Codigo = "55530", NomeMunicipio = "CALMON" },
                new DimeTabelaMunicipiosModel() { Codigo = "55557", NomeMunicipio = "SANTA TEREZINHA" },
                new DimeTabelaMunicipiosModel() { Codigo = "55573", NomeMunicipio = "BRA�O DO TROMBUDO" },
                new DimeTabelaMunicipiosModel() { Codigo = "55590", NomeMunicipio = "MIRIM DOCE" },
                new DimeTabelaMunicipiosModel() { Codigo = "55611", NomeMunicipio = "MONTE CARLO" },
                new DimeTabelaMunicipiosModel() { Codigo = "55638", NomeMunicipio = "VARGEM" },
                new DimeTabelaMunicipiosModel() { Codigo = "55654", NomeMunicipio = "VARGEM BONITA" },
                new DimeTabelaMunicipiosModel() { Codigo = "55670", NomeMunicipio = "CERRO NEGRO" },
                new DimeTabelaMunicipiosModel() { Codigo = "55697", NomeMunicipio = "PONTE ALTA DO NORTE" },
                new DimeTabelaMunicipiosModel() { Codigo = "55719", NomeMunicipio = "RIO RUFINO" },
                new DimeTabelaMunicipiosModel() { Codigo = "55735", NomeMunicipio = "S�O CRIST�V�O DO SUL" },
                new DimeTabelaMunicipiosModel() { Codigo = "55751", NomeMunicipio = "MACIEIRA" },
                new DimeTabelaMunicipiosModel() { Codigo = "55778", NomeMunicipio = "�GUAS FRIAS" },
                new DimeTabelaMunicipiosModel() { Codigo = "55794", NomeMunicipio = "CORDILHEIRA ALTA" },
                new DimeTabelaMunicipiosModel() { Codigo = "55816", NomeMunicipio = "FORMOSA DO SUL" },
                new DimeTabelaMunicipiosModel() { Codigo = "55832", NomeMunicipio = "GUATAMBU" },
                new DimeTabelaMunicipiosModel() { Codigo = "55859", NomeMunicipio = "IRATI" },
                new DimeTabelaMunicipiosModel() { Codigo = "55875", NomeMunicipio = "JARDIN�POLIS" },
                new DimeTabelaMunicipiosModel() { Codigo = "55891", NomeMunicipio = "NOVA ITABERABA" },
                new DimeTabelaMunicipiosModel() { Codigo = "55913", NomeMunicipio = "NOVO HORIZONTE" },
                new DimeTabelaMunicipiosModel() { Codigo = "55930", NomeMunicipio = "PLANALTO ALEGRE" },
                new DimeTabelaMunicipiosModel() { Codigo = "55956", NomeMunicipio = "SUL BRASIL" },
                new DimeTabelaMunicipiosModel() { Codigo = "55972", NomeMunicipio = "ARABUT�" },
                new DimeTabelaMunicipiosModel() { Codigo = "55999", NomeMunicipio = "ARVOREDO" },
                new DimeTabelaMunicipiosModel() { Codigo = "57355", NomeMunicipio = "CORONEL MARTINS" },
                new DimeTabelaMunicipiosModel() { Codigo = "57371", NomeMunicipio = "IPUA�U" },
                new DimeTabelaMunicipiosModel() { Codigo = "57398", NomeMunicipio = "LAJEADO GRANDE" },
                new DimeTabelaMunicipiosModel() { Codigo = "57410", NomeMunicipio = "OURO VERDE" },
                new DimeTabelaMunicipiosModel() { Codigo = "57436", NomeMunicipio = "PASSOS MAIA" },
                new DimeTabelaMunicipiosModel() { Codigo = "57452", NomeMunicipio = "BELMONTE" },
                new DimeTabelaMunicipiosModel() { Codigo = "57479", NomeMunicipio = "PARA�SO" },
                new DimeTabelaMunicipiosModel() { Codigo = "57495", NomeMunicipio = "RIQUEZA" },
                new DimeTabelaMunicipiosModel() { Codigo = "57517", NomeMunicipio = "SANTA HELENA" },
                new DimeTabelaMunicipiosModel() { Codigo = "57533", NomeMunicipio = "S�O JO�O DO OESTE" },
                new DimeTabelaMunicipiosModel() { Codigo = "57550", NomeMunicipio = "S�O MIGUEL DA BOA VISTA" },
                new DimeTabelaMunicipiosModel() { Codigo = "80012", NomeMunicipio = "ABELARDO LUZ" },
                new DimeTabelaMunicipiosModel() { Codigo = "80039", NomeMunicipio = "AGROL�NDIA" },
                new DimeTabelaMunicipiosModel() { Codigo = "80055", NomeMunicipio = "AGRON�MICA" },
                new DimeTabelaMunicipiosModel() { Codigo = "80071", NomeMunicipio = "�GUA DOCE" },
                new DimeTabelaMunicipiosModel() { Codigo = "80098", NomeMunicipio = "�GUAS DE CHAPEC�" },
                new DimeTabelaMunicipiosModel() { Codigo = "80110", NomeMunicipio = "�GUAS MORNAS" },
                new DimeTabelaMunicipiosModel() { Codigo = "80136", NomeMunicipio = "ALFREDO WAGNER" },
                new DimeTabelaMunicipiosModel() { Codigo = "80152", NomeMunicipio = "ANCHIETA" },
                new DimeTabelaMunicipiosModel() { Codigo = "80179", NomeMunicipio = "ANGELINA" },
                new DimeTabelaMunicipiosModel() { Codigo = "80195", NomeMunicipio = "ANITA GARIBALDI" },
                new DimeTabelaMunicipiosModel() { Codigo = "80217", NomeMunicipio = "ANIT�POLIS" },
                new DimeTabelaMunicipiosModel() { Codigo = "80233", NomeMunicipio = "ANT�NIO CARLOS" },
                new DimeTabelaMunicipiosModel() { Codigo = "80250", NomeMunicipio = "ARAQUARI" },
                new DimeTabelaMunicipiosModel() { Codigo = "80276", NomeMunicipio = "ARARANGU�" },
                new DimeTabelaMunicipiosModel() { Codigo = "80292", NomeMunicipio = "ARMAZ�M" },
                new DimeTabelaMunicipiosModel() { Codigo = "80314", NomeMunicipio = "ARROIO TRINTA" },
                new DimeTabelaMunicipiosModel() { Codigo = "80330", NomeMunicipio = "ASCURRA" },
                new DimeTabelaMunicipiosModel() { Codigo = "80357", NomeMunicipio = "ATALANTA" },
                new DimeTabelaMunicipiosModel() { Codigo = "80373", NomeMunicipio = "AURORA" },
                new DimeTabelaMunicipiosModel() { Codigo = "80390", NomeMunicipio = "BALNE�RIO CAMBORI�" },
                new DimeTabelaMunicipiosModel() { Codigo = "80411", NomeMunicipio = "BARRA VELHA" },
                new DimeTabelaMunicipiosModel() { Codigo = "80438", NomeMunicipio = "BENEDITO NOVO" },
                new DimeTabelaMunicipiosModel() { Codigo = "80454", NomeMunicipio = "BIGUA�U" },
                new DimeTabelaMunicipiosModel() { Codigo = "80470", NomeMunicipio = "BLUMENAU" },
                new DimeTabelaMunicipiosModel() { Codigo = "80497", NomeMunicipio = "BOM RETIRO" },
                new DimeTabelaMunicipiosModel() { Codigo = "80519", NomeMunicipio = "BOTUVER�" },
                new DimeTabelaMunicipiosModel() { Codigo = "80535", NomeMunicipio = "BRA�O DO NORTE" },
                new DimeTabelaMunicipiosModel() { Codigo = "80551", NomeMunicipio = "BRUSQUE" },
                new DimeTabelaMunicipiosModel() { Codigo = "80578", NomeMunicipio = "CA�ADOR" },
                new DimeTabelaMunicipiosModel() { Codigo = "80594", NomeMunicipio = "CAIBI" },
                new DimeTabelaMunicipiosModel() { Codigo = "80616", NomeMunicipio = "CAMBORI�" },
                new DimeTabelaMunicipiosModel() { Codigo = "80632", NomeMunicipio = "CAMPO ALEGRE" },
                new DimeTabelaMunicipiosModel() { Codigo = "80659", NomeMunicipio = "CAMPO BELO DO SUL" },
                new DimeTabelaMunicipiosModel() { Codigo = "80675", NomeMunicipio = "CAMPO ER�" },
                new DimeTabelaMunicipiosModel() { Codigo = "80691", NomeMunicipio = "CAMPOS NOVOS" },
                new DimeTabelaMunicipiosModel() { Codigo = "80713", NomeMunicipio = "CANELINHA" },
                new DimeTabelaMunicipiosModel() { Codigo = "80730", NomeMunicipio = "CANOINHAS" },
                new DimeTabelaMunicipiosModel() { Codigo = "80756", NomeMunicipio = "CAPINZAL" },
                new DimeTabelaMunicipiosModel() { Codigo = "80772", NomeMunicipio = "CATANDUVAS" },
                new DimeTabelaMunicipiosModel() { Codigo = "80799", NomeMunicipio = "CAXAMBU DO SUL" },
                new DimeTabelaMunicipiosModel() { Codigo = "80810", NomeMunicipio = "CHAPEC�" },
                new DimeTabelaMunicipiosModel() { Codigo = "80837", NomeMunicipio = "CONC�RDIA" },
                new DimeTabelaMunicipiosModel() { Codigo = "80853", NomeMunicipio = "CORONEL FREITAS" },
                new DimeTabelaMunicipiosModel() { Codigo = "80870", NomeMunicipio = "CORUP�" },
                new DimeTabelaMunicipiosModel() { Codigo = "80896", NomeMunicipio = "CRICI�MA" },
                new DimeTabelaMunicipiosModel() { Codigo = "80918", NomeMunicipio = "CUNHA POR�" },
                new DimeTabelaMunicipiosModel() { Codigo = "80934", NomeMunicipio = "CURITIBANOS" },
                new DimeTabelaMunicipiosModel() { Codigo = "80950", NomeMunicipio = "DESCANSO" },
                new DimeTabelaMunicipiosModel() { Codigo = "80977", NomeMunicipio = "DION�SIO CERQUEIRA" },
                new DimeTabelaMunicipiosModel() { Codigo = "80993", NomeMunicipio = "DONA EMMA" },
                new DimeTabelaMunicipiosModel() { Codigo = "81019", NomeMunicipio = "ERVAL VELHO" },
                new DimeTabelaMunicipiosModel() { Codigo = "81035", NomeMunicipio = "FAXINAL DOS GUEDES" },
                new DimeTabelaMunicipiosModel() { Codigo = "81051", NomeMunicipio = "FLORIAN�POLIS" },
                new DimeTabelaMunicipiosModel() { Codigo = "81078", NomeMunicipio = "FRAIBURGO" },
                new DimeTabelaMunicipiosModel() { Codigo = "81094", NomeMunicipio = "GALV�O" },
                new DimeTabelaMunicipiosModel() { Codigo = "81116", NomeMunicipio = "GOVERNADOR CELSO RAMOS" },
                new DimeTabelaMunicipiosModel() { Codigo = "81132", NomeMunicipio = "GAROPABA" },
                new DimeTabelaMunicipiosModel() { Codigo = "81159", NomeMunicipio = "GARUVA" },
                new DimeTabelaMunicipiosModel() { Codigo = "81175", NomeMunicipio = "GASPAR" },
                new DimeTabelaMunicipiosModel() { Codigo = "81191", NomeMunicipio = "GR�O PAR�" },
                new DimeTabelaMunicipiosModel() { Codigo = "81213", NomeMunicipio = "GRAVATAL" },
                new DimeTabelaMunicipiosModel() { Codigo = "81230", NomeMunicipio = "GUABIRUBA" },
                new DimeTabelaMunicipiosModel() { Codigo = "81256", NomeMunicipio = "GUARACIABA" },
                new DimeTabelaMunicipiosModel() { Codigo = "81272", NomeMunicipio = "GUARAMIRIM" },
                new DimeTabelaMunicipiosModel() { Codigo = "81299", NomeMunicipio = "GUARUJ� DO SUL" },
                new DimeTabelaMunicipiosModel() { Codigo = "81310", NomeMunicipio = "HERVAL DO OESTE" },
                new DimeTabelaMunicipiosModel() { Codigo = "81337", NomeMunicipio = "IBICAR�" },
                new DimeTabelaMunicipiosModel() { Codigo = "81353", NomeMunicipio = "IBIRAMA" },
                new DimeTabelaMunicipiosModel() { Codigo = "81370", NomeMunicipio = "I�ARA" },
                new DimeTabelaMunicipiosModel() { Codigo = "81396", NomeMunicipio = "ILHOTA" },
                new DimeTabelaMunicipiosModel() { Codigo = "81418", NomeMunicipio = "IMARU�" },
                new DimeTabelaMunicipiosModel() { Codigo = "81434", NomeMunicipio = "IMBITUBA" },
                new DimeTabelaMunicipiosModel() { Codigo = "81450", NomeMunicipio = "IMBUIA" },
                new DimeTabelaMunicipiosModel() { Codigo = "81477", NomeMunicipio = "INDAIAL" },
                new DimeTabelaMunicipiosModel() { Codigo = "81493", NomeMunicipio = "IPIRA" },
                new DimeTabelaMunicipiosModel() { Codigo = "81515", NomeMunicipio = "IPUMIRIM" },
                new DimeTabelaMunicipiosModel() { Codigo = "81531", NomeMunicipio = "IRANI" },
                new DimeTabelaMunicipiosModel() { Codigo = "81558", NomeMunicipio = "IRINE�POLIS" },
                new DimeTabelaMunicipiosModel() { Codigo = "81574", NomeMunicipio = "IT�" },
                new DimeTabelaMunicipiosModel() { Codigo = "81590", NomeMunicipio = "ITAI�POLIS" },
                new DimeTabelaMunicipiosModel() { Codigo = "81612", NomeMunicipio = "ITAJA�" },
                new DimeTabelaMunicipiosModel() { Codigo = "81639", NomeMunicipio = "ITAPEMA" },
                new DimeTabelaMunicipiosModel() { Codigo = "81655", NomeMunicipio = "ITAPIRANGA" },
                new DimeTabelaMunicipiosModel() { Codigo = "81671", NomeMunicipio = "ITUPORANGA" },
                new DimeTabelaMunicipiosModel() { Codigo = "81698", NomeMunicipio = "JABOR�" },
                new DimeTabelaMunicipiosModel() { Codigo = "81710", NomeMunicipio = "JACINTO MACHADO" },
                new DimeTabelaMunicipiosModel() { Codigo = "81736", NomeMunicipio = "JAGUARUNA" },
                new DimeTabelaMunicipiosModel() { Codigo = "81752", NomeMunicipio = "JARAGU� DO SUL" },
                new DimeTabelaMunicipiosModel() { Codigo = "81779", NomeMunicipio = "JOA�ABA" },
                new DimeTabelaMunicipiosModel() { Codigo = "81795", NomeMunicipio = "JOINVILLE" },
                new DimeTabelaMunicipiosModel() { Codigo = "81817", NomeMunicipio = "LACERD�POLIS" },
                new DimeTabelaMunicipiosModel() { Codigo = "81833", NomeMunicipio = "LAGES" },
                new DimeTabelaMunicipiosModel() { Codigo = "81850", NomeMunicipio = "LAGUNA" },
                new DimeTabelaMunicipiosModel() { Codigo = "81876", NomeMunicipio = "LAURENTINO" },
                new DimeTabelaMunicipiosModel() { Codigo = "81892", NomeMunicipio = "LAURO MULLER" },
                new DimeTabelaMunicipiosModel() { Codigo = "81914", NomeMunicipio = "LEBON R�GIS" },
                new DimeTabelaMunicipiosModel() { Codigo = "81930", NomeMunicipio = "LEOBERTO LEAL" },
                new DimeTabelaMunicipiosModel() { Codigo = "81957", NomeMunicipio = "LONTRAS" },
                new DimeTabelaMunicipiosModel() { Codigo = "81973", NomeMunicipio = "LUIZ ALVES" },
                new DimeTabelaMunicipiosModel() { Codigo = "81990", NomeMunicipio = "MAFRA" },
                new DimeTabelaMunicipiosModel() { Codigo = "82015", NomeMunicipio = "MAJOR GERCINO" },
                new DimeTabelaMunicipiosModel() { Codigo = "82031", NomeMunicipio = "MAJOR VIEIRA" },
                new DimeTabelaMunicipiosModel() { Codigo = "82058", NomeMunicipio = "MARAVILHA" },
                new DimeTabelaMunicipiosModel() { Codigo = "82074", NomeMunicipio = "MASSARANDUBA" },
                new DimeTabelaMunicipiosModel() { Codigo = "82090", NomeMunicipio = "MATOS COSTA" },
                new DimeTabelaMunicipiosModel() { Codigo = "82112", NomeMunicipio = "MELEIRO" },
                new DimeTabelaMunicipiosModel() { Codigo = "82139", NomeMunicipio = "MODELO" },
                new DimeTabelaMunicipiosModel() { Codigo = "82155", NomeMunicipio = "MONDA�" },
                new DimeTabelaMunicipiosModel() { Codigo = "82171", NomeMunicipio = "MONTE CASTELO" },
                new DimeTabelaMunicipiosModel() { Codigo = "82198", NomeMunicipio = "MORRO DA FUMA�A" },
                new DimeTabelaMunicipiosModel() { Codigo = "82210", NomeMunicipio = "NAVEGANTES" },
                new DimeTabelaMunicipiosModel() { Codigo = "82236", NomeMunicipio = "NOVA ERECHIM" },
                new DimeTabelaMunicipiosModel() { Codigo = "82252", NomeMunicipio = "NOVA TRENTO" },
                new DimeTabelaMunicipiosModel() { Codigo = "82279", NomeMunicipio = "NOVA VENEZA" },
                new DimeTabelaMunicipiosModel() { Codigo = "82295", NomeMunicipio = "ORLEANS" },
                new DimeTabelaMunicipiosModel() { Codigo = "82317", NomeMunicipio = "OURO" },
                new DimeTabelaMunicipiosModel() { Codigo = "82333", NomeMunicipio = "PALHO�A" },
                new DimeTabelaMunicipiosModel() { Codigo = "82350", NomeMunicipio = "PALMA SOLA" },
                new DimeTabelaMunicipiosModel() { Codigo = "82376", NomeMunicipio = "PALMITOS" },
                new DimeTabelaMunicipiosModel() { Codigo = "82392", NomeMunicipio = "PAPANDUVA" },
                new DimeTabelaMunicipiosModel() { Codigo = "82414", NomeMunicipio = "PAULO LOPES" },
                new DimeTabelaMunicipiosModel() { Codigo = "82430", NomeMunicipio = "PEDRAS GRANDES" },
                new DimeTabelaMunicipiosModel() { Codigo = "82457", NomeMunicipio = "PENHA" },
                new DimeTabelaMunicipiosModel() { Codigo = "82473", NomeMunicipio = "PERITIBA" },
                new DimeTabelaMunicipiosModel() { Codigo = "82490", NomeMunicipio = "PETROL�NDIA" },
                new DimeTabelaMunicipiosModel() { Codigo = "82511", NomeMunicipio = "BALNE�RIO PI�ARRAS" },
                new DimeTabelaMunicipiosModel() { Codigo = "82538", NomeMunicipio = "PINHALZINHO" },
                new DimeTabelaMunicipiosModel() { Codigo = "82554", NomeMunicipio = "PINHEIRO PRETO" },
                new DimeTabelaMunicipiosModel() { Codigo = "82570", NomeMunicipio = "PIRATUBA" },
                new DimeTabelaMunicipiosModel() { Codigo = "82597", NomeMunicipio = "POMERODE" },
                new DimeTabelaMunicipiosModel() { Codigo = "82619", NomeMunicipio = "PONTE ALTA" },
                new DimeTabelaMunicipiosModel() { Codigo = "82635", NomeMunicipio = "PONTE SERRADA" },
                new DimeTabelaMunicipiosModel() { Codigo = "82651", NomeMunicipio = "PORTO BELO" },
                new DimeTabelaMunicipiosModel() { Codigo = "82678", NomeMunicipio = "PORTO UNI�O" },
                new DimeTabelaMunicipiosModel() { Codigo = "82694", NomeMunicipio = "POUSO REDONDO" },
                new DimeTabelaMunicipiosModel() { Codigo = "82716", NomeMunicipio = "PRAIA GRANDE" },
                new DimeTabelaMunicipiosModel() { Codigo = "82732", NomeMunicipio = "PRESIDENTE CASTELO BRANCO" },
                new DimeTabelaMunicipiosModel() { Codigo = "82759", NomeMunicipio = "PRESIDENTE GET�LIO" },
                new DimeTabelaMunicipiosModel() { Codigo = "82775", NomeMunicipio = "PRESIDENTE NEREU" },
                new DimeTabelaMunicipiosModel() { Codigo = "82791", NomeMunicipio = "QUILOMBO" },
                new DimeTabelaMunicipiosModel() { Codigo = "82813", NomeMunicipio = "RANCHO QUEIMADO" },
                new DimeTabelaMunicipiosModel() { Codigo = "82830", NomeMunicipio = "RIO DAS ANTAS" },
                new DimeTabelaMunicipiosModel() { Codigo = "82856", NomeMunicipio = "RIO DO CAMPO" },
                new DimeTabelaMunicipiosModel() { Codigo = "82872", NomeMunicipio = "RIO DO OESTE" },
                new DimeTabelaMunicipiosModel() { Codigo = "82899", NomeMunicipio = "RIO DOS CEDROS" },
                new DimeTabelaMunicipiosModel() { Codigo = "82910", NomeMunicipio = "RIO DO SUL" },
                new DimeTabelaMunicipiosModel() { Codigo = "82937", NomeMunicipio = "RIO FORTUNA" },
                new DimeTabelaMunicipiosModel() { Codigo = "82953", NomeMunicipio = "RIO NEGRINHO" },
                new DimeTabelaMunicipiosModel() { Codigo = "82970", NomeMunicipio = "RODEIO" },
                new DimeTabelaMunicipiosModel() { Codigo = "82996", NomeMunicipio = "ROMEL�NDIA" },
                new DimeTabelaMunicipiosModel() { Codigo = "83011", NomeMunicipio = "SALETE" },
                new DimeTabelaMunicipiosModel() { Codigo = "83038", NomeMunicipio = "SALTO VELOSO" },
                new DimeTabelaMunicipiosModel() { Codigo = "83054", NomeMunicipio = "SANTA CEC�LIA" },
                new DimeTabelaMunicipiosModel() { Codigo = "83070", NomeMunicipio = "SANTA ROSA DE LIMA" },
                new DimeTabelaMunicipiosModel() { Codigo = "83097", NomeMunicipio = "SANTO AMARO DA IMPERATRIZ" },
                new DimeTabelaMunicipiosModel() { Codigo = "83119", NomeMunicipio = "S�O BENTO DO SUL" },
                new DimeTabelaMunicipiosModel() { Codigo = "83135", NomeMunicipio = "S�O BONIF�CIO" },
                new DimeTabelaMunicipiosModel() { Codigo = "83151", NomeMunicipio = "S�O CARLOS" },
                new DimeTabelaMunicipiosModel() { Codigo = "83178", NomeMunicipio = "S�O DOMINGOS" },
                new DimeTabelaMunicipiosModel() { Codigo = "83194", NomeMunicipio = "S�O FRANCISCO DO SUL" },
                new DimeTabelaMunicipiosModel() { Codigo = "83216", NomeMunicipio = "S�O JO�O BATISTA" },
                new DimeTabelaMunicipiosModel() { Codigo = "83232", NomeMunicipio = "S�O JO�O DO SUL" },
                new DimeTabelaMunicipiosModel() { Codigo = "83259", NomeMunicipio = "S�O JOAQUIM" },
                new DimeTabelaMunicipiosModel() { Codigo = "83275", NomeMunicipio = "S�O JOS�" },
                new DimeTabelaMunicipiosModel() { Codigo = "83291", NomeMunicipio = "S�O JOS� DO CEDRO" },
                new DimeTabelaMunicipiosModel() { Codigo = "83313", NomeMunicipio = "S�O JOS� DO CERRITO" },
                new DimeTabelaMunicipiosModel() { Codigo = "83330", NomeMunicipio = "S�O LOUREN�O DO OESTE" },
                new DimeTabelaMunicipiosModel() { Codigo = "83356", NomeMunicipio = "S�O LUDGERO" },
                new DimeTabelaMunicipiosModel() { Codigo = "83372", NomeMunicipio = "S�O MARTINHO" },
                new DimeTabelaMunicipiosModel() { Codigo = "83399", NomeMunicipio = "S�O MIGUEL DO OESTE" },
                new DimeTabelaMunicipiosModel() { Codigo = "83410", NomeMunicipio = "SAUDADES" },
                new DimeTabelaMunicipiosModel() { Codigo = "83437", NomeMunicipio = "SCHROEDER" },
                new DimeTabelaMunicipiosModel() { Codigo = "83453", NomeMunicipio = "SEARA" },
                new DimeTabelaMunicipiosModel() { Codigo = "83470", NomeMunicipio = "SIDER�POLIS" },
                new DimeTabelaMunicipiosModel() { Codigo = "83496", NomeMunicipio = "SOMBRIO" },
                new DimeTabelaMunicipiosModel() { Codigo = "83518", NomeMunicipio = "TAI�" },
                new DimeTabelaMunicipiosModel() { Codigo = "83534", NomeMunicipio = "TANGAR�" },
                new DimeTabelaMunicipiosModel() { Codigo = "83550", NomeMunicipio = "TIJUCAS" },
                new DimeTabelaMunicipiosModel() { Codigo = "83577", NomeMunicipio = "TIMB�" },
                new DimeTabelaMunicipiosModel() { Codigo = "83593", NomeMunicipio = "TR�S BARRAS" },
                new DimeTabelaMunicipiosModel() { Codigo = "83615", NomeMunicipio = "TREZE DE MAIO" },
                new DimeTabelaMunicipiosModel() { Codigo = "83631", NomeMunicipio = "TREZE T�LIAS" },
                new DimeTabelaMunicipiosModel() { Codigo = "83658", NomeMunicipio = "TROMBUDO CENTRAL" },
                new DimeTabelaMunicipiosModel() { Codigo = "83674", NomeMunicipio = "TUBAR�O" },
                new DimeTabelaMunicipiosModel() { Codigo = "83690", NomeMunicipio = "TURVO" },
                new DimeTabelaMunicipiosModel() { Codigo = "83712", NomeMunicipio = "URUBICI" },
                new DimeTabelaMunicipiosModel() { Codigo = "83739", NomeMunicipio = "URUSSANGA" },
                new DimeTabelaMunicipiosModel() { Codigo = "83755", NomeMunicipio = "VARGE�O" },
                new DimeTabelaMunicipiosModel() { Codigo = "83771", NomeMunicipio = "VIDAL RAMOS" },
                new DimeTabelaMunicipiosModel() { Codigo = "83798", NomeMunicipio = "VIDEIRA" },
                new DimeTabelaMunicipiosModel() { Codigo = "83810", NomeMunicipio = "WITMARSUM" },
                new DimeTabelaMunicipiosModel() { Codigo = "83836", NomeMunicipio = "XANXER�" },
                new DimeTabelaMunicipiosModel() { Codigo = "83852", NomeMunicipio = "XAVANTINA" },
                new DimeTabelaMunicipiosModel() { Codigo = "83879", NomeMunicipio = "XAXIM" },
                new DimeTabelaMunicipiosModel() { Codigo = "83895", NomeMunicipio = "BOM JARDIM DA SERRA" },
                new DimeTabelaMunicipiosModel() { Codigo = "83917", NomeMunicipio = "MARACAJ�" },
                new DimeTabelaMunicipiosModel() { Codigo = "83933", NomeMunicipio = "TIMB� DO SUL" },
                new DimeTabelaMunicipiosModel() { Codigo = "83950", NomeMunicipio = "CORREIA PINTO" },
                new DimeTabelaMunicipiosModel() { Codigo = "83976", NomeMunicipio = "OTAC�LIO COSTA" },
                new DimeTabelaMunicipiosModel() { Codigo = "99392", NomeMunicipio = "ABDON BATISTA" },
                new DimeTabelaMunicipiosModel() { Codigo = "99414", NomeMunicipio = "API�NA" },
                new DimeTabelaMunicipiosModel() { Codigo = "99430", NomeMunicipio = "CELSO RAMOS" },
                new DimeTabelaMunicipiosModel() { Codigo = "99457", NomeMunicipio = "DOUTOR PEDRINHO" },
                new DimeTabelaMunicipiosModel() { Codigo = "99511", NomeMunicipio = "IPOR� DO OESTE" },
                new DimeTabelaMunicipiosModel() { Codigo = "99538", NomeMunicipio = "IRACEMINHA" },
                new DimeTabelaMunicipiosModel() { Codigo = "99570", NomeMunicipio = "JOS� BOITEUX" },
                new DimeTabelaMunicipiosModel() { Codigo = "99619", NomeMunicipio = "LIND�IA DO SUL" },
                new DimeTabelaMunicipiosModel() { Codigo = "99635", NomeMunicipio = "MAREMA" },
                new DimeTabelaMunicipiosModel() { Codigo = "99678", NomeMunicipio = "SANTA ROSA DO SUL" },
                new DimeTabelaMunicipiosModel() { Codigo = "99716", NomeMunicipio = "TIMB� GRANDE" },
                new DimeTabelaMunicipiosModel() { Codigo = "99732", NomeMunicipio = "UNI�O DO OESTE" },
                new DimeTabelaMunicipiosModel() { Codigo = "99759", NomeMunicipio = "URUPEMA" },
                new DimeTabelaMunicipiosModel() { Codigo = "99775", NomeMunicipio = "VITOR MEIRELES" },
                new DimeTabelaMunicipiosModel() { Codigo = "99856", NomeMunicipio = "ITAPO�" },
                new DimeTabelaMunicipiosModel() { Codigo = "99899", NomeMunicipio = "SERRA ALTA" },
                new DimeTabelaMunicipiosModel() { Codigo = "99910", NomeMunicipio = "TUN�POLIS" }
                );

            #endregion

            #region TABELA QUADRO 03

            context.TabelaQuadro03.AddOrUpdate(p => p.NumeroItem,
                new DimeTabelaQuadro03Model() { NumeroItem = "010", Descricao = "Valor cont�bil", TipoItem = "1" },
                new DimeTabelaQuadro03Model() { NumeroItem = "020", Descricao = "Base de c�lculo", TipoItem = "1" },
                new DimeTabelaQuadro03Model() { NumeroItem = "030", Descricao = "Imposto creditado", TipoItem = "1" },
                new DimeTabelaQuadro03Model() { NumeroItem = "040", Descricao = "Opera��es isentas ou n�o tributadas", TipoItem = "1" },
                new DimeTabelaQuadro03Model() { NumeroItem = "050", Descricao = "Outras opera��es sem cr�dito de imposto", TipoItem = "1" },
                new DimeTabelaQuadro03Model() { NumeroItem = "053", Descricao = "Base de C�lculo Imposto Retido", TipoItem = "1" },
                new DimeTabelaQuadro03Model() { NumeroItem = "054", Descricao = "Imposto Retido", TipoItem = "1" },
                new DimeTabelaQuadro03Model() { NumeroItem = "057", Descricao = "Imposto Diferencial Al�quota", TipoItem = "1" },
                new DimeTabelaQuadro03Model() { NumeroItem = "060", Descricao = "Valor Cont�bil", TipoItem = "2" },
                new DimeTabelaQuadro03Model() { NumeroItem = "070", Descricao = "Base de C�lculo", TipoItem = "2" },
                new DimeTabelaQuadro03Model() { NumeroItem = "080", Descricao = "Imposto debitado", TipoItem = "2" },
                new DimeTabelaQuadro03Model() { NumeroItem = "090", Descricao = "Opera��es isentas ou n�o tributadas", TipoItem = "2" },
                new DimeTabelaQuadro03Model() { NumeroItem = "100", Descricao = "Outras opera��es sem d�bito de imposto", TipoItem = "2" },
                new DimeTabelaQuadro03Model() { NumeroItem = "103", Descricao = "Base de C�lculo Imposto Retido", TipoItem = "2" },
                new DimeTabelaQuadro03Model() { NumeroItem = "104", Descricao = "Imposto Retido", TipoItem = "2" }
                );

            #endregion

            #region TABELA QUADRO 04
            context.TabelaQuadro04.AddOrUpdate(p => p.NumeroItem,
                new DimeTabelaQuadro04Model() { NumeroItem = "010", Descricao = "(+) D�bito pelas sa�das", TipoItem = "1" },
                new DimeTabelaQuadro04Model() { NumeroItem = "020", Descricao = "(+) D�bito por diferencial de al�quota de ativo permanente", TipoItem = "1" },
                new DimeTabelaQuadro04Model() { NumeroItem = "030", Descricao = "(+) D�bito por diferencial de al�quota do material de uso ou consumo", TipoItem = "1" },
                new DimeTabelaQuadro04Model() { NumeroItem = "040", Descricao = "(+) D�bito de m�quinas / equipamentos importados para ativo permanente", TipoItem = "1" },
                new DimeTabelaQuadro04Model() { NumeroItem = "045", Descricao = "(+) D�bito da Diferen�a de Al�quota de Opera��o ou Presta��o a Consumidor Final de Outro Estado", TipoItem = "1" },
                new DimeTabelaQuadro04Model() { NumeroItem = "050", Descricao = "(+) Estorno de cr�dito", TipoItem = "2" },
                new DimeTabelaQuadro04Model() { NumeroItem = "060", Descricao = "(+) Outros estornos de cr�dito", TipoItem = "2" },
                new DimeTabelaQuadro04Model() { NumeroItem = "065", Descricao = "(+) Estorno de Cr�dito da Entrada em Decorr�ncia da Utiliza��o de Cr�dito Presumido", TipoItem = "2" },
                new DimeTabelaQuadro04Model() { NumeroItem = "070", Descricao = "(+) Outros d�bitos", TipoItem = "3" },
                new DimeTabelaQuadro04Model() { NumeroItem = "990", Descricao = "(=) Subtotal de D�bitos => (transportar para o item 010 do quadro 09 C�lculo do Imposto a Pagar ou Saldo Credor)", TipoItem = "3" }
                );
            #endregion

            #region TABELA QUADRO 05
            context.TabelaQuadro05.AddOrUpdate(p => p.NumeroItem,
                new DimeTabelaQuadro05Model() { NumeroItem = "010", Descricao = "(+) Saldo credor do m�s anterior", TipoItem = "1" },
                new DimeTabelaQuadro05Model() { NumeroItem = "020", Descricao = "(+) Cr�dito pelas entradas", TipoItem = "2" },
                new DimeTabelaQuadro05Model() { NumeroItem = "030", Descricao = "(+) Cr�dito de ativo permanente", TipoItem = "2" },
                new DimeTabelaQuadro05Model() { NumeroItem = "040", Descricao = "(+) Cr�dito por diferencial de al�quota material de uso / consumo", TipoItem = "2" },
                new DimeTabelaQuadro05Model() { NumeroItem = "045", Descricao = "(+) Cr�dito da Diferen�a de Al�quota de Opera��o ou Presta��o a Consumidor Final de Outro Estado", TipoItem = "2" },
                new DimeTabelaQuadro05Model() { NumeroItem = "050", Descricao = "(+) Cr�dito de ICMS retido por substitui��o tribut�ria", TipoItem = "2" },
                new DimeTabelaQuadro05Model() { NumeroItem = "990", Descricao = "(=) Subtotal de cr�ditos => (transportar para o item 050 do quadro 09 - C�lculo do Imposto a Pagar ou Saldo Credor)", TipoItem = "9" }
            );
            #endregion

            #region TABELA QUADRO 09
            context.TabelaQuadro09.AddOrUpdate(p => p.NumeroItem,
                new DimeTabelaQuadro09Model() { NumeroItem = "010", Descricao = "(+) Subtotal de d�bitos", TipoItem = "1" },
                new DimeTabelaQuadro09Model() { NumeroItem = "011", Descricao = "(+) Complemento de d�bito por mudan�a de regime de apura��o", TipoItem = "1" },
                new DimeTabelaQuadro09Model() { NumeroItem = "020", Descricao = "(+) Saldos devedores recebidos de estabelecimentos consolidados", TipoItem = "1" },
                new DimeTabelaQuadro09Model() { NumeroItem = "030", Descricao = "(+) D�bito por reserva de cr�dito acumulado", TipoItem = "1" },
                new DimeTabelaQuadro09Model() { NumeroItem = "036", Descricao = "(+) Segrega��o do Cr�dito Presumido Utilizado em Substitui��o aos Cr�ditos pelas Entradas", TipoItem = "1" },
                new DimeTabelaQuadro09Model() { NumeroItem = "037", Descricao = "(+) Segrega��o do Cr�dito Decorrente do Pagamento Antecipado do ICMS Devido na Sa�da Subsequente � Importa��o, com Utiliza��o de Cr�dito Presumido", TipoItem = "1" },
                new DimeTabelaQuadro09Model() { NumeroItem = "038", Descricao = "(+) Segrega��o de outros cr�ditos permitidos para compensar com o d�bito pela utiliza��o do cr�dito presumido", TipoItem = "1" },
                new DimeTabelaQuadro09Model() { NumeroItem = "040", Descricao = "(=) Total de d�bitos", TipoItem = "1" },
                new DimeTabelaQuadro09Model() { NumeroItem = "050", Descricao = "(+) Subtotal de cr�ditos", TipoItem = "2" },
                new DimeTabelaQuadro09Model() { NumeroItem = "060", Descricao = "(+) Saldos credores recebidos de estabelecimentos consolidados", TipoItem = "2" },
                new DimeTabelaQuadro09Model() { NumeroItem = "070", Descricao = "(+) Cr�ditos recebidos por transfer�ncia de outros contribuintes", TipoItem = "2" },
                new DimeTabelaQuadro09Model() { NumeroItem = "075", Descricao = "(+) Cr�ditos declarados no DCIP", TipoItem = "2" },
                new DimeTabelaQuadro09Model() { NumeroItem = "076", Descricao = "(+) Segrega��o dos D�bitos Relativos �s Sa�das com Cr�dito Presumido em Substitui��o aos Cr�ditos pelas Entradas", TipoItem = "2" },
                new DimeTabelaQuadro09Model() { NumeroItem = "080", Descricao = "(=) Total de cr�ditos", TipoItem = "2 " },
                new DimeTabelaQuadro09Model() { NumeroItem = "090", Descricao = "(+) Imposto do 1� dec�ndio", TipoItem = "3" },
                new DimeTabelaQuadro09Model() { NumeroItem = "100", Descricao = "(+) Imposto do 2� dec�ndio", TipoItem = "3" },
                new DimeTabelaQuadro09Model() { NumeroItem = "105", Descricao = "(+) Antecipa��es Combust�veis l�quidos e gasosos", TipoItem = "3" },
                new DimeTabelaQuadro09Model() { NumeroItem = "110", Descricao = "(=) Total de ajustes da apura��o decendial e antecipa��es", TipoItem = "3" },
                new DimeTabelaQuadro09Model() { NumeroItem = "120", Descricao = "(=) Saldo devedor (Total de D�bitos - (Total de Cr�ditos + Total de ajustes da apura��o decendial e antecipa��es))", TipoItem = "4" },
                new DimeTabelaQuadro09Model() { NumeroItem = "130", Descricao = "(-) Saldo devedor transferido ao estabelecimento consolidador", TipoItem = "4" },
                new DimeTabelaQuadro09Model() { NumeroItem = "140", Descricao = "((=) Saldo Credor (Total de Cr�ditos + Total de ajustes da apura��o decendial e antecipa��es) - (Total de D�bitos))", TipoItem = "5" },
                new DimeTabelaQuadro09Model() { NumeroItem = "150", Descricao = "(-) Saldo credor transferido ao estabelecimento consolidador", TipoItem = "5" },
                new DimeTabelaQuadro09Model() { NumeroItem = "160", Descricao = "Saldo credor transfer�vel relativo � exporta��o", TipoItem = "6" },
                new DimeTabelaQuadro09Model() { NumeroItem = "170", Descricao = "Saldo credor transfer�vel relativo a sa�das isentas", TipoItem = "6" },
                new DimeTabelaQuadro09Model() { NumeroItem = "180", Descricao = "Saldo credor transfer�vel relativo a sa�das diferidas", TipoItem = "6" },
                new DimeTabelaQuadro09Model() { NumeroItem = "190", Descricao = "Saldo credor relativo a outros cr�ditos", TipoItem = "6" },
                new DimeTabelaQuadro09Model() { NumeroItem = "998", Descricao = "(=) Saldo Credor para o m�s seguinte", TipoItem = "5" },
                new DimeTabelaQuadro09Model() { NumeroItem = "999", Descricao = "(=) Imposto a recolher", TipoItem = "4" }
            );
            #endregion

            #region TABELA QUADRO 11
            context.TabelaQuadro11.AddOrUpdate(p => p.NumeroItem,
                new DimeTabelaQuadro11Model() { NumeroItem = "010", Descricao = "Valor dos produtos (n�o preencher a partir do per�odo de refer�ncia agosto de 2013)", TipoItem = "0" },
                new DimeTabelaQuadro11Model() { NumeroItem = "020", Descricao = "Valor do IPI (n�o preencher a partir do per�odo de refer�ncia agosto de 2013)", TipoItem = "0" },
                new DimeTabelaQuadro11Model() { NumeroItem = "030", Descricao = "Despesas acess�rias (n�o preencher a partir do per�odo de refer�ncia agosto de 2013)", TipoItem = "0" },
                new DimeTabelaQuadro11Model() { NumeroItem = "040", Descricao = "Base de c�lculo do ICMS pr�prio (n�o preencher a partir do per�odo de refer�ncia agosto de 2013)", TipoItem = "0" },
                new DimeTabelaQuadro11Model() { NumeroItem = "050", Descricao = "ICMS pr�prio (n�o preencher a partir do per�odo de refer�ncia agosto de 2013)", TipoItem = "0" },
                new DimeTabelaQuadro11Model() { NumeroItem = "060", Descricao = "Base c�lculo do imposto retido", TipoItem = "0" },
                new DimeTabelaQuadro11Model() { NumeroItem = "065", Descricao = "Imposto Retido apurado por mercadoria e recolhido por opera��o", TipoItem = "0" },
                new DimeTabelaQuadro11Model() { NumeroItem = "070", Descricao = "(+) Imposto retido com apura��o mensal", TipoItem = "1" },
                new DimeTabelaQuadro11Model() { NumeroItem = "073", Descricao = "(+) Imposto Retido pelo AEHC com regime especial de apura��o mensal", TipoItem = "1" },
                new DimeTabelaQuadro11Model() { NumeroItem = "075", Descricao = "(+) Saldos devedores recebidos de estabelecimentos consolidados", TipoItem = "1" },
                new DimeTabelaQuadro11Model() { NumeroItem = "080", Descricao = "Total de d�bitos", TipoItem = "1" },
                new DimeTabelaQuadro11Model() { NumeroItem = "090", Descricao = "(+) Saldo credor do per�odo anterior sobre a substitui��o tribut�ria", TipoItem = "2" },
                new DimeTabelaQuadro11Model() { NumeroItem = "100", Descricao = "(+) Devolu��o de mercadorias e desfazimento de venda (n�o preencher a partir do per�odo de refer�ncia agosto de 2013)", TipoItem = "2" },
                new DimeTabelaQuadro11Model() { NumeroItem = "105", Descricao = "(+) Cr�ditos declarados no DCIP", TipoItem = "2" },
                new DimeTabelaQuadro11Model() { NumeroItem = "110", Descricao = "(+) Ressarcimento de ICMS substitui��o tribut�ria (n�o preencher a partir do per�odo de refer�ncia agosto de 2013)", TipoItem = "2" },
                new DimeTabelaQuadro11Model() { NumeroItem = "120", Descricao = "(+) Outros cr�ditos (n�o preencher a partir do per�odo de refer�ncia agosto de 2013)", TipoItem = "2" },
                new DimeTabelaQuadro11Model() { NumeroItem = "125", Descricao = "(+) Saldos credores recebidos de estabelecimentos consolidados", TipoItem = "2" },
                new DimeTabelaQuadro11Model() { NumeroItem = "130", Descricao = "(=) Total de cr�ditos", TipoItem = "2" },
                new DimeTabelaQuadro11Model() { NumeroItem = "140", Descricao = "N�o se aplica", TipoItem = "3" },
                new DimeTabelaQuadro11Model() { NumeroItem = "150", Descricao = "N�o se aplica", TipoItem = "3" },
                new DimeTabelaQuadro11Model() { NumeroItem = "155", Descricao = "(+) Antecipa��es Combust�veis l�quidos e gasosos", TipoItem = "3" },
                new DimeTabelaQuadro11Model() { NumeroItem = "160", Descricao = "(=) Total de ajustes das Antecipa��es Combust�veis", TipoItem = "3" },
                new DimeTabelaQuadro11Model() { NumeroItem = "170", Descricao = "(=) Saldo devedor (Total de D�bitos - (Total de Cr�ditos + Total de ajustes das antecipa��es combust�veis))", TipoItem = "4" },
                new DimeTabelaQuadro11Model() { NumeroItem = "180", Descricao = "(-) Saldo devedor transferido ao estabelecimento consolidador", TipoItem = "4" },
                new DimeTabelaQuadro11Model() { NumeroItem = "190", Descricao = "((=) Saldo Credor (Total de Cr�ditos + Total de ajustes das antecipa��es combust�veis) - (Total de D�bitos))", TipoItem = "5" },
                new DimeTabelaQuadro11Model() { NumeroItem = "200", Descricao = "(-) Saldo credor transferido ao estabelecimento consolidador", TipoItem = "5" },
                new DimeTabelaQuadro11Model() { NumeroItem = "998", Descricao = "(=) Saldo Credor para o m�s seguinte", TipoItem = "5" },
                new DimeTabelaQuadro11Model() { NumeroItem = "999", Descricao = "(=) Imposto a recolher sobre a substitui��o tribut�ria", TipoItem = "4" }
            );
            #endregion

            #region TABELA QUADRO 14
            context.TabelaQuadro14.AddOrUpdate(p => p.NumeroItem,
                new DimeTabelaQuadro14Model() { NumeroItem = "010", Descricao = "Valor da Base de C�lculo das Sa�das com Cr�dito Presumido", TipoItem = "1" },
                new DimeTabelaQuadro14Model() { NumeroItem = "020", Descricao = "(+) D�bitos Relativos �s Sa�das com Cr�dito Presumido em Substitui��o aos Cr�ditos pelas Entradas", TipoItem = "1" },
                new DimeTabelaQuadro14Model() { NumeroItem = "030", Descricao = "(-) Cr�dito Presumido Utilizado em Substitui��o aos Cr�ditos pelas Entradas", TipoItem = "1" },
                new DimeTabelaQuadro14Model() { NumeroItem = "031", Descricao = "(-) Cr�ditos Permitidos para Compensar com o D�bito pela Utiliza��o do Cr�dito Presumido (Reda��o dada pelo Art. 4� da Portaria SEF n� 186/17 - Efeitos fatos geradores a partir da refer�ncia abril/2017)", TipoItem = "1" },
                new DimeTabelaQuadro14Model() { NumeroItem = "040", Descricao = "(=) Saldo Devedor Apurado pela Apropria��o do Cr�dito Presumido no M�s", TipoItem = "1" },
                new DimeTabelaQuadro14Model() { NumeroItem = "045", Descricao = "(=) D�bito pela utiliza��o do cr�dito presumido recebido de estabelecimento consolidado (Reda��o dada pelo Art. 1� da Portaria SEF n� 230/17 - Efeitos a partir da refer�ncia agosto/2017)", TipoItem = "1" },
                new DimeTabelaQuadro14Model() { NumeroItem = "050", Descricao = "(=) D�bito Apurado pela Apropria��o Extempor�nea do Cr�dito Presumido", TipoItem = "1" },
                new DimeTabelaQuadro14Model() { NumeroItem = "110", Descricao = "(+) Saldo Credor das Antecipa��es para o M�s Seguinte Apurado no M�s Anterior", TipoItem = "2" },
                new DimeTabelaQuadro14Model() { NumeroItem = "120", Descricao = "(+) Cr�dito decorrente do Pagamento Antecipado do ICMS Devido na Sa�da Subsequente � Importa��o, com Utiliza��o de Cr�dito Presumido", TipoItem = "2" },
                new DimeTabelaQuadro14Model() { NumeroItem = "130", Descricao = "(=) Total das Antecipa��es", TipoItem = "2" },
                new DimeTabelaQuadro14Model() { NumeroItem = "150", Descricao = "(-) D�bito pela utiliza��o do cr�dito presumido transferido ao estabelecimento consolidador consolidado (Reda��o dada pelo Art. 1� da Portaria SEF n� 230/17 - Efeitos a partir da refer�ncia agosto/2017)", TipoItem = "2" },
                new DimeTabelaQuadro14Model() { NumeroItem = "199", Descricao = "(=) Imposto a Recolher pela Utiliza��o do Cr�dito Presumido", TipoItem = "3" },
                new DimeTabelaQuadro14Model() { NumeroItem = "198", Descricao = "(=) Saldo Credor das Antecipa��es para o M�s Seguinte", TipoItem = "3" }
            );
            #endregion

            #region TABELA QUADRO 80
            context.TabelaQuadro80.AddOrUpdate(p => p.NumeroItem,
                new DimeTabelaQuadro80Model() { NumeroItem = "010", Descricao = "(+) Estoque no in�cio do exerc�cio", TipoItem = "1" },
                new DimeTabelaQuadro80Model() { NumeroItem = "020", Descricao = "(+) Estoque no fim do exerc�cio", TipoItem = "1" },
                new DimeTabelaQuadro80Model() { NumeroItem = "030", Descricao = "(+) Receita bruta de vendas e servi�os", TipoItem = "1" }
            );
            #endregion

            #region TABELA QUADRO 81
            context.TabelaQuadro81.AddOrUpdate(p => p.NumeroItem,
            new DimeTabelaQuadro81Model() { NumeroItem = "110", Descricao = "(=) Circulante", TipoItem = "1" },
            new DimeTabelaQuadro81Model() { NumeroItem = "111", Descricao = "(+) Disponibilidades", TipoItem = "1" },
            new DimeTabelaQuadro81Model() { NumeroItem = "113", Descricao = "(+) Contas a receber do circulante", TipoItem = "1" },
            new DimeTabelaQuadro81Model() { NumeroItem = "121", Descricao = "(+) Estoque de mercadorias e mat�ria-prima", TipoItem = "1" },
            new DimeTabelaQuadro81Model() { NumeroItem = "123", Descricao = "(+) Outros estoques", TipoItem = "1" },
            new DimeTabelaQuadro81Model() { NumeroItem = "128", Descricao = "(+) Outras contas do ativo circulante", TipoItem = "1" },
            new DimeTabelaQuadro81Model() { NumeroItem = "130", Descricao = "(=) Realiz�vel a longo prazo", TipoItem = "1" },
            new DimeTabelaQuadro81Model() { NumeroItem = "131", Descricao = "(+) Contas a receber do realiz�vel", TipoItem = "1" },
            new DimeTabelaQuadro81Model() { NumeroItem = "148", Descricao = "(+) Outras contas do realiz�vel", TipoItem = "1" },
            new DimeTabelaQuadro81Model() { NumeroItem = "150", Descricao = "(=) Permanente", TipoItem = "1" },
            new DimeTabelaQuadro81Model() { NumeroItem = "151", Descricao = "(+) Investimentos", TipoItem = "1" },
            new DimeTabelaQuadro81Model() { NumeroItem = "155", Descricao = "(+) Imobilizado (l�quido)", TipoItem = "1" },
            new DimeTabelaQuadro81Model() { NumeroItem = "157", Descricao = "(+) Diferido (l�quido)", TipoItem = "1" },
            new DimeTabelaQuadro81Model() { NumeroItem = "159", Descricao = "(+) Intang�vel", TipoItem = "1" },
            new DimeTabelaQuadro81Model() { NumeroItem = "199", Descricao = "(=) Total geral do ativo", TipoItem = "1" }
            );
            #endregion

            #region TABELA QUADRO 82
            context.TabelaQuadro82.AddOrUpdate(p => p.NumeroItem,
                new DimeTabelaQuadro82Model() { NumeroItem = "210", Descricao = "(=) Circulante", TipoItem = "1" },
                new DimeTabelaQuadro82Model() { NumeroItem = "211", Descricao = "(+) Fornecedores", TipoItem = "1" },
                new DimeTabelaQuadro82Model() { NumeroItem = "213", Descricao = "(+) Empr�stimos e financiamentos", TipoItem = "1" },
                new DimeTabelaQuadro82Model() { NumeroItem = "215", Descricao = "(+) Outras contas do passivo circulante", TipoItem = "1" },
                new DimeTabelaQuadro82Model() { NumeroItem = "230", Descricao = "(=) Exig�vel a longo prazo", TipoItem = "1" },
                new DimeTabelaQuadro82Model() { NumeroItem = "240", Descricao = "(=) Resultado de exerc�cios futuros", TipoItem = "1" },
                new DimeTabelaQuadro82Model() { NumeroItem = "269", Descricao = "(=) Passivo a Descoberto", TipoItem = "1" },
                new DimeTabelaQuadro82Model() { NumeroItem = "270", Descricao = "(=) Patrim�nio l�quido", TipoItem = "1" },
                new DimeTabelaQuadro82Model() { NumeroItem = "271", Descricao = "(+) capital Social", TipoItem = "1" },
                new DimeTabelaQuadro82Model() { NumeroItem = "278", Descricao = "(+) Outras contas do patrim�nio l�quido", TipoItem = "1" },
                new DimeTabelaQuadro82Model() { NumeroItem = "279", Descricao = "(-) Outras contas do patrim�nio l�quido (de valor)", TipoItem = "1" },
                new DimeTabelaQuadro82Model() { NumeroItem = "299", Descricao = "(=) Total geral do passivo", TipoItem = "1" }
            );
            #endregion

            #region TABELA QUADRO 83
            context.TabelaQuadro83.AddOrUpdate(p => p.NumeroItem,
                new DimeTabelaQuadro83Model() { NumeroItem = "310", Descricao = "(+) Receita bruta de vendas/servi�os", TipoItem = "1" },
                new DimeTabelaQuadro83Model() { NumeroItem = "311", Descricao = "(-) Dedu��es da receita bruta", TipoItem = "1" },
                new DimeTabelaQuadro83Model() { NumeroItem = "320", Descricao = "(=) Receita l�quida vendas/servi�os", TipoItem = "1" },
                new DimeTabelaQuadro83Model() { NumeroItem = "323", Descricao = "(-) Custo da mercadoria ou produtos vendida(os) ou dos servi�os prestados", TipoItem = "1" },
                new DimeTabelaQuadro83Model() { NumeroItem = "330", Descricao = "(=) Lucro bruto", TipoItem = "1" },
                new DimeTabelaQuadro83Model() { NumeroItem = "331", Descricao = "(=) Preju�zo bruto", TipoItem = "1" },
                new DimeTabelaQuadro83Model() { NumeroItem = "333", Descricao = "(+) Outras receita operacionais", TipoItem = "1" },
                new DimeTabelaQuadro83Model() { NumeroItem = "335", Descricao = "(-) Despezas operacionais", TipoItem = "1" },
                new DimeTabelaQuadro83Model() { NumeroItem = "340", Descricao = "(=) Lucro operacional", TipoItem = "1" },
                new DimeTabelaQuadro83Model() { NumeroItem = "341", Descricao = "(=) Preju�zo operacional", TipoItem = "1" },
                new DimeTabelaQuadro83Model() { NumeroItem = "343", Descricao = "(+) Receitas n�o operacionais", TipoItem = "1" },
                new DimeTabelaQuadro83Model() { NumeroItem = "345", Descricao = "(-) Despesas n�o operacionais", TipoItem = "1" },
                new DimeTabelaQuadro83Model() { NumeroItem = "350", Descricao = "(=) Resultado antes do I.R. e da contribui��o social", TipoItem = "1" },
                new DimeTabelaQuadro83Model() { NumeroItem = "351", Descricao = "(=) Resultado negativo antes do I.R. e da contribui��o social", TipoItem = "1" },
                new DimeTabelaQuadro83Model() { NumeroItem = "353", Descricao = "(-) Provis�o para o I.R. e para a contribui��o social", TipoItem = "1" },
                new DimeTabelaQuadro83Model() { NumeroItem = "354", Descricao = "(+) Provis�o para o I.R. e para contribui��o social (Acrescentando Art. 4� da Port. 274/15)", TipoItem = "1" },
                new DimeTabelaQuadro83Model() { NumeroItem = "360", Descricao = "(=) Resultado ap�s I.R. e a contribui��o social", TipoItem = "1" },
                new DimeTabelaQuadro83Model() { NumeroItem = "361", Descricao = "(=) Resultado negativo ap�s o I.R e a contribui��o social", TipoItem = "1" },
                new DimeTabelaQuadro83Model() { NumeroItem = "363", Descricao = "(-) Participa��es e contribui��es", TipoItem = "1" },
                new DimeTabelaQuadro83Model() { NumeroItem = "398", Descricao = "(=) Preju�zo do exerc�cio", TipoItem = "1" },
                new DimeTabelaQuadro83Model() { NumeroItem = "399", Descricao = "(=) Lucro do exerc�cio", TipoItem = "1" }
            );
            #endregion

            #region TABELA QUADRO 84
            context.TabelaQuadro84.AddOrUpdate(p => p.NumeroItem,
                new DimeTabelaQuadro84Model() { NumeroItem = "411", Descricao = "(=) Pr�-labore", TipoItem = "1" },
                new DimeTabelaQuadro84Model() { NumeroItem = "412", Descricao = "(+) Comiss�es, sal�rios, ordenados", TipoItem = "1" },
                new DimeTabelaQuadro84Model() { NumeroItem = "421", Descricao = "(+) Encargos sociais", TipoItem = "1" },
                new DimeTabelaQuadro84Model() { NumeroItem = "422", Descricao = "(+) Tributos federais", TipoItem = "1" },
                new DimeTabelaQuadro84Model() { NumeroItem = "423", Descricao = "(+) Tributos estaduais", TipoItem = "1" },
                new DimeTabelaQuadro84Model() { NumeroItem = "424", Descricao = "(+) tributos municipais", TipoItem = "1" },
                new DimeTabelaQuadro84Model() { NumeroItem = "431", Descricao = "(+) Combust�veis e lubrificantes", TipoItem = "1" },
                new DimeTabelaQuadro84Model() { NumeroItem = "432", Descricao = "(+) �gua e telefone", TipoItem = "1" },
                new DimeTabelaQuadro84Model() { NumeroItem = "433", Descricao = "(+) Energia el�trica", TipoItem = "1" },
                new DimeTabelaQuadro84Model() { NumeroItem = "441", Descricao = "(+) Alugu�is", TipoItem = "1" },
                new DimeTabelaQuadro84Model() { NumeroItem = "442", Descricao = "(+) Seguros", TipoItem = "1" },
                new DimeTabelaQuadro84Model() { NumeroItem = "443", Descricao = "(+) Fretes e carretos", TipoItem = "1" },
                new DimeTabelaQuadro84Model() { NumeroItem = "451", Descricao = "(+) Servi�os profissionais", TipoItem = "1" },
                new DimeTabelaQuadro84Model() { NumeroItem = "461", Descricao = "(+) Despesas financeiras", TipoItem = "1" },
                new DimeTabelaQuadro84Model() { NumeroItem = "498", Descricao = "(+) Outras despesas", TipoItem = "1" },
                new DimeTabelaQuadro84Model() { NumeroItem = "499", Descricao = "(=) Total", TipoItem = "1" }
            );
            #endregion

            #region TABELA QUADRO 90
            context.TabelaQuadro90.AddOrUpdate(p => p.NumeroItem,
                new DimeTabelaQuadro90Model() { NumeroItem = "010", Descricao = "(+) Estoque no in�cio do exerc�cio", TipoItem = "1" },
                new DimeTabelaQuadro90Model() { NumeroItem = "020", Descricao = "(+) Estoque no fim do exerc�cio", TipoItem = "1" },
                new DimeTabelaQuadro90Model() { NumeroItem = "030", Descricao = "(+) Receita bruta de vendas e servi�os", TipoItem = "1" }
            );
            #endregion

            #region TABELA QUADRO 91
            context.TabelaQuadro91.AddOrUpdate(p => p.NumeroItem,
                new DimeTabelaQuadro91Model() { NumeroItem = "110", Descricao = "(=) Circulante", TipoItem = "1" },
                new DimeTabelaQuadro91Model() { NumeroItem = "111", Descricao = "(+) Disponibilidades", TipoItem = "1" },
                new DimeTabelaQuadro91Model() { NumeroItem = "113", Descricao = "(+) Contas a receber do circulante", TipoItem = "1" },
                new DimeTabelaQuadro91Model() { NumeroItem = "121", Descricao = "(+) Estoque de mercadorias e mat�ria-prima", TipoItem = "1" },
                new DimeTabelaQuadro91Model() { NumeroItem = "123", Descricao = "(+) Outros estoques", TipoItem = "1" },
                new DimeTabelaQuadro91Model() { NumeroItem = "128", Descricao = "(+) Outras contas do ativo circulante", TipoItem = "1" },
                new DimeTabelaQuadro91Model() { NumeroItem = "130", Descricao = "(=) Realiz�vel a longo prazo", TipoItem = "1" },
                new DimeTabelaQuadro91Model() { NumeroItem = "131", Descricao = "(+) Contas a receber do realiz�vel", TipoItem = "1" },
                new DimeTabelaQuadro91Model() { NumeroItem = "148", Descricao = "(+) Outras contas do realiz�vel", TipoItem = "1" },
                new DimeTabelaQuadro91Model() { NumeroItem = "150", Descricao = "(=) Permanente", TipoItem = "1" },
                new DimeTabelaQuadro91Model() { NumeroItem = "151", Descricao = "(+) Investimentos", TipoItem = "1" },
                new DimeTabelaQuadro91Model() { NumeroItem = "155", Descricao = "(+) Imobilizado (l�quido)", TipoItem = "1" },
                new DimeTabelaQuadro91Model() { NumeroItem = "157", Descricao = "(+) Diferido (l�quido)", TipoItem = "1" },
                new DimeTabelaQuadro91Model() { NumeroItem = "159", Descricao = "(+) Intang�vel", TipoItem = "1" },
                new DimeTabelaQuadro91Model() { NumeroItem = "199", Descricao = "(=) Total geral do ativo", TipoItem = "1" }
            );
            #endregion

            #region TABELA QUADRO 92
            context.TabelaQuadro92.AddOrUpdate(p => p.NumeroItem,
                new DimeTabelaQuadro92Model() { NumeroItem = "210", Descricao = "(=) Circulante", TipoItem = "1" },
                new DimeTabelaQuadro92Model() { NumeroItem = "211", Descricao = "(+) Fornecedores", TipoItem = "1" },
                new DimeTabelaQuadro92Model() { NumeroItem = "213", Descricao = "(+) Empr�stimos e financiamentos", TipoItem = "1" },
                new DimeTabelaQuadro92Model() { NumeroItem = "215", Descricao = "(+) Outras contas do passivo circulante", TipoItem = "1" },
                new DimeTabelaQuadro92Model() { NumeroItem = "230", Descricao = "(=) Exig�vel a longo prazo", TipoItem = "1" },
                new DimeTabelaQuadro92Model() { NumeroItem = "240", Descricao = "(=) Resultado de exerc�cios futuros", TipoItem = "1" },
                new DimeTabelaQuadro92Model() { NumeroItem = "269", Descricao = "(=) Passivo a Descoberto", TipoItem = "1" },
                new DimeTabelaQuadro92Model() { NumeroItem = "270", Descricao = "(=) Patrim�nio l�quido", TipoItem = "1" },
                new DimeTabelaQuadro92Model() { NumeroItem = "271", Descricao = "(+) capital Social", TipoItem = "1" },
                new DimeTabelaQuadro92Model() { NumeroItem = "278", Descricao = "(+) Outras contas do patrim�nio l�quido", TipoItem = "1" },
                new DimeTabelaQuadro92Model() { NumeroItem = "279", Descricao = "(-) Outras contas do patrim�nio l�quido (de valor)", TipoItem = "1" },
                new DimeTabelaQuadro92Model() { NumeroItem = "299", Descricao = "(=) Total geral do passivo", TipoItem = "1" }
            );
            #endregion

            #region TABELA QUADRO 93
            context.TabelaQuadro93.AddOrUpdate(p => p.NumeroItem,
                new DimeTabelaQuadro93Model() { NumeroItem = "310", Descricao = "(+) Receita bruta de vendas/servi�os", TipoItem = "1" },
                new DimeTabelaQuadro93Model() { NumeroItem = "311", Descricao = "(-) Dedu��es da receita bruta", TipoItem = "1" },
                new DimeTabelaQuadro93Model() { NumeroItem = "320", Descricao = "(=) Receita l�quida vendas/servi�os", TipoItem = "1" },
                new DimeTabelaQuadro93Model() { NumeroItem = "323", Descricao = "(-) Custo da mercadoria ou produtos vendida(os) ou dos servi�os prestados", TipoItem = "1" },
                new DimeTabelaQuadro93Model() { NumeroItem = "330", Descricao = "(=) Lucro bruto", TipoItem = "1" },
                new DimeTabelaQuadro93Model() { NumeroItem = "331", Descricao = "(=) Preju�zo bruto", TipoItem = "1" },
                new DimeTabelaQuadro93Model() { NumeroItem = "333", Descricao = "(+) Outras receita operacionais", TipoItem = "1" },
                new DimeTabelaQuadro93Model() { NumeroItem = "335", Descricao = "(-) Despezas operacionais", TipoItem = "1" },
                new DimeTabelaQuadro93Model() { NumeroItem = "340", Descricao = "(=) Lucro operacional", TipoItem = "1" },
                new DimeTabelaQuadro93Model() { NumeroItem = "341", Descricao = "(=) Preju�zo operacional", TipoItem = "1" },
                new DimeTabelaQuadro93Model() { NumeroItem = "343", Descricao = "(+) Receitas n�o operacionais", TipoItem = "1" },
                new DimeTabelaQuadro93Model() { NumeroItem = "345", Descricao = "(-) Despesas n�o operacionais", TipoItem = "1" },
                new DimeTabelaQuadro93Model() { NumeroItem = "350", Descricao = "(=) Resultado antes do I.R. e da contribui��o social", TipoItem = "1" },
                new DimeTabelaQuadro93Model() { NumeroItem = "351", Descricao = "(=) Resultado negativo antes do I.R. e da contribui��o social", TipoItem = "1" },
                new DimeTabelaQuadro93Model() { NumeroItem = "353", Descricao = "(-) Provis�o para o I.R. e para a contribui��o social", TipoItem = "1" },
                new DimeTabelaQuadro93Model() { NumeroItem = "354", Descricao = "(+) Provis�o para o I.R. e para contribui��o social (Acrescentando Art. 4� da Port. 274/15)", TipoItem = "1" },
                new DimeTabelaQuadro93Model() { NumeroItem = "360", Descricao = "(=) Resultado ap�s I.R. e a contribui��o social", TipoItem = "1" },
                new DimeTabelaQuadro93Model() { NumeroItem = "361", Descricao = "(=) Resultado negativo ap�s o I.R e a contribui��o social", TipoItem = "1" },
                new DimeTabelaQuadro93Model() { NumeroItem = "363", Descricao = "(-) Participa��es e contribui��es", TipoItem = "1" },
                new DimeTabelaQuadro93Model() { NumeroItem = "398", Descricao = "(=) Preju�zo do exerc�cio", TipoItem = "1" },
                new DimeTabelaQuadro93Model() { NumeroItem = "399", Descricao = "(=) Lucro do exerc�cio", TipoItem = "1" }
            );
            #endregion

            #region TABELA QUADRO 94
            context.TabelaQuadro94.AddOrUpdate(p => p.NumeroItem,
                new DimeTabelaQuadro94Model() { NumeroItem = "411", Descricao = "(=) Pr�-labore", TipoItem = "1" },
                new DimeTabelaQuadro94Model() { NumeroItem = "412", Descricao = "(+) Comiss�es, sal�rios, ordenados", TipoItem = "1" },
                new DimeTabelaQuadro94Model() { NumeroItem = "421", Descricao = "(+) Encargos sociais", TipoItem = "1" },
                new DimeTabelaQuadro94Model() { NumeroItem = "422", Descricao = "(+) Tributos federais", TipoItem = "1" },
                new DimeTabelaQuadro94Model() { NumeroItem = "423", Descricao = "(+) Tributos estaduais", TipoItem = "1" },
                new DimeTabelaQuadro94Model() { NumeroItem = "424", Descricao = "(+) tributos municipais", TipoItem = "1" },
                new DimeTabelaQuadro94Model() { NumeroItem = "431", Descricao = "(+) Combust�veis e lubrificantes", TipoItem = "1" },
                new DimeTabelaQuadro94Model() { NumeroItem = "432", Descricao = "(+) �gua e telefone", TipoItem = "1" },
                new DimeTabelaQuadro94Model() { NumeroItem = "433", Descricao = "(+) Energia el�trica", TipoItem = "1" },
                new DimeTabelaQuadro94Model() { NumeroItem = "441", Descricao = "(+) Alugu�is", TipoItem = "1" },
                new DimeTabelaQuadro94Model() { NumeroItem = "442", Descricao = "(+) Seguros", TipoItem = "1" },
                new DimeTabelaQuadro94Model() { NumeroItem = "443", Descricao = "(+) Fretes e carretos", TipoItem = "1" },
                new DimeTabelaQuadro94Model() { NumeroItem = "451", Descricao = "(+) Servi�os profissionais", TipoItem = "1" },
                new DimeTabelaQuadro94Model() { NumeroItem = "461", Descricao = "(+) Despesas financeiras", TipoItem = "1" },
                new DimeTabelaQuadro94Model() { NumeroItem = "498", Descricao = "(+) Outras despesas", TipoItem = "1" },
                new DimeTabelaQuadro94Model() { NumeroItem = "499", Descricao = "(=) Total", TipoItem = "1" }
            );
            #endregion

            #region TIPO REGISTRO
            context.TipoRegistro.AddOrUpdate(p => p.Quadro,
                new DimeTipoRegistroModel() { Quadro = "01", TipoRegistro = 22, Descricao = "Valores Fiscais Entradas" },
                new DimeTipoRegistroModel() { Quadro = "02", TipoRegistro = 23, Descricao = "Valores Fiscais Sa�das" },
                new DimeTipoRegistroModel() { Quadro = "03", TipoRegistro = 24, Descricao = "Resumo dos Valores Fiscais" },
                new DimeTipoRegistroModel() { Quadro = "04", TipoRegistro = 25, Descricao = "Resumo da Apura��o dos D�bitos" },
                new DimeTipoRegistroModel() { Quadro = "05", TipoRegistro = 26, Descricao = "Resumo da Apura��o dos Cr�ditos" },
                new DimeTipoRegistroModel() { Quadro = "09", TipoRegistro = 30, Descricao = "C�lculo do Imposto a Pagar ou Saldo Credor" },
                new DimeTipoRegistroModel() { Quadro = "11", TipoRegistro = 32, Descricao = "Informa��es sobre Substitui��o Tribut�ria" },
                new DimeTipoRegistroModel() { Quadro = "12", TipoRegistro = 33, Descricao = "Discrimina��o dos Pagamentos do Imposto e dos D�bitos Espec�ficos" },
                new DimeTipoRegistroModel() { Quadro = "14", TipoRegistro = 35, Descricao = "Demonstrativo da Apura��o do Imposto Devido (DAICP)" },
                new DimeTipoRegistroModel() { Quadro = "46", TipoRegistro = 46, Descricao = "Cr�ditos por Regimes e Autoriza��es Especiais" },
                new DimeTipoRegistroModel() { Quadro = "48", TipoRegistro = 48, Descricao = "Informa��es para Rateio do Valor Adicionado" },
                new DimeTipoRegistroModel() { Quadro = "49", TipoRegistro = 49, Descricao = "Entradas por Unidade da Federa��o" },
                new DimeTipoRegistroModel() { Quadro = "50", TipoRegistro = 50, Descricao = "Sa�das por Unidade da Federa��o" },
                new DimeTipoRegistroModel() { Quadro = "80", TipoRegistro = 80, Descricao = "Resumo do Livro Registro de Invent�rio e Receita Bruta" },
                new DimeTipoRegistroModel() { Quadro = "81", TipoRegistro = 81, Descricao = "Ativo" },
                new DimeTipoRegistroModel() { Quadro = "82", TipoRegistro = 82, Descricao = "Passivo" },
                new DimeTipoRegistroModel() { Quadro = "83", TipoRegistro = 83, Descricao = "Demonstra��o de Resultado" },
                new DimeTipoRegistroModel() { Quadro = "84", TipoRegistro = 84, Descricao = "Detalhamento das Despesas" },
                new DimeTipoRegistroModel() { Quadro = "90", TipoRegistro = 90, Descricao = "Resumo do Livro Registro de Invent�rio - Encerramento de Atividade" },
                new DimeTipoRegistroModel() { Quadro = "91", TipoRegistro = 91, Descricao = "Ativo" },
                new DimeTipoRegistroModel() { Quadro = "92", TipoRegistro = 92, Descricao = "Passivo" },
                new DimeTipoRegistroModel() { Quadro = "93", TipoRegistro = 93, Descricao = "Demonstra��o de Resultado" },
                new DimeTipoRegistroModel() { Quadro = "94", TipoRegistro = 94, Descricao = "Detalhamento de Despesas" }
            );
            #endregion
            //
        }
    }
}
