﻿using SAPbouiCOM;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.IO;
using System.Windows.Forms;
using SPSFiscal.EFCodeFirst.Dao;
using SPSFiscal.Model;
using SPSFiscal.Model.Dime;

namespace SPSFiscal.Addon
{
    class frmfrmCadDeclarDime : IForm
    {
        public static string IdDeclaracao;
        public static string Periodo;
        private ItemEvent _eventoItem;
        public frmfrmCadDeclarDime(ItemEvent evento)
        {
            this._eventoItem = evento;
        }
        public bool ItemEvent()
        {
            try
            {
                switch (_eventoItem.EventType)
                {
                    case BoEventTypes.et_ALL_EVENTS:
                        break;
                    case BoEventTypes.et_B1I_SERVICE_COMPLETE:
                        break;
                    case BoEventTypes.et_CHOOSE_FROM_LIST:
                        break;
                    case BoEventTypes.et_CLICK:
                        break;
                    case BoEventTypes.et_COMBO_SELECT:
                        #region COMBO CONTRIBUINTE
                        if (!_eventoItem.Before_Action && _eventoItem.ItemUID == "contri")
                        {
                            SAPbouiCOM.Form oForm = Conexao.uiApplication.Forms.Item(_eventoItem.FormUID);

                            oForm.Freeze(true);

                            try
                            {
                                int id = Convert.ToInt32(oForm.DataSources.UserDataSources.Item("contri").Value);
                                foreach (var itens in frmfrmCadContribDao.CarregaUmContribuinte(id))
                                {
                                    oForm.DataSources.UserDataSources.Item("cnpj").Value = itens.CNPJ;
                                    oForm.DataSources.UserDataSources.Item("IE").Value = itens.NroInscricao;
                                    oForm.DataSources.UserDataSources.Item("tpDeclar").Value = itens.TipoDeclaracao;
                                    oForm.DataSources.UserDataSources.Item("regApur").Value = itens.RegimeApuracao;
                                    oForm.DataSources.UserDataSources.Item("ApurConso").Value = itens.ApuracaoConsolidada;
                                    oForm.DataSources.UserDataSources.Item("subsTrib").Value = itens.SubstitutoTributario;
                                    oForm.DataSources.UserDataSources.Item("temEscr").Value = itens.TemEscritaContabil;
                                    oForm.DataSources.UserDataSources.Item("qtdTrab").Value = itens.QtdeTrabAtividade.ToString();
                                }
                                foreach (var itens in frmfrmCadContribDao.CarregaUmContabilista(id))
                                {
                                    oForm.DataSources.UserDataSources.Item("nomeContab").Value = itens.NomeContabilista;
                                    oForm.DataSources.UserDataSources.Item("cpfContab").Value = itens.CPF;
                                }

                            }
                            catch (Exception e)
                            {
                                throw new Exception(e.Message);
                            }
                            oForm.Freeze(false);
                        }
                        #endregion
                        break;
                    case BoEventTypes.et_DATASOURCE_LOAD:
                        break;
                    case BoEventTypes.et_DOUBLE_CLICK:
                        break;
                    case BoEventTypes.et_Drag:
                        break;
                    case BoEventTypes.et_EDIT_REPORT:
                        break;
                    case BoEventTypes.et_FORMAT_SEARCH_COMPLETED:
                        break;
                    case BoEventTypes.et_FORM_ACTIVATE:
                        break;
                    case BoEventTypes.et_FORM_CLOSE:
                        break;
                    case BoEventTypes.et_FORM_DATA_ADD:
                        break;
                    case BoEventTypes.et_FORM_DATA_DELETE:
                        break;
                    case BoEventTypes.et_FORM_DATA_LOAD:
                        break;
                    case BoEventTypes.et_FORM_DATA_UPDATE:
                        break;
                    case BoEventTypes.et_FORM_DEACTIVATE:
                        break;
                    case BoEventTypes.et_FORM_DRAW:
                        break;
                    case BoEventTypes.et_FORM_KEY_DOWN:
                        break;
                    case BoEventTypes.et_FORM_LOAD:
                        break;
                    case BoEventTypes.et_FORM_MENU_HILIGHT:
                        break;
                    case BoEventTypes.et_FORM_RESIZE:
                        break;
                    case BoEventTypes.et_FORM_UNLOAD:
                        break;
                    case BoEventTypes.et_FORM_VISIBLE:
                        break;
                    case BoEventTypes.et_GOT_FOCUS:
                        break;
                    case BoEventTypes.et_GRID_SORT:
                        break;
                    case BoEventTypes.et_ITEM_PRESSED:
                        #region BOTÃO OK
                        if (!_eventoItem.Before_Action && _eventoItem.ItemUID == "btnOK")
                        {
                            SAPbouiCOM.Form oForm = Conexao.uiApplication.Forms.Item(_eventoItem.FormUID);

                            if (string.IsNullOrEmpty(oForm.DataSources.UserDataSources.Item("periodo").Value))
                            {
                                Conexao.uiApplication.SetStatusBarMessage("Campo PERIODO obrigatório!", BoMessageTime.bmt_Short, true);
                                return true;
                            }

                            Conexao.uiApplication.SetStatusBarMessage("Salvando Declaração!", BoMessageTime.bmt_Short, false);
                            IdDeclaracao = string.IsNullOrEmpty(IdDeclaracao) ? "0" : IdDeclaracao;

                            if (oForm.Items.Item("periodo").Enabled == true)
                            {
                                if (frmfrmCadDeclarDimeDao.ValidaPeriodo(((SAPbouiCOM.ComboBox)oForm.Items.Item("contri").Specific).Selected.Description, oForm.DataSources.UserDataSources.Item("periodo").Value))
                                {
                                    Conexao.uiApplication.SetStatusBarMessage("Periodo já Registrado para o Contribuinte selecionado", BoMessageTime.bmt_Short, true);
                                    return true;
                                }
                            }

                            if (IdDeclaracao == "0")
                                Save(ref oForm);
                            else
                                Update(ref oForm);
                            Conexao.uiApplication.SetStatusBarMessage("Declaração Salva com Sucesso!", BoMessageTime.bmt_Short, false);
                            oForm.Close();
                        }
                        #endregion

                        #region BOTÃO BOTÃO RECARREGAR
                        if (!_eventoItem.Before_Action && _eventoItem.ItemUID == "btnRecar")
                        {
                            SAPbouiCOM.Form oForm = Conexao.uiApplication.Forms.Item(_eventoItem.FormUID);

                            Conexao.diCompany.StartTransaction();

                            #region SELECT QUE PREENCHE O DATATABLE dtInfo
                            string sql = string.Format(@"SELECT [Contribuinte].[IdContribuinte]
                                                                  ,[IdContabilista]
                                                                  ,[NomeRazaoSocial]
                                                                  ,[CNPJ]
                                                                  ,[NroInscricao]
                                                                  ,[TipoDeclaracao]
                                                                  ,[RegimeApuracao]
                                                                  ,[PorteEmpresa]
                                                                  ,[ApuracaoConsolidada]
                                                                  ,[ApuracaoCentralizada]
                                                                  ,[TemCreditosPresumido]
                                                                  ,[TemCredIncentFiscais]
                                                                  ,[SubstitutoTributario]
                                                                  ,[TemEscritaContabil]
                                                                  ,[QtdeTrabAtividade]
                                                                  ,[ErpIdEmpresa]
                                                                  ,[ErpBaseDados]
                                                                  ,[Excluido]
                                                                  ,[Periodo]
                                                                  FROM [SPS_Fiscal].[dime].[Contribuinte] 
                                                                  inner join [SPS_Fiscal].[dime].[PeriodoApuracao] T1 ON IdDeclaracao = T1.IdDeclaracao
                                                                WHERE [CNPJ] = '{0}'", oForm.DataSources.UserDataSources.Item("cnpj").Value);
                            System.Data.DataTable dtInfo = Conexao.ExecuteSqlDataTable(sql);
                            #endregion

                            sql = string.Format(@"DECLARE @NOMECONTRIBUINTE AS NVARCHAR(50) = (SELECT [NomeContribuinte] 
											                                                    FROM [SPS_Fiscal].[dime].[Declaracao]	
											                                                    WHERE [IdDeclaracao] = '{0}')
                                                  DECLARE @IDCONTRIBUINTE AS NVARCHAR(50) = (SELECT [IdContribuinte] 
											                                                    FROM [SPS_Fiscal].[dime].[Contribuinte] 
											                                                    WHERE [NomeRazaoSocial] = @NOMECONTRIBUINTE)
                                                  select [IdPeriodo] 
                                                  FROM [SPS_Fiscal].[dime].[PeriodoApuracao] 
                                                  WHERE [Periodo] = '{1}' AND [IdContribuinte] = @IDCONTRIBUINTE", IdDeclaracao, Periodo);

                            string IdPeriodo = Convert.ToString(Conexao.ExecuteSqlScalar(sql));

                            sql = string.Format(@"select bplid from obpl where bplname LIKE '{0}%'", oForm.DataSources.UserDataSources.Item("contri").Value);
                            string bplid = Convert.ToString(Conexao.ExecuteSqlScalar(sql));
                            string dtDe = Convert.ToDateTime(oForm.DataSources.UserDataSources.Item("periodo").Value + "-01").ToString("yyyyMMdd");
                            DateTime dtAte = Convert.ToDateTime(oForm.DataSources.UserDataSources.Item("periodo").Value + "-01").AddMonths(1).AddDays(-1);
                            string dtFim = Convert.ToDateTime(dtAte).ToString("yyyyMMdd");

                            string periodoApuracao = oForm.DataSources.UserDataSources.Item("periodo").Value;
                            periodoApuracao = periodoApuracao.Replace("-", "");
                            periodoApuracao = (Convert.ToInt32(periodoApuracao) - 1).ToString();
                            periodoApuracao = periodoApuracao.Substring(0, 4) + "-" + periodoApuracao.Substring(4, 2);

                            Conexao.uiApplication.SetStatusBarMessage("Carregando Declarações", BoMessageTime.bmt_Short, false);

                            #region POPULANDO TABELAS PT2
                            sql = string.Format(@"exec [SPS_Fiscal].[dbo].[SP_SPSLivros] '{0}','{1}', '{2}', '{3}', '{4}'", dtDe, dtFim, dtInfo.Rows[0]["ErpIdEmpresa"].ToString(), IdPeriodo, dtInfo.Rows[0]["ErpBaseDados"].ToString());
                            Conexao.ExecuteSqlScalar(sql);

                            sql = string.Format(@"exec [SPS_Fiscal].[dbo].[SP_SPSQuadros] '{0}','{1}', '{2}', '{3}', '{4}', '{5}'", dtInfo.Rows[0]["ErpIdEmpresa"].ToString(), dtDe, dtFim, IdPeriodo, IdDeclaracao, periodoApuracao);
                            Conexao.ExecuteSqlScalar(sql);
                            #endregion
                            
                            sql = $"select * FROM [SPS_Fiscal].[dime].[RegistroDeclaracao] where IdDeclaracao = {IdDeclaracao} and TipoRegistro >= '80'";
                            System.Data.DataTable table = Conexao.ExecuteSqlDataTable(sql);

                            if (table.Rows.Count == 0)
                            {
                                sql = $@"INSERT INTO [SPS_Fiscal].[dime].[RegistroDeclaracao]
                                                ([IdDeclaracao],
                                                [TipoRegistro])
                                                SELECT {IdDeclaracao},'80' UNION ALL
                                                SELECT {IdDeclaracao},'81' UNION ALL
                                                SELECT {IdDeclaracao},'82' UNION ALL
                                                SELECT {IdDeclaracao},'83' UNION ALL
                                                SELECT {IdDeclaracao},'84' UNION ALL
                                                SELECT {IdDeclaracao},'90' UNION ALL
                                                SELECT {IdDeclaracao},'91' UNION ALL
                                                SELECT {IdDeclaracao},'92' UNION ALL
                                                SELECT {IdDeclaracao},'93' UNION ALL
                                                SELECT {IdDeclaracao},'94'";
                                Conexao.ExecuteSqlScalar(sql);
                            }
                            if (Conexao.diCompany.InTransaction)
                                Conexao.diCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                            Conexao.uiApplication.SetStatusBarMessage("Carregamento Finalizado", BoMessageTime.bmt_Short, false);
                        }
                        #endregion

                        #region BOTÃO GERAR
                        if (!_eventoItem.Before_Action && _eventoItem.ItemUID == "btnGerar")
                        {
                            SAPbouiCOM.Form oForm = Conexao.uiApplication.Forms.Item(_eventoItem.FormUID);

                            if (Periodo.Substring(5) == "06")
                            {
                                string sql = string.Empty;
                                int[] tabs = { 80, 81, 82, 83, 84, 90, 91, 92, 93, 94 };
                                System.Data.DataTable odt;
                                bool existe = false;

                                foreach (var item in tabs)
                                {
                                    sql = $@"select * from [SPS_Fiscal].[dime].[DadosQuadro{item}] where IdDeclaracao = {IdDeclaracao} and Valor > 0.0";
                                    odt = Conexao.ExecuteSqlDataTable(sql);

                                    if (odt.Rows.Count > 0)
                                    {
                                        existe = true;
                                        break;
                                    }
                                }
                                if (!existe)
                                {
                                    Conexao.uiApplication.MessageBox(@"Gerar o Arquivo para o período de Junho, é obrigatório informar os Quadros Anuais: 
                                        (80, 81, 82, 83, 84) para Período Normal;
                                        (90, 91, 92, 93, 94) para Encerramento da Empresa;
                                        Informe e Salve as informações na Aba Resumo Anual.");
                                    return false;
                                }

                            }

                            string nomeArquivo = Support.GetFileNameViaOFD("Text files (*.txt)|*.txt|All files (*.*)|*.*", @"C:\", "Salvar Arquivo", false);

                            ArrayList ArrayLinhas = new ArrayList();

                            Contabilista contabilista = new Contabilista();
                            #region SELECT CONTABILISTA
                            string SQL = string.Format(@"Declare @IdDeclaracao INT = {0}
                                                            Declare @Data nvarchar(14) =  (SELECT REPLACE(REPLACE(CONVERT(NVARCHAR,[DtCriacao], 112) + CONVERT(NVARCHAR,[HrCriacao]), '/',''), ':','')
                                                            FROM [SPS_Fiscal].[dime].[ContribuinteDeclaracao] where IdDeclaracao = @IdDeclaracao)
                                                            SELECT ContabilistaCPF, --Número do CPF do Contador - 11
                                                                NomeContabilista,	--Nome do Contabilista - 50
                                                                Data = @data --Data e Hora - 14
                                                            FROM[SPS_Fiscal].[dime].[Declaracao] Where IdDeclaracao = @IdDeclaracao", IdDeclaracao);
                            System.Data.DataTable dt = Conexao.ExecuteSqlDataTable(SQL);
                            if (dt.Rows.Count > 0)
                            {
                                contabilista.CPF = dt.Rows[0]["ContabilistaCPF"].ToString().Replace(".", "").Replace("-", "");
                                contabilista.NomeContabilista = dt.Rows[0]["NomeContabilista"].ToString();
                                contabilista.DataHora = dt.Rows[0]["Data"].ToString();

                                ArrayLinhas.Add(contabilista.LinhaArquivo());
                            }
                            #endregion

                            Contribuinte contribuinte = new Contribuinte();
                            #region SELECT CONTRIBUINTE
                            SQL = string.Format(@"Declare @IdDeclaracao INT = {0}
                                                             Declare @Data nvarchar(14) =  (SELECT REPLACE(REPLACE(CONVERT(NVARCHAR,[DtCriacao], 112) + CONVERT(NVARCHAR,[HrCriacao]), '/',''), ':','')
                                                            FROM [SPS_Fiscal].[dime].[ContribuinteDeclaracao] where IdDeclaracao = @IdDeclaracao)
                                                            SELECT NroInscricao, 
                                                            NomeContribuinte,
                                                            PeriodoReferencia, 
                                                            TipoDeclaracao, 
                                                            RegimeApuracao, 
                                                            PorteEmpresa, 
                                                            ApuracaoConsolidada, 
                                                            ApuracaoCentralizada, 
                                                            TransCredPeriodo, 
                                                            TemCreditosPresumido,
                                                            TemCredIncentFiscais,
                                                            Movimento,
                                                            SubstitutoTributario,
                                                            TemEscritaContabil,
                                                            QtdeTrabAtividade
                                                            FROM [SPS_Fiscal].[dime].[Declaracao] Where IdDeclaracao = @IdDeclaracao", IdDeclaracao);
                            dt = Conexao.ExecuteSqlDataTable(SQL);
                            if (dt.Rows.Count > 0)
                            {
                                contribuinte.NumeroInscricao = dt.Rows[0]["NroInscricao"].ToString();
                                contribuinte.NomeContribuinte = dt.Rows[0]["NomeContribuinte"].ToString();
                                contribuinte.PeriodoReferencia = dt.Rows[0]["PeriodoReferencia"].ToString() == null ? string.Empty : Convert.ToDateTime(dt.Rows[0]["PeriodoReferencia"]).ToString("MMyyyy");
                                contribuinte.TipoDeclaracao = dt.Rows[0]["TipoDeclaracao"].ToString() == null ? string.Empty : dt.Rows[0]["TipoDeclaracao"].ToString();
                                contribuinte.RegimeApuracao = dt.Rows[0]["RegimeApuracao"].ToString() == null ? string.Empty : dt.Rows[0]["RegimeApuracao"].ToString();
                                contribuinte.PorteEmpresa = dt.Rows[0]["PorteEmpresa"].ToString() == null ? string.Empty : dt.Rows[0]["PorteEmpresa"].ToString();
                                contribuinte.ApuracaoConsolidada = dt.Rows[0]["ApuracaoConsolidada"].ToString() == null ? string.Empty : dt.Rows[0]["ApuracaoConsolidada"].ToString();
                                contribuinte.ApuracaoCentralizada = dt.Rows[0]["ApuracaoCentralizada"].ToString() == null ? string.Empty : dt.Rows[0]["ApuracaoCentralizada"].ToString();
                                contribuinte.Transferencia = dt.Rows[0]["TransCredPeriodo"].ToString() == null ? string.Empty : dt.Rows[0]["TransCredPeriodo"].ToString();
                                //contribuinte.CreditoPeriodo = "1";
                                contribuinte.CreditoPresumido = dt.Rows[0]["TemCreditosPresumido"].ToString() == null ? string.Empty : dt.Rows[0]["TemCreditosPresumido"].ToString();
                                contribuinte.CreditoInsentivosFiscais = dt.Rows[0]["TemCredIncentFiscais"].ToString() == null ? string.Empty : dt.Rows[0]["TemCredIncentFiscais"].ToString();
                                contribuinte.Movimento = dt.Rows[0]["Movimento"].ToString() == null ? string.Empty : dt.Rows[0]["Movimento"].ToString();
                                contribuinte.SubstitutoTributario = dt.Rows[0]["SubstitutoTributario"].ToString() == null ? string.Empty : dt.Rows[0]["SubstitutoTributario"].ToString();
                                contribuinte.EscritaContabil = dt.Rows[0]["TemEscritaContabil"].ToString() == null ? string.Empty : dt.Rows[0]["TemEscritaContabil"].ToString();
                                contribuinte.Qtdtrab = dt.Rows[0]["QtdeTrabAtividade"].ToString() == null ? string.Empty : dt.Rows[0]["QtdeTrabAtividade"].ToString();

                                ArrayLinhas.Add(contribuinte.LinhaArquivo());
                            }
                            #endregion

                            DadosQuadro01 dadosQuadro01 = new DadosQuadro01();
                            #region SELECT DADOS QUADRO 01
                            SQL = string.Format(@"Declare @IdDeclaracao INT = {0}
                                                             Declare @Data nvarchar(14) =  (SELECT REPLACE(REPLACE(CONVERT(NVARCHAR,[DtCriacao], 112) + CONVERT(NVARCHAR,[HrCriacao]), '/',''), ':','')
                                                            FROM [SPS_Fiscal].[dime].[ContribuinteDeclaracao] where IdDeclaracao = @IdDeclaracao)
                                                            SELECT [CFOP]
                                                              ,CONVERT(NVARCHAR(17),[Vl_Contabil]) AS 'Vl_Contabil'
                                                              ,CONVERT(NVARCHAR(17),[Vl_Base_Calc]) AS 'Vl_Base_Calc'
                                                              ,CONVERT(NVARCHAR(17),[Vl_Imp_Cred]) AS 'Vl_Imp_Cred'
                                                              ,CONVERT(NVARCHAR(17),[Vl_Isentas_Nao_Trib]) AS 'Vl_Isentas_Nao_Trib'
                                                              ,CONVERT(NVARCHAR(17),[Vl_Outras]) AS 'Vl_Outras'
                                                              ,CONVERT(NVARCHAR(17),[Vl_Base_Calc_IR]) AS 'Vl_Base_Calc_IR'
                                                              ,CONVERT(NVARCHAR(17),[Vl_IR]) AS 'Vl_IR'
                                                              ,CONVERT(NVARCHAR(17),[Vl_Difal]) AS 'Vl_Difal'
                                                          FROM [SPS_Fiscal].[dime].[DadosQuadro01] Where IdDeclaracao = @IdDeclaracao
                                                          ORDER BY CFOP ASC", IdDeclaracao);
                            dt = Conexao.ExecuteSqlDataTable(SQL);
                            if (dt.Rows.Count > 0)
                            {
                                for (int i = 0; i < dt.Rows.Count; i++)
                                {
                                    dadosQuadro01.CFOP = dt.Rows[i]["CFOP"].ToString();
                                    dadosQuadro01.ValorContabil = dt.Rows[i]["Vl_Contabil"].ToString();
                                    dadosQuadro01.BaseCalculo = dt.Rows[i]["Vl_Base_Calc"].ToString();
                                    dadosQuadro01.ImpostoCreditado = dt.Rows[i]["Vl_Imp_Cred"].ToString();
                                    dadosQuadro01.InsentaNaoTributaria = dt.Rows[i]["Vl_Isentas_Nao_Trib"].ToString();
                                    dadosQuadro01.Outras = dt.Rows[i]["Vl_Outras"].ToString();
                                    dadosQuadro01.BaseCalculoImpostoRetido = dt.Rows[i]["Vl_Base_Calc_IR"].ToString();
                                    dadosQuadro01.ImpostoRetido = dt.Rows[i]["Vl_IR"].ToString();
                                    dadosQuadro01.DirefencaAliquoa = dt.Rows[i]["Vl_Difal"].ToString();

                                    ArrayLinhas.Add(dadosQuadro01.LinhaArquivo());
                                }
                            }
                            #endregion

                            DadosQuadro02 dadosQuadro02 = new DadosQuadro02();
                            #region SELECT DADOS QUADRO 02
                            SQL = string.Format(@"Declare @IdDeclaracao INT = {0}
                                                             Declare @Data nvarchar(14) =  (SELECT REPLACE(REPLACE(CONVERT(NVARCHAR,[DtCriacao], 112) + CONVERT(NVARCHAR,[HrCriacao]), '/',''), ':','')
                                                            FROM [SPS_Fiscal].[dime].[ContribuinteDeclaracao] where IdDeclaracao = @IdDeclaracao)
                                                            SELECT [CFOP]
                                                                  ,CONVERT(NVARCHAR(17),[Vl_Contabil]) AS 'Vl_Contabil'
                                                                  ,CONVERT(NVARCHAR(17),[Vl_Base_Calc]) AS 'Vl_Base_Calc'
                                                                  ,CONVERT(NVARCHAR(17),[Vl_Imp_Debit]) AS 'Vl_Imp_Debit'
                                                                  ,CONVERT(NVARCHAR(17),[Vl_Isentas_Nao_Trib]) AS 'Vl_Isentas_Nao_Trib'
                                                                  ,CONVERT(NVARCHAR(17),[Vl_Outras]) AS 'Vl_Outras'
                                                                  ,CONVERT(NVARCHAR(17),[Vl_Base_Calc_IR]) AS 'Vl_Base_Calc_IR'
                                                                  ,CONVERT(NVARCHAR(17),[Vl_IR]) AS 'Vl_IR'
                                                          FROM [SPS_Fiscal].[dime].[DadosQuadro02] Where IdDeclaracao = @IdDeclaracao
                                                          ORDER BY CFOP ASC", IdDeclaracao);
                            dt = Conexao.ExecuteSqlDataTable(SQL);
                            if (dt.Rows.Count > 0)
                            {
                                for (int i = 0; i < dt.Rows.Count; i++)
                                {
                                    dadosQuadro02.CFOP = dt.Rows[i]["CFOP"].ToString();
                                    dadosQuadro02.ValorContabil = dt.Rows[i]["Vl_Contabil"].ToString();
                                    dadosQuadro02.BaseCalculo = dt.Rows[i]["Vl_Base_Calc"].ToString();
                                    dadosQuadro02.ImpostoDebitado = dt.Rows[i]["Vl_Imp_Debit"].ToString();
                                    dadosQuadro02.InsentaNaoTributaria = dt.Rows[i]["Vl_Isentas_Nao_Trib"].ToString();
                                    dadosQuadro02.Outras = dt.Rows[i]["Vl_Outras"].ToString();
                                    dadosQuadro02.BaseCalculoImpostoRetido = dt.Rows[i]["Vl_Base_Calc_IR"].ToString();
                                    dadosQuadro02.ImpostoRetido = dt.Rows[i]["Vl_IR"].ToString();

                                    ArrayLinhas.Add(dadosQuadro02.LinhaArquivo());
                                }
                            }
                            #endregion

                            DadosQuadro03 dadosQuadro03 = new DadosQuadro03();
                            #region SELECT DADOS QUADRO 03
                            SQL = string.Format(@"Declare @IdDeclaracao INT = {0}
                                                             Declare @Data nvarchar(14) =  (SELECT REPLACE(REPLACE(CONVERT(NVARCHAR,[DtCriacao], 112) + CONVERT(NVARCHAR,[HrCriacao]), '/',''), ':','')
                                                            FROM [SPS_Fiscal].[dime].[ContribuinteDeclaracao] where IdDeclaracao = @IdDeclaracao)
                                                            SELECT [NumeroItem]
                                                                  ,[IdDeclaracao]
                                                                  ,CONVERT(NVARCHAR(17),[Valor]) AS 'Valor'
                                                              FROM [SPS_Fiscal].[dime].[DadosQuadro03] Where Valor > 0 AND IdDeclaracao = @IdDeclaracao", IdDeclaracao);
                            dt = Conexao.ExecuteSqlDataTable(SQL);
                            if (dt.Rows.Count > 0)
                            {
                                for (int i = 0; i < dt.Rows.Count; i++)
                                {
                                    dadosQuadro03.Item = dt.Rows[i]["NumeroItem"].ToString();
                                    dadosQuadro03.Valor = dt.Rows[i]["Valor"].ToString();

                                    ArrayLinhas.Add(dadosQuadro03.LinhaArquivo());
                                }
                            }
                            #endregion

                            DadosQuadro04 dadosQuadro04 = new DadosQuadro04();
                            #region SELECT DADOS QUADRO 04
                            SQL = string.Format(@"Declare @IdDeclaracao INT = {0}
                                                             Declare @Data nvarchar(14) =  (SELECT REPLACE(REPLACE(CONVERT(NVARCHAR,[DtCriacao], 112) + CONVERT(NVARCHAR,[HrCriacao]), '/',''), ':','')
                                                            FROM [SPS_Fiscal].[dime].[ContribuinteDeclaracao] where IdDeclaracao = @IdDeclaracao)
                                                            SELECT [NumeroItem]
                                                                  ,[IdDeclaracao]
                                                                  ,CONVERT(NVARCHAR(17),[Valor]) AS 'Valor'
                                                              FROM [SPS_Fiscal].[dime].[DadosQuadro04] Where Valor > 0 AND IdDeclaracao = @IdDeclaracao", IdDeclaracao);
                            dt = Conexao.ExecuteSqlDataTable(SQL);
                            if (dt.Rows.Count > 0)
                            {

                                for (int i = 0; i < dt.Rows.Count; i++)
                                {
                                    dadosQuadro04.Item = dt.Rows[i]["NumeroItem"].ToString();
                                    dadosQuadro04.Valor = dt.Rows[i]["Valor"].ToString();

                                    ArrayLinhas.Add(dadosQuadro04.LinhaArquivo());
                                }
                            }
                            #endregion

                            DadosQuadro05 dadosQuadro05 = new DadosQuadro05();
                            #region SELECT DADOS QUADRO 05
                            SQL = string.Format(@"Declare @IdDeclaracao INT = {0}
                                                             Declare @Data nvarchar(14) =  (SELECT REPLACE(REPLACE(CONVERT(NVARCHAR,[DtCriacao], 112) + CONVERT(NVARCHAR,[HrCriacao]), '/',''), ':','')
                                                            FROM [SPS_Fiscal].[dime].[ContribuinteDeclaracao] where IdDeclaracao = @IdDeclaracao)
                                                            SELECT [NumeroItem]
                                                                  ,[IdDeclaracao]
                                                                  ,CONVERT(NVARCHAR(17),[Valor]) AS 'Valor'
                                                              FROM [SPS_Fiscal].[dime].[DadosQuadro05] Where Valor > 0 AND IdDeclaracao = @IdDeclaracao", IdDeclaracao);
                            dt = Conexao.ExecuteSqlDataTable(SQL);
                            if (dt.Rows.Count > 0)
                            {
                                for (int i = 0; i < dt.Rows.Count; i++)
                                {
                                    dadosQuadro05.Item = dt.Rows[i]["NumeroItem"].ToString();
                                    dadosQuadro05.Valor = dt.Rows[i]["Valor"].ToString();

                                    ArrayLinhas.Add(dadosQuadro05.LinhaArquivo());
                                }
                            }
                            #endregion

                            DadosQuadro09 dadosQuadro09 = new DadosQuadro09();
                            #region SELECT DADOS QUADRO 09
                            SQL = string.Format(@"Declare @IdDeclaracao INT = {0}
                                                             Declare @Data nvarchar(14) =  (SELECT REPLACE(REPLACE(CONVERT(NVARCHAR,[DtCriacao], 112) + CONVERT(NVARCHAR,[HrCriacao]), '/',''), ':','')
                                                            FROM [SPS_Fiscal].[dime].[ContribuinteDeclaracao] where IdDeclaracao = @IdDeclaracao)
                                                            SELECT [NumeroItem]
                                                                  ,[IdDeclaracao]
                                                                  ,CONVERT(NVARCHAR(17),[Valor]) AS 'Valor'
                                                              FROM [SPS_Fiscal].[dime].[DadosQuadro09] Where Valor > 0 AND IdDeclaracao = @IdDeclaracao", IdDeclaracao);
                            dt = Conexao.ExecuteSqlDataTable(SQL);
                            if (dt.Rows.Count > 0)
                            {
                                for (int i = 0; i < dt.Rows.Count; i++)
                                {
                                    dadosQuadro09.Item = dt.Rows[i]["NumeroItem"].ToString();
                                    dadosQuadro09.Valor = dt.Rows[i]["Valor"].ToString();

                                    ArrayLinhas.Add(dadosQuadro09.LinhaArquivo());
                                }
                            }
                            #endregion

                            DadosQuadro11 dadosQuadro11 = new DadosQuadro11();
                            #region SELECT DADOS QUADRO 011
                            SQL = string.Format(@"Declare @IdDeclaracao INT = {0}
                                                             Declare @Data nvarchar(14) =  (SELECT REPLACE(REPLACE(CONVERT(NVARCHAR,[DtCriacao], 112) + CONVERT(NVARCHAR,[HrCriacao]), '/',''), ':','')
                                                            FROM [SPS_Fiscal].[dime].[ContribuinteDeclaracao] where IdDeclaracao = @IdDeclaracao)
                                                            SELECT [NumeroItem]
                                                                  ,[IdDeclaracao]
                                                                  ,CONVERT(NVARCHAR(17),[Valor]) AS 'Valor'
                                                              FROM [SPS_Fiscal].[dime].[DadosQuadro11] Where Valor > 0 AND IdDeclaracao = @IdDeclaracao", IdDeclaracao);
                            dt = Conexao.ExecuteSqlDataTable(SQL);
                            if (dt.Rows.Count > 0)
                            {
                                for (int i = 0; i < dt.Rows.Count; i++)
                                {
                                    dadosQuadro11.Item = dt.Rows[i]["NumeroItem"].ToString();
                                    dadosQuadro11.Valor = dt.Rows[i]["Valor"].ToString();

                                    ArrayLinhas.Add(dadosQuadro11.LinhaArquivo());
                                }
                            }
                            #endregion

                            DadosQuadro12 dadosQuadro12 = new DadosQuadro12();
                            #region SELECT DADOS QUADRO 012
                            SQL = string.Format(@"Declare @IdDeclaracao INT = {0}
                                                            Declare @Data nvarchar(14) =  (SELECT REPLACE(REPLACE(CONVERT(NVARCHAR,[DtCriacao], 112) + CONVERT(NVARCHAR,[HrCriacao]), '/',''), ':','')
                                                            FROM [SPS_Fiscal].[dime].[ContribuinteDeclaracao] where IdDeclaracao = @IdDeclaracao)
                                                            SELECT [Ind_Org_Recol]
                                                                  ,[Cod_Receita]
                                                                  ,[Dt_Vcto_Recol]
                                                                  ,CONVERT(NVARCHAR(17),[Vl_Recol]) AS 'Vl_Recol'
                                                                  ,[Cod_Class_Vcto]
                                                                  ,[Nro_Acordo]
                                                              FROM [SPS_Fiscal].[dime].[DadosQuadro12] Where IdDeclaracao = @IdDeclaracao", IdDeclaracao);
                            dt = Conexao.ExecuteSqlDataTable(SQL);

                            if (dt.Rows.Count > 0)
                            {

                                for (int i = 0; i < dt.Rows.Count; i++)
                                {
                                    dadosQuadro12.OrigemRecolhimento = dt.Rows[i]["Ind_Org_Recol"].ToString();
                                    dadosQuadro12.CodReceita = dt.Rows[i]["Cod_Receita"].ToString();
                                    dadosQuadro12.Data = Convert.ToDateTime(dt.Rows[i]["Dt_Vcto_Recol"].ToString()).ToString("ddMMyyyy");
                                    dadosQuadro12.Valor = dt.Rows[i]["Vl_Recol"].ToString();
                                    dadosQuadro12.ClasseVenc = dt.Rows[i]["Cod_Class_Vcto"].ToString();
                                    dadosQuadro12.NumeroAcordo = dt.Rows[i]["Nro_Acordo"].ToString();

                                    ArrayLinhas.Add(dadosQuadro12.LinhaArquivo());
                                }

                            }
                            #endregion

                            DadosQuadro14 dadosQuadro14 = new DadosQuadro14();
                            #region SELECT DADOS QUADRO 14
                            SQL = string.Format(@"Declare @IdDeclaracao INT = {0}
                                                             Declare @Data nvarchar(14) =  (SELECT REPLACE(REPLACE(CONVERT(NVARCHAR,[DtCriacao], 112) + CONVERT(NVARCHAR,[HrCriacao]), '/',''), ':','')
                                                            FROM [SPS_Fiscal].[dime].[ContribuinteDeclaracao] where IdDeclaracao = @IdDeclaracao)
                                                            SELECT [NumeroItem]
                                                                  ,[IdDeclaracao]
                                                                  ,CONVERT(NVARCHAR(17),[Valor]) AS 'Valor'
                                                              FROM [SPS_Fiscal].[dime].[DadosQuadro14] Where Valor > 0 AND IdDeclaracao = @IdDeclaracao", IdDeclaracao);
                            dt = Conexao.ExecuteSqlDataTable(SQL);
                            if (dt.Rows.Count > 0)
                            {
                                for (int i = 0; i < dt.Rows.Count; i++)
                                {
                                    dadosQuadro14.Item = dt.Rows[i]["NumeroItem"].ToString();
                                    dadosQuadro14.Valor = dt.Rows[i]["Valor"].ToString();

                                    ArrayLinhas.Add(dadosQuadro14.LinhaArquivo());
                                }
                            }
                            #endregion

                            DadosQuadro46 dadosQuadro46 = new DadosQuadro46();
                            #region SELECT DADOS QUADRO 046
                            SQL = string.Format(@"Declare @IdDeclaracao INT = {0}
                                                            Declare @Data nvarchar(14) =  (SELECT REPLACE(REPLACE(CONVERT(NVARCHAR,[DtCriacao], 112) + CONVERT(NVARCHAR,[HrCriacao]), '/',''), ':','')
                                                            FROM [SPS_Fiscal].[dime].[ContribuinteDeclaracao] where IdDeclaracao = @IdDeclaracao)
                                                            SELECT Cod_Identif
                                                                    ,CONVERT(NVARCHAR(17),Vl_Cred_Apur) AS 'Valor'
                                                                    ,Ind_Origem
                                                                FROM [SPS_Fiscal].[dime].[DadosQuadro46] Where IdDeclaracao = @IdDeclaracao", IdDeclaracao);
                            dt = Conexao.ExecuteSqlDataTable(SQL);
                            if (dt.Rows.Count > 0)
                            {
                                for (int i = 0; i < dt.Rows.Count; i++)
                                {
                                    dadosQuadro46.Sequencia = Convert.ToString(i + 1);
                                    dadosQuadro46.Identificacao = dt.Rows[i]["Cod_Identif"].ToString();
                                    dadosQuadro46.Valor = dt.Rows[i]["Valor"].ToString();
                                    dadosQuadro46.Origem = dt.Rows[i]["Ind_Origem"].ToString();

                                    ArrayLinhas.Add(dadosQuadro46.LinhaArquivo());
                                }
                            }
                            #endregion

                            DadosQuadro80 dadosQuadro80 = new DadosQuadro80();
                            #region SELECT DADOS QUADRO 080
                            SQL = string.Format(@"Declare @IdDeclaracao INT = {0}
                                                             Declare @Data nvarchar(14) =  (SELECT REPLACE(REPLACE(CONVERT(NVARCHAR,[DtCriacao], 112) + CONVERT(NVARCHAR,[HrCriacao]), '/',''), ':','')
                                                            FROM [SPS_Fiscal].[dime].[ContribuinteDeclaracao] where IdDeclaracao = @IdDeclaracao)
                                                            SELECT [NumeroItem]
                                                                  ,[IdDeclaracao]
                                                                  ,CONVERT(NVARCHAR(17),[Valor]) AS 'Valor'
                                                              FROM [SPS_Fiscal].[dime].[DadosQuadro80] Where Valor > 0 AND IdDeclaracao = @IdDeclaracao", IdDeclaracao);
                            dt = Conexao.ExecuteSqlDataTable(SQL);
                            if (dt.Rows.Count > 0)
                            {
                                for (int i = 0; i < dt.Rows.Count; i++)
                                {
                                    dadosQuadro80.Item = dt.Rows[i]["NumeroItem"].ToString();
                                    dadosQuadro80.Valor = dt.Rows[i]["Valor"].ToString();

                                    ArrayLinhas.Add(dadosQuadro80.LinhaArquivo());
                                }
                            }
                            #endregion

                            DadosQuadro81 dadosQuadro81 = new DadosQuadro81();
                            #region SELECT DADOS QUADRO 081
                            SQL = string.Format(@"Declare @IdDeclaracao INT = {0}
                                                             Declare @Data nvarchar(14) =  (SELECT REPLACE(REPLACE(CONVERT(NVARCHAR,[DtCriacao], 112) + CONVERT(NVARCHAR,[HrCriacao]), '/',''), ':','')
                                                            FROM [SPS_Fiscal].[dime].[ContribuinteDeclaracao] where IdDeclaracao = @IdDeclaracao)
                                                            SELECT [NumeroItem]
                                                                  ,[IdDeclaracao]
                                                                  ,CONVERT(NVARCHAR(17),[Valor]) AS 'Valor'
                                                              FROM [SPS_Fiscal].[dime].[DadosQuadro81] Where Valor > 0 AND IdDeclaracao = @IdDeclaracao", IdDeclaracao);
                            dt = Conexao.ExecuteSqlDataTable(SQL);
                            if (dt.Rows.Count > 0)
                            {
                                for (int i = 0; i < dt.Rows.Count; i++)
                                {
                                    dadosQuadro81.Item = dt.Rows[i]["NumeroItem"].ToString();
                                    dadosQuadro81.Valor = dt.Rows[i]["Valor"].ToString();

                                    ArrayLinhas.Add(dadosQuadro81.LinhaArquivo());
                                }
                            }
                            #endregion

                            DadosQuadro82 dadosQuadro82 = new DadosQuadro82();
                            #region SELECT DADOS QUADRO 082
                            SQL = string.Format(@"Declare @IdDeclaracao INT = {0}
                                                             Declare @Data nvarchar(14) =  (SELECT REPLACE(REPLACE(CONVERT(NVARCHAR,[DtCriacao], 112) + CONVERT(NVARCHAR,[HrCriacao]), '/',''), ':','')
                                                            FROM [SPS_Fiscal].[dime].[ContribuinteDeclaracao] where IdDeclaracao = @IdDeclaracao)
                                                            SELECT [NumeroItem]
                                                                  ,[IdDeclaracao]
                                                                  ,CONVERT(NVARCHAR(17),[Valor]) AS 'Valor'
                                                              FROM [SPS_Fiscal].[dime].[DadosQuadro82] Where Valor > 0 AND IdDeclaracao = @IdDeclaracao", IdDeclaracao);
                            dt = Conexao.ExecuteSqlDataTable(SQL);
                            if (dt.Rows.Count > 0)
                            {
                                for (int i = 0; i < dt.Rows.Count; i++)
                                {
                                    dadosQuadro82.Item = dt.Rows[i]["NumeroItem"].ToString();
                                    dadosQuadro82.Valor = dt.Rows[i]["Valor"].ToString();

                                    ArrayLinhas.Add(dadosQuadro82.LinhaArquivo());
                                }
                            }
                            #endregion

                            DadosQuadro83 dadosQuadro83 = new DadosQuadro83();
                            #region SELECT DADOS QUADRO 083
                            SQL = string.Format(@"Declare @IdDeclaracao INT = {0}
                                                             Declare @Data nvarchar(14) =  (SELECT REPLACE(REPLACE(CONVERT(NVARCHAR,[DtCriacao], 112) + CONVERT(NVARCHAR,[HrCriacao]), '/',''), ':','')
                                                            FROM [SPS_Fiscal].[dime].[ContribuinteDeclaracao] where IdDeclaracao = @IdDeclaracao)
                                                            SELECT [NumeroItem]
                                                                  ,[IdDeclaracao]
                                                                  ,CONVERT(NVARCHAR(17),[Valor]) AS 'Valor'
                                                              FROM [SPS_Fiscal].[dime].[DadosQuadro83] Where Valor > 0 AND IdDeclaracao = @IdDeclaracao", IdDeclaracao);
                            dt = Conexao.ExecuteSqlDataTable(SQL);
                            if (dt.Rows.Count > 0)
                            {
                                for (int i = 0; i < dt.Rows.Count; i++)
                                {
                                    dadosQuadro83.Item = dt.Rows[i]["NumeroItem"].ToString();
                                    dadosQuadro83.Valor = dt.Rows[i]["Valor"].ToString();

                                    ArrayLinhas.Add(dadosQuadro83.LinhaArquivo());
                                }
                            }
                            #endregion

                            DadosQuadro84 dadosQuadro84 = new DadosQuadro84();
                            #region SELECT DADOS QUADRO 084
                            SQL = string.Format(@"Declare @IdDeclaracao INT = {0}
                                                             Declare @Data nvarchar(14) =  (SELECT REPLACE(REPLACE(CONVERT(NVARCHAR,[DtCriacao], 112) + CONVERT(NVARCHAR,[HrCriacao]), '/',''), ':','')
                                                            FROM [SPS_Fiscal].[dime].[ContribuinteDeclaracao] where IdDeclaracao = @IdDeclaracao)
                                                            SELECT [NumeroItem]
                                                                  ,[IdDeclaracao]
                                                                  ,CONVERT(NVARCHAR(17),[Valor]) AS 'Valor'
                                                              FROM [SPS_Fiscal].[dime].[DadosQuadro84] Where Valor > 0 AND IdDeclaracao = @IdDeclaracao", IdDeclaracao);
                            dt = Conexao.ExecuteSqlDataTable(SQL);
                            if (dt.Rows.Count > 0)
                            {
                                for (int i = 0; i < dt.Rows.Count; i++)
                                {
                                    dadosQuadro84.Item = dt.Rows[i]["NumeroItem"].ToString();
                                    dadosQuadro84.Valor = dt.Rows[i]["Valor"].ToString();

                                    ArrayLinhas.Add(dadosQuadro84.LinhaArquivo());
                                }
                            }
                            #endregion

                            DadosQuadro90 dadosQuadro90 = new DadosQuadro90();
                            #region SELECT DADOS QUADRO 090
                            SQL = string.Format(@"Declare @IdDeclaracao INT = {0}
                                                             Declare @Data nvarchar(14) =  (SELECT REPLACE(REPLACE(CONVERT(NVARCHAR,[DtCriacao], 112) + CONVERT(NVARCHAR,[HrCriacao]), '/',''), ':','')
                                                            FROM [SPS_Fiscal].[dime].[ContribuinteDeclaracao] where IdDeclaracao = @IdDeclaracao)
                                                            SELECT [NumeroItem]
                                                                  ,[IdDeclaracao]
                                                                  ,CONVERT(NVARCHAR(17),[Valor]) AS 'Valor'
                                                              FROM [SPS_Fiscal].[dime].[DadosQuadro90] Where Valor > 0 AND IdDeclaracao = @IdDeclaracao", IdDeclaracao);
                            dt = Conexao.ExecuteSqlDataTable(SQL);
                            if (dt.Rows.Count > 0)
                            {
                                for (int i = 0; i < dt.Rows.Count; i++)
                                {
                                    dadosQuadro90.Item = dt.Rows[i]["NumeroItem"].ToString();
                                    dadosQuadro90.Valor = dt.Rows[i]["Valor"].ToString();

                                    ArrayLinhas.Add(dadosQuadro90.LinhaArquivo());
                                }
                            }
                            #endregion

                            DadosQuadro91 dadosQuadro91 = new DadosQuadro91();
                            #region SELECT DADOS QUADRO 091
                            SQL = string.Format(@"Declare @IdDeclaracao INT = {0}
                                                             Declare @Data nvarchar(14) =  (SELECT REPLACE(REPLACE(CONVERT(NVARCHAR,[DtCriacao], 112) + CONVERT(NVARCHAR,[HrCriacao]), '/',''), ':','')
                                                            FROM [SPS_Fiscal].[dime].[ContribuinteDeclaracao] where IdDeclaracao = @IdDeclaracao)
                                                            SELECT [NumeroItem]
                                                                  ,[IdDeclaracao]
                                                                  ,CONVERT(NVARCHAR(17),[Valor]) AS 'Valor'
                                                              FROM [SPS_Fiscal].[dime].[DadosQuadro91] Where Valor > 0 AND IdDeclaracao = @IdDeclaracao", IdDeclaracao);
                            dt = Conexao.ExecuteSqlDataTable(SQL);
                            if (dt.Rows.Count > 0)
                            {
                                for (int i = 0; i < dt.Rows.Count; i++)
                                {
                                    dadosQuadro91.Item = dt.Rows[i]["NumeroItem"].ToString();
                                    dadosQuadro91.Valor = dt.Rows[i]["Valor"].ToString();

                                    ArrayLinhas.Add(dadosQuadro91.LinhaArquivo());
                                }
                            }
                            #endregion

                            DadosQuadro92 dadosQuadro92 = new DadosQuadro92();
                            #region SELECT DADOS QUADRO 092
                            SQL = string.Format(@"Declare @IdDeclaracao INT = {0}
                                                             Declare @Data nvarchar(14) =  (SELECT REPLACE(REPLACE(CONVERT(NVARCHAR,[DtCriacao], 112) + CONVERT(NVARCHAR,[HrCriacao]), '/',''), ':','')
                                                            FROM [SPS_Fiscal].[dime].[ContribuinteDeclaracao] where IdDeclaracao = @IdDeclaracao)
                                                            SELECT [NumeroItem]
                                                                  ,[IdDeclaracao]
                                                                  ,CONVERT(NVARCHAR(17),[Valor]) AS 'Valor'
                                                              FROM [SPS_Fiscal].[dime].[DadosQuadro92] Where Valor > 0 AND IdDeclaracao = @IdDeclaracao", IdDeclaracao);
                            dt = Conexao.ExecuteSqlDataTable(SQL);
                            if (dt.Rows.Count > 0)
                            {
                                for (int i = 0; i < dt.Rows.Count; i++)
                                {
                                    dadosQuadro92.Item = dt.Rows[i]["NumeroItem"].ToString();
                                    dadosQuadro92.Valor = dt.Rows[i]["Valor"].ToString();

                                    ArrayLinhas.Add(dadosQuadro92.LinhaArquivo());
                                }
                            }
                            #endregion

                            DadosQuadro93 dadosQuadro93 = new DadosQuadro93();
                            #region SELECT DADOS QUADRO 093
                            SQL = string.Format(@"Declare @IdDeclaracao INT = {0}
                                                             Declare @Data nvarchar(14) =  (SELECT REPLACE(REPLACE(CONVERT(NVARCHAR,[DtCriacao], 112) + CONVERT(NVARCHAR,[HrCriacao]), '/',''), ':','')
                                                            FROM [SPS_Fiscal].[dime].[ContribuinteDeclaracao] where IdDeclaracao = @IdDeclaracao)
                                                            SELECT [NumeroItem]
                                                                  ,[IdDeclaracao]
                                                                  ,CONVERT(NVARCHAR(17),[Valor]) AS 'Valor'
                                                              FROM [SPS_Fiscal].[dime].[DadosQuadro93] Where Valor > 0 AND IdDeclaracao = @IdDeclaracao", IdDeclaracao);
                            dt = Conexao.ExecuteSqlDataTable(SQL);
                            if (dt.Rows.Count > 0)
                            {
                                for (int i = 0; i < dt.Rows.Count; i++)
                                {
                                    dadosQuadro93.Item = dt.Rows[i]["NumeroItem"].ToString();
                                    dadosQuadro93.Valor = dt.Rows[i]["Valor"].ToString();

                                    ArrayLinhas.Add(dadosQuadro93.LinhaArquivo());
                                }
                            }
                            #endregion

                            DadosQuadro94 dadosQuadro94 = new DadosQuadro94();
                            #region SELECT DADOS QUADRO 094
                            SQL = string.Format(@"Declare @IdDeclaracao INT = {0}
                                                             Declare @Data nvarchar(14) =  (SELECT REPLACE(REPLACE(CONVERT(NVARCHAR,[DtCriacao], 112) + CONVERT(NVARCHAR,[HrCriacao]), '/',''), ':','')
                                                            FROM [SPS_Fiscal].[dime].[ContribuinteDeclaracao] where IdDeclaracao = @IdDeclaracao)
                                                            SELECT [NumeroItem]
                                                                  ,[IdDeclaracao]
                                                                  ,CONVERT(NVARCHAR(17),[Valor]) AS 'Valor'
                                                              FROM [SPS_Fiscal].[dime].[DadosQuadro94] Where Valor > 0 AND IdDeclaracao = @IdDeclaracao", IdDeclaracao);
                            dt = Conexao.ExecuteSqlDataTable(SQL);
                            if (dt.Rows.Count > 0)
                            {
                                for (int i = 0; i < dt.Rows.Count; i++)
                                {
                                    dadosQuadro94.Item = dt.Rows[i]["NumeroItem"].ToString();
                                    dadosQuadro94.Valor = dt.Rows[i]["Valor"].ToString();

                                    ArrayLinhas.Add(dadosQuadro93.LinhaArquivo());
                                }
                            }
                            #endregion

                            RegistroTipo98 registroTipo98 = new RegistroTipo98();
                            registroTipo98.QtdReg = ArrayLinhas.Count.ToString();
                            ArrayLinhas.Add(registroTipo98.LinhaArquivo());

                            RegistroTipo99 registroTipo99 = new RegistroTipo99();
                            registroTipo99.QtdReg = Convert.ToString(ArrayLinhas.Count + 1);

                            ArrayLinhas.Add(registroTipo99.LinhaArquivo());

                            StreamWriter sw = new StreamWriter(nomeArquivo);
                            foreach (var item in ArrayLinhas)
                            {
                                sw.WriteLine(item);
                            }
                            sw.Close();
                            sw.Dispose();

                            Conexao.uiApplication.MessageBox("Arquivo gerado com sucesso!");
                        }
                        #endregion
                        break;
                    case BoEventTypes.et_KEY_DOWN:
                        break;
                    case BoEventTypes.et_LOST_FOCUS:
                        break;
                    case BoEventTypes.et_MATRIX_COLLAPSE_PRESSED:
                        break;
                    case BoEventTypes.et_MATRIX_LINK_PRESSED:
                        #region LINK QUADRO
                        if (!_eventoItem.BeforeAction && _eventoItem.ColUID == "Quadro" && _eventoItem.ItemUID == "grdQ")
                        {
                            SAPbouiCOM.Form oFormPrincipal = Conexao.uiApplication.Forms.Item(_eventoItem.FormUID);

                            Grid oGrid = (Grid)oFormPrincipal.Items.Item("grdQ").Specific;

                            if (oFormPrincipal.DataSources.DataTables.Item("grdQ").GetValue("Quadro", _eventoItem.Row) == "01")
                            {
                                #region ABRIR TELA
                                SAPbouiCOM.Form oForm = null;
                                XmlDocument xmlDoc;
                                string appPath = System.Windows.Forms.Application.StartupPath;
                                if (!appPath.EndsWith(@"\")) appPath += @"\";
                                //carregar XML
                                xmlDoc = new XmlDocument();
                                xmlDoc.Load(appPath + @"\srf\frmValFiscEnt.srf");
                                string strXML = xmlDoc.InnerXml;

                                SAPbouiCOM.FormCreationParams oCreationParams = ((SAPbouiCOM.FormCreationParams)(Conexao.uiApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_FormCreationParams)));
                                oCreationParams.XmlData = strXML;

                                oForm = Conexao.uiApplication.Forms.AddEx(oCreationParams);

                                #endregion

                                oForm.Freeze(true);
                                var model = new DimeDadosQuadro01Model();
                                model.IdDeclaracao = Convert.ToInt32(frmfrmCadDeclarDime.IdDeclaracao);
                                frmfrmValFiscEntDao.CarregaGrid(ref oForm, model);
                                ((EditText)oForm.Items.Item("contrib").Specific).Value = oFormPrincipal.DataSources.UserDataSources.Item("contri").Value;
                                ((EditText)oForm.Items.Item("IE").Specific).Value = oFormPrincipal.DataSources.UserDataSources.Item("IE").Value;
                                ((EditText)oForm.Items.Item("perApur").Specific).Value = oFormPrincipal.DataSources.UserDataSources.Item("periodo").Value;
                                oForm.Freeze(false);
                            }
                            if (oFormPrincipal.DataSources.DataTables.Item("grdQ").GetValue("Quadro", _eventoItem.Row) == "02")
                            {
                                #region ABRIR TELA
                                SAPbouiCOM.Form oForm = null;
                                XmlDocument xmlDoc;
                                string appPath = System.Windows.Forms.Application.StartupPath;
                                if (!appPath.EndsWith(@"\")) appPath += @"\";
                                //carregar XML
                                xmlDoc = new XmlDocument();
                                xmlDoc.Load(appPath + @"\srf\frmValFiscSai.srf");
                                string strXML = xmlDoc.InnerXml;

                                SAPbouiCOM.FormCreationParams oCreationParams = ((SAPbouiCOM.FormCreationParams)(Conexao.uiApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_FormCreationParams)));
                                oCreationParams.XmlData = strXML;

                                oForm = Conexao.uiApplication.Forms.AddEx(oCreationParams);

                                #endregion

                                oForm.Freeze(true);
                                var model = new DimeDadosQuadro02Model();
                                model.IdDeclaracao = Convert.ToInt32(frmfrmCadDeclarDime.IdDeclaracao);
                                frmfrmValFiscSaiDao.CarregaGrid(ref oForm, model);
                                ((EditText)oForm.Items.Item("contrib").Specific).Value = oFormPrincipal.DataSources.UserDataSources.Item("contri").Value;
                                ((EditText)oForm.Items.Item("IE").Specific).Value = oFormPrincipal.DataSources.UserDataSources.Item("IE").Value;
                                ((EditText)oForm.Items.Item("perApur").Specific).Value = oFormPrincipal.DataSources.UserDataSources.Item("periodo").Value;
                                oForm.Freeze(false);
                            }

                            if (oFormPrincipal.DataSources.DataTables.Item("grdQ").GetValue("Quadro", _eventoItem.Row) == "03")
                            {
                                #region ABRIR TELA
                                SAPbouiCOM.Form oForm = null;
                                XmlDocument xmlDoc;
                                string appPath = System.Windows.Forms.Application.StartupPath;
                                if (!appPath.EndsWith(@"\")) appPath += @"\";
                                //carregar XML
                                xmlDoc = new XmlDocument();
                                xmlDoc.Load(appPath + @"\srf\frmResValores.srf");
                                string strXML = xmlDoc.InnerXml;

                                SAPbouiCOM.FormCreationParams oCreationParams = ((SAPbouiCOM.FormCreationParams)(Conexao.uiApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_FormCreationParams)));
                                oCreationParams.XmlData = strXML;

                                oForm = Conexao.uiApplication.Forms.AddEx(oCreationParams);

                                #endregion

                                oForm.Freeze(true);
                                frmfrmResValoresDao.CarregaGrid(ref oForm, IdDeclaracao);
                                ((EditText)oForm.Items.Item("contrib").Specific).Value = oFormPrincipal.DataSources.UserDataSources.Item("contri").Value;
                                ((EditText)oForm.Items.Item("IE").Specific).Value = oFormPrincipal.DataSources.UserDataSources.Item("IE").Value;
                                ((EditText)oForm.Items.Item("perApur").Specific).Value = oFormPrincipal.DataSources.UserDataSources.Item("periodo").Value;
                                oForm.Freeze(false);
                            }

                            if (oFormPrincipal.DataSources.DataTables.Item("grdQ").GetValue("Quadro", _eventoItem.Row) == "04")
                            {
                                #region ABRIR TELA
                                SAPbouiCOM.Form oForm = null;
                                XmlDocument xmlDoc;
                                string appPath = System.Windows.Forms.Application.StartupPath;
                                if (!appPath.EndsWith(@"\")) appPath += @"\";
                                //carregar XML
                                xmlDoc = new XmlDocument();
                                xmlDoc.Load(appPath + @"\srf\frmResApurDeb.srf");
                                string strXML = xmlDoc.InnerXml;

                                SAPbouiCOM.FormCreationParams oCreationParams = ((SAPbouiCOM.FormCreationParams)(Conexao.uiApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_FormCreationParams)));
                                oCreationParams.XmlData = strXML;

                                oForm = Conexao.uiApplication.Forms.AddEx(oCreationParams);

                                #endregion

                                oForm.Freeze(true);
                                frmfrmResApurDebDao.CarregaGrid(ref oForm, IdDeclaracao);
                                ((EditText)oForm.Items.Item("contrib").Specific).Value = oFormPrincipal.DataSources.UserDataSources.Item("contri").Value;
                                ((EditText)oForm.Items.Item("IE").Specific).Value = oFormPrincipal.DataSources.UserDataSources.Item("IE").Value;
                                ((EditText)oForm.Items.Item("perApur").Specific).Value = oFormPrincipal.DataSources.UserDataSources.Item("periodo").Value;
                                oForm.Freeze(false);
                            }
                            if (oFormPrincipal.DataSources.DataTables.Item("grdQ").GetValue("Quadro", _eventoItem.Row) == "05")
                            {
                                #region ABRIR TELA
                                SAPbouiCOM.Form oForm = null;
                                XmlDocument xmlDoc;
                                string appPath = System.Windows.Forms.Application.StartupPath;
                                if (!appPath.EndsWith(@"\")) appPath += @"\";
                                //carregar XML
                                xmlDoc = new XmlDocument();
                                xmlDoc.Load(appPath + @"\srf\frmResApurCred.srf");
                                string strXML = xmlDoc.InnerXml;

                                SAPbouiCOM.FormCreationParams oCreationParams = ((SAPbouiCOM.FormCreationParams)(Conexao.uiApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_FormCreationParams)));
                                oCreationParams.XmlData = strXML;

                                oForm = Conexao.uiApplication.Forms.AddEx(oCreationParams);

                                #endregion

                                oForm.Freeze(true);
                                var model = new DimeDadosQuadro05Model();
                                model.IdDeclaracao = Convert.ToInt32(frmfrmCadDeclarDime.IdDeclaracao);
                                frmfrmResApurCredDao.CarregaGrid(ref oForm, model);
                                ((EditText)oForm.Items.Item("contrib").Specific).Value = oFormPrincipal.DataSources.UserDataSources.Item("contri").Value;
                                ((EditText)oForm.Items.Item("IE").Specific).Value = oFormPrincipal.DataSources.UserDataSources.Item("IE").Value;
                                ((EditText)oForm.Items.Item("perApur").Specific).Value = oFormPrincipal.DataSources.UserDataSources.Item("periodo").Value;
                                oForm.Freeze(false);
                            }

                            if (oFormPrincipal.DataSources.DataTables.Item("grdQ").GetValue("Quadro", _eventoItem.Row) == "09")
                            {
                                #region ABRIR TELA
                                SAPbouiCOM.Form oForm = null;
                                XmlDocument xmlDoc;
                                string appPath = System.Windows.Forms.Application.StartupPath;
                                if (!appPath.EndsWith(@"\")) appPath += @"\";
                                //carregar XML
                                xmlDoc = new XmlDocument();
                                xmlDoc.Load(appPath + @"\srf\frmCalcImpPagar.srf");
                                string strXML = xmlDoc.InnerXml;

                                SAPbouiCOM.FormCreationParams oCreationParams = ((SAPbouiCOM.FormCreationParams)(Conexao.uiApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_FormCreationParams)));
                                oCreationParams.XmlData = strXML;

                                oForm = Conexao.uiApplication.Forms.AddEx(oCreationParams);

                                #endregion

                                oForm.Freeze(true);
                                var model = new DimeDadosQuadro09Model();
                                model.IdDeclaracao = Convert.ToInt32(frmfrmCadDeclarDime.IdDeclaracao);
                                frmfrmCalcImpPagarDao.CarregaDados(ref oForm, model);
                                ((EditText)oForm.Items.Item("contrib").Specific).Value = oFormPrincipal.DataSources.UserDataSources.Item("contri").Value;
                                ((EditText)oForm.Items.Item("IE").Specific).Value = oFormPrincipal.DataSources.UserDataSources.Item("IE").Value;
                                ((EditText)oForm.Items.Item("perApur").Specific).Value = oFormPrincipal.DataSources.UserDataSources.Item("periodo").Value;
                                oForm.Freeze(false);
                            }
                            if (oFormPrincipal.DataSources.DataTables.Item("grdQ").GetValue("Quadro", _eventoItem.Row) == "12")
                            {
                                #region ABRIR TELA
                                SAPbouiCOM.Form oForm = null;
                                XmlDocument xmlDoc;
                                string appPath = System.Windows.Forms.Application.StartupPath;
                                if (!appPath.EndsWith(@"\")) appPath += @"\";
                                //carregar XML
                                xmlDoc = new XmlDocument();
                                xmlDoc.Load(appPath + @"\srf\frmDiscrPagtoImp.srf");
                                string strXML = xmlDoc.InnerXml;

                                SAPbouiCOM.FormCreationParams oCreationParams = ((SAPbouiCOM.FormCreationParams)(Conexao.uiApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_FormCreationParams)));
                                oCreationParams.XmlData = strXML;

                                oForm = Conexao.uiApplication.Forms.AddEx(oCreationParams);

                                #endregion

                                oForm.Freeze(true);
                                var model = new DimeDadosQuadro12Model();
                                model.IdDeclaracao = Convert.ToInt32(frmfrmCadDeclarDime.IdDeclaracao);
                                frmfrmDiscrPagtoImpDao.CarregaGrid(ref oForm, model);
                                ((EditText)oForm.Items.Item("contrib").Specific).Value = oFormPrincipal.DataSources.UserDataSources.Item("contri").Value;
                                ((EditText)oForm.Items.Item("IE").Specific).Value = oFormPrincipal.DataSources.UserDataSources.Item("IE").Value;
                                ((EditText)oForm.Items.Item("perApur").Specific).Value = oFormPrincipal.DataSources.UserDataSources.Item("periodo").Value;
                                oForm.Freeze(false);
                            }
                            if (oFormPrincipal.DataSources.DataTables.Item("grdQ").GetValue("Quadro", _eventoItem.Row) == "14")
                            {
                                #region ABRIR TELA
                                SAPbouiCOM.Form oForm = null;
                                XmlDocument xmlDoc;
                                string appPath = System.Windows.Forms.Application.StartupPath;
                                if (!appPath.EndsWith(@"\")) appPath += @"\";
                                //carregar XML
                                xmlDoc = new XmlDocument();
                                xmlDoc.Load(appPath + @"\srf\frmDemApurImpDev.srf");
                                string strXML = xmlDoc.InnerXml;

                                SAPbouiCOM.FormCreationParams oCreationParams = ((SAPbouiCOM.FormCreationParams)(Conexao.uiApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_FormCreationParams)));
                                oCreationParams.XmlData = strXML;

                                oForm = Conexao.uiApplication.Forms.AddEx(oCreationParams);

                                #endregion

                                oForm.Freeze(true);
                                var model = new DimeDadosQuadro14Model();
                                model.IdDeclaracao = Convert.ToInt32(frmfrmCadDeclarDime.IdDeclaracao);
                                frmfrmDemApurImpDevDao.CarregaGrid(ref oForm, model);
                                ((EditText)oForm.Items.Item("contrib").Specific).Value = oFormPrincipal.DataSources.UserDataSources.Item("contri").Value;
                                ((EditText)oForm.Items.Item("IE").Specific).Value = oFormPrincipal.DataSources.UserDataSources.Item("IE").Value;
                                ((EditText)oForm.Items.Item("perApur").Specific).Value = oFormPrincipal.DataSources.UserDataSources.Item("periodo").Value;
                                oForm.Freeze(false);
                            }
                            if (oFormPrincipal.DataSources.DataTables.Item("grdQ").GetValue("Quadro", _eventoItem.Row) == "46")
                            {
                                #region ABRIR TELA
                                SAPbouiCOM.Form oForm = null;
                                XmlDocument xmlDoc;
                                string appPath = System.Windows.Forms.Application.StartupPath;
                                if (!appPath.EndsWith(@"\")) appPath += @"\";
                                //carregar XML
                                xmlDoc = new XmlDocument();
                                xmlDoc.Load(appPath + @"\srf\frmCredRegAuto.srf");
                                string strXML = xmlDoc.InnerXml;

                                SAPbouiCOM.FormCreationParams oCreationParams = ((SAPbouiCOM.FormCreationParams)(Conexao.uiApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_FormCreationParams)));
                                oCreationParams.XmlData = strXML;

                                oForm = Conexao.uiApplication.Forms.AddEx(oCreationParams);

                                #endregion

                                oForm.Freeze(true);
                                var model = new DimeDadosQuadro46Model();
                                model.IdDeclaracao = Convert.ToInt32(frmfrmCadDeclarDime.IdDeclaracao);
                                frmfrmCredRegAutoDao.CarregaGrid(ref oForm, model);
                                ((EditText)oForm.Items.Item("contrib").Specific).Value = oFormPrincipal.DataSources.UserDataSources.Item("contri").Value;
                                ((EditText)oForm.Items.Item("IE").Specific).Value = oFormPrincipal.DataSources.UserDataSources.Item("IE").Value;
                                ((EditText)oForm.Items.Item("perApur").Specific).Value = oFormPrincipal.DataSources.UserDataSources.Item("periodo").Value;
                                oForm.Freeze(false);
                            }
                            if (oFormPrincipal.DataSources.DataTables.Item("grdQ").GetValue("Quadro", _eventoItem.Row) == "48")
                            {
                                #region ABRIR TELA
                                SAPbouiCOM.Form oForm = null;
                                XmlDocument xmlDoc;
                                string appPath = System.Windows.Forms.Application.StartupPath;
                                if (!appPath.EndsWith(@"\")) appPath += @"\";
                                //carregar XML
                                xmlDoc = new XmlDocument();
                                xmlDoc.Load(appPath + @"\srf\frmInfoRateio.srf");
                                string strXML = xmlDoc.InnerXml;

                                SAPbouiCOM.FormCreationParams oCreationParams = ((SAPbouiCOM.FormCreationParams)(Conexao.uiApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_FormCreationParams)));
                                oCreationParams.XmlData = strXML;

                                oForm = Conexao.uiApplication.Forms.AddEx(oCreationParams);

                                #endregion

                                oForm.Freeze(true);
                                var model = new DimeDadosQuadro48Model();
                                model.IdDeclaracao = Convert.ToInt32(frmfrmCadDeclarDime.IdDeclaracao);
                                frmfrmInfoRateioDao.CarregaGrid(ref oForm, model);
                                ((EditText)oForm.Items.Item("contrib").Specific).Value = oFormPrincipal.DataSources.UserDataSources.Item("contri").Value;
                                ((EditText)oForm.Items.Item("IE").Specific).Value = oFormPrincipal.DataSources.UserDataSources.Item("IE").Value;
                                ((EditText)oForm.Items.Item("perApur").Specific).Value = oFormPrincipal.DataSources.UserDataSources.Item("periodo").Value;
                                oForm.Freeze(false);
                            }
                            if (oFormPrincipal.DataSources.DataTables.Item("grdQ").GetValue("Quadro", _eventoItem.Row) == "49")
                            {
                                #region ABRIR TELA
                                SAPbouiCOM.Form oForm = null;
                                XmlDocument xmlDoc;
                                string appPath = System.Windows.Forms.Application.StartupPath;
                                if (!appPath.EndsWith(@"\")) appPath += @"\";
                                //carregar XML
                                xmlDoc = new XmlDocument();
                                xmlDoc.Load(appPath + @"\srf\frmEntUniFed.srf");
                                string strXML = xmlDoc.InnerXml;

                                SAPbouiCOM.FormCreationParams oCreationParams = ((SAPbouiCOM.FormCreationParams)(Conexao.uiApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_FormCreationParams)));
                                oCreationParams.XmlData = strXML;

                                oForm = Conexao.uiApplication.Forms.AddEx(oCreationParams);

                                #endregion

                                oForm.Freeze(true);
                                var model = new DimeDadosQuadro49Model();
                                model.IdDeclaracao = Convert.ToInt32(frmfrmCadDeclarDime.IdDeclaracao);
                                frmfrmEntUniFedDao.CarregaGrid(ref oForm, model);
                                ((EditText)oForm.Items.Item("contrib").Specific).Value = oFormPrincipal.DataSources.UserDataSources.Item("contri").Value;
                                ((EditText)oForm.Items.Item("IE").Specific).Value = oFormPrincipal.DataSources.UserDataSources.Item("IE").Value;
                                ((EditText)oForm.Items.Item("perApur").Specific).Value = oFormPrincipal.DataSources.UserDataSources.Item("periodo").Value;
                                oForm.Freeze(false);
                            }
                            if (oFormPrincipal.DataSources.DataTables.Item("grdQ").GetValue("Quadro", _eventoItem.Row) == "50")
                            {
                                #region ABRIR TELA
                                SAPbouiCOM.Form oForm = null;
                                XmlDocument xmlDoc;
                                string appPath = System.Windows.Forms.Application.StartupPath;
                                if (!appPath.EndsWith(@"\")) appPath += @"\";
                                //carregar XML
                                xmlDoc = new XmlDocument();
                                xmlDoc.Load(appPath + @"\srf\frmSaiUnFed.srf");
                                string strXML = xmlDoc.InnerXml;

                                SAPbouiCOM.FormCreationParams oCreationParams = ((SAPbouiCOM.FormCreationParams)(Conexao.uiApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_FormCreationParams)));
                                oCreationParams.XmlData = strXML;

                                oForm = Conexao.uiApplication.Forms.AddEx(oCreationParams);

                                #endregion

                                oForm.Freeze(true);
                                var model = new DimeDadosQuadro50Model();
                                model.IdDeclaracao = Convert.ToInt32(frmfrmCadDeclarDime.IdDeclaracao);
                                frmfrmSaiUnFedDao.CarregaGrid(ref oForm, model);
                                ((EditText)oForm.Items.Item("contrib").Specific).Value = oFormPrincipal.DataSources.UserDataSources.Item("contri").Value;
                                ((EditText)oForm.Items.Item("IE").Specific).Value = oFormPrincipal.DataSources.UserDataSources.Item("IE").Value;
                                ((EditText)oForm.Items.Item("perApur").Specific).Value = oFormPrincipal.DataSources.UserDataSources.Item("periodo").Value;
                                oForm.Freeze(false);
                            }


                        }
                        #endregion

                        #region LINK RESUMO ANUAL

                        if (!_eventoItem.BeforeAction && _eventoItem.ColUID == "Quadro" && _eventoItem.ItemUID == "GrdRA")
                        {
                            SAPbouiCOM.Form oFormPrincipal = Conexao.uiApplication.Forms.Item(_eventoItem.FormUID);

                            Grid oGrid = (Grid)oFormPrincipal.Items.Item("GrdRA").Specific;

                            #region ABRIR TELA
                            SAPbouiCOM.Form oForm = null;
                            XmlDocument xmlDoc;
                            string appPath = System.Windows.Forms.Application.StartupPath;
                            if (!appPath.EndsWith(@"\")) appPath += @"\";
                            //carregar XML
                            xmlDoc = new XmlDocument();
                            xmlDoc.Load(appPath + @"\srf\frmResLivRegIn.srf");
                            string strXML = xmlDoc.InnerXml;

                            SAPbouiCOM.FormCreationParams oCreationParams = ((SAPbouiCOM.FormCreationParams)(Conexao.uiApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_FormCreationParams)));
                            oCreationParams.XmlData = strXML;

                            oForm = Conexao.uiApplication.Forms.AddEx(oCreationParams);

                            #endregion

                            if (oFormPrincipal.DataSources.DataTables.Item("GrdRA").GetValue("Quadro", _eventoItem.Row) == "80")
                            {
                                oForm.Freeze(true);
                                oForm.Title = "Resumo do livro Registro de Inventário e Receita Bruta";
                                frmfrmResLivRegIn.quadro = string.Empty;
                                frmfrmResLivRegInDao.CarregaGridQuadro80(ref oForm, frmfrmCadDeclarDime.IdDeclaracao);
                                ((EditText)oForm.Items.Item("Contrib").Specific).Value = oFormPrincipal.DataSources.UserDataSources.Item("contri").Value;
                                ((EditText)oForm.Items.Item("IE").Specific).Value = oFormPrincipal.DataSources.UserDataSources.Item("IE").Value;
                                ((EditText)oForm.Items.Item("perApur").Specific).Value = oFormPrincipal.DataSources.UserDataSources.Item("periodo").Value;
                                frmfrmResLivRegIn.quadro = oFormPrincipal.DataSources.DataTables.Item("GrdRA").GetValue("Quadro", _eventoItem.Row);
                                oForm.Freeze(false);
                            }
                            if (oFormPrincipal.DataSources.DataTables.Item("GrdRA").GetValue("Quadro", _eventoItem.Row) == "81")
                            {
                                oForm.Freeze(true);
                                oForm.Title = "Ativo";
                                frmfrmResLivRegIn.quadro = string.Empty;
                                frmfrmResLivRegInDao.CarregaGridQuadro81(ref oForm, frmfrmCadDeclarDime.IdDeclaracao);
                                ((EditText)oForm.Items.Item("Contrib").Specific).Value = oFormPrincipal.DataSources.UserDataSources.Item("contri").Value;
                                ((EditText)oForm.Items.Item("IE").Specific).Value = oFormPrincipal.DataSources.UserDataSources.Item("IE").Value;
                                ((EditText)oForm.Items.Item("perApur").Specific).Value = oFormPrincipal.DataSources.UserDataSources.Item("periodo").Value;
                                frmfrmResLivRegIn.quadro = oFormPrincipal.DataSources.DataTables.Item("GrdRA").GetValue("Quadro", _eventoItem.Row);
                                oForm.Freeze(false);
                            }
                            if (oFormPrincipal.DataSources.DataTables.Item("GrdRA").GetValue("Quadro", _eventoItem.Row) == "82")
                            {
                                oForm.Freeze(true);
                                oForm.Title = "Passivo";
                                frmfrmResLivRegIn.quadro = string.Empty;
                                frmfrmResLivRegInDao.CarregaGridQuadro82(ref oForm, frmfrmCadDeclarDime.IdDeclaracao);
                                ((EditText)oForm.Items.Item("Contrib").Specific).Value = oFormPrincipal.DataSources.UserDataSources.Item("contri").Value;
                                ((EditText)oForm.Items.Item("IE").Specific).Value = oFormPrincipal.DataSources.UserDataSources.Item("IE").Value;
                                ((EditText)oForm.Items.Item("perApur").Specific).Value = oFormPrincipal.DataSources.UserDataSources.Item("periodo").Value;
                                frmfrmResLivRegIn.quadro = oFormPrincipal.DataSources.DataTables.Item("GrdRA").GetValue("Quadro", _eventoItem.Row);
                                oForm.Freeze(false);
                            }
                            if (oFormPrincipal.DataSources.DataTables.Item("GrdRA").GetValue("Quadro", _eventoItem.Row) == "83")
                            {
                                oForm.Freeze(true);
                                oForm.Title = "Demonstração de Resultado";
                                frmfrmResLivRegIn.quadro = string.Empty;
                                frmfrmResLivRegInDao.CarregaGridQuadro83(ref oForm, frmfrmCadDeclarDime.IdDeclaracao);
                                ((EditText)oForm.Items.Item("Contrib").Specific).Value = oFormPrincipal.DataSources.UserDataSources.Item("contri").Value;
                                ((EditText)oForm.Items.Item("IE").Specific).Value = oFormPrincipal.DataSources.UserDataSources.Item("IE").Value;
                                ((EditText)oForm.Items.Item("perApur").Specific).Value = oFormPrincipal.DataSources.UserDataSources.Item("periodo").Value;
                                frmfrmResLivRegIn.quadro = oFormPrincipal.DataSources.DataTables.Item("GrdRA").GetValue("Quadro", _eventoItem.Row);
                                oForm.Freeze(false);
                            }
                            if (oFormPrincipal.DataSources.DataTables.Item("GrdRA").GetValue("Quadro", _eventoItem.Row) == "84")
                            {
                                oForm.Freeze(true);
                                oForm.Title = "Detalhamento das Despesas";
                                frmfrmResLivRegIn.quadro = string.Empty;
                                frmfrmResLivRegInDao.CarregaGridQuadro84(ref oForm, frmfrmCadDeclarDime.IdDeclaracao);
                                ((EditText)oForm.Items.Item("Contrib").Specific).Value = oFormPrincipal.DataSources.UserDataSources.Item("contri").Value;
                                ((EditText)oForm.Items.Item("IE").Specific).Value = oFormPrincipal.DataSources.UserDataSources.Item("IE").Value;
                                ((EditText)oForm.Items.Item("perApur").Specific).Value = oFormPrincipal.DataSources.UserDataSources.Item("periodo").Value;
                                frmfrmResLivRegIn.quadro = oFormPrincipal.DataSources.DataTables.Item("GrdRA").GetValue("Quadro", _eventoItem.Row);
                                oForm.Freeze(false);
                            }
                            if (oFormPrincipal.DataSources.DataTables.Item("GrdRA").GetValue("Quadro", _eventoItem.Row) == "90")
                            {
                                oForm.Freeze(true);
                                oForm.Title = "Resumo do livro registro de inventário - Encerramento de Atividade";
                                frmfrmResLivRegIn.quadro = string.Empty;
                                frmfrmResLivRegInDao.CarregaGridQuadro90(ref oForm, frmfrmCadDeclarDime.IdDeclaracao);
                                ((EditText)oForm.Items.Item("Contrib").Specific).Value = oFormPrincipal.DataSources.UserDataSources.Item("contri").Value;
                                ((EditText)oForm.Items.Item("IE").Specific).Value = oFormPrincipal.DataSources.UserDataSources.Item("IE").Value;
                                ((EditText)oForm.Items.Item("perApur").Specific).Value = oFormPrincipal.DataSources.UserDataSources.Item("periodo").Value;
                                frmfrmResLivRegIn.quadro = oFormPrincipal.DataSources.DataTables.Item("GrdRA").GetValue("Quadro", _eventoItem.Row);
                                oForm.Freeze(false);
                            }
                            if (oFormPrincipal.DataSources.DataTables.Item("GrdRA").GetValue("Quadro", _eventoItem.Row) == "91")
                            {
                                oForm.Freeze(true);
                                oForm.Title = "Ativo - Encerramento de Atividade";
                                frmfrmResLivRegIn.quadro = string.Empty;
                                frmfrmResLivRegInDao.CarregaGridQuadro91(ref oForm, frmfrmCadDeclarDime.IdDeclaracao);
                                ((EditText)oForm.Items.Item("Contrib").Specific).Value = oFormPrincipal.DataSources.UserDataSources.Item("contri").Value;
                                ((EditText)oForm.Items.Item("IE").Specific).Value = oFormPrincipal.DataSources.UserDataSources.Item("IE").Value;
                                ((EditText)oForm.Items.Item("perApur").Specific).Value = oFormPrincipal.DataSources.UserDataSources.Item("periodo").Value;
                                frmfrmResLivRegIn.quadro = oFormPrincipal.DataSources.DataTables.Item("GrdRA").GetValue("Quadro", _eventoItem.Row);
                                oForm.Freeze(false);
                            }
                            if (oFormPrincipal.DataSources.DataTables.Item("GrdRA").GetValue("Quadro", _eventoItem.Row) == "92")
                            {
                                oForm.Freeze(true);
                                oForm.Title = "Passivo - Encerramento de Atividade";
                                frmfrmResLivRegIn.quadro = string.Empty;
                                frmfrmResLivRegInDao.CarregaGridQuadro92(ref oForm, frmfrmCadDeclarDime.IdDeclaracao);
                                ((EditText)oForm.Items.Item("Contrib").Specific).Value = oFormPrincipal.DataSources.UserDataSources.Item("contri").Value;
                                ((EditText)oForm.Items.Item("IE").Specific).Value = oFormPrincipal.DataSources.UserDataSources.Item("IE").Value;
                                ((EditText)oForm.Items.Item("perApur").Specific).Value = oFormPrincipal.DataSources.UserDataSources.Item("periodo").Value;
                                frmfrmResLivRegIn.quadro = oFormPrincipal.DataSources.DataTables.Item("GrdRA").GetValue("Quadro", _eventoItem.Row);
                                oForm.Freeze(false);
                            }
                            if (oFormPrincipal.DataSources.DataTables.Item("GrdRA").GetValue("Quadro", _eventoItem.Row) == "93")
                            {
                                oForm.Freeze(true);
                                oForm.Title = "Demonstração de Resultado - Encerramento de Atividade";
                                frmfrmResLivRegIn.quadro = string.Empty;
                                frmfrmResLivRegInDao.CarregaGridQuadro93(ref oForm, frmfrmCadDeclarDime.IdDeclaracao);
                                ((EditText)oForm.Items.Item("Contrib").Specific).Value = oFormPrincipal.DataSources.UserDataSources.Item("contri").Value;
                                ((EditText)oForm.Items.Item("IE").Specific).Value = oFormPrincipal.DataSources.UserDataSources.Item("IE").Value;
                                ((EditText)oForm.Items.Item("perApur").Specific).Value = oFormPrincipal.DataSources.UserDataSources.Item("periodo").Value;
                                frmfrmResLivRegIn.quadro = oFormPrincipal.DataSources.DataTables.Item("GrdRA").GetValue("Quadro", _eventoItem.Row);
                                oForm.Freeze(false);
                            }
                            if (oFormPrincipal.DataSources.DataTables.Item("GrdRA").GetValue("Quadro", _eventoItem.Row) == "94")
                            {
                                oForm.Freeze(true);
                                oForm.Title = "Detalhamento das despesas - Encerramento de Atividade";
                                frmfrmResLivRegIn.quadro = string.Empty;
                                frmfrmResLivRegInDao.CarregaGridQuadro94(ref oForm, frmfrmCadDeclarDime.IdDeclaracao);
                                ((EditText)oForm.Items.Item("Contrib").Specific).Value = oFormPrincipal.DataSources.UserDataSources.Item("contri").Value;
                                ((EditText)oForm.Items.Item("IE").Specific).Value = oFormPrincipal.DataSources.UserDataSources.Item("IE").Value;
                                ((EditText)oForm.Items.Item("perApur").Specific).Value = oFormPrincipal.DataSources.UserDataSources.Item("periodo").Value;
                                frmfrmResLivRegIn.quadro = oFormPrincipal.DataSources.DataTables.Item("GrdRA").GetValue("Quadro", _eventoItem.Row);
                                oForm.Freeze(false);
                            }
                        }

                        #endregion
                        break;
                    case BoEventTypes.et_MATRIX_LOAD:
                        break;
                    case BoEventTypes.et_MENU_CLICK:
                        break;
                    case BoEventTypes.et_PICKER_CLICKED:
                        break;
                    case BoEventTypes.et_PRINT:
                        break;
                    case BoEventTypes.et_PRINT_DATA:
                        break;
                    case BoEventTypes.et_PRINT_LAYOUT_KEY:
                        break;
                    case BoEventTypes.et_RIGHT_CLICK:
                        break; ;
                    case BoEventTypes.et_UDO_FORM_BUILD:
                        break;
                    case BoEventTypes.et_UDO_FORM_OPEN:
                        break;
                    case BoEventTypes.et_VALIDATE:
                        break;
                    default:
                        break;
                }
                return true;
            }
            catch (Exception ex)
            {
                if (Conexao.diCompany.InTransaction)
                    Conexao.diCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);

                throw new Exception(ex.Message);
            }
        }

        #region SALVAR
        private static void Save(ref SAPbouiCOM.Form oForm)
        {
            SAPbouiCOM.ComboBox oCombo = (SAPbouiCOM.ComboBox)oForm.Items.Item("contri").Specific;

            string data = oForm.DataSources.UserDataSources.Item("periodo").Value;
            DateTime dtInicio = Convert.ToDateTime(data + "-01");
            DateTime dtFim = Convert.ToDateTime(data + "-01").AddMonths(1).AddDays(-1);
            System.Data.DataTable dt = new System.Data.DataTable();

            try
            {
                #region INSERT [Declaracao]
                var model = new DimeDeclaracaoModel();
                model.IdDeclaracao = Convert.ToInt32(IdDeclaracao);
                model.DtInicio = Convert.ToDateTime(dtInicio);
                model.DtTermino = Convert.ToDateTime(dtFim);
                model.NomeContabilista = oForm.DataSources.UserDataSources.Item("nomeContab").Value;
                model.ContabilistaCPF = oForm.DataSources.UserDataSources.Item("cpfContab").Value;
                model.NomeContribuinte = oCombo.Selected.Description;
                model.NroInscricao = oForm.DataSources.UserDataSources.Item("IE").Value;
                model.ContribuinteCNPJ = oForm.DataSources.UserDataSources.Item("cnpj").Value;
                model.PeriodoReferencia = oForm.DataSources.UserDataSources.Item("periodo").Value;
                model.TipoDeclaracao = oForm.DataSources.UserDataSources.Item("tpDeclar").Value;
                model.RegimeApuracao = oForm.DataSources.UserDataSources.Item("regApur").Value;
                model.PorteEmpresa = oForm.DataSources.UserDataSources.Item("porteEmp").Value;
                model.ApuracaoConsolidada = oForm.DataSources.UserDataSources.Item("ApurConso").Value;
                model.ApuracaoCentralizada = oForm.DataSources.UserDataSources.Item("apurCent").Value;
                model.TransCredPeriodo = oForm.DataSources.UserDataSources.Item("transfCred").Value;
                model.TemCreditosPresumido = oForm.DataSources.UserDataSources.Item("credPres").Value;
                model.TemCredIncentFiscais = oForm.DataSources.UserDataSources.Item("credFisc").Value;
                model.SubstitutoTributario = oForm.DataSources.UserDataSources.Item("subsTrib").Value;
                model.TemEscritaContabil = oForm.DataSources.UserDataSources.Item("temEscr").Value;
                model.QtdeTrabAtividade = Convert.ToInt32(oForm.DataSources.UserDataSources.Item("qtdTrab").Value);
                model.Excluido = "N";
                model.Movimento = oForm.DataSources.UserDataSources.Item("tpMov").Value;
                model.ErpBaseDados = Conexao.diCompany.CompanyDB;

                frmfrmCadDeclarDimeDao.SaveDeclaracao(model);
                #endregion

                #region INSERT [PeriodoApuracao]

                #region GET BPLID
                string sql = string.Format(@"select BPLID from obpl where TaxIdNum = '{0}' OR TaxIdNum2 = '{0}'", oForm.DataSources.UserDataSources.Item("cnpj").Value);
                string bplid = Conexao.ExecuteSqlScalar(sql).ToString();
                #endregion

                var modelPeriodoApuracao = new DimePeriodoApuracaoModel();
                modelPeriodoApuracao.DB = Conexao.diCompany.CompanyDB.ToString();
                modelPeriodoApuracao.Filial = bplid;
                modelPeriodoApuracao.TipoReg = "DIME";
                modelPeriodoApuracao.IdDeclaracao = frmfrmCadDeclarDimeDao.IdDeclaracao();
                modelPeriodoApuracao.IdContribuinte = frmfrmCadDeclarDimeDao.IdContribuinte(oForm.DataSources.UserDataSources.Item("contri").Value);
                modelPeriodoApuracao.Periodo = oForm.DataSources.UserDataSources.Item("periodo").Value;
                modelPeriodoApuracao.Usuario = Conexao.diCompany.UserName.ToString();
                frmfrmCadDeclarDimeDao.SavePeriodoApuracao(modelPeriodoApuracao);
                #endregion

                #region INSERT [RegistroDeclaracao]

                var modelRegistroDeclaracao = new DimeRegistroDeclaracaoModel();
                foreach (var itens in frmfrmCadDeclarDimeDao.TipoRegistroList())
                {
                    modelRegistroDeclaracao.IdDeclaracao = frmfrmCadDeclarDimeDao.IdDeclaracao();
                    modelRegistroDeclaracao.TipoRegistro = itens.TipoRegistro;
                    frmfrmCadDeclarDimeDao.SaveRegistroDeclaracao(modelRegistroDeclaracao);
                }
                #endregion

                #region INSERT [ContribuinteDeclaracao]
                var modelContribuinteDeclaracao = new DimeContribuinteDeclaracaoModel();

                modelContribuinteDeclaracao.IdDeclaracao = frmfrmCadDeclarDimeDao.IdDeclaracao();
                modelContribuinteDeclaracao.IdContribuinte = Convert.ToInt32(oForm.DataSources.UserDataSources.Item("contri").Value);
                modelContribuinteDeclaracao.PeriodoReferencia = oForm.DataSources.UserDataSources.Item("periodo").Value;
                modelContribuinteDeclaracao.DtCriacao = DateTime.Now;
                modelContribuinteDeclaracao.HrCriacao = DateTime.Now.ToString("HH:mm:ss");
                frmfrmCadDeclarDimeDao.SaveContribuinteDeclaracao(modelContribuinteDeclaracao);
                #endregion
            }
            catch (SqlException erro)
            {
                throw new Exception(erro.Message);
            }
        }
        #endregion

        #region UPDATE
        private void Update(ref SAPbouiCOM.Form oForm)
        {
            try
            {
                #region UPDATE [Declaracao]

                var model = new DimeDeclaracaoModel
                {
                    IdDeclaracao = Convert.ToInt32(IdDeclaracao),
                    TipoDeclaracao = oForm.DataSources.UserDataSources.Item("tpDeclar").Value,
                    RegimeApuracao = oForm.DataSources.UserDataSources.Item("regApur").Value,
                    SubstitutoTributario = oForm.DataSources.UserDataSources.Item("subsTrib").Value,
                    TemEscritaContabil = oForm.DataSources.UserDataSources.Item("temEscr").Value,
                    QtdeTrabAtividade = Convert.ToInt32(oForm.DataSources.UserDataSources.Item("qtdTrab").Value),
                    PorteEmpresa = oForm.DataSources.UserDataSources.Item("porteEmp").Value,
                    ApuracaoCentralizada = oForm.DataSources.UserDataSources.Item("apurCent").Value,
                    TransCredPeriodo = oForm.DataSources.UserDataSources.Item("transfCred").Value,
                    TemCreditosPresumido = oForm.DataSources.UserDataSources.Item("credPres").Value,
                    TemCredIncentFiscais = oForm.DataSources.UserDataSources.Item("credFisc").Value,
                    NomeContabilista = oForm.DataSources.UserDataSources.Item("nomeContab").Value,
                    ContabilistaCPF = oForm.DataSources.UserDataSources.Item("cpfContab").Value,
                    NroInscricao = oForm.DataSources.UserDataSources.Item("IE").Value,
                    Movimento = oForm.DataSources.UserDataSources.Item("tpMov").Value
            };

                frmfrmCadDeclarDimeDao.UpdateDeclaracao(model);
                #endregion
            }
            catch (Exception ex)
            {
                throw new Exception("Erro ao Salvar: " + ex.Message);
            }
        }
        #endregion

        #region OUTROS EVENTOS
        public bool MenuEvent()
        {
            throw new NotImplementedException();
        }

        public bool FormDataEvent()
        {
            throw new NotImplementedException();
        }

        public bool AppEvent()
        {
            throw new NotImplementedException();
        }

        public bool PrintEvent()
        {
            throw new NotImplementedException();
        }

        public bool ProgressBarEvent()
        {
            throw new NotImplementedException();
        }

        public bool ReportDataEvent()
        {
            throw new NotImplementedException();
        }

        public bool RightClickEvent()
        {
            throw new NotImplementedException();
        }

        public bool StatusBarEvent()
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
