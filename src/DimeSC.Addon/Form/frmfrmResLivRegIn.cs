﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SAPbouiCOM;
using SPSFiscal.EFCodeFirst.Dao;
using SPSFiscal.Model;

namespace SPSFiscal.Addon
{
    class frmfrmResLivRegIn : IForm
    {
        private ItemEvent _eventoItem;
        public static string quadro;
        public frmfrmResLivRegIn(ItemEvent evento)
        {
            this._eventoItem = evento;
        }

        public bool ItemEvent()
        {
            try
            {
                switch (_eventoItem.EventType)
                {
                    case BoEventTypes.et_ALL_EVENTS:
                        break;
                    case BoEventTypes.et_ITEM_PRESSED:
                        #region BOTÃO CADASTRAR

                        if (!_eventoItem.Before_Action && _eventoItem.ItemUID == "1")
                        {
                            SAPbouiCOM.Form oForm = Conexao.uiApplication.Forms.Item(_eventoItem.FormUID);

                            if (quadro == "80")
                            {
                                SaveQuadro80(ref oForm);
                                frmfrmResLivRegInDao.CarregaGridQuadro80(ref oForm, frmfrmCadDeclarDime.IdDeclaracao);
                            }
                            else if (quadro == "81")
                            {
                                SaveQuadro81(ref oForm);
                                frmfrmResLivRegInDao.CarregaGridQuadro81(ref oForm, frmfrmCadDeclarDime.IdDeclaracao);
                            }
                            else if (quadro == "82")
                            {
                                SaveQuadro82(ref oForm);
                                frmfrmResLivRegInDao.CarregaGridQuadro82(ref oForm, frmfrmCadDeclarDime.IdDeclaracao);
                            }
                            else if (quadro == "83")
                            {
                                SaveQuadro83(ref oForm);
                                frmfrmResLivRegInDao.CarregaGridQuadro83(ref oForm, frmfrmCadDeclarDime.IdDeclaracao);
                            }
                            else if (quadro == "84")
                            {
                                SaveQuadro84(ref oForm);
                                frmfrmResLivRegInDao.CarregaGridQuadro84(ref oForm, frmfrmCadDeclarDime.IdDeclaracao);
                            }
                            else if (quadro == "90")
                            {
                                SaveQuadro90(ref oForm);
                                frmfrmResLivRegInDao.CarregaGridQuadro90(ref oForm, frmfrmCadDeclarDime.IdDeclaracao);
                            }
                            else if (quadro == "91")
                            {
                                SaveQuadro91(ref oForm);
                                frmfrmResLivRegInDao.CarregaGridQuadro91(ref oForm, frmfrmCadDeclarDime.IdDeclaracao);
                            }
                            else if (quadro == "92")
                            {
                                SaveQuadro92(ref oForm);
                                frmfrmResLivRegInDao.CarregaGridQuadro92(ref oForm, frmfrmCadDeclarDime.IdDeclaracao);
                            }
                            else if (quadro == "93")
                            {
                                SaveQuadro93(ref oForm);
                                frmfrmResLivRegInDao.CarregaGridQuadro93(ref oForm, frmfrmCadDeclarDime.IdDeclaracao);
                            }
                            else
                            {
                                SaveQuadro94(ref oForm);
                                frmfrmResLivRegInDao.CarregaGridQuadro94(ref oForm, frmfrmCadDeclarDime.IdDeclaracao);
                            }
                            Conexao.uiApplication.SetStatusBarMessage("Informações salvas com sucesso.", BoMessageTime.bmt_Short, false);
                        }
                        #endregion
                        break;
                    case BoEventTypes.et_KEY_DOWN:
                        break;
                    case BoEventTypes.et_GOT_FOCUS:
                        break;
                    case BoEventTypes.et_LOST_FOCUS:
                        break;
                    case BoEventTypes.et_COMBO_SELECT:
                        break;
                    case BoEventTypes.et_CLICK:
                        break;
                    case BoEventTypes.et_DOUBLE_CLICK:
                        break;
                    case BoEventTypes.et_MATRIX_LINK_PRESSED:
                        break;
                    case BoEventTypes.et_MATRIX_COLLAPSE_PRESSED:
                        break;
                    case BoEventTypes.et_VALIDATE:
                        break;
                    case BoEventTypes.et_MATRIX_LOAD:
                        break;
                    case BoEventTypes.et_DATASOURCE_LOAD:
                        break;
                    case BoEventTypes.et_FORM_LOAD:
                        break;
                    case BoEventTypes.et_FORM_UNLOAD:
                        break;
                    case BoEventTypes.et_FORM_ACTIVATE:
                        break;
                    case BoEventTypes.et_FORM_DEACTIVATE:
                        break;
                    case BoEventTypes.et_FORM_CLOSE:
                        break;
                    case BoEventTypes.et_FORM_RESIZE:
                        break;
                    case BoEventTypes.et_FORM_KEY_DOWN:
                        break;
                    case BoEventTypes.et_FORM_MENU_HILIGHT:
                        break;
                    case BoEventTypes.et_PRINT:
                        break;
                    case BoEventTypes.et_PRINT_DATA:
                        break;
                    case BoEventTypes.et_EDIT_REPORT:
                        break;
                    case BoEventTypes.et_CHOOSE_FROM_LIST:
                        break;
                    case BoEventTypes.et_RIGHT_CLICK:
                        break;
                    case BoEventTypes.et_MENU_CLICK:
                        break;
                    case BoEventTypes.et_FORM_DATA_ADD:
                        break;
                    case BoEventTypes.et_FORM_DATA_UPDATE:
                        break;
                    case BoEventTypes.et_FORM_DATA_DELETE:
                        break;
                    case BoEventTypes.et_FORM_DATA_LOAD:
                        break;
                    case BoEventTypes.et_PICKER_CLICKED:
                        break;
                    case BoEventTypes.et_GRID_SORT:
                        break;
                    case BoEventTypes.et_Drag:
                        break;
                    case BoEventTypes.et_FORM_DRAW:
                        break;
                    case BoEventTypes.et_UDO_FORM_BUILD:
                        break;
                    case BoEventTypes.et_UDO_FORM_OPEN:
                        break;
                    case BoEventTypes.et_B1I_SERVICE_COMPLETE:
                        break;
                    case BoEventTypes.et_FORMAT_SEARCH_COMPLETED:
                        break;
                    case BoEventTypes.et_PRINT_LAYOUT_KEY:
                        break;
                    case BoEventTypes.et_FORM_VISIBLE:
                        break;
                    case BoEventTypes.et_ITEM_WEBMESSAGE:
                        break;
                    default:
                        break;
                }

                return true;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        

        #region SAVE

        private void SaveQuadro80(ref Form oForm)
        {
            Grid oGrid = (Grid)oForm.Items.Item("grd").Specific;

            var model = new DimeDadosQuadro80Model();
            try
            {
                for (int i = 0; i < oGrid.Rows.Count; i++)
                {
                    model.NumeroItem = oForm.DataSources.DataTables.Item("grd").GetValue("NumeroItem", i);
                    model.Valor = Convert.ToDecimal(oForm.DataSources.DataTables.Item("grd").GetValue("Valor", i));
                    frmfrmResLivRegInDao.SaveDadosQuadro80(model);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        private void SaveQuadro81(ref Form oForm)
        {
            Grid oGrid = (Grid)oForm.Items.Item("grd").Specific;

            var model = new DimeDadosQuadro81Model();
            try
            {
                for (int i = 0; i < oGrid.Rows.Count; i++)
                {
                    model.NumeroItem = oForm.DataSources.DataTables.Item("grd").GetValue("NumeroItem", i);
                    model.Valor = Convert.ToDecimal(oForm.DataSources.DataTables.Item("grd").GetValue("Valor", i));
                    frmfrmResLivRegInDao.SaveDadosQuadro81(model);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        private void SaveQuadro82(ref Form oForm)
        {
            Grid oGrid = (Grid)oForm.Items.Item("grd").Specific;
            var model = new DimeDadosQuadro82Model();
            try
            {
                for (int i = 0; i < oGrid.Rows.Count; i++)
                {
                    model.NumeroItem = oForm.DataSources.DataTables.Item("grd").GetValue("NumeroItem", i);
                    model.Valor = Convert.ToDecimal(oForm.DataSources.DataTables.Item("grd").GetValue("Valor", i));
                    frmfrmResLivRegInDao.SaveDadosQuadro82(model);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        private void SaveQuadro83(ref Form oForm)
        {
            Grid oGrid = (Grid)oForm.Items.Item("grd").Specific;
            var model = new DimeDadosQuadro83Model();
            try
            {
                for (int i = 0; i < oGrid.Rows.Count; i++)
                {
                    model.NumeroItem = oForm.DataSources.DataTables.Item("grd").GetValue("NumeroItem", i);
                    model.Valor = Convert.ToDecimal(oForm.DataSources.DataTables.Item("grd").GetValue("Valor", i));
                    frmfrmResLivRegInDao.SaveDadosQuadro83(model);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        private void SaveQuadro84(ref Form oForm)
        {
            Grid oGrid = (Grid)oForm.Items.Item("grd").Specific;
            var model = new DimeDadosQuadro84Model();
            try
            {
                for (int i = 0; i < oGrid.Rows.Count; i++)
                {
                    model.NumeroItem = oForm.DataSources.DataTables.Item("grd").GetValue("NumeroItem", i);
                    model.Valor = Convert.ToDecimal(oForm.DataSources.DataTables.Item("grd").GetValue("Valor", i));
                    frmfrmResLivRegInDao.SaveDadosQuadro84(model);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        private void SaveQuadro90(ref Form oForm)
        {
            Grid oGrid = (Grid)oForm.Items.Item("grd").Specific;
            var model = new DimeDadosQuadro90Model();
            try
            {
                for (int i = 0; i < oGrid.Rows.Count; i++)
                {
                    model.NumeroItem = oForm.DataSources.DataTables.Item("grd").GetValue("NumeroItem", i);
                    model.Valor = Convert.ToDecimal(oForm.DataSources.DataTables.Item("grd").GetValue("Valor", i));
                    frmfrmResLivRegInDao.SaveDadosQuadro90(model);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        private void SaveQuadro91(ref Form oForm)
        {
            Grid oGrid = (Grid)oForm.Items.Item("grd").Specific;
            var model = new DimeDadosQuadro91Model();
            try
            {
                for (int i = 0; i < oGrid.Rows.Count; i++)
                {
                    model.NumeroItem = oForm.DataSources.DataTables.Item("grd").GetValue("NumeroItem", i);
                    model.Valor = Convert.ToDecimal(oForm.DataSources.DataTables.Item("grd").GetValue("Valor", i));
                    frmfrmResLivRegInDao.SaveDadosQuadro91(model);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        private void SaveQuadro92(ref Form oForm)
        {
            Grid oGrid = (Grid)oForm.Items.Item("grd").Specific;
            var model = new DimeDadosQuadro92Model();
            try
            {
                for (int i = 0; i < oGrid.Rows.Count; i++)
                {
                    model.NumeroItem = oForm.DataSources.DataTables.Item("grd").GetValue("NumeroItem", i);
                    model.Valor = Convert.ToDecimal(oForm.DataSources.DataTables.Item("grd").GetValue("Valor", i));
                    frmfrmResLivRegInDao.SaveDadosQuadro92(model);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        private void SaveQuadro93(ref Form oForm)
        {
            Grid oGrid = (Grid)oForm.Items.Item("grd").Specific;
            var model = new DimeDadosQuadro93Model();
            try
            {
                for (int i = 0; i < oGrid.Rows.Count; i++)
                {
                    model.NumeroItem = oForm.DataSources.DataTables.Item("grd").GetValue("NumeroItem", i);
                    model.Valor = Convert.ToDecimal(oForm.DataSources.DataTables.Item("grd").GetValue("Valor", i));
                    frmfrmResLivRegInDao.SaveDadosQuadro93(model);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        private void SaveQuadro94(ref Form oForm)
        {
            Grid oGrid = (Grid)oForm.Items.Item("grd").Specific;
            var model = new DimeDadosQuadro94Model();
            try
            {
                for (int i = 0; i < oGrid.Rows.Count; i++)
                {
                    model.NumeroItem = oForm.DataSources.DataTables.Item("grd").GetValue("NumeroItem", i);
                    model.Valor = Convert.ToDecimal(oForm.DataSources.DataTables.Item("grd").GetValue("Valor", i));
                    frmfrmResLivRegInDao.SaveDadosQuadro94(model);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region OUTROS EVENTOS

        public bool MenuEvent()
        {
            throw new NotImplementedException();
        }

        public bool FormDataEvent()
        {
            throw new NotImplementedException();
        }

        public bool AppEvent()
        {
            throw new NotImplementedException();
        }

        public bool PrintEvent()
        {
            throw new NotImplementedException();
        }

        public bool ProgressBarEvent()
        {
            throw new NotImplementedException();
        }

        public bool ReportDataEvent()
        {
            throw new NotImplementedException();
        }

        public bool RightClickEvent()
        {
            throw new NotImplementedException();
        }

        public bool StatusBarEvent()
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
