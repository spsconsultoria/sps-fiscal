﻿using SAPbouiCOM;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using SPSFiscal.EFCodeFirst.Dao;
using SPSFiscal.Model;

namespace SPSFiscal.Addon
{
    class frmfrmInfoRateio:IForm
    {
        public static SAPbouiCOM.Form oFormPrincipal;
        private ItemEvent _eventoItem;
        public frmfrmInfoRateio(ItemEvent evento)
        {
            this._eventoItem = evento;
        }

        public bool ItemEvent()
        {
            try
            {
                switch (_eventoItem.EventType)
                {
                    case BoEventTypes.et_ALL_EVENTS:
                        break;
                    case BoEventTypes.et_B1I_SERVICE_COMPLETE:
                        break;
                    case BoEventTypes.et_CHOOSE_FROM_LIST:
                        break;
                    case BoEventTypes.et_CLICK:
                        break;
                    case BoEventTypes.et_COMBO_SELECT:
                        break;
                    case BoEventTypes.et_DATASOURCE_LOAD:
                        break;
                    case BoEventTypes.et_DOUBLE_CLICK:
                        break;
                    case BoEventTypes.et_Drag:
                        break;
                    case BoEventTypes.et_EDIT_REPORT:
                        break;
                    case BoEventTypes.et_FORMAT_SEARCH_COMPLETED:
                        break;
                    case BoEventTypes.et_FORM_ACTIVATE:
                        break;
                    case BoEventTypes.et_FORM_CLOSE:
                        break;
                    case BoEventTypes.et_FORM_DATA_ADD:
                        break;
                    case BoEventTypes.et_FORM_DATA_DELETE:
                        break;
                    case BoEventTypes.et_FORM_DATA_LOAD:
                        break;
                    case BoEventTypes.et_FORM_DATA_UPDATE:
                        break;
                    case BoEventTypes.et_FORM_DEACTIVATE:
                        break;
                    case BoEventTypes.et_FORM_DRAW:
                        break;
                    case BoEventTypes.et_FORM_KEY_DOWN:
                        break;
                    case BoEventTypes.et_FORM_LOAD:
                        break;
                    case BoEventTypes.et_FORM_MENU_HILIGHT:
                        break;
                    case BoEventTypes.et_FORM_RESIZE:
                        break;
                    case BoEventTypes.et_FORM_UNLOAD:
                        break;
                    case BoEventTypes.et_FORM_VISIBLE:
                        break;
                    case BoEventTypes.et_GOT_FOCUS:
                        break;
                    case BoEventTypes.et_GRID_SORT:
                        break;
                    case BoEventTypes.et_ITEM_PRESSED:
                        #region BOTÃO CADASTRAR
                        if (!_eventoItem.Before_Action && _eventoItem.ItemUID == "btnCad")
                        {
                            oFormPrincipal = Conexao.uiApplication.Forms.Item(_eventoItem.FormUID);

                            #region ABRIR TELA
                            SAPbouiCOM.Form oForm = null;
                            XmlDocument xmlDoc;
                            string appPath = System.Windows.Forms.Application.StartupPath;
                            if (!appPath.EndsWith(@"\")) appPath += @"\";
                            //carregar XML
                            xmlDoc = new XmlDocument();
                            xmlDoc.Load(appPath + @"\srf\frmCadRateio.srf");
                            string strXML = xmlDoc.InnerXml;

                            SAPbouiCOM.FormCreationParams oCreationParams = ((SAPbouiCOM.FormCreationParams)(Conexao.uiApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_FormCreationParams)));
                            oCreationParams.XmlData = strXML;

                            oForm = Conexao.uiApplication.Forms.AddEx(oCreationParams);
                            #endregion

                            CarregaCombos(ref oForm);

                            frmfrmCadRateio.Id = string.Empty;
                        }
                        #endregion

                        #region BOTÃO EXCLUIR
                        if (!_eventoItem.Before_Action && _eventoItem.ItemUID == "btnDelete")
                        {
                            SAPbouiCOM.Form oForm = Conexao.uiApplication.Forms.Item(_eventoItem.FormUID);
                            Grid oGrid = (Grid)oForm.Items.Item("grd").Specific;
                            string code = string.Empty;
                            bool confirmacao = false;

                            var model = new DimeDadosQuadro48Model();
                            model.IdDeclaracao = Convert.ToInt32(frmfrmCadDeclarDime.IdDeclaracao);

                            for (int i = 0; i < oGrid.Rows.SelectedRows.Count; i++)
                            {
                                code = oForm.DataSources.DataTables.Item("grd").GetValue("#", i).ToString();
                                if (Conexao.uiApplication.MessageBox("Deseja realmente excluir?", 1, "Sim", "Não") == 1 && confirmacao == false)
                                    confirmacao = true;
                                else
                                    return true;

                                frmfrmInfoRateioDao.Delete(Convert.ToInt32(code));
                            }
                            Conexao.uiApplication.SetStatusBarMessage("Exclusão realizada com sucesso!", BoMessageTime.bmt_Short, false);

                            oForm.Freeze(true);
                            frmfrmInfoRateioDao.CarregaGrid(ref oForm, model);
                            oForm.Freeze(false);
                        }
                        #endregion
                        break;
                    case BoEventTypes.et_KEY_DOWN:
                        break;
                    case BoEventTypes.et_LOST_FOCUS:
                        break;
                    case BoEventTypes.et_MATRIX_COLLAPSE_PRESSED:
                        break;
                    case BoEventTypes.et_MATRIX_LINK_PRESSED:
                        #region LINK QUADRO
                        if (!_eventoItem.BeforeAction && _eventoItem.ColUID == "#")
                        {
                            oFormPrincipal = Conexao.uiApplication.Forms.Item(_eventoItem.FormUID);

                            Grid oGrid = (Grid)oFormPrincipal.Items.Item("grd").Specific;

                            #region ABRIR TELA
                            SAPbouiCOM.Form oForm = null;
                            XmlDocument xmlDoc;
                            string appPath = System.Windows.Forms.Application.StartupPath;
                            if (!appPath.EndsWith(@"\")) appPath += @"\";
                            //carregar XML
                            xmlDoc = new XmlDocument();
                            xmlDoc.Load(appPath + @"\srf\frmCadRateio.srf");
                            string strXML = xmlDoc.InnerXml;

                            SAPbouiCOM.FormCreationParams oCreationParams = ((SAPbouiCOM.FormCreationParams)(Conexao.uiApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_FormCreationParams)));
                            oCreationParams.XmlData = strXML;

                            oForm = Conexao.uiApplication.Forms.AddEx(oCreationParams);
                            #endregion

                            frmfrmCadRateio.Id = oFormPrincipal.DataSources.DataTables.Item("grd").GetValue("#", _eventoItem.Row).ToString();

                            CarregaCombos(ref oForm);

                            oForm.Freeze(true);
                            oForm.DataSources.UserDataSources.Item("codMun").Value = oFormPrincipal.DataSources.DataTables.Item("grd").GetValue("Cód. do Município de Santa Catarina", _eventoItem.Row);
                            oForm.DataSources.UserDataSources.Item("valor").Value = oFormPrincipal.DataSources.DataTables.Item("grd").GetValue("Valor ou Percentual", _eventoItem.Row).ToString();
                            oForm.DataSources.UserDataSources.Item("codTipo").Value = oFormPrincipal.DataSources.DataTables.Item("grd").GetValue("Cod. do Tipo de Atividade", _eventoItem.Row);
                            oForm.Freeze(false);
                        }
                        #endregion
                        break;
                    case BoEventTypes.et_MATRIX_LOAD:
                        break;
                    case BoEventTypes.et_MENU_CLICK:
                        break;
                    case BoEventTypes.et_PICKER_CLICKED:
                        break;
                    case BoEventTypes.et_PRINT:
                        break;
                    case BoEventTypes.et_PRINT_DATA:
                        break;
                    case BoEventTypes.et_PRINT_LAYOUT_KEY:
                        break;
                    case BoEventTypes.et_RIGHT_CLICK:
                        break;
                    case BoEventTypes.et_UDO_FORM_BUILD:
                        break;
                    case BoEventTypes.et_UDO_FORM_OPEN:
                        break;
                    case BoEventTypes.et_VALIDATE:
                        break;
                    default:
                        break;
                }
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region CARREGA COMBOS
        public static void CarregaCombos(ref SAPbouiCOM.Form oForm)
        {
            try
            {
                ComboBox oComboMun = (ComboBox)oForm.Items.Item("codMun").Specific;
                foreach (var itens in frmfrmInfoRateioDao.GetComboTabelaMunicipios())
                    oComboMun.ValidValues.Add(itens.Codigo, itens.NomeMunicipio);
            }
            catch {}

            try
            {
                ComboBox oComboTipo = (ComboBox)oForm.Items.Item("codTipo").Specific;
            foreach (var itens in frmfrmInfoRateioDao.GetComboTabelaCodAtividade())
                oComboTipo.ValidValues.Add(itens.Codigo, itens.Descricao);
            }
            catch { }
        }
        #endregion

        #region CARREGA GRID
        //public static void CarregaGrid(ref SAPbouiCOM.Form oForm)
        //{
        //    try
        //    {
        //        string sql = string.Format(@"SELECT [IdLancamento] as '#'
        //                                              ,[IdDeclaracao]
        //                                              ,[Cod_Municipio] as 'Cód. do Município de Santa Catarina'
        //                                              ,[Vl_Perc_Ad] as 'Valor ou Percentual'
        //                                              ,[Cod_Tipo_Atv] as 'Cod. do Tipo de Atividade'
        //                                          FROM [SPS_Fiscal].[dbo].[DIME_DadosQuadro48]
        //                                          WHERE IdDeclaracao = {0}", frmfrmCadDeclarDime.IdDeclaracao);
                
               
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(ex.Message);
        //    }
        //}
        #endregion

        #region OUTROS EVENTOS
        public bool MenuEvent()
        {
            throw new NotImplementedException();
        }

        public bool FormDataEvent()
        {
            throw new NotImplementedException();
        }

        public bool AppEvent()
        {
            throw new NotImplementedException();
        }

        public bool PrintEvent()
        {
            throw new NotImplementedException();
        }

        public bool ProgressBarEvent()
        {
            throw new NotImplementedException();
        }

        public bool ReportDataEvent()
        {
            throw new NotImplementedException();
        }

        public bool RightClickEvent()
        {
            throw new NotImplementedException();
        }

        public bool StatusBarEvent()
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
