﻿using SAPbouiCOM;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SPSFiscal.EFCodeFirst.Dao;
using SPSFiscal.Model;

namespace SPSFiscal.Addon
{
    class frmfrmLancCred : IForm
    {
        public static string Id;
        public static ItemEvent _eventoItem;
        public frmfrmLancCred(ItemEvent evento)
        {
            _eventoItem = evento;
        }

        public bool ItemEvent()
        {
            try
            {
                switch (_eventoItem.EventType)
                {
                    case BoEventTypes.et_ALL_EVENTS:
                        break;
                    case BoEventTypes.et_B1I_SERVICE_COMPLETE:
                        break;
                    case BoEventTypes.et_CHOOSE_FROM_LIST:
                        break;
                    case BoEventTypes.et_CLICK:
                        break;
                    case BoEventTypes.et_COMBO_SELECT:
                        break;
                    case BoEventTypes.et_DATASOURCE_LOAD:
                        break;
                    case BoEventTypes.et_DOUBLE_CLICK:
                        break;
                    case BoEventTypes.et_Drag:
                        break;
                    case BoEventTypes.et_EDIT_REPORT:
                        break;
                    case BoEventTypes.et_FORMAT_SEARCH_COMPLETED:
                        break;
                    case BoEventTypes.et_FORM_ACTIVATE:
                        break;
                    case BoEventTypes.et_FORM_CLOSE:
                        break;
                    case BoEventTypes.et_FORM_DATA_ADD:
                        break;
                    case BoEventTypes.et_FORM_DATA_DELETE:
                        break;
                    case BoEventTypes.et_FORM_DATA_LOAD:
                        break;
                    case BoEventTypes.et_FORM_DATA_UPDATE:
                        break;
                    case BoEventTypes.et_FORM_DEACTIVATE:
                        break;
                    case BoEventTypes.et_FORM_DRAW:
                        break;
                    case BoEventTypes.et_FORM_KEY_DOWN:
                        break;
                    case BoEventTypes.et_FORM_LOAD:
                        break;
                    case BoEventTypes.et_FORM_MENU_HILIGHT:
                        break;
                    case BoEventTypes.et_FORM_RESIZE:
                        break;
                    case BoEventTypes.et_FORM_UNLOAD:
                        break;
                    case BoEventTypes.et_FORM_VISIBLE:
                        break;
                    case BoEventTypes.et_GOT_FOCUS:
                        break;
                    case BoEventTypes.et_GRID_SORT:
                        break;
                    case BoEventTypes.et_ITEM_PRESSED:
                        #region BOTÃO CADASTRAR
                        if (!_eventoItem.Before_Action && _eventoItem.ItemUID == "btnSave")
                        {
                            SAPbouiCOM.Form oForm = Conexao.uiApplication.Forms.Item(_eventoItem.FormUID);

                            if (string.IsNullOrEmpty(Id))
                            {
                                if (frmfrmLancCredDao.Existe(oForm.DataSources.UserDataSources.Item("origem").Value))
                                    Conexao.uiApplication.SetStatusBarMessage("Lançamento já realizado para a origem selecionada", BoMessageTime.bmt_Short, true);
                                else
                                {
                                    Save(ref oForm);
                                    Conexao.uiApplication.SetStatusBarMessage("Lançamento salvo com sucesso", BoMessageTime.bmt_Short, false);
                                }
                            }
                            else
                            {
                                Update(ref oForm);
                                Conexao.uiApplication.SetStatusBarMessage("Lançamento alterado com sucesso", BoMessageTime.bmt_Short, false);
                            }
                        }
                        #endregion
                        break;
                    case BoEventTypes.et_KEY_DOWN:
                        break;
                    case BoEventTypes.et_LOST_FOCUS:
                        break;
                    case BoEventTypes.et_MATRIX_COLLAPSE_PRESSED:
                        break;
                    case BoEventTypes.et_MATRIX_LINK_PRESSED:
                        break;
                    case BoEventTypes.et_MATRIX_LOAD:
                        break;
                    case BoEventTypes.et_MENU_CLICK:
                        break;
                    case BoEventTypes.et_PICKER_CLICKED:
                        break;
                    case BoEventTypes.et_PRINT:
                        break;
                    case BoEventTypes.et_PRINT_DATA:
                        break;
                    case BoEventTypes.et_PRINT_LAYOUT_KEY:
                        break;
                    case BoEventTypes.et_RIGHT_CLICK:
                        break;
                    case BoEventTypes.et_UDO_FORM_BUILD:
                        break;
                    case BoEventTypes.et_UDO_FORM_OPEN:
                        break;
                    case BoEventTypes.et_VALIDATE:
                        break;
                    default:
                        break;
                }
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region SAVE
        private static void Save(ref SAPbouiCOM.Form oForm)
        {
            try
            {
                var model = new DimeDadosQuadro46Model
                {
                    IdDeclaracao = Convert.ToInt32(frmfrmCadDeclarDime.IdDeclaracao),
                    Cod_Identif = oForm.DataSources.UserDataSources.Item("ident").Value,
                    Vl_Cred_Apur = Convert.ToDecimal(oForm.DataSources.UserDataSources.Item("valor").Value),
                    Ind_Origem = oForm.DataSources.UserDataSources.Item("origem").Value
                };
                frmfrmLancCredDao.Save(model);

                if (oForm.DataSources.UserDataSources.Item("origem").Value == "1")
                {
                    var model46 = new DimeDadosQuadro46Model();
                    model46.IdDeclaracao = Convert.ToInt32(frmfrmCadDeclarDime.IdDeclaracao);

                    frmfrmLancCredDao.UpdateOrigem1(model46);
                }

                if (oForm.DataSources.UserDataSources.Item("origem").Value == "14")
                {
                    var model46 = new DimeDadosQuadro46Model();
                    model46.IdDeclaracao = Convert.ToInt32(frmfrmCadDeclarDime.IdDeclaracao);
                    frmfrmLancCredDao.UpdateOrigem14(model46);

                    oForm.Close();
                    frmfrmCredRegAuto.oFormPrincipal.Freeze(true);
                    frmfrmCredRegAutoDao.CarregaGrid(ref frmfrmCredRegAuto.oFormPrincipal, model);
                    frmfrmCredRegAuto.oFormPrincipal.Freeze(false);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region UPDATE
        private static void Update(ref SAPbouiCOM.Form oForm)
        {
            try
            {
                var model = new DimeDadosQuadro46Model
                {
                    IdLancamento = Convert.ToInt32(Id),
                    IdDeclaracao = Convert.ToInt32(frmfrmCadDeclarDime.IdDeclaracao),
                    Cod_Identif = oForm.DataSources.UserDataSources.Item("ident").Value,
                    Vl_Cred_Apur = Convert.ToDecimal(oForm.DataSources.UserDataSources.Item("valor").Value),
                    Ind_Origem = oForm.DataSources.UserDataSources.Item("origem").Value
                };
                frmfrmLancCredDao.Update(model);
                oForm.Close();
                
                model.IdDeclaracao = Convert.ToInt32(frmfrmCadDeclarDime.IdDeclaracao);
                frmfrmCredRegAuto.oFormPrincipal.Freeze(true);
                frmfrmCredRegAutoDao.CarregaGrid(ref frmfrmCredRegAuto.oFormPrincipal, model);
                frmfrmCredRegAuto.oFormPrincipal.Freeze(false);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region OUTROS EVENTOS
        public bool MenuEvent()
        {
            throw new NotImplementedException();
        }

        public bool FormDataEvent()
        {
            throw new NotImplementedException();
        }

        public bool AppEvent()
        {
            throw new NotImplementedException();
        }

        public bool PrintEvent()
        {
            throw new NotImplementedException();
        }

        public bool ProgressBarEvent()
        {
            throw new NotImplementedException();
        }

        public bool ReportDataEvent()
        {
            throw new NotImplementedException();
        }

        public bool RightClickEvent()
        {
            throw new NotImplementedException();
        }

        public bool StatusBarEvent()
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
