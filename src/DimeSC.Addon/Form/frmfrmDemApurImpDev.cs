﻿using SAPbouiCOM;
using SPSFiscal.EFCodeFirst.Dao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPSFiscal.Addon
{
    class frmfrmDemApurImpDev : IForm
    {
        private readonly ItemEvent itemEvento;
        public frmfrmDemApurImpDev(ItemEvent evento)
        {
            this.itemEvento = evento;
        }
        public bool ItemEvent()
        {
            try
            {
                switch (itemEvento.EventType)
                {
                    case BoEventTypes.et_ALL_EVENTS:
                        break;
                    case BoEventTypes.et_ITEM_PRESSED:
                        #region BOTÃO SALVAR
                        if (!itemEvento.BeforeAction && itemEvento.ItemUID == "btnSave")
                        {
                            var oForm = Conexao.uiApplication.Forms.ActiveForm;
                            Grid oGrid = (Grid)oForm.Items.Item("grd").Specific;

                            decimal vl1 = 0;
                            decimal vl2 = 0;
                            
                            for (int i = 0; i < oGrid.Rows.Count; i++)
                            {
                                try
                                {
                                    if (oForm.DataSources.DataTables.Item("grd").GetValue("Numero", i) == "045")
                                        vl1 = Convert.ToDecimal(oForm.DataSources.DataTables.Item("grd").GetValue("Valor", i));

                                    if (oForm.DataSources.DataTables.Item("grd").GetValue("Numero", i) == "050")
                                        vl2 = Convert.ToDecimal(oForm.DataSources.DataTables.Item("grd").GetValue("Valor", i));
                                }
                                catch {}
                            }

                            frmfrmDemApurImpDevDao.Update(vl1, vl2, Convert.ToInt32(frmfrmCadDeclarDime.IdDeclaracao));
                            Conexao.uiApplication.SetStatusBarMessage("Informações salva com sucesso!", BoMessageTime.bmt_Short, false);
                            oForm.Close();
                        }
                        #endregion
                        break;
                    case BoEventTypes.et_KEY_DOWN:
                        break;
                    case BoEventTypes.et_GOT_FOCUS:
                        break;
                    case BoEventTypes.et_LOST_FOCUS:
                        break;
                    case BoEventTypes.et_COMBO_SELECT:
                        break;
                    case BoEventTypes.et_CLICK:
                        break;
                    case BoEventTypes.et_DOUBLE_CLICK:
                        break;
                    case BoEventTypes.et_MATRIX_LINK_PRESSED:
                        break;
                    case BoEventTypes.et_MATRIX_COLLAPSE_PRESSED:
                        break;
                    case BoEventTypes.et_VALIDATE:
                        break;
                    case BoEventTypes.et_MATRIX_LOAD:
                        break;
                    case BoEventTypes.et_DATASOURCE_LOAD:
                        break;
                    case BoEventTypes.et_FORM_LOAD:
                        break;
                    case BoEventTypes.et_FORM_UNLOAD:
                        break;
                    case BoEventTypes.et_FORM_ACTIVATE:
                        break;
                    case BoEventTypes.et_FORM_DEACTIVATE:
                        break;
                    case BoEventTypes.et_FORM_CLOSE:
                        break;
                    case BoEventTypes.et_FORM_RESIZE:
                        break;
                    case BoEventTypes.et_FORM_KEY_DOWN:
                        break;
                    case BoEventTypes.et_FORM_MENU_HILIGHT:
                        break;
                    case BoEventTypes.et_PRINT:
                        break;
                    case BoEventTypes.et_PRINT_DATA:
                        break;
                    case BoEventTypes.et_EDIT_REPORT:
                        break;
                    case BoEventTypes.et_CHOOSE_FROM_LIST:
                        break;
                    case BoEventTypes.et_RIGHT_CLICK:
                        break;
                    case BoEventTypes.et_MENU_CLICK:
                        break;
                    case BoEventTypes.et_FORM_DATA_ADD:
                        break;
                    case BoEventTypes.et_FORM_DATA_UPDATE:
                        break;
                    case BoEventTypes.et_FORM_DATA_DELETE:
                        break;
                    case BoEventTypes.et_FORM_DATA_LOAD:
                        break;
                    case BoEventTypes.et_PICKER_CLICKED:
                        break;
                    case BoEventTypes.et_GRID_SORT:
                        break;
                    case BoEventTypes.et_Drag:
                        break;
                    case BoEventTypes.et_FORM_DRAW:
                        break;
                    case BoEventTypes.et_UDO_FORM_BUILD:
                        break;
                    case BoEventTypes.et_UDO_FORM_OPEN:
                        break;
                    case BoEventTypes.et_B1I_SERVICE_COMPLETE:
                        break;
                    case BoEventTypes.et_FORMAT_SEARCH_COMPLETED:
                        break;
                    case BoEventTypes.et_PRINT_LAYOUT_KEY:
                        break;
                    case BoEventTypes.et_FORM_VISIBLE:
                        break;
                    case BoEventTypes.et_ITEM_WEBMESSAGE:
                        break;
                    default:
                        break;
                }
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }    
        }

        #region OUTROS EVENTOS
        public bool AppEvent()
        {
            throw new NotImplementedException();
        }

        public bool FormDataEvent()
        {
            throw new NotImplementedException();
        }



        public bool MenuEvent()
        {
            throw new NotImplementedException();
        }

        public bool PrintEvent()
        {
            throw new NotImplementedException();
        }

        public bool ProgressBarEvent()
        {
            throw new NotImplementedException();
        }

        public bool ReportDataEvent()
        {
            throw new NotImplementedException();
        }

        public bool RightClickEvent()
        {
            throw new NotImplementedException();
        }

        public bool StatusBarEvent()
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
