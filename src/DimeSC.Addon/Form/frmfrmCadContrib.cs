﻿using SAPbouiCOM;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SPSFiscal.EFCodeFirst.Dao;
using SPSFiscal.Model;

namespace SPSFiscal.Addon
{
    class frmfrmCadContrib : IForm
    {
        private ItemEvent _eventoItem;
        public frmfrmCadContrib(ItemEvent evento)
        {
            this._eventoItem = evento;
        }

        public static string Code;

        public bool ItemEvent()
        {
            try
            {
                switch (_eventoItem.EventType)
                {
                    case BoEventTypes.et_ALL_EVENTS:
                        break;
                    case BoEventTypes.et_B1I_SERVICE_COMPLETE:
                        break;
                    case BoEventTypes.et_CHOOSE_FROM_LIST:
                        break;
                    case BoEventTypes.et_CLICK:
                        break;
                    case BoEventTypes.et_COMBO_SELECT:
                        #region COMBO FILIAL
                        if (!_eventoItem.Before_Action && _eventoItem.ItemUID == "cbFilial")
                        {
                            SAPbouiCOM.Form oForm = Conexao.uiApplication.Forms.Item(_eventoItem.FormUID);

                            CarregaCNPJ(ref oForm);
                        }
                        #endregion
                        break;
                    case BoEventTypes.et_DATASOURCE_LOAD:
                        break;
                    case BoEventTypes.et_DOUBLE_CLICK:
                        break;
                    case BoEventTypes.et_Drag:
                        break;
                    case BoEventTypes.et_EDIT_REPORT:
                        break;
                    case BoEventTypes.et_FORMAT_SEARCH_COMPLETED:
                        break;
                    case BoEventTypes.et_FORM_ACTIVATE:
                        break;
                    case BoEventTypes.et_FORM_CLOSE:
                        break;
                    case BoEventTypes.et_FORM_DATA_ADD:
                        break;
                    case BoEventTypes.et_FORM_DATA_DELETE:
                        break;
                    case BoEventTypes.et_FORM_DATA_LOAD:
                        break;
                    case BoEventTypes.et_FORM_DATA_UPDATE:
                        break;
                    case BoEventTypes.et_FORM_DEACTIVATE:
                        break;
                    case BoEventTypes.et_FORM_DRAW:
                        break;
                    case BoEventTypes.et_FORM_KEY_DOWN:
                        break;
                    case BoEventTypes.et_FORM_LOAD:
                        break;
                    case BoEventTypes.et_FORM_MENU_HILIGHT:
                        break;
                    case BoEventTypes.et_FORM_RESIZE:
                        break;
                    case BoEventTypes.et_FORM_UNLOAD:
                        break;
                    case BoEventTypes.et_FORM_VISIBLE:
                        break;
                    case BoEventTypes.et_GOT_FOCUS:
                        break;
                    case BoEventTypes.et_GRID_SORT:
                        break;
                    case BoEventTypes.et_ITEM_PRESSED:
                        #region BOTÃO SALVAR
                        if (!_eventoItem.Before_Action && _eventoItem.ItemUID == "btnSave")
                        {
                            SAPbouiCOM.Form oForm = Conexao.uiApplication.Forms.Item(_eventoItem.FormUID);
                            try
                            {
                                if (string.IsNullOrEmpty(Code))
                                {
                                    string filial = frmfrmCadContribDao.GetFilial(oForm.DataSources.UserDataSources.Item("cbFilial").Value, Convert.ToInt32(oForm.DataSources.UserDataSources.Item("contab").Value));

                                    if (string.IsNullOrEmpty(filial))
                                    {
                                        frmfrmConfigDime.oFormPrincipal.Freeze(true);
                                        Save(ref oForm, oForm.DataSources.UserDataSources.Item("cbFilial").Value);
                                        frmfrmConfigDime.oFormPrincipal.Freeze(false);
                                    }
                                    else
                                    {
                                        Conexao.uiApplication.SetStatusBarMessage("Filial já cadastrada", BoMessageTime.bmt_Short, true);
                                        return true;

                                    }
                                }
                                else
                                {
                                    frmfrmConfigDime.oFormPrincipal.Freeze(true);
                                    Update(ref oForm);
                                    frmfrmConfigDime.oFormPrincipal.Freeze(false);
                                }
                            }
                            catch (Exception ex)
                            {
                                throw new Exception(ex.Message);
                            }
                            Conexao.uiApplication.SetStatusBarMessage(
                                string.IsNullOrEmpty(Code)
                                    ? "Contribuinte Salvo com Sucesso!"
                                    : "Contribuinte Alterado com Sucesso!", BoMessageTime.bmt_Short, false);
                            frmfrmConfigDime.oFormPrincipal.Freeze(true);
                            frmfrmConfigDimeDao.CarregaGridContribuinte(ref frmfrmConfigDime.oFormPrincipal);
                            frmfrmConfigDime.oFormPrincipal.Freeze(false);
                            oForm.Close();
                        }
                        #endregion

                        #region BOTÃO EXCLUIR
                        if (!_eventoItem.Before_Action && _eventoItem.ItemUID == "btnDelete")
                        {
                            SAPbouiCOM.Form oForm = Conexao.uiApplication.Forms.Item(_eventoItem.FormUID);

                            if (Conexao.uiApplication.MessageBox("Deseja realmente excluir?", 1, "Sim", "Não") == 1)
                            {
                                try
                                {
                                    Delete(ref oForm);
                                    Conexao.uiApplication.SetStatusBarMessage("Contribuinte Excluido com Sucesso!", BoMessageTime.bmt_Short, false);
                                    frmfrmConfigDimeDao.CarregaGridContribuinte(ref frmfrmConfigDime.oFormPrincipal);
                                    oForm.Close();
                                }
                                catch (Exception ex)
                                {
                                    throw new Exception(ex.Message);
                                }
                            }
                        }
                        #endregion
                        break;
                    case BoEventTypes.et_KEY_DOWN:
                        break;
                    case BoEventTypes.et_LOST_FOCUS:
                        break;
                    case BoEventTypes.et_MATRIX_COLLAPSE_PRESSED:
                        break;
                    case BoEventTypes.et_MATRIX_LINK_PRESSED:
                        break;
                    case BoEventTypes.et_MATRIX_LOAD:
                        break;
                    case BoEventTypes.et_MENU_CLICK:
                        break;
                    case BoEventTypes.et_PICKER_CLICKED:
                        break;
                    case BoEventTypes.et_PRINT:
                        break;
                    case BoEventTypes.et_PRINT_DATA:
                        break;
                    case BoEventTypes.et_PRINT_LAYOUT_KEY:
                        break;
                    case BoEventTypes.et_RIGHT_CLICK:
                        break;
                    case BoEventTypes.et_UDO_FORM_BUILD:
                        break;
                    case BoEventTypes.et_UDO_FORM_OPEN:
                        break;
                    case BoEventTypes.et_VALIDATE:
                        break;
                    default:
                        break;
                }
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region CARREGA CNPJ
        public static void CarregaCNPJ(ref SAPbouiCOM.Form oForm)
        {
            string sql = string.Format("select TaxIdNum, TaxIdNum2, bplName from OBPL where BPLId = '{0}'", oForm.DataSources.UserDataSources.Item("cbFilial").Value);
            System.Data.DataTable dt = Conexao.ExecuteSqlDataTable(sql);
            oForm.DataSources.UserDataSources.Item("cnpj").Value = dt.Rows[0]["TaxIdNum"].ToString();
            oForm.DataSources.UserDataSources.Item("razao").Value = dt.Rows[0]["bplName"].ToString();
            oForm.DataSources.UserDataSources.Item("IE").Value = dt.Rows[0]["TaxIdNum2"].ToString();
        }
        #endregion

        #region SALVAR
        private void Save(ref SAPbouiCOM.Form oForm, string bplid)
        {
            string DB = Conexao.diCompany.CompanyDB.ToString();
            try
            {
                int qtdTrab = 0;

                qtdTrab = oForm.DataSources.UserDataSources.Item("qtdTrab").Value == "" ? 0 : Convert.ToInt32(oForm.DataSources.UserDataSources.Item("qtdTrab").Value);


                var model = new DimeContribuinteModel
                {
                    IdContabilista = Convert.ToInt32(oForm.DataSources.UserDataSources.Item("contab").Value),
                    NomeRazaoSocial = oForm.DataSources.UserDataSources.Item("razao").Value.Substring(0, 50),
                    CNPJ = oForm.DataSources.UserDataSources.Item("cnpj").Value,
                    NroInscricao = oForm.DataSources.UserDataSources.Item("IE").Value,
                    TipoDeclaracao = oForm.DataSources.UserDataSources.Item("tipoDec").Value,
                    RegimeApuracao = oForm.DataSources.UserDataSources.Item("regimeApur").Value,
                    ApuracaoConsolidada = oForm.DataSources.UserDataSources.Item("ApurCon").Value,
                    SubstitutoTributario = oForm.DataSources.UserDataSources.Item("subTrib").Value,
                    TemEscritaContabil = oForm.DataSources.UserDataSources.Item("TemEscr").Value,
                    QtdeTrabAtividade = qtdTrab,
                    ErpIdEmpresa = bplid,
                    ErpBaseDados = DB,
                    Excluido = "N"
                };

                frmfrmCadContribDao.Save(model);
            }
            catch (SqlException erro)
            {
                throw new Exception(erro.Message);
            }

        }
        #endregion

        #region ALTERAR
        private void Update(ref SAPbouiCOM.Form oForm)
        {
            try
            {
                string DB = Conexao.diCompany.CompanyDB.ToString();
                int qtdTrab = oForm.DataSources.UserDataSources.Item("qtdTrab").Value == "" 
                    ? 0 
                    : Convert.ToInt32(oForm.DataSources.UserDataSources.Item("qtdTrab").Value);

                var model = new DimeContribuinteModel
                {
                    IdContabilista = Convert.ToInt32(oForm.DataSources.UserDataSources.Item("contab").Value),
                    NomeRazaoSocial = oForm.DataSources.UserDataSources.Item("razao").Value.Substring(0, 50),
                    CNPJ = oForm.DataSources.UserDataSources.Item("cnpj").Value,
                    NroInscricao = oForm.DataSources.UserDataSources.Item("IE").Value,
                    TipoDeclaracao = oForm.DataSources.UserDataSources.Item("tipoDec").Value,
                    RegimeApuracao = oForm.DataSources.UserDataSources.Item("regimeApur").Value,
                    ApuracaoConsolidada = oForm.DataSources.UserDataSources.Item("ApurCon").Value,
                    SubstitutoTributario = oForm.DataSources.UserDataSources.Item("subTrib").Value,
                    TemEscritaContabil = oForm.DataSources.UserDataSources.Item("TemEscr").Value,
                    QtdeTrabAtividade = qtdTrab,
                    ErpIdEmpresa = oForm.DataSources.UserDataSources.Item("cbFilial").Value,
                    ErpBaseDados = DB,
                    Excluido = "N"
                };
            }
            catch (SqlException erro)
            {
                throw new Exception(erro.Message);
            }
            finally
            {
                Support.Conectar().Close();
            }
        }
        #endregion

        #region DELETE
        private void Delete(ref SAPbouiCOM.Form oForm)
        {
            try
            {
                var model = new DimeContribuinteModel {Excluido = "Sim"};
                frmfrmCadContribDao.Delete(model);
            }
            catch (SqlException erro)
            {
                throw new Exception(erro.Message);
            }
        }
        #endregion

        #region OUTROS EVENTOS
        public bool MenuEvent()
        {
            throw new NotImplementedException();
        }

        public bool FormDataEvent()
        {
            throw new NotImplementedException();
        }

        public bool AppEvent()
        {
            throw new NotImplementedException();
        }

        public bool PrintEvent()
        {
            throw new NotImplementedException();
        }

        public bool ProgressBarEvent()
        {
            throw new NotImplementedException();
        }

        public bool ReportDataEvent()
        {
            throw new NotImplementedException();
        }

        public bool RightClickEvent()
        {
            throw new NotImplementedException();
        }

        public bool StatusBarEvent()
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
