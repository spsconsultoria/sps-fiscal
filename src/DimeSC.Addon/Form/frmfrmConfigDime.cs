﻿using SAPbouiCOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Data.SqlClient;

namespace SPSFiscal.Addon
{
    class frmfrmConfigDime : IForm
    {
        private ItemEvent _eventoItem;
        public static SAPbouiCOM.Form oFormPrincipal;
        public frmfrmConfigDime(ItemEvent evento)
        {
            this._eventoItem = evento;
        }
        public bool ItemEvent()
        {
            try
            {
                switch (_eventoItem.EventType)
                {
                    case BoEventTypes.et_ALL_EVENTS:
                        break;
                    case BoEventTypes.et_B1I_SERVICE_COMPLETE:
                        break;
                    case BoEventTypes.et_CHOOSE_FROM_LIST:
                        break;
                    case BoEventTypes.et_CLICK:
                        break;
                    case BoEventTypes.et_COMBO_SELECT:
                        break;
                    case BoEventTypes.et_DATASOURCE_LOAD:
                        break;
                    case BoEventTypes.et_DOUBLE_CLICK:
                        break;
                    case BoEventTypes.et_Drag:
                        break;
                    case BoEventTypes.et_EDIT_REPORT:
                        break;
                    case BoEventTypes.et_FORMAT_SEARCH_COMPLETED:
                        break;
                    case BoEventTypes.et_FORM_ACTIVATE:
                        break;
                    case BoEventTypes.et_FORM_CLOSE:
                        break;
                    case BoEventTypes.et_FORM_DATA_ADD:
                        break;
                    case BoEventTypes.et_FORM_DATA_DELETE:
                        break;
                    case BoEventTypes.et_FORM_DATA_LOAD:
                        break;
                    case BoEventTypes.et_FORM_DATA_UPDATE:
                        break;
                    case BoEventTypes.et_FORM_DEACTIVATE:
                        break;
                    case BoEventTypes.et_FORM_DRAW:
                        break;
                    case BoEventTypes.et_FORM_KEY_DOWN:
                        break;
                    case BoEventTypes.et_FORM_LOAD:
                        break;
                    case BoEventTypes.et_FORM_MENU_HILIGHT:
                        break;
                    case BoEventTypes.et_FORM_RESIZE:
                        break;
                    case BoEventTypes.et_FORM_UNLOAD:
                        break;
                    case BoEventTypes.et_FORM_VISIBLE:
                        break;
                    case BoEventTypes.et_GOT_FOCUS:
                        break;
                    case BoEventTypes.et_GRID_SORT:
                        break;
                    case BoEventTypes.et_ITEM_PRESSED:
                        #region BOTÃO NOVO CONTABILISTA
                        if (!_eventoItem.Before_Action && _eventoItem.ItemUID == "btnNContab")
                        {
                            oFormPrincipal = Conexao.uiApplication.Forms.Item(_eventoItem.FormUID);

                            #region ABRIR TELA
                            SAPbouiCOM.Form oForm = null;
                            XmlDocument xmlDoc;
                            string appPath = System.Windows.Forms.Application.StartupPath;
                            if (!appPath.EndsWith(@"\")) appPath += @"\";
                            //carregar XML
                            xmlDoc = new XmlDocument();
                            xmlDoc.Load(appPath + @"\srf\frmCadContab.srf");
                            string strXML = xmlDoc.InnerXml;

                            SAPbouiCOM.FormCreationParams oCreationParams = ((SAPbouiCOM.FormCreationParams)(Conexao.uiApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_FormCreationParams)));
                            oCreationParams.XmlData = strXML;

                            oForm = Conexao.uiApplication.Forms.AddEx(oCreationParams);

                            #endregion

                            frmfrmCadContab.Code = "";

                            oForm.Freeze(true);
                            oForm.Items.Item("btnDelete").Visible = false;
                            oForm.Freeze(false);
                        }
                        #endregion

                        #region BOTÃO NOVO CONTRIBUINTE
                        if (!_eventoItem.Before_Action && _eventoItem.ItemUID == "btnNContri")
                        {
                            oFormPrincipal = Conexao.uiApplication.Forms.Item(_eventoItem.FormUID);

                            #region ABRIR TELA
                            SAPbouiCOM.Form oForm = null;
                            XmlDocument xmlDoc;
                            string appPath = System.Windows.Forms.Application.StartupPath;
                            if (!appPath.EndsWith(@"\")) appPath += @"\";
                            //carregar XML
                            xmlDoc = new XmlDocument();
                            xmlDoc.Load(appPath + @"\srf\frmCadContrib.srf");
                            string strXML = xmlDoc.InnerXml;

                            SAPbouiCOM.FormCreationParams oCreationParams = ((SAPbouiCOM.FormCreationParams)(Conexao.uiApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_FormCreationParams)));
                            oCreationParams.XmlData = strXML;

                            oForm = Conexao.uiApplication.Forms.AddEx(oCreationParams);

                            #endregion

                            oForm.Freeze(true);
                            Support.AdicionarComboFiliaisOBPL(ref oForm);
                            Support.CarregaContabilista(ref oForm);
                            oForm.Items.Item("btnDelete").Visible = false;
                            oForm.Freeze(false);
                            frmfrmCadContrib.Code = string.Empty;
                        }
                        #endregion

                        #region BOTÃO SAIR
                        if (!_eventoItem.Before_Action && _eventoItem.ItemUID == "btnSair")
                        {
                            oFormPrincipal = Conexao.uiApplication.Forms.Item(_eventoItem.FormUID);

                            oFormPrincipal.Close();
                            Support.CarregaContribuinte(ref frmfrmAdmDecDime.oFormPrincipal, true);
                        }
                        #endregion
                        break;
                    case BoEventTypes.et_KEY_DOWN:
                        break;
                    case BoEventTypes.et_LOST_FOCUS:
                        break;
                    case BoEventTypes.et_MATRIX_COLLAPSE_PRESSED:
                        break;
                    case BoEventTypes.et_MATRIX_LINK_PRESSED:
                        #region LINK CÓDIGO CONTABILISTA
                        if (!_eventoItem.BeforeAction && _eventoItem.ColUID == "#" && _eventoItem.ItemUID == "grdContab")
                        {
                            oFormPrincipal = Conexao.uiApplication.Forms.Item(_eventoItem.FormUID);

                            #region ABRIR TELA
                            SAPbouiCOM.Form oForm = null;
                            XmlDocument xmlDoc;
                            string appPath = System.Windows.Forms.Application.StartupPath;
                            if (!appPath.EndsWith(@"\")) appPath += @"\";
                            //carregar XML
                            xmlDoc = new XmlDocument();
                            xmlDoc.Load(appPath + @"\srf\frmCadContab.srf");
                            string strXML = xmlDoc.InnerXml;

                            SAPbouiCOM.FormCreationParams oCreationParams = ((SAPbouiCOM.FormCreationParams)(Conexao.uiApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_FormCreationParams)));
                            oCreationParams.XmlData = strXML;

                            oForm = Conexao.uiApplication.Forms.AddEx(oCreationParams);
                            //oForm.AutoManaged = true;
                            #endregion

                            oForm.Freeze(true);

                            frmfrmCadContab.Code = "";

                            frmfrmCadContab.Code = oFormPrincipal.DataSources.DataTables.Item("grdContab").GetValue("#", _eventoItem.Row).ToString();
                            oForm.DataSources.UserDataSources.Item("cpf").Value = oFormPrincipal.DataSources.DataTables.Item("grdContab").GetValue("CPF", _eventoItem.Row).ToString();
                            oForm.DataSources.UserDataSources.Item("Nome").Value = oFormPrincipal.DataSources.DataTables.Item("grdContab").GetValue("Nome Contabilista", _eventoItem.Row).ToString();

                            oForm.Freeze(false);
                        }
                        #endregion

                        #region LINK CÓDIGO CONTRIBUINTE
                        if (!_eventoItem.BeforeAction && _eventoItem.ColUID == "#" && _eventoItem.ItemUID == "grdContri")
                        {
                            oFormPrincipal = Conexao.uiApplication.Forms.Item(_eventoItem.FormUID);

                            #region ABRIR TELA
                            SAPbouiCOM.Form oForm = null;
                            XmlDocument xmlDoc;
                            string appPath = System.Windows.Forms.Application.StartupPath;
                            if (!appPath.EndsWith(@"\")) appPath += @"\";
                            //carregar XML
                            xmlDoc = new XmlDocument();
                            xmlDoc.Load(appPath + @"\srf\frmCadContrib.srf");
                            string strXML = xmlDoc.InnerXml;

                            SAPbouiCOM.FormCreationParams oCreationParams = ((SAPbouiCOM.FormCreationParams)(Conexao.uiApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_FormCreationParams)));
                            oCreationParams.XmlData = strXML;

                            oForm = Conexao.uiApplication.Forms.AddEx(oCreationParams);

                            #endregion

                            Support.AdicionarComboFiliaisOBPL(ref oForm);
                            Support.CarregaContabilista(ref oForm);

                            oForm.Freeze(true);

                            frmfrmCadContrib.Code = "";

                            frmfrmCadContrib.Code = oFormPrincipal.DataSources.DataTables.Item("grdContri").GetValue("#", _eventoItem.Row).ToString();
                            oForm.DataSources.UserDataSources.Item("cbFilial").Value = oFormPrincipal.DataSources.DataTables.Item("grdContri").GetValue("Nome ou Razão Social", _eventoItem.Row).ToString();
                            oForm.DataSources.UserDataSources.Item("razao").Value = oFormPrincipal.DataSources.DataTables.Item("grdContri").GetValue("Nome ou Razão Social", _eventoItem.Row).ToString();
                            oForm.DataSources.UserDataSources.Item("cnpj").Value = oFormPrincipal.DataSources.DataTables.Item("grdContri").GetValue("CNPJ", _eventoItem.Row).ToString();
                            oForm.DataSources.UserDataSources.Item("IE").Value = oFormPrincipal.DataSources.DataTables.Item("grdContri").GetValue("NroInscricao", _eventoItem.Row).ToString();
                            oForm.DataSources.UserDataSources.Item("contab").Value = oFormPrincipal.DataSources.DataTables.Item("grdContri").GetValue("IdContabilista", _eventoItem.Row).ToString();
                            oForm.DataSources.UserDataSources.Item("tipoDec").Value = oFormPrincipal.DataSources.DataTables.Item("grdContri").GetValue("TipoDeclaracao", _eventoItem.Row).ToString();
                            oForm.DataSources.UserDataSources.Item("regimeApur").Value = oFormPrincipal.DataSources.DataTables.Item("grdContri").GetValue("Regime Apuração", _eventoItem.Row).ToString();
                            oForm.DataSources.UserDataSources.Item("ApurCon").Value = oFormPrincipal.DataSources.DataTables.Item("grdContri").GetValue("ApuracaoConsolidada", _eventoItem.Row).ToString();
                            oForm.DataSources.UserDataSources.Item("subTrib").Value = oFormPrincipal.DataSources.DataTables.Item("grdContri").GetValue("Substituto Tributário", _eventoItem.Row).ToString();
                            oForm.DataSources.UserDataSources.Item("TemEscr").Value = oFormPrincipal.DataSources.DataTables.Item("grdContri").GetValue("TemEscritaContabil", _eventoItem.Row).ToString();
                            oForm.DataSources.UserDataSources.Item("qtdTrab").Value = oFormPrincipal.DataSources.DataTables.Item("grdContri").GetValue("QtdeTrabAtividade", _eventoItem.Row).ToString();

                            oForm.Items.Item("qtdTrab").Click();
                            oForm.Items.Item("cbFilial").Enabled = false;
                            oForm.Items.Item("cnpj").Enabled = false;
                            oForm.Items.Item("razao").Enabled = false;
                            oForm.Items.Item("IE").Enabled = false;

                            oForm.Freeze(false);
                        }
                        #endregion
                        break;
                    case BoEventTypes.et_MATRIX_LOAD:
                        break;
                    case BoEventTypes.et_MENU_CLICK:
                        break;
                    case BoEventTypes.et_PICKER_CLICKED:
                        break;
                    case BoEventTypes.et_PRINT:
                        break;
                    case BoEventTypes.et_PRINT_DATA:
                        break;
                    case BoEventTypes.et_PRINT_LAYOUT_KEY:
                        break;
                    case BoEventTypes.et_RIGHT_CLICK:
                        break;
                    case BoEventTypes.et_UDO_FORM_BUILD:
                        break;
                    case BoEventTypes.et_UDO_FORM_OPEN:
                        break;
                    case BoEventTypes.et_VALIDATE:
                        break;
                    default:
                        break;
                }
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region CARREGA GRID CONTABILISTA
        //public static void carregaGridContabilista(ref SAPbouiCOM.Form oForm)
        //{
        //    try
        //    {
        //        string sql = (@"SELECT [IdContabilista] as '#',
        //                                                    [CPF] as 'CPF',
        //                                                    [NomeContabilista] as 'Nome do Contabilista'
        //                                                    FROM [SPS_Fiscal].[dbo].[DIME_Contabilista]
        //                                                    WHERE [Excluido] = 'N'");
        //        oForm.DataSources.DataTables.Item("grdContab").ExecuteQuery(sql);
        //        (((SAPbouiCOM.EditTextColumn)(((Grid)oForm.Items.Item("grdContab").Specific).Columns.Item("#")))).LinkedObjectType = "BOY_IPT";
        //        ((Grid)oForm.Items.Item("grdContab").Specific).AutoResizeColumns();
        //        ((Grid)oForm.Items.Item("grdContab").Specific).SelectionMode = BoMatrixSelect.ms_Auto;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(ex.Message);
        //    }
        //}
        #endregion

        #region CARREGA GRID CONTRIBUINTE
        //public static void carregaGridContribuinte(ref SAPbouiCOM.Form oForm)
        //{
        //    try
        //    {
        //        string sql = (@"SELECT [IdContribuinte] as '#'
        //                              ,[IdContabilista]
        //                              ,[NomeRazaoSocial] as 'Nome ou Razão Social'
        //                              ,[CNPJ]
        //                              ,[NroInscricao]
        //                              ,[TipoDeclaracao]
        //                              ,[RegimeApuracao] as 'Regime Apuração'
        //                              ,[PorteEmpresa]
        //                              ,[ApuracaoConsolidada]
        //                              ,[ApuracaoCentralizada]
        //                              ,[TemCreditosPresumido]
        //                              ,[TemCredIncentFiscais]
        //                              ,[SubstitutoTributario] as 'Substituto Tributário'
        //                              ,[TemEscritaContabil]
        //                              ,[QtdeTrabAtividade]
        //                              ,[ErpIdEmpresa]
        //                              ,[ErpBaseDados]
        //                              ,[Excluido]
        //                                FROM [SPS_Fiscal].[dbo].[DIME_Contribuinte]
        //                                WHERE [Excluido] = 'N'");
        //        oForm.DataSources.DataTables.Item("grdContri").ExecuteQuery(sql);
        //        ((Grid)oForm.Items.Item("grdContri").Specific).Columns.Item("IdContabilista").Visible = false;
        //        ((Grid)oForm.Items.Item("grdContri").Specific).Columns.Item("CNPJ").Visible = false;
        //        ((Grid)oForm.Items.Item("grdContri").Specific).Columns.Item("NroInscricao").Visible = false;
        //        ((Grid)oForm.Items.Item("grdContri").Specific).Columns.Item("TipoDeclaracao").Visible = false;
        //        ((Grid)oForm.Items.Item("grdContri").Specific).Columns.Item("PorteEmpresa").Visible = false;
        //        ((Grid)oForm.Items.Item("grdContri").Specific).Columns.Item("ApuracaoConsolidada").Visible = false;
        //        ((Grid)oForm.Items.Item("grdContri").Specific).Columns.Item("ApuracaoCentralizada").Visible = false;
        //        ((Grid)oForm.Items.Item("grdContri").Specific).Columns.Item("TemCreditosPresumido").Visible = false;
        //        ((Grid)oForm.Items.Item("grdContri").Specific).Columns.Item("TemCredIncentFiscais").Visible = false;
        //        ((Grid)oForm.Items.Item("grdContri").Specific).Columns.Item("TemEscritaContabil").Visible = false;
        //        ((Grid)oForm.Items.Item("grdContri").Specific).Columns.Item("QtdeTrabAtividade").Visible = false;
        //        ((Grid)oForm.Items.Item("grdContri").Specific).Columns.Item("ErpIdEmpresa").Visible = false;
        //        ((Grid)oForm.Items.Item("grdContri").Specific).Columns.Item("ErpBaseDados").Visible = false;
        //        ((Grid)oForm.Items.Item("grdContri").Specific).Columns.Item("Excluido").Visible = false;
        //        (((SAPbouiCOM.EditTextColumn)(((Grid)oForm.Items.Item("grdContri").Specific).Columns.Item("#")))).LinkedObjectType = "BOY_IPT";
        //        ((Grid)oForm.Items.Item("grdContri").Specific).AutoResizeColumns();
        //        ((Grid)oForm.Items.Item("grdContri").Specific).SelectionMode = BoMatrixSelect.ms_Auto;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(ex.Message);
        //    }
        //}
        #endregion

        #region OUTROS EVENTOS
        public bool MenuEvent()
        {
            throw new NotImplementedException();
        }

        public bool FormDataEvent()
        {
            throw new NotImplementedException();
        }

        public bool AppEvent()
        {
            throw new NotImplementedException();
        }

        public bool PrintEvent()
        {
            throw new NotImplementedException();
        }

        public bool ProgressBarEvent()
        {
            throw new NotImplementedException();
        }

        public bool ReportDataEvent()
        {
            throw new NotImplementedException();
        }

        public bool RightClickEvent()
        {
            throw new NotImplementedException();
        }

        public bool StatusBarEvent()
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
