﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using SPSFiscal.EFCodeFirst;

namespace SPSFiscal.Addon
{
    public static class DB
    {
        public static void CriaDB()
        {
            try
            {
                Help.Migration();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static void CriaProc()
        {
            SqlCommand command = new SqlCommand();
            #region CRIANDO PROCEDURES

            command = new System.Data.SqlClient.SqlCommand(@"IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'SP_SPSQuadros')
                                                                            DROP PROCEDURE [dbo].[SP_SPSQuadros]

                                                                                    IF EXISTS(SELECT * FROM sys.objects WHERE type = 'P' AND name = 'SP_SPSLivros')
                                                                            DROP PROCEDURE [dbo].[SP_SPSLivros]", Support.Conectar());
            command.ExecuteNonQuery();

            command = new SqlCommand(@"SELECT Name FROM SYSOBJECTS WHERE NAME = 'SP_SPSLivros'", Support.Conectar());
            int nome = Convert.ToInt32(command.ExecuteScalar());
            if (nome == 0)
            {
                #region QUERY DE CRIAÇÃO DA PROCEDURE
                command = new SqlCommand(@"CREATE PROCEDURE [dbo].[SP_SPSLivros] ( 
	@dataDe datetime, 
	@dataAte datetime, 
	@BPLID NVARCHAR(4) , 
	@IdPeriodo NVARCHAR(MAX),
	@BD NVARCHAR(50) )
--[SPS_Fiscal].[dbo].[SP_SPSLivros] '20170201','20170228',3,2, 'SBO_GPO_KIKOS'

--SELECT * FROM [SPS_Fiscal].[dbo].[EntradaP1] where idPeriodo = 2 ORDER BY DataLanc, DataDoc, Convert(Int, Serial)
--SELECT * FROM [SPS_Fiscal].[dbo].[SaidasP2] where idPeriodo = 2

as
begin

DECLARE @Cod int = 1
Declare @Tab nvarchar(3)
Declare @Array Table (Codigo Char(2), Nome nvarchar(3), Tipo nvarchar(30))

SET NOCOUNT ON;

-- Inserindo Colunas para o laço

Insert @Array Values('1','PCH','NF Entrada')
Insert @Array Values('2','PDN','Receb Merc')
Insert @Array Values('3','RDN','Devolucao')
Insert @Array Values('4','RIN','Dev. Saida')
Insert @Array Values('5','INV','NF Saida')
Insert @Array Values('6','DLN','Enrega')
Insert @Array Values('7','RPD','Dev. Mercadoria')
Insert @Array Values('8','RPC','Dev. Entrada')

/******************************************************************************************************
	INICIO DA APURAÇÂO ICMS
******************************************************************************************************/
/*
------------------
	ENTRADAS
------------------
*/
Declare @Entrada01 nvarchar(max)
Declare @Entrada02 nvarchar(max)

PRINT ('ENTRADA')

CREATE TABLE #ENTRADA ( IDPeriodo NVARCHAR(MAX), BD NVARCHAR(50) NULL, Filial NVARCHAR(3) NULL,TPDoc NVARCHAR(3) NULL,DatadeEntrada date NULL, Especie NVARCHAR(5) NULL, SerieSubSerie nVARCHAR(15) NULL, 
						Serial nVARCHAR(30) NULL, DataDoc date NULL, Emitente nVARCHAR(254) NULL,IE nVARCHAR(30) NULL,
						CNPJ nVARCHAR(30) NULL,State nVARCHAR(3) NULL,ValorContabil numeric (18,2) NULL, CFOPCode NVARCHAR(5) NULL,
						BaseCalculo numeric (18,2) NULL,Aliq numeric (19,6) NULL, TaxSum numeric (18,2) NULL, BaseIsenta numeric (18,2) NULL,
						BaseOutras numeric (18,2) NULL, BaseST numeric (18,2) NULL, ValorST numeric (18,2) NULL)

WHILE  @Cod < 5
Begin
SET @Tab = (Select Nome From @Array where codigo=@Cod)  

SET @Entrada01 = '
INSERT INTO #ENTRADA
SELECT '''+@IdPeriodo+''' AS IDPeriodo, '''+@BD+''' AS DB, '''+@BPLId+''' AS Filial, '''+@Tab+''' AS TPDoc, T0.DocDate AS ''DatadeEntrada'',  
	CASE WHEN T0.Model = 39 THEN ''NFe'' WHEN T0.Model = 44 Then ''CT-e'' WHEN T0.Model = 51 Then ''08'' WHEN T0.Model = 19 Then ''22'' ELSE ''NFEE'' END AS Especie, 
	isNULL(T0.SeriesStr,'''') AS ''SerieSubSerie'', 
	T0.Serial, 
	T0.TaxDate AS ''DataDoc'', 
	T0.CardName AS ''Emitente'',
	isNULL(T2.TaxId1,'''') AS ''IE'',
	isNULL(T2.TaxId0,'''') AS ''CNPJ'',
	T2.State, 
	isNULL(T1.LineTotal,0.0) ValorContabil, 
	T1.CFOPCode, 
	isNULL(T4.BaseSum,0.0) BaseCalculo, 
	isNULL(T4.TaxRate,0.0) AS Aliq,
	isNULL(T4.TaxSum,0.0) TaxSum, 
	CASE WHEN T4.U_Isento = 100 THEN isNULL(T4.U_OthAmtL,0.0) ELSE 0.0 END BaseIsenta, 
	CASE WHEN T4.U_Outros = 100 THEN isNULL(T4.U_OthAmtL,0.0) ELSE 0.0 END BaseOutras,
	BaseST = isNULL((SELECT SUM(V4.BaseSum) FROM '+@BD+'.[dbo].['+@Tab+'4] V4 WHERE T0.DocEntry = V4.DocEntry  AND V4.RelateType = 1 AND T1.LineNum = V4.LineNum AND V4.staType in (SELECT TT.AbsId FROM '+@BD+'.[dbo].[OSTT] TT INNER JOIN '+@BD+'.[dbo].[ONFT] FT ON TT.NfTaxId = FT.AbsId WHERE Code = ''ICMS-ST'')), 0.0),
	ValorST = isNULL((SELECT SUM(V4.TaxSum) FROM '+@BD+'.[dbo].['+@Tab+'4] V4 WHERE T0.DocEntry = V4.DocEntry  AND V4.RelateType = 1 AND T1.LineNum = V4.LineNum AND V4.staType in (SELECT TT.AbsId FROM '+@BD+'.[dbo].[OSTT] TT INNER JOIN '+@BD+'.[dbo].[ONFT] FT ON TT.NfTaxId = FT.AbsId WHERE Code = ''ICMS-ST'')), 0.0)
FROM '+@BD+'.[dbo].[O'+@Tab+'] T0 
	INNER JOIN '+@BD+'.[dbo].['+@Tab+'1] T1 ON T0.Docentry = T1.DocEntry 
	INNER JOIN '+@BD+'.[dbo].['+@Tab+'12] T2 ON T0.DocEntry = T2.DocEntry
	INNER JOIN '+@BD+'.[dbo].['+@Tab+'4] T4 ON T0.DocEntry = T4.DocEntry AND T4. RelateType = 1 AND T1.LineNum = T4.LineNum 
		AND T4.staType in (SELECT TT.AbsId FROM '+@BD+'.[dbo].[OSTT] TT INNER JOIN '+@BD+'.[dbo].[ONFT] FT ON TT.NfTaxId = FT.AbsId WHERE Code = ''ICMS'')
WHERE Model in (39, 6, 44, 51, 19) AND BPLID = '+@BPLId+' AND CANCELED = ''N'' 
	AND T0.DocDate between convert(datetime,'''+convert(nvarchar,@datade,102)+''') and convert(datetime,'''+convert(nvarchar,@DataAte,102)+''')
'
/** ADIANTAMENTO **/
SET @Entrada02 = ' INSERT INTO #ENTRADA
SELECT '''+@IdPeriodo+''' AS IDPeriodo, '''+@BD+''' AS DB, '''+@BPLId+''' AS Filial, '''+@Tab+''' AS TPDoc, T0.DocDate AS ''DatadeEntrada'',  
	CASE WHEN T0.Model = 39 THEN ''NFe'' WHEN T0.Model = 44 Then ''CT-e'' WHEN T0.Model = 51 Then ''08'' WHEN T0.Model = 19 Then ''22'' ELSE ''NFEE'' END AS Especie, 
	isNULL(T0.SeriesStr,'''') AS ''SerieSubSerie'', 
	T0.Serial, 
	T0.TaxDate AS ''DataDoc'', 
	T0.CardName AS ''Emitente'',
	isNULL(T2.TaxId1,'''') AS ''IE'',
	isNULL(T2.TaxId0,'''') AS ''CNPJ'',
	isNULL(T2.State,''''), 
	isNULL(T1.DistribSum,0.0) ValorContabil, 
	T1.CFOPCode, 
	isNULL(T4.BaseSum,0.0) BaseCalculo, 
	isNULL(T4.TaxRate,0.0) AS Aliq,
	isNULL(T4.TaxSum,0.0) TaxSum, 
	CASE WHEN T4.U_Isento = 100 THEN isNULL(T4.U_OthAmtL,0.0) ELSE 0.0 END BaseIsenta, 
	CASE WHEN T4.U_Outros = 100 THEN isNULL(T4.U_OthAmtL,0.0) ELSE 0.0 END BaseOutras,
	BaseST = isNULL((SELECT SUM(V4.BaseSum) FROM '+@BD+'.[dbo].['+@Tab+'4] V4 WHERE T0.DocEntry = V4.DocEntry  AND V4.RelateType = 1 AND T1.LineNum = V4.LineNum AND V4.staType in (SELECT TT.AbsId FROM '+@BD+'.[dbo].[OSTT] TT INNER JOIN '+@BD+'.[dbo].[ONFT] FT ON TT.NfTaxId = FT.AbsId WHERE Code = ''ICMS-ST'')), 0.0),
	ValorST = isNULL((SELECT SUM(V4.TaxSum) FROM '+@BD+'.[dbo].['+@Tab+'4] V4 WHERE T0.DocEntry = V4.DocEntry  AND V4.RelateType = 1 AND T1.LineNum = V4.LineNum AND V4.staType in (SELECT TT.AbsId FROM '+@BD+'.[dbo].[OSTT] TT INNER JOIN '+@BD+'.[dbo].[ONFT] FT ON TT.NfTaxId = FT.AbsId WHERE Code = ''ICMS-ST'')), 0.0)
FROM '+@BD+'.[dbo].[O'+@Tab+'] T0 
	INNER JOIN '+@BD+'.[dbo].['+@Tab+'1] T1 ON T0.Docentry = T1.DocEntry 
	INNER JOIN '+@BD+'.[dbo].['+@Tab+'12] T2 ON T0.DocEntry = T2.DocEntry
	INNER JOIN '+@BD+'.[dbo].['+@Tab+'4] T4 ON T0.DocEntry = T4.DocEntry AND T4. RelateType = 13 AND T1.LineNum = T4.LineNum 
		AND T4.staType in (SELECT TT.AbsId FROM '+@BD+'.[dbo].[OSTT] TT INNER JOIN '+@BD+'.[dbo].[ONFT] FT ON TT.NfTaxId = FT.AbsId WHERE Code = ''ICMS'')
WHERE Model in (39, 6, 44, 51, 19) AND BPLID = '+@BPLId+' AND CANCELED = ''N'' 
	AND T0.DocDate between convert(datetime,'''+convert(nvarchar,@datade,102)+''') and convert(datetime,'''+convert(nvarchar,@DataAte,102)+''')
'

--print @Entrada01
--print @Entrada02
exec sp_executesql @Entrada01
exec sp_executesql @Entrada02

SET @Cod = @Cod + 1

END

/*
------------------
	SAIDAS
------------------
*/

PRINT ('SAIDA')

Declare @SAIDA01 nvarchar(max)
Declare @SAIDA02 nvarchar(max)

CREATE TABLE #SAIDA ( IDPeriodo NVARCHAR(MAX), BD NVARCHAR(50) NULL, Filial NVARCHAR(3) NULL, TPDoc NVARCHAR(3) NULL,Especie NVARCHAR(5) NULL, SerieSubSerie nVARCHAR(15) NULL, 
						Serial nVARCHAR(30) NULL, DataDoc nVARCHAR(3) NULL, UFDest nVARCHAR(3) NULL,ValorContabil numeric (18,2) NULL, CFOPCode NVARCHAR(5) NULL,
						BaseCalculo numeric (18,2) NULL,Aliq numeric (19,6) NULL, TaxSum numeric (18,2) NULL, BaseIsenta numeric (18,2) NULL,
						BaseOutras numeric (18,2) NULL, BaseST numeric (18,2) NULL, ValorST numeric (18,2) NULL)

SET @Cod = 5
WHILE @Cod < 9
Begin

SET @Tab = (Select Nome From @Array where codigo=@Cod)  

SET @SAIDA01 = '
INSERT INTO #SAIDA
SELECT  '''+@IdPeriodo+''' AS IDPeriodo,  '''+@BD+''' AS DB, '''+@BPLId+''' AS Filial, '''+@Tab+''' AS TPDoc, 
	CASE WHEN T0.Model = 39 THEN ''NFe'' WHEN T0.Model = 44 Then ''CT-e'' ELSE ''NFEE'' END AS Especie, 
	isNULL(T0.SeriesStr, '''') AS ''SerieSubSerie'', 
	T0.Serial, 
	DATEPART(DAY, T0.TaxDate) AS ''DataDoc'', 
	isNULL(T2.State, '''') AS UFDest, 
	isNULL(T1.LineTotal,0.0) ValorContabil, 
	T1.CFOPCode, 
	isNULL(T4.BaseSum,0.0) BaseCalculo, 
	isNULL(T4.TaxRate, 0.0) AS Aliq,
	isNULL(T4.TaxSum,0.0) TaxSum, 
	CASE WHEN T4.U_Isento = 100 THEN isNULL(T4.U_OthAmtL,0.0) ELSE 0.0 END BaseIsenta, 
	CASE WHEN T4.U_Outros = 100 THEN isNULL(T4.U_OthAmtL,0.0) ELSE 0.0 END BaseOutras,
	BaseST = isNULL((SELECT SUM(V4.BaseSum) FROM '+@BD+'.[dbo].['+@Tab+'4] V4 WHERE T0.DocEntry = V4.DocEntry  AND V4.RelateType = 1 AND T1.LineNum = V4.LineNum AND V4.staType in (SELECT TT.AbsId FROM '+@BD+'.[dbo].[OSTT] TT INNER JOIN '+@BD+'.[dbo].[ONFT] FT ON TT.NfTaxId = FT.AbsId WHERE Code = ''ICMS-ST'')), 0.0),
	ValorST = isNULL((SELECT SUM(V4.TaxSum) FROM '+@BD+'.[dbo].['+@Tab+'4] V4 WHERE T0.DocEntry = V4.DocEntry  AND V4.RelateType = 1 AND T1.LineNum = V4.LineNum AND V4.staType in (SELECT TT.AbsId FROM '+@BD+'.[dbo].[OSTT] TT INNER JOIN '+@BD+'.[dbo].[ONFT] FT ON TT.NfTaxId = FT.AbsId WHERE Code = ''ICMS-ST'')), 0.0)
FROM '+@BD+'.[dbo].[O'+@Tab+'] T0 
	INNER JOIN '+@BD+'.[dbo].['+@Tab+'1] T1 ON T0.Docentry = T1.DocEntry 
	INNER JOIN '+@BD+'.[dbo].['+@Tab+'12] T2 ON T0.DocEntry = T2.DocEntry
	INNER JOIN '+@BD+'.[dbo].['+@Tab+'4] T4 ON T0.DocEntry = T4.DocEntry AND T4. RelateType = 1 AND T1.LineNum = T4.LineNum 
		AND T4.staType in (SELECT TT.AbsId FROM '+@BD+'.[dbo].[OSTT] TT INNER JOIN '+@BD+'.[dbo].[ONFT] FT ON TT.NfTaxId = FT.AbsId WHERE Code = ''ICMS'')
WHERE Model in (39, 44) AND BPLID = '+@BPLId+' AND CANCELED = ''N'' 
	AND T0.DocDate between convert(datetime,'''+convert(nvarchar,@datade,102)+''') and convert(datetime,'''+convert(nvarchar,@DataAte,102)+''')
'

/** ADIANTAMENTO **/

SET @SAIDA02 = 'INSERT INTO #SAIDA
SELECT  '''+@IdPeriodo+''' AS IDPeriodo,  '''+@BD+''' AS DB, '''+@BPLId+''' AS Filial, '''+@Tab+''' AS TPDoc, 
	CASE WHEN T0.Model = 39 THEN ''NFe'' WHEN T0.Model = 44 Then ''CT-e'' ELSE ''NFEE'' END AS Especie, 
	isNULL(T0.SeriesStr,'''') AS ''SerieSubSerie'', 
	T0.Serial, 
	DATEPART(DAY, T0.TaxDate) AS ''DataDoc'', 
	isNULL(T2.State,'''') AS UFDest, 
	isNULL(T1.DistribSum, 0.0) ValorContabil, 
	T1.CFOPCode, 
	isNULL(T4.BaseSum, 0.0) BaseCalculo, 
	isNULL(T4.TaxRate,0.0) AS Aliq,
	isNULL(T4.TaxSum, 0.0) TaxSum, 
	CASE WHEN T4.U_Isento = 100 THEN isNULL(T4.U_OthAmtL, 0.0) ELSE 0.0 END BaseIsenta, 
	CASE WHEN T4.U_Outros = 100 THEN isNULL(T4.U_OthAmtL, 0.0) ELSE 0.0 END BaseOutras,
	BaseST = isNULL((SELECT SUM(V4.BaseSum) FROM '+@BD+'.[dbo].['+@Tab+'4] V4 WHERE T0.DocEntry = V4.DocEntry  AND V4.RelateType = 1 AND T1.LineNum = V4.LineNum AND V4.staType in (SELECT TT.AbsId FROM '+@BD+'.[dbo].[OSTT] TT INNER JOIN '+@BD+'.[dbo].[ONFT] FT ON TT.NfTaxId = FT.AbsId WHERE Code = ''ICMS-ST'')), 0.0),
	ValorST = isNULL((SELECT SUM(V4.TaxSum) FROM '+@BD+'.[dbo].['+@Tab+'4] V4 WHERE T0.DocEntry = V4.DocEntry  AND V4.RelateType = 1 AND T1.LineNum = V4.LineNum AND V4.staType in (SELECT TT.AbsId FROM '+@BD+'.[dbo].[OSTT] TT INNER JOIN '+@BD+'.[dbo].[ONFT] FT ON TT.NfTaxId = FT.AbsId WHERE Code = ''ICMS-ST'')), 0.0)
FROM '+@BD+'.[dbo].[O'+@Tab+'] T0 
	INNER JOIN '+@BD+'.[dbo].['+@Tab+'1] T1 ON T0.Docentry = T1.DocEntry 
	INNER JOIN '+@BD+'.[dbo].['+@Tab+'12] T2 ON T0.DocEntry = T2.DocEntry
	INNER JOIN '+@BD+'.[dbo].['+@Tab+'4] T4 ON T0.DocEntry = T4.DocEntry AND T4. RelateType = 13 AND T1.LineNum = T4.LineNum 
		AND T4.staType in (SELECT TT.AbsId FROM '+@BD+'.[dbo].[OSTT] TT INNER JOIN '+@BD+'.[dbo].[ONFT] FT ON TT.NfTaxId = FT.AbsId WHERE Code = ''ICMS'')
WHERE Model in (39, 44) AND BPLID = '+@BPLId+' AND CANCELED = ''N'' 
	AND T0.DocDate between convert(datetime,'''+convert(nvarchar,@datade,102)+''') and convert(datetime,'''+convert(nvarchar,@DataAte,102)+''')

'
--PRINT @Saida01
--PRINT @Saida02
exec sp_executesql @Saida01
exec sp_executesql @Saida02

SET @Cod = @Cod + 1

END


DELETE FROM [SPS_Fiscal].[dime].[EntradaP1] WHERE [IdPeriodo] = @IdPeriodo 

INSERT INTO [SPS_Fiscal].[dime].[EntradaP1]
            ([IdPeriodo]
            ,[BD]
		    ,[Filial]
            ,[TPDoc]
            ,[DataLanc]
            ,[DataDoc]
            ,[Serial]
            ,[Especie]
            ,[Estado]
            ,[CFOP]
            ,[Vl_Contabil]
            ,[Vl_Base_Calc]
            ,[Aliq]
            ,[Vl_Imp_Cred]
            ,[Vl_Isentas_Nao_Trib]
            ,[Vl_Outras]
            ,[Vl_Base_ST]
            ,[Vl_ST]
            ,[Vl_Base_Calc_IR]
            ,[Vl_IR]
            ,[Vl_Difal])

SELECT	 [IdPeriodo]
            ,[BD]
		    ,[Filial]
		    ,[TPDoc]
		    ,[DatadeEntrada]
		    ,[DataDoc]
		    ,[Serial]
            ,[Especie]
            ,[State]
            ,[CFOPCode]
            ,[ValorContabil]
            ,[BaseCalculo]
            ,[Aliq]
            ,[TaxSum]
            ,[BaseIsenta]
            ,[BaseOutras]
            ,[BaseST]
            ,[ValorST]
            ,0.0
            ,0.0
            ,0.0
FROM #ENTRADA
--GROUP BY [IdPeriodo],[BD],[Filial],[TPDoc],[DatadeEntrada],[DataDoc],[Serial],[Especie],[State],[CFOPCode],[Aliq]

DELETE FROM [SPS_Fiscal].[dime].[SaidasP2] WHERE [IdPeriodo] = @IdPeriodo 
INSERT INTO [SPS_Fiscal].[dime].[SaidasP2]
            ([IdPeriodo]
            ,[BD]
		    ,[Filial]
            ,[TPDoc]
            ,[Especie]
            ,[Serial]
            ,[CFOP]
            ,[Vl_Contabil]
            ,[Vl_Base_Calc]
            ,[Aliq]
            ,[Vl_Imp_Debit]
            ,[Vl_Isentas_Nao_Trib]
            ,[Vl_Outras]
            ,[Vl_Base_ST]
            ,[Vl_ST]
            ,[Vl_Base_Calc_IR]
            ,[Vl_IR]
			,[Dia])

SELECT	 [IdPeriodo]
		    ,[BD]
		    ,[Filial]
		    ,[TPDoc]
		    ,[Especie]
            ,[Serial]
            ,[CFOPCode]
            ,[ValorContabil] + [ValorST]
            ,[BaseCalculo]
            ,[Aliq]
            ,[TaxSum]
            ,[BAseIsenta]
            ,[BaseOutras]
            ,[BaseST]
            ,[ValorST]
            ,0.0
            ,0.0
			,DataDoc

FROM #SAIDA
--GROUP BY [IdPeriodo],[BD],[Filial],[TPDoc],[Serial],[Especie],[CFOPCode],[Aliq]

DROP TABLE #ENTRADA
DROP TABLE #SAIDA
END", Support.Conectar());
                #endregion
                command.ExecuteNonQuery();
            }

            command = new SqlCommand($@"SELECT Name FROM SYSOBJECTS WHERE NAME = 'SP_SPSQuadros'", Support.Conectar());
            nome = Convert.ToInt32(command.ExecuteScalar());
            if (nome == 0)
            {
                #region QUERY DE CRIAÇÃO DE PROCEDURE QUADROS
                command = new SqlCommand(@"CREATE PROCEDURE [dbo].[SP_SPSQuadros] ( 
		@BPLID NVARCHAR(3) ,
		@DataDe Date ,
		@DataAte Date ,
		@IdPeriodo int ,
		@IdDeclaracao int,
        @PeriodoApuracao NVARCHAR(10))

-- EXEC [dime].[SP_SPSQuadros] '5', '20170201',  '20170228',  2, 5
--SELECT * FROM [SPS_Fiscal].[dime].[EntradaP1] WHERE IdPeriodo = @IdPeriodo
--SELECT * FROM [SPS_Fiscal].[dime].[DadosQuadro01] WHERE IdDeclaracao = @IdDeclaracao
--SELECT * FROM [SPS_Fiscal].[dime].[SaidasP2] WHERE IdPeriodo = @IdPeriodo
--SELECT * FROM [SPS_Fiscal].[dime].[DadosQuadro02] WHERE IdDeclaracao = @IdDeclaracao
--SELECT * FROM [SPS_Fiscal].[dime].[DadosQuadro03] WHERE IdDeclaracao = @IdDeclaracao
--SELECT * FROM [SPS_Fiscal].[dime].[DadosQuadro04] WHERE IdDeclaracao = @IdDeclaracao
--SELECT * FROM [SPS_Fiscal].[dime].[DadosQuadro05] WHERE IdDeclaracao = @IdDeclaracao
--SELECT * FROM [SPS_Fiscal].[dime].[DadosQuadro09] WHERE IdDeclaracao = @IdDeclaracao
--SELECT * FROM [SPS_Fiscal].[dime].[DadosQuadro11] WHERE IdDeclaracao = @IdDeclaracao

--------------------------------------------------------------------------------------------------------------------
-- INSERT DA APURAÇÃO
--------------------------------------------------------------------------------------------------------------------
as
begin
-------------------------------------------------------------------------------------------------------------
PRINT ('[DadosQuadro01]')
DELETE FROM [SPS_Fiscal].[dime].[DadosQuadro01] WHERE [IdDeclaracao] = @IdDeclaracao
/*
-- [DadosQuadro01]
*/
-------------------------------------------------------------------------------------------------------------
INSERT INTO [SPS_Fiscal].[dime].[DadosQuadro01]
           ([IdDeclaracao]
           ,[CFOP]
           ,[Vl_Contabil]
           ,[Vl_Base_Calc]
           ,[Vl_Imp_Cred]
           ,[Vl_Isentas_Nao_Trib]
           ,[Vl_Outras]
           ,[Vl_Base_Calc_IR]
           ,[Vl_IR]
           ,[Vl_Difal])
SELECT
	@IdDeclaracao,
	CFOP, 
	SUM(Vl_Contabil) ValorContabil,  
	SUM(Vl_Base_Calc) BaseCalculo, 
	Sum(Vl_Imp_Cred) Imposto, 
	isNULL(Sum(Vl_Isentas_Nao_Trib), 0.0) BaseIsenta, 
	isNULL(Sum(Vl_Outras), 0.0) BaseOutras,
	isNULL(Sum(Vl_Base_ST), 0.0) BaseST,
	isNULL(Sum(Vl_ST),0.0) ValorST,
	0.0
FROM [SPS_Fiscal].[dime].[EntradaP1]
WHERE [IdPeriodo] = @IdPeriodo
GROUP BY CFOP



-------------------------------------------------------------------------------------------------------------
PRINT ('[DadosQuadro02]')
DELETE FROM [SPS_Fiscal].[dime].[DadosQuadro02] WHERE IdDeclaracao = @IdDeclaracao
/*
-- [DadosQuadro02]
*/
-------------------------------------------------------------------------------------------------------------
INSERT INTO [SPS_Fiscal].[dime].[DadosQuadro02]
           ([IdDeclaracao]
           ,[CFOP]
           ,[Vl_Contabil]
           ,[Vl_Base_Calc]
           ,[Vl_Imp_Debit]
           ,[Vl_Isentas_Nao_Trib]
           ,[Vl_Outras]
           ,[Vl_Base_Calc_IR]
           ,[Vl_IR])
SELECT @IdDeclaracao,
CFOP, 
SUM(Vl_Contabil) ValorContabil,  
SUM(Vl_Base_Calc) BaseCalculo, 
Sum(Vl_Imp_Debit) Imposto, 
isNULL(Sum(Vl_Isentas_Nao_Trib), 0.0) BaseIsenta, 
isNULL(Sum(Vl_Outras), 0.0) BaseOutras,
isNULL(Sum([Vl_Base_Calc_IR]), 0.0) BaseST,
isNULL(Sum([Vl_IR]), 0.0) ValorST
FROM [SPS_Fiscal].[dime].[SaidasP2]
WHERE [IdPeriodo] = @IdPeriodo
GROUP BY CFOP

-------------------------------------------------------------------------------------------------------------
PRINT ('[DadosQuadro03]')
DELETE FROM [SPS_Fiscal].[dime].[DadosQuadro03] WHERE IdDeclaracao = @IdDeclaracao
/*
-- [DadosQuadro03]
*/
-------------------------------------------------------------------------------------------------------------
INSERT INTO [SPS_Fiscal].[dime].[DadosQuadro03]
           ([NumeroItem]
           ,[IdDeclaracao]
           ,[Valor])

SELECT   '010', @IdDeclaracao, SUM(Vl_Contabil) ValorContabil FROM [SPS_Fiscal].[dime].[EntradaP1] WHERE [IdPeriodo] = @IdPeriodo UNION ALL
SELECT   '020', @IdDeclaracao, SUM(Vl_Base_Calc) BaseCalculo FROM [SPS_Fiscal].[dime].[EntradaP1] WHERE [IdPeriodo] = @IdPeriodo UNION ALL
SELECT   '030', @IdDeclaracao, Sum(Vl_Imp_Cred) Imposto FROM [SPS_Fiscal].[dime].[EntradaP1] WHERE [IdPeriodo] = @IdPeriodo UNION ALL
SELECT   '040', @IdDeclaracao, Sum(Vl_Isentas_Nao_Trib) BaseIsenta FROM [SPS_Fiscal].[dime].[EntradaP1] WHERE [IdPeriodo] = @IdPeriodo UNION ALL
SELECT   '050', @IdDeclaracao, Sum(Vl_Outras) BaseOutras FROM [SPS_Fiscal].[dime].[EntradaP1] WHERE [IdPeriodo] = @IdPeriodo UNION ALL
SELECT   '053', @IdDeclaracao, Sum(Vl_Base_ST) BaseST FROM [SPS_Fiscal].[dime].[EntradaP1] WHERE [IdPeriodo] = @IdPeriodo UNION ALL
SELECT   '054', @IdDeclaracao, Sum([Vl_ST]) ValorST FROM [SPS_Fiscal].[dime].[EntradaP1] WHERE [IdPeriodo] = @IdPeriodo UNION ALL
SELECT   '057', @IdDeclaracao, 0.0  UNION ALL
SELECT   '060', @IdDeclaracao, SUM(Vl_Contabil) ValorContabil FROM [SPS_Fiscal].[dime].[SaidasP2] WHERE [IdPeriodo] = @IdPeriodo UNION ALL
SELECT   '070', @IdDeclaracao, SUM(Vl_Base_Calc) BaseCalculo FROM [SPS_Fiscal].[dime].[SaidasP2] WHERE [IdPeriodo] = @IdPeriodo UNION ALL
SELECT   '080', @IdDeclaracao, Sum(Vl_Imp_Debit) Imposto FROM [SPS_Fiscal].[dime].[SaidasP2] WHERE [IdPeriodo] = @IdPeriodo UNION ALL
SELECT   '090', @IdDeclaracao, Sum(Vl_Isentas_Nao_Trib) BaseIsenta FROM [SPS_Fiscal].[dime].[SaidasP2] WHERE [IdPeriodo] = @IdPeriodo UNION ALL
SELECT   '100', @IdDeclaracao, Sum(Vl_Outras) BaseOutras FROM [SPS_Fiscal].[dime].[SaidasP2] WHERE [IdPeriodo] = @IdPeriodo UNION ALL
SELECT   '103', @IdDeclaracao, Sum(Vl_Base_Calc_IR) BaseIR FROM [SPS_Fiscal].[dime].[SaidasP2] WHERE [IdPeriodo] = @IdPeriodo UNION ALL
SELECT   '104', @IdDeclaracao, Sum(Vl_IR) ValorIR FROM [SPS_Fiscal].[dime].[SaidasP2] WHERE [IdPeriodo] = @IdPeriodo 

-------------------------------------------------------------------------------------------------------------
PRINT('[DadosQuadro04]')
declare @valor decimal(18,2) = (select valor from [SPS_Fiscal].[dime].[DadosQuadro04] WHERE IdDeclaracao = @IdDeclaracao AND NumeroItem = 070)
declare @total decimal(18,2) = (SELECT SUM(valor) FROM [dime].[DadosQuadro04] WHERE IdDeclaracao = @IdDeclaracao and [NumeroItem] NOT IN (990))
DELETE FROM [SPS_Fiscal].[dime].[DadosQuadro04] WHERE IdDeclaracao = @IdDeclaracao
/*
-- [DadosQuadro04]
*/
-------------------------------------------------------------------------------------------------------------
INSERT INTO [SPS_Fiscal].[dime].[DadosQuadro04]
           ([NumeroItem]
           ,[IdDeclaracao]
           ,[Valor])

SELECT '010', @IdDeclaracao, ISNULL(Sum(Vl_Imp_Debit), 0.0) Imposto FROM [SPS_Fiscal].[dime].[SaidasP2] WHERE [IdPeriodo] = @IdPeriodo UNION ALL
SELECT '020', @IdDeclaracao, 0.0  UNION ALL
SELECT '030', @IdDeclaracao, 0.0  UNION ALL
SELECT '040', @IdDeclaracao, 0.0  UNION ALL
SELECT '045', @IdDeclaracao, 0.0  UNION ALL
SELECT '050', @IdDeclaracao, 0.0  UNION ALL
SELECT '060', @IdDeclaracao, 0.0  UNION ALL
SELECT '065', @IdDeclaracao, 0.0  UNION ALL
SELECT '070', @IdDeclaracao, ISNULL(@valor, 0.0)  UNION ALL
SELECT '990', @IdDeclaracao, ISNULL(@total, 0.0)

-------------------------------------------------------------------------------------------------------------
PRINT ('[DadosQuadro05]')
declare @Q5_10 decimal(18,2) = (select T0.Valor from [SPS_Fiscal].[dime].[DadosQuadro09] T0
INNER JOIN [SPS_Fiscal].[dime].[Declaracao] T1 ON T0.IdDeclaracao = T1.IdDeclaracao 
WHERE T0.NumeroItem = '998' AND T1.PeriodoReferencia = @PeriodoApuracao)
declare @Q5_30 decimal(18,2) = (select valor from [SPS_Fiscal].[dime].[DadosQuadro05] WHERE IdDeclaracao = @IdDeclaracao AND NumeroItem = '030')
declare @Q5_40 decimal(18,2) = (select valor from [SPS_Fiscal].[dime].[DadosQuadro05] WHERE IdDeclaracao = @IdDeclaracao AND NumeroItem = '040')
declare @Q5_45 decimal(18,2) = (select valor from [SPS_Fiscal].[dime].[DadosQuadro05] WHERE IdDeclaracao = @IdDeclaracao AND NumeroItem = '045')
declare @Q5_50 decimal(18,2) = (select valor from [SPS_Fiscal].[dime].[DadosQuadro05] WHERE IdDeclaracao = @IdDeclaracao AND NumeroItem = '050')

DELETE FROM [SPS_Fiscal].[dime].[DadosQuadro05] WHERE IdDeclaracao = @IdDeclaracao
/*
-- [DadosQuadro05]
*/
-------------------------------------------------------------------------------------------------------------
INSERT INTO [SPS_Fiscal].[dime].[DadosQuadro05]
           ([NumeroItem]
           ,[IdDeclaracao]
           ,[Valor])
SELECT '010', @IdDeclaracao, isNULL(@Q5_10, 0.0) UNION ALL
SELECT '020', @IdDeclaracao, Sum(Vl_Imp_Cred) Imposto FROM [SPS_Fiscal].[dime].[EntradaP1] WHERE [IdPeriodo] = @IdPeriodo UNION ALL
SELECT '030', @IdDeclaracao, isNULL(@Q5_30,0.0) UNION ALL
SELECT '040', @IdDeclaracao, isNULL(@Q5_40,0.0) UNION ALL
SELECT '045', @IdDeclaracao, isNULL(@Q5_45,0.0) UNION ALL
SELECT '050', @IdDeclaracao, isNULL(@Q5_50,0.0) UNION ALL
SELECT '990', @IdDeclaracao, Sum(Vl_Imp_Cred) Imposto FROM [SPS_Fiscal].[dime].[EntradaP1] WHERE [IdPeriodo] = @IdPeriodo 

-------------------------------------------------------------------------------------------------------------
PRINT ('[DadosQuadro09]')
declare @Q9_11 decimal(18,2) = (select ISNULL(valor,0.0) from [SPS_Fiscal].[dime].[DadosQuadro09] WHERE IdDeclaracao = @IdDeclaracao AND NumeroItem = '011')
declare @Q9_20 decimal(18,2) = (select ISNULL(valor,0.0) from [SPS_Fiscal].[dime].[DadosQuadro09] WHERE IdDeclaracao = @IdDeclaracao AND NumeroItem = '020')
declare @Q9_30 decimal(18,2) = (select ISNULL(valor,0.0) from [SPS_Fiscal].[dime].[DadosQuadro09] WHERE IdDeclaracao = @IdDeclaracao AND NumeroItem = '030')
declare @Q9_60 decimal(18,2) = (select ISNULL(valor,0.0) from [SPS_Fiscal].[dime].[DadosQuadro09] WHERE IdDeclaracao = @IdDeclaracao AND NumeroItem = '060')
DECLARE @Q9_70 decimal(18,2) = (select ISNULL(SUM(Vl_Cred_Apur),0.0) FROM [SPS_Fiscal].[dime].[DadosQuadro46] where Ind_Origem = '1' AND IdDeclaracao = @IdDeclaracao)
DECLARE @Q9_75 decimal(18,2) = (select ISNULL(SUM(Vl_Cred_Apur),0.0) FROM [SPS_Fiscal].[dime].[DadosQuadro46] where Ind_Origem = '14' AND IdDeclaracao = @IdDeclaracao)
declare @Q9_90 decimal(18,2) = (select ISNULL(valor,0.0) from [SPS_Fiscal].[dime].[DadosQuadro09] WHERE IdDeclaracao = @IdDeclaracao AND NumeroItem = '090')
declare @Q9_100 decimal(18,2) = (select ISNULL(valor,0.0) from [SPS_Fiscal].[dime].[DadosQuadro09] WHERE IdDeclaracao = @IdDeclaracao AND NumeroItem = '100')
declare @Q9_105 decimal(18,2) = (select ISNULL(valor,0.0) from [SPS_Fiscal].[dime].[DadosQuadro09] WHERE IdDeclaracao = @IdDeclaracao AND NumeroItem = '105')
declare @Q9_130 decimal(18,2) = (select ISNULL(valor,0.0) from [SPS_Fiscal].[dime].[DadosQuadro09] WHERE IdDeclaracao = @IdDeclaracao AND NumeroItem = '130')
declare @Q9_150 decimal(18,2) = (select ISNULL(valor,0.0) from [SPS_Fiscal].[dime].[DadosQuadro09] WHERE IdDeclaracao = @IdDeclaracao AND NumeroItem = '150')
declare @Q9_160 decimal(18,2) = (select ISNULL(valor,0.0) from [SPS_Fiscal].[dime].[DadosQuadro09] WHERE IdDeclaracao = @IdDeclaracao AND NumeroItem = '160')
declare @Q9_170 decimal(18,2) = (select ISNULL(valor,0.0) from [SPS_Fiscal].[dime].[DadosQuadro09] WHERE IdDeclaracao = @IdDeclaracao AND NumeroItem = '170')
declare @Q9_180 decimal(18,2) = (select ISNULL(valor,0.0) from [SPS_Fiscal].[dime].[DadosQuadro09] WHERE IdDeclaracao = @IdDeclaracao AND NumeroItem = '180')
declare @Q9_190 decimal(18,2) = (select ISNULL(valor,0.0) from [SPS_Fiscal].[dime].[DadosQuadro09] WHERE IdDeclaracao = @IdDeclaracao AND NumeroItem = '190')
DELETE FROM [SPS_Fiscal].[dime].[DadosQuadro09] WHERE IdDeclaracao = @IdDeclaracao
/*
-- [DadosQuadro09]
*/
-------------------------------------------------------------------------------------------------------------
INSERT INTO [SPS_Fiscal].[dime].[DadosQuadro09]
           ([NumeroItem]
           ,[IdDeclaracao]
           ,[Valor])
SELECT '010',@IdDeclaracao, @total UNION ALL
SELECT '011',@IdDeclaracao, isNULL(@Q9_11,0.0) UNION ALL
SELECT '020',@IdDeclaracao, isNULL(@Q9_20,0.0) UNION ALL
SELECT '030',@IdDeclaracao, isNULL(@Q9_30,0.0) UNION ALL
SELECT '036',@IdDeclaracao, 0.0 UNION ALL
SELECT '037',@IdDeclaracao, 0.0 UNION ALL
SELECT '038',@IdDeclaracao, 0.0 UNION ALL
SELECT '040',@IdDeclaracao, Sum(Vl_Imp_Debit) Imposto FROM [SPS_Fiscal].[dime].[SaidasP2] WHERE [IdPeriodo] = @IdPeriodo UNION ALL
SELECT '050',@IdDeclaracao, Sum(Vl_Imp_Cred) Imposto FROM [SPS_Fiscal].[dime].[EntradaP1] WHERE [IdPeriodo] = @IdPeriodo UNION ALL
SELECT '060',@IdDeclaracao, isNULL(@Q9_60,0.0) UNION ALL
SELECT '070',@IdDeclaracao, isNULL(@Q9_70,0.0) UNION ALL
SELECT '075',@IdDeclaracao, isNULL(@Q9_75,0.0) UNION ALL
SELECT '076',@IdDeclaracao, 0.0 UNION ALL
SELECT '080',@IdDeclaracao, Sum(Vl_Imp_Cred) Imposto FROM [SPS_Fiscal].[dime].[EntradaP1] WHERE [IdPeriodo] = @IdPeriodo UNION ALL
SELECT '090',@IdDeclaracao, isNULL(@Q9_90,0.0) UNION ALL
SELECT '100',@IdDeclaracao, isNULL(@Q9_100,0.0) UNION ALL
SELECT '105',@IdDeclaracao, isNULL(@Q9_105,0.0) UNION ALL
SELECT '110',@IdDeclaracao, 0.0 UNION ALL
SELECT '120',@IdDeclaracao, (	
								(SELECT Sum(Vl_Imp_Debit) FROM [SPS_Fiscal].[dime].[SaidasP2] WHERE [IdPeriodo] = @IdPeriodo) 
								- (SELECT Sum(Vl_Imp_Cred) FROM [SPS_Fiscal].[dime].[EntradaP1] WHERE [IdPeriodo] = @IdPeriodo)
							) UNION ALL
SELECT '130',@IdDeclaracao, isNULL(@Q9_130,0.0) UNION ALL
SELECT '140',@IdDeclaracao, 0.0 UNION ALL
SELECT '150',@IdDeclaracao, isNULL(@Q9_150,0.0) UNION ALL
SELECT '160',@IdDeclaracao, isNULL(@Q9_160,0.0) UNION ALL
SELECT '170',@IdDeclaracao, isNULL(@Q9_170,0.0) UNION ALL
SELECT '180',@IdDeclaracao, isNULL(@Q9_180,0.0) UNION ALL
SELECT '190',@IdDeclaracao, isNULL(@Q9_190,0.0) UNION ALL
SELECT '998',@IdDeclaracao, 0.0 UNION ALL
SELECT '999',@IdDeclaracao, (	
								(SELECT Sum(Vl_Imp_Debit) FROM [SPS_Fiscal].[dime].[SaidasP2] WHERE [IdPeriodo] = @IdPeriodo) 
								- (SELECT Sum(Vl_Imp_Cred) FROM [SPS_Fiscal].[dime].[EntradaP1] WHERE [IdPeriodo] = @IdPeriodo)
							)
/*
-- Valida Saldo Credor ou Devedor
*/
DECLARE @Reg120 Float = (SELECT TOP 1 Valor FROM [SPS_Fiscal].[dime].[DadosQuadro09] where NumeroItem = '120' AND IdDeclaracao = @IdDeclaracao)

IF @Reg120 < 0
Begin 

UPDATE [SPS_Fiscal].[dime].[DadosQuadro09] SET Valor = 0 WHERE NumeroItem in ('120','999') AND IdDeclaracao = @IdDeclaracao
UPDATE [SPS_Fiscal].[dime].[DadosQuadro09] SET Valor = (@Reg120 * - 1) WHERE NumeroItem in ('140','998') AND IdDeclaracao = @IdDeclaracao

END


-------------------------------------------------------------------------------------------------------------
PRINT ('[DadosQuadro11]')
DELETE FROM [SPS_Fiscal].[dime].[DadosQuadro11] WHERE IdDeclaracao = @IdDeclaracao
/*
-- [DadosQuadro11]
*/
-------------------------------------------------------------------------------------------------------------
INSERT INTO [SPS_Fiscal].[dime].[DadosQuadro11]
           ([NumeroItem]
           ,[IdDeclaracao]
           ,[Valor])
SELECT '010',@IdDeclaracao, 0.0 UNION ALL
SELECT '020',@IdDeclaracao, 0.0 UNION ALL
SELECT '030',@IdDeclaracao, 0.0 UNION ALL
SELECT '040',@IdDeclaracao, 0.0 UNION ALL
SELECT '050',@IdDeclaracao, 0.0 UNION ALL
SELECT '060',@IdDeclaracao, 0.0 UNION ALL
SELECT '065',@IdDeclaracao, 0.0 UNION ALL
SELECT '070',@IdDeclaracao, 0.0 UNION ALL
SELECT '073',@IdDeclaracao, 0.0 UNION ALL
SELECT '075',@IdDeclaracao, 0.0 UNION ALL
SELECT '080',@IdDeclaracao, 0.0 UNION ALL
SELECT '090',@IdDeclaracao, 0.0 UNION ALL
SELECT '100',@IdDeclaracao, 0.0 UNION ALL
SELECT '105',@IdDeclaracao, 0.0 UNION ALL
SELECT '110',@IdDeclaracao, 0.0 UNION ALL
SELECT '120',@IdDeclaracao, 0.0 UNION ALL
SELECT '125',@IdDeclaracao, 0.0 UNION ALL
SELECT '130',@IdDeclaracao, 0.0 UNION ALL
SELECT '140',@IdDeclaracao, 0.0 UNION ALL
SELECT '150',@IdDeclaracao, 0.0 UNION ALL
SELECT '155',@IdDeclaracao, 0.0 UNION ALL
SELECT '160',@IdDeclaracao, 0.0 UNION ALL
SELECT '170',@IdDeclaracao, 0.0 UNION ALL
SELECT '180',@IdDeclaracao, 0.0 UNION ALL
SELECT '190',@IdDeclaracao, 0.0 UNION ALL
SELECT '200',@IdDeclaracao, 0.0 UNION ALL
SELECT '998',@IdDeclaracao, 0.0 UNION ALL
SELECT '999',@IdDeclaracao, 0.0 

-------------------------------------------------------------------------------------------------------------
PRINT ('[DadosQuadro14]')
DELETE FROM [SPS_Fiscal].[dime].[DadosQuadro14] WHERE IdDeclaracao = @IdDeclaracao
/*
-- [DadosQuadro14]
*/
-------------------------------------------------------------------------------------------------------------

DECLARE @vlCampo020Quadro09 DECIMAL(18,2) = (SELECT Valor FROM [dime].[DadosQuadro09] WHERE IdDeclaracao = @IdDeclaracao AND NumeroItem = '020')
DECLARE @vlCampo030Quadro09 DECIMAL(18,2) = (SELECT Valor FROM [dime].[DadosQuadro09] WHERE IdDeclaracao = @IdDeclaracao AND NumeroItem = '030')
DECLARE @vlCampo031Quadro09 DECIMAL(18,2) = (SELECT Valor FROM [dime].[DadosQuadro09] WHERE IdDeclaracao = @IdDeclaracao AND NumeroItem = '031')
DECLARE @vlCampo036Quadro09 DECIMAL(18,2) = (SELECT Valor FROM [dime].[DadosQuadro09] WHERE IdDeclaracao = @IdDeclaracao AND NumeroItem = '036')
DECLARE @vlCampo037Quadro09 DECIMAL(18,2) = (SELECT Valor FROM [dime].[DadosQuadro09] WHERE IdDeclaracao = @IdDeclaracao AND NumeroItem = '037')
DECLARE @vlCampo038Quadro09 DECIMAL(18,2) = (SELECT Valor FROM [dime].[DadosQuadro09] WHERE IdDeclaracao = @IdDeclaracao AND NumeroItem = '038')
DECLARE @vlCampo076Quadro09 DECIMAL(18,2) = (SELECT Valor FROM [dime].[DadosQuadro09] WHERE IdDeclaracao = @IdDeclaracao AND NumeroItem = '076')
DECLARE @vlCampo045Quadro14 DECIMAL(18,2) = (SELECT Valor FROM [dime].[DadosQuadro14] WHERE IdDeclaracao = @IdDeclaracao AND NumeroItem = '045')
DECLARE @vlCampo050Quadro14 DECIMAL(18,2) = (SELECT Valor FROM [dime].[DadosQuadro14] WHERE IdDeclaracao = @IdDeclaracao AND NumeroItem = '050')
DECLARE @vlCampo040Quadro14 DECIMAL(18,2) = (SELECT Valor FROM [dime].[DadosQuadro14] WHERE IdDeclaracao = @IdDeclaracao AND NumeroItem = '040')
DECLARE @vlCampo110Quadro14 DECIMAL(18,2) = (SELECT Valor FROM [dime].[DadosQuadro14] WHERE IdDeclaracao = @IdDeclaracao AND NumeroItem = '110')
declare @cnpj nvarchar(50) = (SELECT ContribuinteCNPJ FROM [dime].[Declaracao] where IdDeclaracao = @IdDeclaracao)
declare @periodo nvarchar(50) = (SELECT PeriodoReferencia FROM [dime].[Declaracao] where IdDeclaracao = @IdDeclaracao)
declare @mesPeriodo int = SUBSTRING(@periodo, 6,2) + 1
declare @periodoF nvarchar(50)= SUBSTRING(@periodo, 0,7) + CONVERT(NVARCHAR(50),@mesPeriodo)
DECLARE @total040 DECIMAL(18,2) = @vlCampo020Quadro09 - (@vlCampo030Quadro09 + @vlCampo031Quadro09)
DECLARE @total150 DECIMAL(18,2) = (@vlCampo040Quadro14 + @vlCampo050Quadro14) - (@vlCampo037Quadro09 + @vlCampo110Quadro14)
DECLARE @total198 DECIMAL(18,2) = (@vlCampo037Quadro09 + @vlCampo110Quadro14) - (@vlCampo040Quadro14 + @vlCampo045Quadro14 + @vlCampo050Quadro14)
DECLARE @total199 DECIMAL(18,2) = (@vlCampo040Quadro14 + @vlCampo045Quadro14 + @vlCampo050Quadro14) - (@vlCampo037Quadro09 + @vlCampo110Quadro14)
DECLARE @110Anterior DECIMAL(18,2)  = (SELECT Valor FROM [dime].[DadosQuadro14] WHERE NumeroItem = '110' AND IdDeclaracao = (SELECT IdDeclaracao FROM [dime].[Declaracao] where ContribuinteCNPJ = @cnpj AND PeriodoReferencia =  @periodoF))

INSERT INTO [SPS_Fiscal].[dime].[DadosQuadro14]
(
NumeroItem,
IdDeclaracao,
Valor
)
SELECT '020' ,@IdDeclaracao, ISNULL(@vlCampo076Quadro09,0.0) UNION ALL
SELECT '030' ,@IdDeclaracao, ISNULL(@vlCampo036Quadro09,0.0) UNION ALL
SELECT '031' ,@IdDeclaracao, ISNULL(@vlCampo038Quadro09,0.0) UNION ALL
SELECT '040' ,@IdDeclaracao, ISNULL(@total040,0.0) UNION ALL
SELECT '045' ,@IdDeclaracao, ISNULL(@vlCampo045Quadro14,0.0) UNION ALL
SELECT '050' ,@IdDeclaracao, ISNULL(@vlCampo050Quadro14,0.0) UNION ALL
SELECT '110' ,@IdDeclaracao, ISNULL(@110Anterior,0.0) UNION ALL
SELECT '120' ,@IdDeclaracao, ISNULL(@vlCampo037Quadro09,0.0) UNION ALL
SELECT '130' ,@IdDeclaracao, ISNULL(@vlCampo037Quadro09 + @vlCampo110Quadro14,0.0) UNION ALL
SELECT '150' ,@IdDeclaracao, ISNULL((@vlCampo040Quadro14 + @vlCampo050Quadro14) - (@vlCampo037Quadro09 + @vlCampo110Quadro14),0.0) UNION ALL
SELECT '199' ,@IdDeclaracao, CASE WHEN @total199 > 0 THEN @total199 ELSE 0 END UNION ALL
SELECT '198' ,@IdDeclaracao, CASE WHEN @total198 > 0 THEN @total198 ELSE 0 END

END", Support.Conectar());
                #endregion
                command.ExecuteNonQuery();
            }
            #endregion
        }

    }
}
