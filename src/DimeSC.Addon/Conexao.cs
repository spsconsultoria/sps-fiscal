using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.IO;
using System.Security.Cryptography;

namespace SPSFiscal.Addon
{
    public class Conexao
    {
        static public SAPbouiCOM.Application uiApplication;
        static public SAPbobsCOM.Company diCompany;
        static public string tipoPnAtivo = "";
        static public string CodPnAtivo = "";
        static public bool validaPreco = true;
        static public SqlConnection SqlConn = new SqlConnection();
        static public SqlConnection SqlConnDR = new SqlConnection();
        static private string passwordSql = "";


        /// <summary>
        /// Construtor
        /// </summary>
        public Conexao()
        {
            try
            {
                SetApplicationUI();
                SetApplicationDI();
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// conecta o objeto da UI API
        /// </summary>
        public void SetApplicationUI()
        {
            SAPbouiCOM.SboGuiApi SboGuiApi;
            String sConnectionString;
            try
            {
                SboGuiApi = new SAPbouiCOM.SboGuiApi();
                sConnectionString = System.Convert.ToString(Environment.GetCommandLineArgs().GetValue(1));
                SboGuiApi.Connect(sConnectionString);
                uiApplication = SboGuiApi.GetApplication(-1);
            }
            catch (Exception e)
            {
                throw e;
            }           
        }

        /// <summary>
        /// conecta o objeto da DI API
        /// </summary>
        public void SetApplicationDI()
        {

            try
            {                
                int lErrCode;
                string sErrMsg = "", sCookie = "", sConnectionContext = "";
                diCompany = new SAPbobsCOM.Company();
                sCookie = diCompany.GetContextCookie();
                sConnectionContext = uiApplication.Company.GetConnectionContext(sCookie);

                if (diCompany.Connected == true)
                {
                    diCompany.Disconnect();
                }
                else
                {
                    diCompany.SetSboLoginContext(sConnectionContext);
                    diCompany.UseTrusted = false;
                    diCompany.Connect();
                }

                diCompany.GetLastError(out lErrCode, out sErrMsg);
                if (lErrCode != 0) throw new Exception(sErrMsg);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public Conexao(string servidor, string licenca, string bancodados, string usuarioB1, string senhaB1, string usuarioSQL, string senhaSQL)
        {
            try
            {
                int lErrCode;
                string sErrMsg = "";
                diCompany = new SAPbobsCOM.Company();
                diCompany.CompanyDB = bancodados;
                diCompany.UserName = usuarioB1;
                diCompany.Password = senhaB1;
                diCompany.Server = servidor;
                diCompany.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2005;
                diCompany.DbUserName = usuarioSQL;
                diCompany.DbPassword = senhaSQL;
                diCompany.language = SAPbobsCOM.BoSuppLangs.ln_Portuguese_Br;
                passwordSql = senhaSQL;

                if (diCompany.Connected == true)
                {
                    diCompany.Disconnect();
                }
                else
                {
                    int iret = diCompany.Connect();
                }
                diCompany.GetLastError(out lErrCode, out sErrMsg);
                if (lErrCode != 0)
                    throw new Exception(sErrMsg);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static DataTable ExecuteSqlDataTable(string Query)
        {
            try
            {
                DataTable oDT = new DataTable();
                SAPbobsCOM.Recordset oRS = (SAPbobsCOM.Recordset)Conexao.diCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                oRS.DoQuery(Query);
                if (!oRS.EoF)
                {
                    oRS.MoveFirst();
                    for (int i = 0; i < oRS.Fields.Count; i++)
                    {
                        oDT.Columns.Add(oRS.Fields.Item(i).Name);
                    }
                    DataRow oDR;
                    for (int x = 0; x < oRS.RecordCount; x++)
                    {
                        oDR = oDT.NewRow();

                        for (int i = 0; i < oRS.Fields.Count; i++)
                        {
                            oDR[i] = Convert.ToString(oRS.Fields.Item(i).Value);
                        }
                        oDT.Rows.Add(oDR);
                        oRS.MoveNext();
                    }
                }

                System.Runtime.InteropServices.Marshal.ReleaseComObject(oRS);
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
                return oDT;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public static object ExecuteSqlScalar(string Query)
        {
            try
            {
                object obj = null;

                SAPbobsCOM.Recordset oRS = (SAPbobsCOM.Recordset)Conexao.diCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                oRS.DoQuery(Query);
                if (!oRS.EoF)
                {
                    obj = oRS.Fields.Item(0).Value;
                }

                System.Runtime.InteropServices.Marshal.ReleaseComObject(oRS);
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();

                return obj;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static string EncodeTo64(string toEncode)
        {
            byte[] toEncodeAsBytes = System.Text.Encoding.Unicode.GetBytes(toEncode);
            string returnValue = System.Convert.ToBase64String(toEncodeAsBytes);

            return returnValue;
        }

        public static string DecodeFrom64(string encodedData)
        {
            byte[] encodedDataAsBytes = System.Convert.FromBase64String(encodedData);
            string returnValue = System.Text.Encoding.Unicode.GetString(encodedDataAsBytes);

            return returnValue;
        }

        
    }
}
