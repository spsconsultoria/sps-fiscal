using System;
using System.Collections.Generic;
using System.Text;
using SAPbouiCOM;
using System.Reflection;
using System.Threading;


namespace SPSFiscal.Addon
{
    public static class Events
    {

        static private string formType;
        static public bool ExecutarEventos = true;
        public static string _FormUID;
        public static string _ItemUID;
        public static int _Row;

        public static void MenuEvent(ref MenuEvent pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            if (mnu1283.res == false) BubbleEvent = false;
            try
            {
                if (BubbleEvent == false) return;
                AssemblyName currentAssemblyName = Assembly.GetExecutingAssembly().GetName();
                Type type = Type.GetType(String.Format("{0}.mnu{1}", currentAssemblyName.Name, pVal.MenuUID));
                if (type == null) return;

                IForm menu = FormFactory.CreateForm(type, ref pVal);
                if (menu == null) return;
                menu.MenuEvent();
            }
            catch (Exception ex)
            {
                if (ex.Message == "Form - already exists  [66000-11]")
                {
                    BubbleEvent = true;
                }
                else
                {
                    Conexao.uiApplication.StatusBar.SetText(ex.Message, BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Error);
                    BubbleEvent = false;
                }
            }
        }

        public static void ItemEvent(string FormUID, ref ItemEvent pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            try
            {
                if (ExecutarEventos)
                {
                    formType = Convert.ToString(pVal.FormTypeEx);

                    AssemblyName currentAssemblyName = Assembly.GetExecutingAssembly().GetName();
                    Type type = Type.GetType(String.Format("{0}.frm{1}", currentAssemblyName.Name, pVal.FormTypeEx.Replace("-", "_")));
                    if (type == null) return;
                    IForm form = FormFactory.CreateForm(type, ref pVal);
                    if (form == null) return;
                    BubbleEvent = form.ItemEvent();
                }

                try
                {
                    _FormUID = pVal.FormUID;
                    if (pVal.FormUID != "0" && pVal.Row >= 0)
                    {
                        _ItemUID = pVal.ItemUID;
                        _Row = pVal.Row;
                    }
                }
                catch (Exception) { }
            }
            catch (Exception ex)
            {
                if (ex.Message == "Form - already exists  [66000-11]")
                {
                    BubbleEvent = true;
                }
                else
                {
                    Conexao.uiApplication.StatusBar.SetText(ex.Message, BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Error);
                    BubbleEvent = false;
                }
            }
        }

        public static void FormDataEvent(ref BusinessObjectInfo BusinessObjectInfo, out bool BubbleEvent)
        {
            BubbleEvent = true;
            if (mnu1283.res == false) BubbleEvent = false;
            try
            {
                if (BubbleEvent == false) return;
                AssemblyName currentAssemblyName = Assembly.GetExecutingAssembly().GetName();
                Type type = Type.GetType(String.Format("{0}.frm{1}", currentAssemblyName.Name, BusinessObjectInfo.FormTypeEx));
                if (type == null) return;

                IForm form = FormFactory.CreateForm(type, ref BusinessObjectInfo);
                if (form == null) return;
                BubbleEvent = form.FormDataEvent();
            }
            catch (Exception ex)
            {
                Conexao.uiApplication.StatusBar.SetText(ex.Message, BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Error);
                BubbleEvent = false;
            }
        }

        public static void StatusBarEvent(string Text, BoStatusBarMessageType MessageType)
        {

        }

        public static void RightClickEvent(ref ContextMenuInfo eventInfo, out bool BubbleEvent)
        {
            BubbleEvent = true;
            try
            {
                SAPbouiCOM.Form oForm = Conexao.uiApplication.Forms.ActiveForm;

                AssemblyName currentAssemblyName = Assembly.GetExecutingAssembly().GetName();
                Type type = Type.GetType(String.Format("{0}.p{1}", currentAssemblyName.Name, oForm.TypeEx));
                if (type == null) return;

                IForm form = FormFactory.CreateForm(type, ref eventInfo);
                if (form == null) return;
                BubbleEvent = form.RightClickEvent();
            }
            catch (Exception ex)
            {
                Conexao.uiApplication.StatusBar.SetText(ex.Message, BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Error);
                BubbleEvent = false;
            }
        }

        public static void ReportDataEvent(ref ReportDataInfo eventInfo, out bool BubbleEvent)
        {
            BubbleEvent = true;
        }

        public static void ProgressBarEvent(ref ProgressBarEvent pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
        }

        public static void PrintEvent(ref PrintEventInfo eventInfo, out bool BubbleEvent)
        {
            BubbleEvent = true;
        }

        public static void AppEvent(BoAppEventTypes EventType)
        {
            Type type = Type.GetType("Program");
            switch (EventType)
            {
                case BoAppEventTypes.aet_LanguageChanged:
                    if (type == null) return;
                    //Program.CreateMenu();
                    //new csMenus(true).AddMenu();
                    break;
                case BoAppEventTypes.aet_CompanyChanged:
                case BoAppEventTypes.aet_ServerTerminition:
                case BoAppEventTypes.aet_ShutDown:
                    System.Windows.Forms.Application.Exit();
                    Environment.Exit(0);
                    break;
                default:
                    break;
            }
        }
    }

}
