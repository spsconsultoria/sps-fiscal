﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPSFiscal.Addon
{
    public class DadosQuadro46
    {
        private string linha;

        public string Sequencia { get; set; }
        public string Identificacao { get; set; }
        public string Valor { get; set; }
        public string Origem { get; set; }

        public string LinhaArquivo()
        {
            linha = string.Empty;
            linha += "46";
            linha += "46";
            linha += Sequencia.PadLeft(3, '0');
            linha += Identificacao.PadRight(15, '0');
            linha += Valor.Replace(".", "").PadLeft(17, '0');
            linha += Origem.PadLeft(2, '0');
            return linha;
        }
    }
}
