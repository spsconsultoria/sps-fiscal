﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPSFiscal.Addon
{
    public class Contabilista
    {
        private string linha;
        public string CPF { get; set; }
        public string NomeContabilista { get; set; }
        public string DataHora { get; set; }

        public string LinhaArquivo()
        {
            linha = string.Empty;
            linha += "20";
            linha += "  ";
            linha += CPF.Substring(0,11).Replace(".","").Replace("-","");
            linha += NomeContabilista.Length > 50 ? NomeContabilista.Substring(0, 50) : NomeContabilista.PadRight(50,' ');
            linha += DataHora;
            return linha;
        }
    }
}
