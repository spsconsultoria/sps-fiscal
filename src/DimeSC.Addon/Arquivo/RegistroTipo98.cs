﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPSFiscal.Addon
{
    public class RegistroTipo98
    {
        private string linha;
        public string QtdReg { get; set; }

        public string LinhaArquivo()
        {
            linha = string.Empty;

            linha += "98";
            linha += "".PadRight(2, ' ');
            linha += QtdReg.PadLeft(5, '0');

            return linha;
        }
    }
}
