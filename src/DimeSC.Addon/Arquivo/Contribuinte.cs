﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPSFiscal.Addon
{
    public class Contribuinte
    {
        private string linha;
        public string NumeroInscricao { get; set; }
        public string NomeContribuinte { get; set; }
        public string PeriodoReferencia { get; set; }
        public string TipoDeclaracao { get; set; }
        public string RegimeApuracao { get; set; }
        public string PorteEmpresa { get; set; }
        public string ApuracaoConsolidada { get; set; }
        public string ApuracaoCentralizada { get; set; }
        public string Transferencia { get; set; }
        public string CreditoPeriodo { get; set; }
        public string CreditoPresumido { get; set; }
        public string CreditoInsentivosFiscais { get; set; }
        public string Movimento { get; set; }
        public string SubstitutoTributario { get; set; }
        public string EscritaContabil { get; set; }
        public string Qtdtrab { get; set; }

        public string LinhaArquivo()
        {
            linha = string.Empty;
            linha += "21";
            linha += "00";
            linha += NumeroInscricao.Replace(".","").Replace("/","").PadRight(9, '0');
            linha += NomeContribuinte.PadRight(50, ' ');
            linha += PeriodoReferencia.PadRight(6, '0');
            linha += TipoDeclaracao.PadRight(1, '0');
            linha += RegimeApuracao.PadRight(1, '0');
            linha += PorteEmpresa.PadRight(1, '0');
            linha += ApuracaoConsolidada.PadRight(1, '0');
            linha += ApuracaoCentralizada.PadRight(1, '0');
            linha += Transferencia.PadRight(1, '0');
            //linha += CreditoPeriodo.PadLeft(1, '0');
            linha += CreditoPresumido.PadRight(1, '0');
            linha += CreditoInsentivosFiscais.PadRight(1, '0');
            linha += Movimento.PadRight(1, '3');
            linha += SubstitutoTributario.PadRight(1, '0');
            linha += EscritaContabil.PadRight(1, '0');
            linha += Qtdtrab.PadLeft(5, '0');
            return linha;
        }
    }
}
