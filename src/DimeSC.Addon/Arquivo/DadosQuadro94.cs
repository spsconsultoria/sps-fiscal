﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPSFiscal.Addon
{
    public class DadosQuadro94
    {
        private string linha;

        public string Item { get; set; }
        public string Valor { get; set; }

        public string LinhaArquivo()
        {
            linha = string.Empty;
            linha += "94";
            linha += "94";
            linha += Item.PadRight(3, '0');
            linha += Valor.Replace(".", "").PadLeft(17, '0');
            return linha;
        }
    }
}
