﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPSFiscal.Addon
{
    public class DadosQuadro12
    {
        private string linha;

        public string OrigemRecolhimento { get; set; }
        public string CodReceita { get; set; }
        public string Data { get; set; }
        public string Valor { get; set; }
        public string ClasseVenc { get; set; }
        public string NumeroAcordo { get; set; }

        public string LinhaArquivo()
        {
            linha = string.Empty;
            linha += "33";
            linha += "12";
            linha += OrigemRecolhimento.PadRight(1, '0');
            linha += CodReceita.PadRight(4, '0');
            linha += Data.PadRight(8, '0');
            linha += Valor.Replace(".", "").PadLeft(17, '0');
            linha += ClasseVenc.PadRight(5, '0');
            linha += NumeroAcordo.PadRight(15, '0');
            return linha;
        }
    }
}
