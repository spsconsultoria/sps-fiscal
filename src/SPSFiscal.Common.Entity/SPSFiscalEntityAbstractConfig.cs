﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SPSFiscal.Model;

namespace SPSFiscal.Common.Entity
{
    public abstract class SPSFiscalEntityAbstractConfig<TEntidade> : EntityTypeConfiguration<TEntidade>
        where TEntidade : class 
    {
        public SPSFiscalEntityAbstractConfig()
        {
            ConfigurarNomeTabela();
            ConfigurarCamposTabela();
            ConfigurarChavePrimaria();
            ConfigurarChavesEstrangeiras();
        }

        protected abstract void ConfigurarNomeTabela();
        protected abstract void ConfigurarCamposTabela();
        protected abstract void ConfigurarChavePrimaria();
        protected abstract void ConfigurarChavesEstrangeiras();
        
        
        
    }
}
