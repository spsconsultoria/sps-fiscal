﻿using SAPbouiCOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace SPS_Fiscal.Addon
{
    class mnufrmTabelas : IForm
    {
         private MenuEvent _eventoMenu;

        public Items Items
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public DataSource DataSources
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public int TypeCount
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public string Title
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public BoFormStateEnum State
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public int Type
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public bool Visible
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public string DefButton
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public bool Modal
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public BoFormMode Mode
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public int PaneLevel
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public int Top
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public int Left
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public int Height
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public int Width
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public bool Selected
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public string UniqueID
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public int ClientHeight
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public int ClientWidth
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public Menus Menu
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public BoFormBorderStyle BorderStyle
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public string TypeEx
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public int SupportedModes
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public BusinessObject BusinessObject
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public bool AutoManaged
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public DataBrowser DataBrowser
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public ChooseFromListCollection ChooseFromLists
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public FormSettings Settings
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public bool IsSystem
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public string ActiveItem
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public string UDFFormUID
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public string ReportType
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public bool VisibleEx
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public int MaxWidth
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public int MaxHeight
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public mnufrmTabelas(MenuEvent evento)
        {
            this._eventoMenu = evento;
        }

        public bool ItemEvent()
        {
            throw new NotImplementedException();
        }

        public bool MenuEvent()
        {
            try
            {
                string sql = string.Format(@"SELECT name FROM sys.databases WHERE name = 'SPS_Fiscal'");
                string resultado = Convert.ToString(Conexao.ExecuteSqlScalar(sql));

                SqlCommand command = new SqlCommand();

                bool Criar = false;

                if (string.IsNullOrEmpty(resultado))
                {
                    Conexao.uiApplication.SetStatusBarMessage("Criando tabela SPS_Fiscal", SAPbouiCOM.BoMessageTime.bmt_Long, false);

                    sql = "CREATE DATABASE [SPS_Fiscal]";
                    Conexao.ExecuteSqlScalar(sql);

                    #region CRIANDO TABELAS
                    command =  new SqlCommand(@"--CRIANDO TABELA MUNICIPIOS
CREATE TABLE [dbo].[DIME_TabelaMunicipios](
	[Codigo] [varchar](5) NOT NULL,
	[NomeMunicipio] [varchar](200) NULL,
PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


--CRIANDO TABELA CODATIVIDADE
CREATE TABLE [dbo].[DIME_TabelaCodAtividade](
	[Codigo] [varchar](3) NOT NULL,
	[Descricao] [varchar](300) NULL,
PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


--CRIANDO TABELA CONTABILISTA
CREATE TABLE [dbo].[DIME_Contabilista](
	[IdContabilista] [int] IDENTITY(1,1) NOT NULL,
	[CPF] [varchar](15) NULL,
	[NomeContabilista] [varchar](50) NULL,
	[Excluido] [varchar](1) NULL,
PRIMARY KEY CLUSTERED 
(
	[IdContabilista] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

--CRIANDO TABELA CONTRIBUENTE
CREATE TABLE [dbo].[DIME_Contribuinte](
	[IdContribuinte] [int] IDENTITY(1,1) NOT NULL,
	[IdContabilista] [int] NULL,
	[NomeRazaoSocial] [varchar](50) NULL,
	[CNPJ] [varchar](18) NULL,
	[NroInscricao] [varchar](20) NULL,
	[TipoDeclaracao] [varchar](1) NULL,
	[RegimeApuracao] [varchar](1) NULL,
	[PorteEmpresa] [varchar](1) NULL,
	[ApuracaoConsolidada] [varchar](1) NULL,
	[ApuracaoCentralizada] [varchar](1) NULL,
	[TemCreditosPresumido] [varchar](1) NULL,
	[TemCredIncentFiscais] [varchar](1) NULL,
	[SubstitutoTributario] [varchar](1) NULL,
	[TemEscritaContabil] [varchar](1) NULL,
	[QtdeTrabAtividade] [int] NULL,
	[ErpIdEmpresa] [varchar](10) NULL,
	[ErpBaseDados] [varchar](50) NULL,
	[Excluido] [varchar](1) NULL,
PRIMARY KEY CLUSTERED 
(
	[IdContribuinte] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[DIME_Contribuinte]  WITH CHECK ADD FOREIGN KEY([IdContabilista])
REFERENCES [dbo].[DIME_Contabilista] ([IdContabilista])

--CRIANDO TABELA DECLARAÇÃO
CREATE TABLE [dbo].[DIME_Declaracao](
	[IdDeclaracao] [int] IDENTITY(1,1) NOT NULL,
	[DtInicio] [datetime] NULL,
	[DtTermino] [datetime] NULL,
	[NomeContabilista] [varchar](50) NULL,
	[ContabilistaCPF] [varchar](15) NULL,
	[NomeContribuinte] [varchar](50) NULL,
	[NroInscricao] [varchar](20) NULL,
	[ContribuinteCNPJ] [varchar](18) NULL,
	[PeriodoReferencia] [varchar](8) NULL,
	[TipoDeclaracao] [varchar](1) NULL,
	[RegimeApuracao] [varchar](1) NULL,
	[PorteEmpresa] [varchar](1) NULL,
	[ApuracaoConsolidada] [varchar](1) NULL,
	[ApuracaoCentralizada] [varchar](1) NULL,
	[TransCredPeriodo] [varchar](1) NULL,
	[TemCreditosPresumido] [varchar](1) NULL,
	[TemCredIncentFiscais] [varchar](1) NULL,
	[Movimento] [varchar](1) NULL,
	[SubstitutoTributario] [varchar](1) NULL,
	[TemEscritaContabil] [varchar](1) NULL,
	[QtdeTrabAtividade] [int] NULL,
	[Excluido] [varchar](1) NULL,
PRIMARY KEY CLUSTERED 
(
	[IdDeclaracao] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


--CRIANDO TABELA CONTRIBUENTE DECLARAÇÃO
CREATE TABLE [dbo].[DIME_ContribuinteDeclaracao](
	[IdDeclaracao] [int] NOT NULL,
	[IdContribuinte] [int] NOT NULL,
	[PeriodoReferencia] [varchar](8) NULL,
	[DtCriacao] [datetime] NULL,
	[HrCriacao] [varchar](8) NULL,
PRIMARY KEY CLUSTERED 
(
	[IdDeclaracao] ASC,
	[IdContribuinte] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[DIME_ContribuinteDeclaracao]  WITH CHECK ADD FOREIGN KEY([IdContribuinte])
REFERENCES [dbo].[DIME_Contribuinte] ([IdContribuinte])

ALTER TABLE [dbo].[DIME_ContribuinteDeclaracao]  WITH CHECK ADD FOREIGN KEY([IdContribuinte])
REFERENCES [dbo].[DIME_Contribuinte] ([IdContribuinte])

ALTER TABLE [dbo].[DIME_ContribuinteDeclaracao]  WITH CHECK ADD FOREIGN KEY([IdDeclaracao])
REFERENCES [dbo].[DIME_Declaracao] ([IdDeclaracao])

--CRIANDO TABELA TIPO REGISTRO
CREATE TABLE [dbo].[DIME_TipoRegistro](
	[Quadro] [varchar](5) NOT NULL,
	[TipoRegistro] [varchar](10) NOT NULL,
	[Descricao] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[TipoRegistro] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

--CRIANDO TABELA REGISTRO DECLARAÇÃO
CREATE TABLE [dbo].[DIME_RegistroDeclaracao](
	[IdDeclaracao] [int] NOT NULL,
	[TipoRegistro] [varchar](10) NOT NULL
) ON [PRIMARY]

ALTER TABLE [dbo].[DIME_RegistroDeclaracao]  WITH CHECK ADD FOREIGN KEY([IdDeclaracao])
REFERENCES [dbo].[DIME_Declaracao] ([IdDeclaracao])

ALTER TABLE [dbo].[DIME_RegistroDeclaracao]  WITH CHECK ADD FOREIGN KEY([TipoRegistro])
REFERENCES [dbo].[DIME_TipoRegistro] ([TipoRegistro])

--CRIANDO TABELA DADOS QUADRO 01
CREATE TABLE [dbo].[DIME_DadosQuadro01](
	[IdLancamento] [int] IDENTITY(1,1) NOT NULL,
	[IdDeclaracao] [int] NOT NULL,
	[CFOP] [varchar](5) NULL,
	[Vl_Contabil] [numeric](15, 2) NULL,
	[Vl_Base_Calc] [numeric](15, 2) NULL,
	[Vl_Imp_Cred] [numeric](15, 2) NULL,
	[Vl_Isentas_Nao_Trib] [numeric](15, 2) NULL,
	[Vl_Outras] [numeric](15, 2) NULL,
	[Vl_Base_Calc_IR] [numeric](15, 2) NULL,
	[Vl_IR] [numeric](15, 2) NULL,
	[Vl_Difal] [numeric](15, 2) NULL,
PRIMARY KEY CLUSTERED 
(
	[IdLancamento] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[DIME_DadosQuadro01]  WITH CHECK ADD FOREIGN KEY([IdDeclaracao])
REFERENCES [dbo].[DIME_Declaracao] ([IdDeclaracao])

--CRIANDO TABELA DADOS QUADRO 02
CREATE TABLE [dbo].[DIME_DadosQuadro02](
	[IdLancamento] [int] IDENTITY(1,1) NOT NULL,
	[IdDeclaracao] [int] NOT NULL,
	[CFOP] [varchar](5) NULL,
	[Vl_Contabil] [numeric](15, 2) NULL,
	[Vl_Base_Calc] [numeric](15, 2) NULL,
	[Vl_Imp_Debit] [numeric](15, 2) NULL,
	[Vl_Isentas_Nao_Trib] [numeric](15, 2) NULL,
	[Vl_Outras] [numeric](15, 2) NULL,
	[Vl_Base_Calc_IR] [numeric](15, 2) NULL,
	[Vl_IR] [numeric](15, 2) NULL,
PRIMARY KEY CLUSTERED 
(
	[IdLancamento] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[DIME_DadosQuadro02]  WITH CHECK ADD FOREIGN KEY([IdDeclaracao])
REFERENCES [dbo].[DIME_Declaracao] ([IdDeclaracao])

--CRIANDO TABELA TABELA QUADRO 03
CREATE TABLE [dbo].[DIME_TabelaQuadro03](
	[NumeroItem] [varchar](3) NOT NULL,
	[Descricao] [varchar](200) NULL,
	[TipoItem] [varchar](2) NULL,
PRIMARY KEY CLUSTERED 
(
	[NumeroItem] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

--CRIANDO TABELA DADOS QUADRO 03
CREATE TABLE [dbo].[DIME_DadosQuadro03](
	[NumeroItem] [varchar](3) NOT NULL,
	[IdDeclaracao] [int] NOT NULL,
	[Valor] [numeric](15, 2) NULL,
PRIMARY KEY CLUSTERED 
(
	[NumeroItem] ASC,
	[IdDeclaracao] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[DIME_DadosQuadro03]  WITH CHECK ADD FOREIGN KEY([IdDeclaracao])
REFERENCES [dbo].[DIME_Declaracao] ([IdDeclaracao])

ALTER TABLE [dbo].[DIME_DadosQuadro03]  WITH CHECK ADD FOREIGN KEY([IdDeclaracao])
REFERENCES [dbo].[DIME_Declaracao] ([IdDeclaracao])

ALTER TABLE [dbo].[DIME_DadosQuadro03]  WITH CHECK ADD FOREIGN KEY([IdDeclaracao])
REFERENCES [dbo].[DIME_Declaracao] ([IdDeclaracao])

ALTER TABLE [dbo].[DIME_DadosQuadro03]  WITH CHECK ADD FOREIGN KEY([NumeroItem])
REFERENCES [dbo].[DIME_TabelaQuadro03] ([NumeroItem])

ALTER TABLE [dbo].[DIME_DadosQuadro03]  WITH CHECK ADD FOREIGN KEY([NumeroItem])
REFERENCES [dbo].[DIME_TabelaQuadro03] ([NumeroItem])

--CRIANDO TABELA TABELA QUADRO 04
CREATE TABLE [dbo].[DIME_TabelaQuadro04](
	[NumeroItem] [varchar](3) NOT NULL,
	[Descricao] [varchar](200) NULL,
	[TipoItem] [varchar](2) NULL,
PRIMARY KEY CLUSTERED 
(
	[NumeroItem] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

--CRIANDO TABELA DADOS QUADRO 04
CREATE TABLE [dbo].[DIME_DadosQuadro04](
	[NumeroItem] [varchar](3) NOT NULL,
	[IdDeclaracao] [int] NOT NULL,
	[Valor] [numeric](15, 2) NULL,
PRIMARY KEY CLUSTERED 
(
	[NumeroItem] ASC,
	[IdDeclaracao] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[DIME_DadosQuadro04]  WITH CHECK ADD FOREIGN KEY([IdDeclaracao])
REFERENCES [dbo].[DIME_Declaracao] ([IdDeclaracao])

ALTER TABLE [dbo].[DIME_DadosQuadro04]  WITH CHECK ADD FOREIGN KEY([NumeroItem])
REFERENCES [dbo].[DIME_TabelaQuadro04] ([NumeroItem])

--CRIANDO TABELA TABELA QUADRO 05
CREATE TABLE [dbo].[DIME_TabelaQuadro05](
	[NumeroItem] [varchar](3) NOT NULL,
	[Descricao] [varchar](200) NULL,
	[TipoItem] [varchar](2) NULL,
PRIMARY KEY CLUSTERED 
(
	[NumeroItem] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

--CRIANDO TABELA DADOS QUADRO 05
CREATE TABLE [dbo].[DIME_DadosQuadro05](
	[NumeroItem] [varchar](3) NOT NULL,
	[IdDeclaracao] [int] NOT NULL,
	[Valor] [numeric](15, 2) NULL,
PRIMARY KEY CLUSTERED 
(
	[NumeroItem] ASC,
	[IdDeclaracao] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[DIME_DadosQuadro05]  WITH CHECK ADD FOREIGN KEY([IdDeclaracao])
REFERENCES [dbo].[DIME_Declaracao] ([IdDeclaracao])

ALTER TABLE [dbo].[DIME_DadosQuadro05]  WITH CHECK ADD FOREIGN KEY([NumeroItem])
REFERENCES [dbo].[DIME_TabelaQuadro05] ([NumeroItem])

--CRIANDO TABELA TABELA QUADRO 09
CREATE TABLE [dbo].[DIME_TabelaQuadro09](
	[NumeroItem] [varchar](3) NOT NULL,
	[Descricao] [varchar](200) NULL,
	[TipoItem] [varchar](2) NULL,
PRIMARY KEY CLUSTERED 
(
	[NumeroItem] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

--CRIANDO TABELA DADOS QUADRO 09
CREATE TABLE [dbo].[DIME_DadosQuadro09](
	[NumeroItem] [varchar](3) NOT NULL,
	[IdDeclaracao] [int] NOT NULL,
	[Valor] [numeric](15, 2) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[NumeroItem] ASC,
	[IdDeclaracao] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[DIME_DadosQuadro09]  WITH CHECK ADD FOREIGN KEY([IdDeclaracao])
REFERENCES [dbo].[DIME_Declaracao] ([IdDeclaracao])

ALTER TABLE [dbo].[DIME_DadosQuadro09]  WITH CHECK ADD FOREIGN KEY([NumeroItem])
REFERENCES [dbo].[DIME_TabelaQuadro09] ([NumeroItem])

--CRIANDO TABELA TABELA QUADRO 11
CREATE TABLE [dbo].[DIME_TabelaQuadro11](
	[NumeroItem] [varchar](3) NOT NULL,
	[Descricao] [varchar](200) NULL,
	[TipoItem] [varchar](2) NULL,
PRIMARY KEY CLUSTERED 
(
	[NumeroItem] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

--CRIANDO TABELA DADOS QUADRO 11
CREATE TABLE [dbo].[DIME_DadosQuadro11](
	[NumeroItem] [varchar](3) NOT NULL,
	[IdDeclaracao] [int] NOT NULL,
	[Valor] [numeric](15, 2) NULL,
PRIMARY KEY CLUSTERED 
(
	[NumeroItem] ASC,
	[IdDeclaracao] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[DIME_DadosQuadro11]  WITH CHECK ADD FOREIGN KEY([IdDeclaracao])
REFERENCES [dbo].[DIME_Declaracao] ([IdDeclaracao])

ALTER TABLE [dbo].[DIME_DadosQuadro11]  WITH CHECK ADD FOREIGN KEY([NumeroItem])
REFERENCES [dbo].[DIME_TabelaQuadro11] ([NumeroItem])

--CRIANDO TABELA DADOS QUADRO 12
CREATE TABLE [dbo].[DIME_DadosQuadro12](
	[IdLancamento] [int] IDENTITY(1,1) NOT NULL,
	[IdDeclaracao] [int] NOT NULL,
	[Ind_Org_Recol] [varchar](2) NULL,
	[Cod_Receita] [varchar](5) NULL,
	[Dt_Vcto_Recol] [datetime] NULL,
	[Vl_Recol] [numeric](15, 2) NULL,
	[Cod_Class_Vcto] [varchar](5) NULL,
	[Nro_Acordo] [varchar](15) NULL,
PRIMARY KEY CLUSTERED 
(
	[IdLancamento] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[DIME_DadosQuadro12]  WITH CHECK ADD FOREIGN KEY([IdDeclaracao])
REFERENCES [dbo].[DIME_Declaracao] ([IdDeclaracao])

--CRIANDO TABELA DADOS QUADRO 46
CREATE TABLE [dbo].[DIME_DadosQuadro46](
	[IdLancamento] [int] IDENTITY(1,1) NOT NULL,
	[IdDeclaracao] [int] NOT NULL,
	[Seq_Reg] [int] NOT NULL,
	[Cod_Identif] [varchar](15) NULL,
	[Vl_Cred_Apur] [numeric](15, 2) NULL,
	[Ind_Origem] [varchar](2) NULL,
PRIMARY KEY CLUSTERED 
(
	[IdLancamento] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[DIME_DadosQuadro46]  WITH CHECK ADD FOREIGN KEY([IdDeclaracao])
REFERENCES [dbo].[DIME_Declaracao] ([IdDeclaracao])

--CRIANDO TABELA DADOS QUADRO 48
CREATE TABLE [dbo].[DIME_DadosQuadro48](
	[IdLancamento] [int] IDENTITY(1,1) NOT NULL,
	[IdDeclaracao] [int] NOT NULL,
	[Cod_Municipio] [varchar](5) NULL,
	[Vl_Perc_Ad] [numeric](15, 2) NULL,
	[Cod_Tipo_Atv] [varchar](3) NULL,
PRIMARY KEY CLUSTERED 
(
	[IdLancamento] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[DIME_DadosQuadro48]  WITH CHECK ADD FOREIGN KEY([IdDeclaracao])
REFERENCES [dbo].[DIME_Declaracao] ([IdDeclaracao])

--CRIANDO TABELA DADOS QUADRO 49
CREATE TABLE [dbo].[DIME_DadosQuadro49](
	[IdLancamento] [int] IDENTITY(1,1) NOT NULL,
	[IdDeclaracao] [int] NOT NULL,
	[Sigla_Estado] [varchar](2) NULL,
	[Vl_Contabil] [numeric](15, 2) NULL,
	[Vl_Base_Calc] [numeric](15, 2) NULL,
	[Vl_Outras] [numeric](15, 2) NULL,
	[Vl_ST] [numeric](15, 2) NULL,
	[Vl_ST_Outros] [numeric](15, 2) NULL,
PRIMARY KEY CLUSTERED 
(
	[IdLancamento] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[DIME_DadosQuadro49]  WITH CHECK ADD FOREIGN KEY([IdDeclaracao])
REFERENCES [dbo].[DIME_Declaracao] ([IdDeclaracao])

--CRIANDO TABELA DADOS QUADRO 50
CREATE TABLE [dbo].[DIME_DadosQuadro50](
	[IdLancamento] [int] IDENTITY(1,1) NOT NULL,
	[IdDeclaracao] [int] NOT NULL,
	[Sigla_Estado] [varchar](2) NULL,
	[Vl_Contabil_Nao_Contrib] [numeric](15, 2) NULL,
	[Vl_Contabil_Contrib] [numeric](15, 2) NULL,
	[Vl_Base_Calc_Nao_Contrib] [numeric](15, 2) NULL,
	[Vl_Base_Calc_Contrib] [numeric](15, 2) NULL,
	[Vl_Outras] [numeric](15, 2) NULL,
	[Vl_ST] [numeric](15, 2) NULL,
PRIMARY KEY CLUSTERED 
(
	[IdLancamento] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[DIME_DadosQuadro50]  WITH CHECK ADD FOREIGN KEY([IdDeclaracao])
REFERENCES [dbo].[DIME_Declaracao] ([IdDeclaracao])

--CRIANDO TABELA PERIODO APURAÇÃO
CREATE TABLE [dbo].[DIME_PeriodoApuracao](
	[IdPeriodo] [int] IDENTITY(1,1) NOT NULL,
	[IdContribuinte] [int] NOT NULL,
	[Periodo] [varchar](8) NOT NULL,
	[Vl_Debitos] [numeric](15, 2) NULL,
	[Vl_Creditos] [numeric](15, 2) NULL,
	[Vl_Deducoes] [numeric](15, 2) NULL,
	[Vl_Saldo_Credor_Ant] [numeric](15, 2) NULL,
	[Vl_Saldo_Credor_Ant_ST] [numeric](15, 2) NULL,
PRIMARY KEY CLUSTERED 
(
	[IdPeriodo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[DIME_PeriodoApuracao]  WITH CHECK ADD FOREIGN KEY([IdContribuinte])
REFERENCES [dbo].[DIME_Contribuinte] ([IdContribuinte])

--CRIANDO TABELA SAIDAP2
CREATE TABLE [dbo].[DIME_SaidasP2](
	[IdPeriodo] [int] NOT NULL,
	[CFOP] [varchar](5) NULL,
	[Vl_Contabil] [numeric](15, 2) NULL,
	[Vl_Base_Calc] [numeric](15, 2) NULL,
	[Vl_Imp_Debit] [numeric](15, 2) NULL,
	[Vl_Isentas_Nao_Trib] [numeric](15, 2) NULL,
	[Vl_Outras] [numeric](15, 2) NULL,
	[Vl_Base_Calc_IR] [numeric](15, 2) NULL,
	[Vl_IR] [numeric](15, 2) NULL
) ON [PRIMARY]

ALTER TABLE [dbo].[DIME_SaidasP2]  WITH CHECK ADD FOREIGN KEY([IdPeriodo])
REFERENCES [dbo].[DIME_PeriodoApuracao] ([IdPeriodo])

--CRIANDO TABELA ENTRADA P1
CREATE TABLE [dbo].[DIME_EntradasP1](
	[IdPeriodo] [int] NOT NULL,
	[CFOP] [varchar](5) NULL,
	[Vl_Contabil] [numeric](15, 2) NULL,
	[Vl_Base_Calc] [numeric](15, 2) NULL,
	[Vl_Imp_Cred] [numeric](15, 2) NULL,
	[Vl_Isentas_Nao_Trib] [numeric](15, 2) NULL,
	[Vl_Outras] [numeric](15, 2) NULL,
	[Vl_Base_Calc_IR] [numeric](15, 2) NULL,
	[Vl_IR] [numeric](15, 2) NULL,
	[Vl_Difal] [numeric](15, 2) NULL
) ON [PRIMARY]

ALTER TABLE [dbo].[DIME_EntradasP1]  WITH CHECK ADD FOREIGN KEY([IdPeriodo])
REFERENCES [dbo].[DIME_PeriodoApuracao] ([IdPeriodo])

--CRIANDO TABELA DEBITOS APURAÇÃO
CREATE TABLE [dbo].[DIME_DebitosApuracao](
	[IdPeriodo] [int] NOT NULL,
	[NumeroItem] [varchar](3) NOT NULL,
	[Vl_Ajuste] [numeric](15, 2) NULL
) ON [PRIMARY]

ALTER TABLE [dbo].[DIME_DebitosApuracao]  WITH CHECK ADD FOREIGN KEY([IdPeriodo])
REFERENCES [dbo].[DIME_PeriodoApuracao] ([IdPeriodo])

ALTER TABLE [dbo].[DIME_DebitosApuracao]  WITH CHECK ADD FOREIGN KEY([NumeroItem])
REFERENCES [dbo].[DIME_TabelaQuadro04] ([NumeroItem])
", Support.Conectar());
                    #endregion
                    command.ExecuteNonQuery();
                    //Conexao.ExecuteSqlScalar(sql);

                    #region POPULANDO TABELAS
                    command  =  new SqlCommand(@"
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaCodAtividade] ([Codigo]  ,[Descricao]) VALUES (	1	,	'Prestador de serviços de transportes'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaCodAtividade] ([Codigo]  ,[Descricao]) VALUES (	2	,	'Prestador de serviços de telecomunicações'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaCodAtividade] ([Codigo]  ,[Descricao]) VALUES (	3	,	'Fornecedor de energia elétrica ao consumidor independente, inclusive da parcela relativa à demanda contratada'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaCodAtividade] ([Codigo]  ,[Descricao]) VALUES (	4	,	'Distribuidor de energia elétrica a consumidor pessoa física ou jurídica inclusive os fornecimentos a consumidor independente e demanda contratada'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaCodAtividade] ([Codigo]  ,[Descricao]) VALUES (	5	,	'Fornecedor de gás natural'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaCodAtividade] ([Codigo]  ,[Descricao]) VALUES (	6	,	'Empresa que opere com o marketing direto'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaCodAtividade] ([Codigo]  ,[Descricao]) VALUES (	7	,	'Depósito ou centro de distribuição ou efetuar a entrega de mercadoria vendida por outro estabelecimento do mesmo titular'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaCodAtividade] ([Codigo]  ,[Descricao]) VALUES (	8	,	'Fornecedor de alimentos preparados'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaCodAtividade] ([Codigo]  ,[Descricao]) VALUES (	501	,	'For Detentor de TTD de obrigação acessória autorizando a remessa de mercadorias ao varejo ou pronta entrega através de postos de abastecimento, situados no Estado'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaCodAtividade] ([Codigo]  ,[Descricao]) VALUES (	502	,	'For Detentor de TTD de obrigação acessória autorizando que nas operações de venda utilize demonstrativo auxiliar especifico para escrituração do Livro de Saídas'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaCodAtividade] ([Codigo]  ,[Descricao]) VALUES (	503	,	'For Detentor de TTD de obrigação acessória autorizando que o estabelecimento gerador de energia elétrica possua inscrição única englobando várias PCHs'	)

INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	8869	,	'	ALTO BELA VISTA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	8885	,	'	BALNEÁRIO ARROIO DO SILVA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	8907	,	'	BALNEÁRIO GAIVOTA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	8923	,	'	BANDEIRANTE	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	8940	,	'	BARRA BONITA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	8966	,	'	BELA VISTA DO TOLDO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	8982	,	'	BOCAINA DO SUL	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	9008	,	'	BOM JESUS	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	9024	,	'	BOM JESUS DO OESTE	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	9040	,	'	BRUNÓPOLIS	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	9067	,	'	CAPÃO ALTO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	9083	,	'	CHAPADÃO DO LAGEADO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	9105	,	'	CUNHATAÍ	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	9121	,	'	ENTRE RIOS	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	9148	,	'	ERMO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	9164	,	'	FLOR DO SERTÃO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	9180	,	'	FREI ROGÉRIO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	9202	,	'	IBIAM	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	9229	,	'	IOMERÊ	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	9245	,	'	JUPIÁ	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	9261	,	'	LUZERNA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	9288	,	'	PAIAL	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	9300	,	'	PAINEL	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	9326	,	'	PALMEIRA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	9342	,	'	PRINCESA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	9369	,	'	SALTINHO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	9385	,	'	SANTA TEREZINHA DO PROGRESSO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	9407	,	'	SANTIAGO DO SUL	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	9423	,	'	SÃO BERNARDINO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	9440	,	'	SÃO PEDRO DE ALCÂNTARA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	9466	,	'	TIGRINHOS	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	9482	,	'	TREVISO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	9504	,	'	ZORTÉA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	9733	,	'	FORQUILHINHA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	11924	,	'	BALNEÁRIO RINCÃO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	11940	,	'	PESCARIA BRAVA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	55379	,	'	BOMBINHAS	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	55395	,	'	MORRO GRANDE	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	55417	,	'	PASSO DE TORRES	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	55433	,	'	COCAL DO SUL	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	55450	,	'	CAPIVARI DE BAIXO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	55476	,	'	SANGÃO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	55492	,	'	BALNEÁRIO BARRA DO SUL	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	55514	,	'	SÃO JOÃO DO ITAPERIU	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	55530	,	'	CALMON	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	55557	,	'	SANTA TEREZINHA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	55573	,	'	BRAÇO DO TROMBUDO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	55590	,	'	MIRIM DOCE	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	55611	,	'	MONTE CARLO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	55638	,	'	VARGEM	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	55654	,	'	VARGEM BONITA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	55670	,	'	CERRO NEGRO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	55697	,	'	PONTE ALTA DO NORTE	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	55719	,	'	RIO RUFINO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	55735	,	'	SÃO CRISTÓVÃO DO SUL	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	55751	,	'	MACIEIRA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	55778	,	'	ÁGUAS FRIAS	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	55794	,	'	CORDILHEIRA ALTA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	55816	,	'	FORMOSA DO SUL	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	55832	,	'	GUATAMBU	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	55859	,	'	IRATI	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	55875	,	'	JARDINÓPOLIS	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	55891	,	'	NOVA ITABERABA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	55913	,	'	NOVO HORIZONTE	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	55930	,	'	PLANALTO ALEGRE	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	55956	,	'	SUL BRASIL	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	55972	,	'	ARABUTÃ	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	55999	,	'	ARVOREDO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	57355	,	'	CORONEL MARTINS	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	57371	,	'	IPUAÇU	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	57398	,	'	LAJEADO GRANDE	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	57410	,	'	OURO VERDE	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	57436	,	'	PASSOS MAIA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	57452	,	'	BELMONTE	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	57479	,	'	PARAÍSO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	57495	,	'	RIQUEZA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	57517	,	'	SANTA HELENA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	57533	,	'	SÃO JOÃO DO OESTE	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	57550	,	'	SÃO MIGUEL DA BOA VISTA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80012	,	'	ABELARDO LUZ	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80039	,	'	AGROLÂNDIA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80055	,	'	AGRONÔMICA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80071	,	'	ÁGUA DOCE	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80098	,	'	ÁGUAS DE CHAPECÓ	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80110	,	'	ÁGUAS MORNAS	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80136	,	'	ALFREDO WAGNER	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80152	,	'	ANCHIETA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80179	,	'	ANGELINA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80195	,	'	ANITA GARIBALDI	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80217	,	'	ANITÁPOLIS	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80233	,	'	ANTÔNIO CARLOS	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80250	,	'	ARAQUARI	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80276	,	'	ARARANGUÁ	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80292	,	'	ARMAZÉM	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80314	,	'	ARROIO TRINTA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80330	,	'	ASCURRA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80357	,	'	ATALANTA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80373	,	'	AURORA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80390	,	'	BALNEÁRIO CAMBORIÚ	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80411	,	'	BARRA VELHA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80438	,	'	BENEDITO NOVO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80454	,	'	BIGUAÇU	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80470	,	'	BLUMENAU	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80497	,	'	BOM RETIRO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80519	,	'	BOTUVERÁ	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80535	,	'	BRAÇO DO NORTE	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80551	,	'	BRUSQUE	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80578	,	'	CAÇADOR	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80594	,	'	CAIBI	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80616	,	'	CAMBORIÚ	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80632	,	'	CAMPO ALEGRE	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80659	,	'	CAMPO BELO DO SUL	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80675	,	'	CAMPO ERÊ	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80691	,	'	CAMPOS NOVOS	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80713	,	'	CANELINHA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80730	,	'	CANOINHAS	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80756	,	'	CAPINZAL	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80772	,	'	CATANDUVAS	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80799	,	'	CAXAMBU DO SUL	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80810	,	'	CHAPECÓ	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80837	,	'	CONCÓRDIA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80853	,	'	CORONEL FREITAS	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80870	,	'	CORUPÁ	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80896	,	'	CRICIÚMA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80918	,	'	CUNHA PORÃ	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80934	,	'	CURITIBANOS	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80950	,	'	DESCANSO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80977	,	'	DIONÍSIO CERQUEIRA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80993	,	'	DONA EMMA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81019	,	'	ERVAL VELHO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81035	,	'	FAXINAL DOS GUEDES	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81051	,	'	FLORIANÓPOLIS	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81078	,	'	FRAIBURGO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81094	,	'	GALVÃO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81116	,	'	GOVERNADOR CELSO RAMOS	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81132	,	'	GAROPABA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81159	,	'	GARUVA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81175	,	'	GASPAR	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81191	,	'	GRÃO PARÁ	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81213	,	'	GRAVATAL	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81230	,	'	GUABIRUBA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81256	,	'	GUARACIABA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81272	,	'	GUARAMIRIM	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81299	,	'	GUARUJÁ DO SUL	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81310	,	'	HERVAL DO OESTE	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81337	,	'	IBICARÉ	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81353	,	'	IBIRAMA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81370	,	'	IÇARA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81396	,	'	ILHOTA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81418	,	'	IMARUÍ	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81434	,	'	IMBITUBA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81450	,	'	IMBUIA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81477	,	'	INDAIAL	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81493	,	'	IPIRA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81515	,	'	IPUMIRIM	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81531	,	'	IRANI	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81558	,	'	IRINEÓPOLIS	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81574	,	'	ITÁ	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81590	,	'	ITAIÓPOLIS	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81612	,	'	ITAJAÍ	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81639	,	'	ITAPEMA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81655	,	'	ITAPIRANGA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81671	,	'	ITUPORANGA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81698	,	'	JABORÁ	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81710	,	'	JACINTO MACHADO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81736	,	'	JAGUARUNA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81752	,	'	JARAGUÁ DO SUL	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81779	,	'	JOAÇABA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81795	,	'	JOINVILLE	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81817	,	'	LACERDÓPOLIS	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81833	,	'	LAGES	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81850	,	'	LAGUNA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81876	,	'	LAURENTINO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81892	,	'	LAURO MULLER	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81914	,	'	LEBON RÉGIS	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81930	,	'	LEOBERTO LEAL	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81957	,	'	LONTRAS	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81973	,	'	LUIZ ALVES	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81990	,	'	MAFRA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82015	,	'	MAJOR GERCINO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82031	,	'	MAJOR VIEIRA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82058	,	'	MARAVILHA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82074	,	'	MASSARANDUBA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82090	,	'	MATOS COSTA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82112	,	'	MELEIRO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82139	,	'	MODELO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82155	,	'	MONDAÍ	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82171	,	'	MONTE CASTELO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82198	,	'	MORRO DA FUMAÇA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82210	,	'	NAVEGANTES	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82236	,	'	NOVA ERECHIM	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82252	,	'	NOVA TRENTO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82279	,	'	NOVA VENEZA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82295	,	'	ORLEANS	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82317	,	'	OURO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82333	,	'	PALHOÇA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82350	,	'	PALMA SOLA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82376	,	'	PALMITOS	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82392	,	'	PAPANDUVA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82414	,	'	PAULO LOPES	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82430	,	'	PEDRAS GRANDES	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82457	,	'	PENHA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82473	,	'	PERITIBA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82490	,	'	PETROLÂNDIA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82511	,	'	BALNEÁRIO PIÇARRAS	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82538	,	'	PINHALZINHO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82554	,	'	PINHEIRO PRETO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82570	,	'	PIRATUBA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82597	,	'	POMERODE	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82619	,	'	PONTE ALTA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82635	,	'	PONTE SERRADA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82651	,	'	PORTO BELO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82678	,	'	PORTO UNIÃO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82694	,	'	POUSO REDONDO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82716	,	'	PRAIA GRANDE	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82732	,	'	PRESIDENTE CASTELO BRANCO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82759	,	'	PRESIDENTE GETÚLIO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82775	,	'	PRESIDENTE NEREU	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82791	,	'	QUILOMBO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82813	,	'	RANCHO QUEIMADO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82830	,	'	RIO DAS ANTAS	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82856	,	'	RIO DO CAMPO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82872	,	'	RIO DO OESTE	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82899	,	'	RIO DOS CEDROS	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82910	,	'	RIO DO SUL	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82937	,	'	RIO FORTUNA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82953	,	'	RIO NEGRINHO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82970	,	'	RODEIO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82996	,	'	ROMELÂNDIA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83011	,	'	SALETE	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83038	,	'	SALTO VELOSO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83054	,	'	SANTA CECÍLIA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83070	,	'	SANTA ROSA DE LIMA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83097	,	'	SANTO AMARO DA IMPERATRIZ	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83119	,	'	SÃO BENTO DO SUL	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83135	,	'	SÃO BONIFÁCIO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83151	,	'	SÃO CARLOS	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83178	,	'	SÃO DOMINGOS	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83194	,	'	SÃO FRANCISCO DO SUL	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83216	,	'	SÃO JOÃO BATISTA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83232	,	'	SÃO JOÃO DO SUL	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83259	,	'	SÃO JOAQUIM	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83275	,	'	SÃO JOSÉ	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83291	,	'	SÃO JOSÉ DO CEDRO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83313	,	'	SÃO JOSÉ DO CERRITO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83330	,	'	SÃO LOURENÇO DO OESTE	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83356	,	'	SÃO LUDGERO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83372	,	'	SÃO MARTINHO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83399	,	'	SÃO MIGUEL DO OESTE	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83410	,	'	SAUDADES	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83437	,	'	SCHROEDER	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83453	,	'	SEARA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83470	,	'	SIDERÓPOLIS	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83496	,	'	SOMBRIO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83518	,	'	TAIÓ	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83534	,	'	TANGARÁ	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83550	,	'	TIJUCAS	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83577	,	'	TIMBÓ	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83593	,	'	TRÊS BARRAS	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83615	,	'	TREZE DE MAIO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83631	,	'	TREZE TÍLIAS	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83658	,	'	TROMBUDO CENTRAL	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83674	,	'	TUBARÃO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83690	,	'	TURVO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83712	,	'	URUBICI	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83739	,	'	URUSSANGA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83755	,	'	VARGEÃO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83771	,	'	VIDAL RAMOS	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83798	,	'	VIDEIRA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83810	,	'	WITMARSUM	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83836	,	'	XANXERÊ	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83852	,	'	XAVANTINA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83879	,	'	XAXIM	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83895	,	'	BOM JARDIM DA SERRA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83917	,	'	MARACAJÁ	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83933	,	'	TIMBÉ DO SUL	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83950	,	'	CORREIA PINTO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83976	,	'	OTACÍLIO COSTA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	99392	,	'	ABDON BATISTA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	99414	,	'	APIÚNA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	99430	,	'	CELSO RAMOS	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	99457	,	'	DOUTOR PEDRINHO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	99511	,	'	IPORÃ DO OESTE	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	99538	,	'	IRACEMINHA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	99570	,	'	JOSÉ BOITEUX	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	99619	,	'	LINDÓIA DO SUL	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	99635	,	'	MAREMA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	99678	,	'	SANTA ROSA DO SUL	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	99716	,	'	TIMBÓ GRANDE	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	99732	,	'	UNIÃO DO OESTE	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	99759	,	'	URUPEMA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	99775	,	'	VITOR MEIRELES	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	99856	,	'	ITAPOÁ	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	99899	,	'	SERRA ALTA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	99910	,	'	TUNÁPOLIS	'	)


INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaQuadro03]
           ([NumeroItem]
           ,[Descricao]
           ,[TipoItem])

SELECT '010','Valor contábil','1' UNION ALL
SELECT '020','Base de cálculo','1' UNION ALL
SELECT '030','Imposto creditado','1' UNION ALL
SELECT '040','Operações isentas ou não tributadas','1' UNION ALL
SELECT '050','Outras operações sem crédito de imposto','1' UNION ALL
SELECT '053','Base de Cálculo Imposto Retido','1' UNION ALL
SELECT '054','Imposto Retido','1' UNION ALL
SELECT '057','Imposto Diferencial Alíquota','1' UNION ALL
SELECT '060','Valor Contábil','2' UNION ALL
SELECT '070','Base de Cálculo','2' UNION ALL
SELECT '080','Imposto debitado','2' UNION ALL
SELECT '090','Operações isentas ou não tributadas','2' UNION ALL
SELECT '100','Outras operações sem débito de imposto','2' UNION ALL
SELECT '103','Base de Cálculo Imposto Retido','2' UNION ALL
SELECT '104','Imposto Retido','2' 

/*
-- [DIME_TabelaQuadro04]
*/
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaQuadro04]
           ([NumeroItem]
           ,[Descricao]
           ,[TipoItem])

SELECT '010','(+) Débito pelas saídas', '1' UNION ALL
SELECT '020','(+) Débito por diferencial de alíquota de ativo permanente','1' UNION ALL
SELECT '030','(+) Débito por diferencial de alíquota do material de uso ou consumo','1' UNION ALL
SELECT '040','(+) Débito de máquinas / equipamentos importados para ativo permanente','1' UNION ALL
SELECT '045','(+) Débito da Diferença de Alíquota de Operação ou Prestação a Consumidor Final de Outro Estado','1' UNION ALL
SELECT '050','(+) Estorno de crédito','2' UNION ALL
SELECT '060','(+) Outros estornos de crédito','2' UNION ALL
SELECT '070','(+) Outros débitos','3' UNION ALL
SELECT '990','(=) Subtotal de Débitos => (transportar para o item 010 do quadro 09 Cálculo do Imposto a Pagar ou Saldo Credor)','3'

/*
-- [DIME_TabelaQuadro05]
*/
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaQuadro05]
           ([NumeroItem]
           ,[Descricao]
           ,[TipoItem])
SELECT '010','(+) Saldo credor do mês anterior','1' UNION ALL
SELECT '020','(+) Crédito pelas entradas','2' UNION ALL
SELECT '030','(+) Crédito de ativo permanente','2' UNION ALL
SELECT '040','(+) Crédito por diferencial de alíquota material de uso / consumo','2' UNION ALL
SELECT '045','(+) Crédito da Diferença de Alíquota de Operação ou Prestação a Consumidor Final de Outro Estado','2' UNION ALL
SELECT '050','(+) Crédito de ICMS retido por substituição tributária','2' UNION ALL
SELECT '990','(=) Subtotal de créditos => (transportar para o item 050 do quadro 09 - Cálculo do Imposto a Pagar ou Saldo Credor)','9'

/*
-- [DIME_TabelaQuadro09]
*/
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaQuadro09]
           ([NumeroItem]
           ,[Descricao]
           ,[TipoItem])
SELECT '010','(+) Subtotal de débitos','1' UNION ALL
SELECT '011','(+) Complemento de débito por mudança de regime de apuração','1' UNION ALL
SELECT '020','(+) Saldos devedores recebidos de estabelecimentos consolidados','1' UNION ALL
SELECT '030','(+) Débito por reserva de crédito acumulado','1' UNION ALL
SELECT '040','(=) Total de débitos','1' UNION ALL
SELECT '050','(+) Subtotal de créditos','2' UNION ALL
SELECT '060','(+) Saldos credores recebidos de estabelecimentos consolidados','2' UNION ALL
SELECT '070','(+) Créditos recebidos por transferência de outros contribuintes','2' UNION ALL
SELECT '075','(+) Créditos declarados no DCIP','2' UNION ALL
SELECT '080','(=) Total de créditos','2 ' UNION ALL
SELECT '090','(+) Imposto do 1º decêndio','3' UNION ALL
SELECT '100','(+) Imposto do 2º decêndio','3' UNION ALL
SELECT '105','(+) Antecipações Combustíveis líquidos e gasosos','3' UNION ALL
SELECT '110','(=) Total de ajustes da apuração decendial e antecipações','3' UNION ALL
SELECT '120','(=) Saldo devedor (Total de Débitos - (Total de Créditos + Total de ajustes da apuração decendial e antecipações))','4' UNION ALL
SELECT '130','(-) Saldo devedor transferido ao estabelecimento consolidador','4' UNION ALL
SELECT '140','((=) Saldo Credor (Total de Créditos + Total de ajustes da apuração decendial e antecipações) - (Total de Débitos))','5' UNION ALL
SELECT '150','(-) Saldo credor transferido ao estabelecimento consolidador','5' UNION ALL
SELECT '160','Saldo credor transferível relativo à exportação','6' UNION ALL
SELECT '170','Saldo credor transferível relativo a saídas isentas','6' UNION ALL
SELECT '180','Saldo credor transferível relativo a saídas diferidas','6' UNION ALL
SELECT '190','Saldo credor relativo a outros créditos','6' UNION ALL
SELECT '998','(=) Saldo Credor para o mês seguinte','5' UNION ALL
SELECT '999','(=) Imposto a recolher','4'

/*
-- [DIME_TabelaQuadro11]
*/
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaQuadro11]
           ([NumeroItem]
           ,[Descricao]
           ,[TipoItem])
SELECT '010','Valor dos produtos (não preencher a partir do período de referência agosto de 2013)','0' UNION ALL
SELECT '020','Valor do IPI (não preencher a partir do período de referência agosto de 2013)','0' UNION ALL
SELECT '030','Despesas acessórias (não preencher a partir do período de referência agosto de 2013)','0' UNION ALL
SELECT '040','Base de cálculo do ICMS próprio (não preencher a partir do período de referência agosto de 2013)','0' UNION ALL
SELECT '050','ICMS próprio (não preencher a partir do período de referência agosto de 2013)','0' UNION ALL
SELECT '060','Base cálculo do imposto retido','0' UNION ALL
SELECT '065','Imposto Retido apurado por mercadoria e recolhido por operação','0' UNION ALL
SELECT '070','(+) Imposto retido com apuração mensal','1' UNION ALL
SELECT '073','(+) Imposto Retido pelo AEHC com regime especial de apuração mensal','1' UNION ALL
SELECT '075','(+) Saldos devedores recebidos de estabelecimentos consolidados','1' UNION ALL
SELECT '080','Total de débitos','1' UNION ALL
SELECT '090','(+) Saldo credor do período anterior sobre a substituição tributária','2' UNION ALL
SELECT '100','(+) Devolução de mercadorias e desfazimento de venda (não preencher a partir do período de referência agosto de 2013)','2' UNION ALL
SELECT '105','(+) Créditos declarados no DCIP','2' UNION ALL
SELECT '110','(+) Ressarcimento de ICMS substituição tributária (não preencher a partir do período de referência agosto de 2013)','2' UNION ALL
SELECT '120','(+) Outros créditos (não preencher a partir do período de referência agosto de 2013)','2' UNION ALL
SELECT '125','(+) Saldos credores recebidos de estabelecimentos consolidados','2' UNION ALL
SELECT '130','(=) Total de créditos','2' UNION ALL
SELECT '140','Não se aplica','3' UNION ALL
SELECT '150','Não se aplica','3' UNION ALL
SELECT '155','(+) Antecipações Combustíveis líquidos e gasosos','3' UNION ALL
SELECT '160','(=) Total de ajustes das Antecipações Combustíveis','3' UNION ALL
SELECT '170','(=) Saldo devedor (Total de Débitos - (Total de Créditos + Total de ajustes das antecipações combustíveis))','4' UNION ALL
SELECT '180','(-) Saldo devedor transferido ao estabelecimento consolidador','4' UNION ALL
SELECT '190','((=) Saldo Credor (Total de Créditos + Total de ajustes das antecipações combustíveis) - (Total de Débitos))','5' UNION ALL
SELECT '200','(-) Saldo credor transferido ao estabelecimento consolidador','5' UNION ALL
SELECT '998','(=) Saldo Credor para o mês seguinte','5' UNION ALL
SELECT '999','(=) Imposto a recolher sobre a substituição tributária','4' 

INSERT INTO [SPS_Fiscal].[dbo].[DIME_TipoRegistro]
           ([Quadro]
           ,[TipoRegistro]
           ,[Descricao])
     
SELECT '01','22','Valores Fiscais Entradas' UNION ALL
SELECT '02','23','Valores Fiscais Saídas' UNION ALL
SELECT '03','24','Resumo dos Valores Fiscais' UNION ALL
SELECT '04','25','Resumo da Apuração dos Débitos' UNION ALL
SELECT '05','26','Resumo da Apuração dos Créditos' UNION ALL
SELECT '09','30','Cálculo do Imposto a Pagar ou Saldo Credor' UNION ALL
SELECT '11','32','Informações sobre Substituição Tributária' UNION ALL
SELECT '12','33','Discriminação dos Pagamentos do Imposto e dos Débitos Específicos' UNION ALL
SELECT '46','46','Créditos por Regimes e Autorizações Especiais' UNION ALL
SELECT '48','48','Informações para Rateio do Valor Adicionado' UNION ALL
SELECT '49','49','Entradas por Unidade da Federação' UNION ALL
SELECT '50','50','Saídas por Unidade da Federação' ", Support.Conectar());
                    #endregion
                    //SqlCommand command = new SqlCommand(@"INSERT INTO [DIME_Contabilista]([CPF],[NomeContabilista]) VALUES (@CPF, @NomeContabilista)", Support.Conectar());
                    command.ExecuteNonQuery();

                    Conexao.uiApplication.SetStatusBarMessage("Tabela Criada com sucesso", SAPbouiCOM.BoMessageTime.bmt_Short, false);
                    Criar = true;
                }
                if (Criar)
                {
                    Conexao.uiApplication.SetStatusBarMessage("Tabela ja existe", SAPbouiCOM.BoMessageTime.bmt_Short, false);
                }

                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region OUTROS EVENTOS
        public bool FormDataEvent()
        {
            throw new NotImplementedException();
        }

        public bool AppEvent()
        {
            throw new NotImplementedException();
        }

        public bool PrintEvent()
        {
            throw new NotImplementedException();
        }

        public bool ProgressBarEvent()
        {
            throw new NotImplementedException();
        }

        public bool ReportDataEvent()
        {
            throw new NotImplementedException();
        }

        public bool RightClickEvent()
        {
            throw new NotImplementedException();
        }

        public bool StatusBarEvent()
        {
            throw new NotImplementedException();
        }

        public void Select()
        {
            throw new NotImplementedException();
        }

        public void Close()
        {
            throw new NotImplementedException();
        }

        public void Refresh()
        {
            throw new NotImplementedException();
        }

        public string GetAsXML()
        {
            throw new NotImplementedException();
        }

        public void Update()
        {
            throw new NotImplementedException();
        }

        public void Freeze(bool newVal)
        {
            throw new NotImplementedException();
        }

        public void EnableMenu(string MenuUID, bool EnableFlag)
        {
            throw new NotImplementedException();
        }

        public void ResetMenuStatus()
        {
            throw new NotImplementedException();
        }

        public void Resize(int lWidth, int lHeight)
        {
            throw new NotImplementedException();
        }

        public void EnableFormatSearch()
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
