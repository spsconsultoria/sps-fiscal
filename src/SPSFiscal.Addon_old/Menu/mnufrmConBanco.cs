﻿using SAPbouiCOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace SPS_Fiscal.Addon
{
    class mnufrmConBanco:IForm
    {
        private MenuEvent _eventoMenu;
        public mnufrmConBanco(MenuEvent evento)
        {
            this._eventoMenu = evento;
        }

        public bool ItemEvent()
        {
            throw new NotImplementedException();
        }

        public bool MenuEvent()
        {
            try
            {
                if (!_eventoMenu.BeforeAction)
                {
                    #region ABRIR TELA
                    SAPbouiCOM.Form oForm = null;
                    XmlDocument xmlDoc;
                    string appPath = System.Windows.Forms.Application.StartupPath;
                    if (!appPath.EndsWith(@"\")) appPath += @"\";
                    //carregar XML
                    xmlDoc = new XmlDocument();
                    xmlDoc.Load(appPath + @"\srf\frmConBanco.srf");
                    string strXML = xmlDoc.InnerXml;

                    SAPbouiCOM.FormCreationParams oCreationParams = ((SAPbouiCOM.FormCreationParams)(Conexao.uiApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_FormCreationParams)));
                    oCreationParams.XmlData = strXML;

                    oForm = Conexao.uiApplication.Forms.AddEx(oCreationParams);

                    #endregion

                    string sql = "select * from [@SPS_CON]";
                    System.Data.DataTable dt = Conexao.ExecuteSqlDataTable(sql);

                    if (dt.Rows.Count > 0)
                    {
                        oForm.DataSources.UserDataSources.Item("User").Value = dt.Rows[0][2].ToString();
                        oForm.DataSources.UserDataSources.Item("Pass").Value = dt.Rows[0][3].ToString();
                        oForm.DataSources.UserDataSources.Item("Server").Value = dt.Rows[0][4].ToString();
                        frmfrmConBanco.Novo = false;
                    }
                    else
                    {
                        frmfrmConBanco.Novo = true;
                    }

                }
                return true;
            }
            catch (Exception)
            {

                throw;
            } 
        }

        public bool FormDataEvent()
        {
            throw new NotImplementedException();
        }

        public bool AppEvent()
        {
            throw new NotImplementedException();
        }

        public bool PrintEvent()
        {
            throw new NotImplementedException();
        }

        public bool ProgressBarEvent()
        {
            throw new NotImplementedException();
        }

        public bool ReportDataEvent()
        {
            throw new NotImplementedException();
        }

        public bool RightClickEvent()
        {
            throw new NotImplementedException();
        }

        public bool StatusBarEvent()
        {
            throw new NotImplementedException();
        }
    }
}
