﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace SPS_Fiscal.Addon
{
    public static class DB
    {
        public static void CriaDB()
        {
            try
            {
                string sql = string.Format(@"SELECT name FROM sys.databases WHERE name = 'SPS_Fiscal'");
                string resultado = Convert.ToString(Conexao.ExecuteSqlScalar(sql));

                SqlCommand command = new SqlCommand();

                if (string.IsNullOrEmpty(resultado))
                {
                    sql = "CREATE DATABASE [SPS_Fiscal]";
                    Conexao.ExecuteSqlScalar(sql);
                }

                #region CRIANDO TABELAS
                command = new SqlCommand(@"--CRIANDO TABELA MUNICIPIOS
IF NOT EXISTS(SELECT Name FROM SYSOBJECTS WHERE  XTYPE = 'U' AND NAME = 'DIME_TabelaMunicipios') 
CREATE TABLE [dime].[TabelaMunicipios](
	[Codigo] [varchar](5) NOT NULL,
	[NomeMunicipio] [varchar](200) NULL,
PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

IF NOT EXISTS(SELECT Name FROM SYSOBJECTS WHERE  XTYPE = 'U' AND NAME = 'DIME_TabelaCodAtividade')
--CRIANDO TABELA CODATIVIDADE
CREATE TABLE [dbo].[DIME_TabelaCodAtividade](
	[Codigo] [varchar](3) NOT NULL,
	[Descricao] [varchar](300) NULL,
PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

IF NOT EXISTS(SELECT Name FROM SYSOBJECTS WHERE  XTYPE = 'U' AND NAME = 'DIME_Contabilista')
--CRIANDO TABELA CONTABILISTA
CREATE TABLE [dbo].[DIME_Contabilista](
	[IdContabilista] [int] IDENTITY(1,1) NOT NULL,
	[CPF] [varchar](15) NULL,
	[NomeContabilista] [varchar](50) NULL,
	[Excluido] [varchar](1) NULL,
PRIMARY KEY CLUSTERED 
(
	[IdContabilista] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

IF NOT EXISTS(SELECT Name FROM SYSOBJECTS WHERE  XTYPE = 'U' AND NAME = 'DIME_Contribuinte')
--CRIANDO TABELA CONTRIBUENTE
CREATE TABLE [dbo].[DIME_Contribuinte](
	[IdContribuinte] [int] IDENTITY(1,1) NOT NULL,
	[IdContabilista] [int] NULL,
	[NomeRazaoSocial] [varchar](50) NULL,
	[CNPJ] [varchar](18) NULL,
	[NroInscricao] [varchar](20) NULL,
	[TipoDeclaracao] [varchar](1) NULL,
	[RegimeApuracao] [varchar](1) NULL,
	[PorteEmpresa] [varchar](1) NULL,
	[ApuracaoConsolidada] [varchar](1) NULL,
	[ApuracaoCentralizada] [varchar](1) NULL,
	[TemCreditosPresumido] [varchar](1) NULL,
	[TemCredIncentFiscais] [varchar](1) NULL,
	[SubstitutoTributario] [varchar](1) NULL,
	[TemEscritaContabil] [varchar](1) NULL,
	[QtdeTrabAtividade] [int] NULL,
	[ErpIdEmpresa] [varchar](10) NULL,
	[ErpBaseDados] [varchar](50) NULL,
	[Excluido] [varchar](1) NULL,
PRIMARY KEY CLUSTERED 
(
	[IdContribuinte] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[DIME_Contribuinte]  WITH CHECK ADD FOREIGN KEY([IdContabilista])
REFERENCES [dbo].[DIME_Contabilista] ([IdContabilista])

IF NOT EXISTS(SELECT Name FROM SYSOBJECTS WHERE  XTYPE = 'U' AND NAME = 'DIME_Declaracao')
--CRIANDO TABELA DECLARAÇÃO
CREATE TABLE [dbo].[DIME_Declaracao](
	[IdDeclaracao] [int] IDENTITY(1,1) NOT NULL,
	[DtInicio] [datetime] NULL,
	[DtTermino] [datetime] NULL,
	[NomeContabilista] [varchar](50) NULL,
	[ContabilistaCPF] [varchar](15) NULL,
	[NomeContribuinte] [varchar](50) NULL,
	[NroInscricao] [varchar](20) NULL,
	[ContribuinteCNPJ] [varchar](18) NULL,
	[PeriodoReferencia] [varchar](8) NULL,
	[TipoDeclaracao] [varchar](1) NULL,
	[RegimeApuracao] [varchar](1) NULL,
	[PorteEmpresa] [varchar](1) NULL,
	[ApuracaoConsolidada] [varchar](1) NULL,
	[ApuracaoCentralizada] [varchar](1) NULL,
	[TransCredPeriodo] [varchar](1) NULL,
	[TemCreditosPresumido] [varchar](1) NULL,
	[TemCredIncentFiscais] [varchar](1) NULL,
	[Movimento] [varchar](1) NULL,
	[SubstitutoTributario] [varchar](1) NULL,
	[TemEscritaContabil] [varchar](1) NULL,
	[QtdeTrabAtividade] [int] NULL,
	[Excluido] [varchar](1) NULL,
PRIMARY KEY CLUSTERED 
(
	[IdDeclaracao] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

IF NOT EXISTS(SELECT Name FROM SYSOBJECTS WHERE  XTYPE = 'U' AND NAME = 'DIME_ContribuinteDeclaracao')
--CRIANDO TABELA CONTRIBUENTE DECLARAÇÃO
CREATE TABLE [dbo].[DIME_ContribuinteDeclaracao](
	[IdDeclaracao] [int] NOT NULL,
	[IdContribuinte] [int] NOT NULL,
	[PeriodoReferencia] [varchar](8) NULL,
	[DtCriacao] [datetime] NULL,
	[HrCriacao] [varchar](8) NULL,
PRIMARY KEY CLUSTERED 
(
	[IdDeclaracao] ASC,
	[IdContribuinte] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[DIME_ContribuinteDeclaracao]  WITH CHECK ADD FOREIGN KEY([IdContribuinte])
REFERENCES [dbo].[DIME_Contribuinte] ([IdContribuinte])

ALTER TABLE [dbo].[DIME_ContribuinteDeclaracao]  WITH CHECK ADD FOREIGN KEY([IdContribuinte])
REFERENCES [dbo].[DIME_Contribuinte] ([IdContribuinte])

ALTER TABLE [dbo].[DIME_ContribuinteDeclaracao]  WITH CHECK ADD FOREIGN KEY([IdDeclaracao])
REFERENCES [dbo].[DIME_Declaracao] ([IdDeclaracao])

IF NOT EXISTS(SELECT Name FROM SYSOBJECTS WHERE  XTYPE = 'U' AND NAME = 'DIME_TipoRegistro')
--CRIANDO TABELA TIPO REGISTRO
CREATE TABLE [dbo].[DIME_TipoRegistro](
	[Quadro] [varchar](5) NOT NULL,
	[TipoRegistro] [varchar](10) NOT NULL,
	[Descricao] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[TipoRegistro] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

IF NOT EXISTS(SELECT Name FROM SYSOBJECTS WHERE  XTYPE = 'U' AND NAME = 'DIME_RegistroDeclaracao')
--CRIANDO TABELA REGISTRO DECLARAÇÃO
CREATE TABLE [dbo].[DIME_RegistroDeclaracao](
	[IdDeclaracao] [int] NOT NULL,
	[TipoRegistro] [varchar](10) NOT NULL
) ON [PRIMARY]

ALTER TABLE [dbo].[DIME_RegistroDeclaracao]  WITH CHECK ADD FOREIGN KEY([IdDeclaracao])
REFERENCES [dbo].[DIME_Declaracao] ([IdDeclaracao])

ALTER TABLE [dbo].[DIME_RegistroDeclaracao]  WITH CHECK ADD FOREIGN KEY([TipoRegistro])
REFERENCES [dbo].[DIME_TipoRegistro] ([TipoRegistro])

IF NOT EXISTS(SELECT Name FROM SYSOBJECTS WHERE  XTYPE = 'U' AND NAME = 'DIME_DadosQuadro01')
--CRIANDO TABELA DADOS QUADRO 01
CREATE TABLE [dbo].[DIME_DadosQuadro01](
	[IdLancamento] [int] IDENTITY(1,1) NOT NULL,
	[IdDeclaracao] [int] NOT NULL,
	[CFOP] [varchar](5) NULL,
	[Vl_Contabil] [numeric](15, 2) NULL,
	[Vl_Base_Calc] [numeric](15, 2) NULL,
	[Vl_Imp_Cred] [numeric](15, 2) NULL,
	[Vl_Isentas_Nao_Trib] [numeric](15, 2) NULL,
	[Vl_Outras] [numeric](15, 2) NULL,
	[Vl_Base_Calc_IR] [numeric](15, 2) NULL,
	[Vl_IR] [numeric](15, 2) NULL,
	[Vl_Difal] [numeric](15, 2) NULL,
PRIMARY KEY CLUSTERED 
(
	[IdLancamento] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[DIME_DadosQuadro01]  WITH CHECK ADD FOREIGN KEY([IdDeclaracao])
REFERENCES [dbo].[DIME_Declaracao] ([IdDeclaracao])

IF NOT EXISTS(SELECT Name FROM SYSOBJECTS WHERE  XTYPE = 'U' AND NAME = 'DIME_DadosQuadro02')
--CRIANDO TABELA DADOS QUADRO 02
CREATE TABLE [dbo].[DIME_DadosQuadro02](
	[IdLancamento] [int] IDENTITY(1,1) NOT NULL,
	[IdDeclaracao] [int] NOT NULL,
	[CFOP] [varchar](5) NULL,
	[Vl_Contabil] [numeric](15, 2) NULL,
	[Vl_Base_Calc] [numeric](15, 2) NULL,
	[Vl_Imp_Debit] [numeric](15, 2) NULL,
	[Vl_Isentas_Nao_Trib] [numeric](15, 2) NULL,
	[Vl_Outras] [numeric](15, 2) NULL,
	[Vl_Base_Calc_IR] [numeric](15, 2) NULL,
	[Vl_IR] [numeric](15, 2) NULL,
PRIMARY KEY CLUSTERED 
(
	[IdLancamento] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[DIME_DadosQuadro02]  WITH CHECK ADD FOREIGN KEY([IdDeclaracao])
REFERENCES [dbo].[DIME_Declaracao] ([IdDeclaracao])

IF NOT EXISTS(SELECT Name FROM SYSOBJECTS WHERE  XTYPE = 'U' AND NAME = 'DIME_TabelaQuadro03')
--CRIANDO TABELA TABELA QUADRO 03
CREATE TABLE [dbo].[DIME_TabelaQuadro03](
	[NumeroItem] [varchar](3) NOT NULL,
	[Descricao] [varchar](200) NULL,
	[TipoItem] [varchar](2) NULL,
PRIMARY KEY CLUSTERED 
(
	[NumeroItem] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

IF NOT EXISTS(SELECT Name FROM SYSOBJECTS WHERE  XTYPE = 'U' AND NAME = 'DIME_DadosQuadro03')
--CRIANDO TABELA DADOS QUADRO 03
CREATE TABLE [dbo].[DIME_DadosQuadro03](
	[NumeroItem] [varchar](3) NOT NULL,
	[IdDeclaracao] [int] NOT NULL,
	[Valor] [numeric](15, 2) NULL,
PRIMARY KEY CLUSTERED 
(
	[NumeroItem] ASC,
	[IdDeclaracao] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[DIME_DadosQuadro03]  WITH CHECK ADD FOREIGN KEY([IdDeclaracao])
REFERENCES [dbo].[DIME_Declaracao] ([IdDeclaracao])

ALTER TABLE [dbo].[DIME_DadosQuadro03]  WITH CHECK ADD FOREIGN KEY([IdDeclaracao])
REFERENCES [dbo].[DIME_Declaracao] ([IdDeclaracao])

ALTER TABLE [dbo].[DIME_DadosQuadro03]  WITH CHECK ADD FOREIGN KEY([IdDeclaracao])
REFERENCES [dbo].[DIME_Declaracao] ([IdDeclaracao])

ALTER TABLE [dbo].[DIME_DadosQuadro03]  WITH CHECK ADD FOREIGN KEY([NumeroItem])
REFERENCES [dbo].[DIME_TabelaQuadro03] ([NumeroItem])

ALTER TABLE [dbo].[DIME_DadosQuadro03]  WITH CHECK ADD FOREIGN KEY([NumeroItem])
REFERENCES [dbo].[DIME_TabelaQuadro03] ([NumeroItem])

IF NOT EXISTS(SELECT Name FROM SYSOBJECTS WHERE  XTYPE = 'U' AND NAME = 'DIME_TabelaQuadro04')
--CRIANDO TABELA TABELA QUADRO 04
CREATE TABLE [dbo].[DIME_TabelaQuadro04](
	[NumeroItem] [varchar](3) NOT NULL,
	[Descricao] [varchar](200) NULL,
	[TipoItem] [varchar](2) NULL,
PRIMARY KEY CLUSTERED 
(
	[NumeroItem] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

IF NOT EXISTS(SELECT Name FROM SYSOBJECTS WHERE  XTYPE = 'U' AND NAME = 'DIME_DadosQuadro04')
--CRIANDO TABELA DADOS QUADRO 04
CREATE TABLE [dbo].[DIME_DadosQuadro04](
	[NumeroItem] [varchar](3) NOT NULL,
	[IdDeclaracao] [int] NOT NULL,
	[Valor] [numeric](15, 2) NULL,
PRIMARY KEY CLUSTERED 
(
	[NumeroItem] ASC,
	[IdDeclaracao] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[DIME_DadosQuadro04]  WITH CHECK ADD FOREIGN KEY([IdDeclaracao])
REFERENCES [dbo].[DIME_Declaracao] ([IdDeclaracao])

ALTER TABLE [dbo].[DIME_DadosQuadro04]  WITH CHECK ADD FOREIGN KEY([NumeroItem])
REFERENCES [dbo].[DIME_TabelaQuadro04] ([NumeroItem])

IF NOT EXISTS(SELECT Name FROM SYSOBJECTS WHERE  XTYPE = 'U' AND NAME = 'DIME_TabelaQuadro05')
--CRIANDO TABELA TABELA QUADRO 05
CREATE TABLE [dbo].[DIME_TabelaQuadro05](
	[NumeroItem] [varchar](3) NOT NULL,
	[Descricao] [varchar](200) NULL,
	[TipoItem] [varchar](2) NULL,
PRIMARY KEY CLUSTERED 
(
	[NumeroItem] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

IF NOT EXISTS(SELECT Name FROM SYSOBJECTS WHERE  XTYPE = 'U' AND NAME = 'DIME_DadosQuadro05')
--CRIANDO TABELA DADOS QUADRO 05
CREATE TABLE [dbo].[DIME_DadosQuadro05](
	[NumeroItem] [varchar](3) NOT NULL,
	[IdDeclaracao] [int] NOT NULL,
	[Valor] [numeric](15, 2) NULL,
PRIMARY KEY CLUSTERED 
(
	[NumeroItem] ASC,
	[IdDeclaracao] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[DIME_DadosQuadro05]  WITH CHECK ADD FOREIGN KEY([IdDeclaracao])
REFERENCES [dbo].[DIME_Declaracao] ([IdDeclaracao])

ALTER TABLE [dbo].[DIME_DadosQuadro05]  WITH CHECK ADD FOREIGN KEY([NumeroItem])
REFERENCES [dbo].[DIME_TabelaQuadro05] ([NumeroItem])

IF NOT EXISTS(SELECT Name FROM SYSOBJECTS WHERE  XTYPE = 'U' AND NAME = 'DIME_TabelaQuadro09')
--CRIANDO TABELA TABELA QUADRO 09
CREATE TABLE [dbo].[DIME_TabelaQuadro09](
	[NumeroItem] [varchar](3) NOT NULL,
	[Descricao] [varchar](200) NULL,
	[TipoItem] [varchar](2) NULL,
PRIMARY KEY CLUSTERED 
(
	[NumeroItem] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

IF NOT EXISTS(SELECT Name FROM SYSOBJECTS WHERE  XTYPE = 'U' AND NAME = 'DIME_DadosQuadro09')
--CRIANDO TABELA DADOS QUADRO 09
CREATE TABLE [dbo].[DIME_DadosQuadro09](
	[NumeroItem] [varchar](3) NOT NULL,
	[IdDeclaracao] [int] NOT NULL,
	[Valor] [numeric](15, 2) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[NumeroItem] ASC,
	[IdDeclaracao] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[DIME_DadosQuadro09]  WITH CHECK ADD FOREIGN KEY([IdDeclaracao])
REFERENCES [dbo].[DIME_Declaracao] ([IdDeclaracao])

ALTER TABLE [dbo].[DIME_DadosQuadro09]  WITH CHECK ADD FOREIGN KEY([NumeroItem])
REFERENCES [dbo].[DIME_TabelaQuadro09] ([NumeroItem])

IF NOT EXISTS(SELECT Name FROM SYSOBJECTS WHERE  XTYPE = 'U' AND NAME = 'DIME_TabelaQuadro11')
--CRIANDO TABELA TABELA QUADRO 11
CREATE TABLE [dbo].[DIME_TabelaQuadro11](
	[NumeroItem] [varchar](3) NOT NULL,
	[Descricao] [varchar](200) NULL,
	[TipoItem] [varchar](2) NULL,
PRIMARY KEY CLUSTERED 
(
	[NumeroItem] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

IF NOT EXISTS(SELECT Name FROM SYSOBJECTS WHERE  XTYPE = 'U' AND NAME = 'DIME_DadosQuadro11')
--CRIANDO TABELA DADOS QUADRO 11
CREATE TABLE [dbo].[DIME_DadosQuadro11](
	[NumeroItem] [varchar](3) NOT NULL,
	[IdDeclaracao] [int] NOT NULL,
	[Valor] [numeric](15, 2) NULL,
PRIMARY KEY CLUSTERED 
(
	[NumeroItem] ASC,
	[IdDeclaracao] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[DIME_DadosQuadro11]  WITH CHECK ADD FOREIGN KEY([IdDeclaracao])
REFERENCES [dbo].[DIME_Declaracao] ([IdDeclaracao])

ALTER TABLE [dbo].[DIME_DadosQuadro11]  WITH CHECK ADD FOREIGN KEY([NumeroItem])
REFERENCES [dbo].[DIME_TabelaQuadro11] ([NumeroItem])

IF NOT EXISTS(SELECT Name FROM SYSOBJECTS WHERE  XTYPE = 'U' AND NAME = 'DIME_DadosQuadro12')
--CRIANDO TABELA DADOS QUADRO 12
CREATE TABLE [dbo].[DIME_DadosQuadro12](
	[IdLancamento] [int] IDENTITY(1,1) NOT NULL,
	[IdDeclaracao] [int] NOT NULL,
	[Ind_Org_Recol] [varchar](2) NULL,
	[Cod_Receita] [varchar](5) NULL,
	[Dt_Vcto_Recol] [datetime] NULL,
	[Vl_Recol] [numeric](15, 2) NULL,
	[Cod_Class_Vcto] [varchar](5) NULL,
	[Nro_Acordo] [varchar](15) NULL,
PRIMARY KEY CLUSTERED 
(
	[IdLancamento] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[DIME_DadosQuadro12]  WITH CHECK ADD FOREIGN KEY([IdDeclaracao])
REFERENCES [dbo].[DIME_Declaracao] ([IdDeclaracao])

IF NOT EXISTS(SELECT Name FROM SYSOBJECTS WHERE  XTYPE = 'U' AND NAME = 'DIME_DadosQuadro46')
--CRIANDO TABELA DADOS QUADRO 46
CREATE TABLE [dbo].[DIME_DadosQuadro46](
	[IdLancamento] [int] IDENTITY(1,1) NOT NULL,
	[IdDeclaracao] [int] NOT NULL,
	[Seq_Reg] [int] NOT NULL,
	[Cod_Identif] [varchar](15) NULL,
	[Vl_Cred_Apur] [numeric](15, 2) NULL,
	[Ind_Origem] [varchar](2) NULL,
PRIMARY KEY CLUSTERED 
(
	[IdLancamento] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[DIME_DadosQuadro46]  WITH CHECK ADD FOREIGN KEY([IdDeclaracao])
REFERENCES [dbo].[DIME_Declaracao] ([IdDeclaracao])

IF NOT EXISTS(SELECT Name FROM SYSOBJECTS WHERE  XTYPE = 'U' AND NAME = 'DIME_DadosQuadro48')
--CRIANDO TABELA DADOS QUADRO 48
CREATE TABLE [dbo].[DIME_DadosQuadro48](
	[IdLancamento] [int] IDENTITY(1,1) NOT NULL,
	[IdDeclaracao] [int] NOT NULL,
	[Cod_Municipio] [varchar](5) NULL,
	[Vl_Perc_Ad] [numeric](15, 2) NULL,
	[Cod_Tipo_Atv] [varchar](3) NULL,
PRIMARY KEY CLUSTERED 
(
	[IdLancamento] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[DIME_DadosQuadro48]  WITH CHECK ADD FOREIGN KEY([IdDeclaracao])
REFERENCES [dbo].[DIME_Declaracao] ([IdDeclaracao])

IF NOT EXISTS(SELECT Name FROM SYSOBJECTS WHERE  XTYPE = 'U' AND NAME = 'DIME_DadosQuadro49')
--CRIANDO TABELA DADOS QUADRO 49
CREATE TABLE [dbo].[DIME_DadosQuadro49](
	[IdLancamento] [int] IDENTITY(1,1) NOT NULL,
	[IdDeclaracao] [int] NOT NULL,
	[Sigla_Estado] [varchar](2) NULL,
	[Vl_Contabil] [numeric](15, 2) NULL,
	[Vl_Base_Calc] [numeric](15, 2) NULL,
	[Vl_Outras] [numeric](15, 2) NULL,
	[Vl_ST] [numeric](15, 2) NULL,
	[Vl_ST_Outros] [numeric](15, 2) NULL,
PRIMARY KEY CLUSTERED 
(
	[IdLancamento] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[DIME_DadosQuadro49]  WITH CHECK ADD FOREIGN KEY([IdDeclaracao])
REFERENCES [dbo].[DIME_Declaracao] ([IdDeclaracao])

IF NOT EXISTS(SELECT Name FROM SYSOBJECTS WHERE  XTYPE = 'U' AND NAME = 'DIME_DadosQuadro50')
--CRIANDO TABELA DADOS QUADRO 50
CREATE TABLE [dbo].[DIME_DadosQuadro50](
	[IdLancamento] [int] IDENTITY(1,1) NOT NULL,
	[IdDeclaracao] [int] NOT NULL,
	[Sigla_Estado] [varchar](2) NULL,
	[Vl_Contabil_Nao_Contrib] [numeric](15, 2) NULL,
	[Vl_Contabil_Contrib] [numeric](15, 2) NULL,
	[Vl_Base_Calc_Nao_Contrib] [numeric](15, 2) NULL,
	[Vl_Base_Calc_Contrib] [numeric](15, 2) NULL,
	[Vl_Outras] [numeric](15, 2) NULL,
	[Vl_ST] [numeric](15, 2) NULL,
PRIMARY KEY CLUSTERED 
(
	[IdLancamento] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[DIME_DadosQuadro50]  WITH CHECK ADD FOREIGN KEY([IdDeclaracao])
REFERENCES [dbo].[DIME_Declaracao] ([IdDeclaracao])

IF NOT EXISTS(SELECT Name FROM SYSOBJECTS WHERE  XTYPE = 'U' AND NAME = 'DIME_TabelaQuadro80')
--CRIANDO TABELA TABELA QUADRO 80
CREATE TABLE [dbo].[DIME_TabelaQuadro80](
	[NumeroItem] [varchar](3) NOT NULL,
	[Descricao] [varchar](200) NULL,
	[TipoItem] [varchar](2) NULL,
PRIMARY KEY CLUSTERED 
(
	[NumeroItem] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

IF NOT EXISTS(SELECT Name FROM SYSOBJECTS WHERE  XTYPE = 'U' AND NAME = 'DIME_DadosQuadro80')
--CRIANDO TABELA DADOS QUADRO 80
CREATE TABLE [dbo].[DIME_DadosQuadro80](
	[NumeroItem] [varchar](3) NOT NULL,
	[IdDeclaracao] [int] NOT NULL,
	[Valor] [numeric](15, 2) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[NumeroItem] ASC,
	[IdDeclaracao] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[DIME_DadosQuadro80]  WITH CHECK ADD FOREIGN KEY([IdDeclaracao])
REFERENCES [dbo].[DIME_Declaracao] ([IdDeclaracao])

ALTER TABLE [dbo].[DIME_DadosQuadro80]  WITH CHECK ADD FOREIGN KEY([NumeroItem])
REFERENCES [dbo].[DIME_TabelaQuadro80] ([NumeroItem])

IF NOT EXISTS(SELECT Name FROM SYSOBJECTS WHERE  XTYPE = 'U' AND NAME = 'DIME_TabelaQuadro81')
--CRIANDO TABELA TABELA QUADRO 81
CREATE TABLE [dbo].[DIME_TabelaQuadro81](
	[NumeroItem] [varchar](3) NOT NULL,
	[Descricao] [varchar](200) NULL,
	[TipoItem] [varchar](2) NULL,
PRIMARY KEY CLUSTERED 
(
	[NumeroItem] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

IF NOT EXISTS(SELECT Name FROM SYSOBJECTS WHERE  XTYPE = 'U' AND NAME = 'DIME_DadosQuadro81')
--CRIANDO TABELA DADOS QUADRO 81
CREATE TABLE [dbo].[DIME_DadosQuadro81](
	[NumeroItem] [varchar](3) NOT NULL,
	[IdDeclaracao] [int] NOT NULL,
	[Valor] [numeric](15, 2) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[NumeroItem] ASC,
	[IdDeclaracao] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[DIME_DadosQuadro81]  WITH CHECK ADD FOREIGN KEY([IdDeclaracao])
REFERENCES [dbo].[DIME_Declaracao] ([IdDeclaracao])

ALTER TABLE [dbo].[DIME_DadosQuadro81]  WITH CHECK ADD FOREIGN KEY([NumeroItem])
REFERENCES [dbo].[DIME_TabelaQuadro81] ([NumeroItem])

IF NOT EXISTS(SELECT Name FROM SYSOBJECTS WHERE  XTYPE = 'U' AND NAME = 'DIME_TabelaQuadro82')
--CRIANDO TABELA TABELA QUADRO 82
CREATE TABLE [dbo].[DIME_TabelaQuadro82](
	[NumeroItem] [varchar](3) NOT NULL,
	[Descricao] [varchar](200) NULL,
	[TipoItem] [varchar](2) NULL,
PRIMARY KEY CLUSTERED 
(
	[NumeroItem] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

IF NOT EXISTS(SELECT Name FROM SYSOBJECTS WHERE  XTYPE = 'U' AND NAME = 'DIME_DadosQuadro82')
--CRIANDO TABELA DADOS QUADRO 82
CREATE TABLE [dbo].[DIME_DadosQuadro82](
	[NumeroItem] [varchar](3) NOT NULL,
	[IdDeclaracao] [int] NOT NULL,
	[Valor] [numeric](15, 2) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[NumeroItem] ASC,
	[IdDeclaracao] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[DIME_DadosQuadro82]  WITH CHECK ADD FOREIGN KEY([IdDeclaracao])
REFERENCES [dbo].[DIME_Declaracao] ([IdDeclaracao])

ALTER TABLE [dbo].[DIME_DadosQuadro82]  WITH CHECK ADD FOREIGN KEY([NumeroItem])
REFERENCES [dbo].[DIME_TabelaQuadro82] ([NumeroItem])

IF NOT EXISTS(SELECT Name FROM SYSOBJECTS WHERE  XTYPE = 'U' AND NAME = 'DIME_TabelaQuadro83')
--CRIANDO TABELA TABELA QUADRO 83
CREATE TABLE [dbo].[DIME_TabelaQuadro83](
	[NumeroItem] [varchar](3) NOT NULL,
	[Descricao] [varchar](200) NULL,
	[TipoItem] [varchar](2) NULL,
PRIMARY KEY CLUSTERED 
(
	[NumeroItem] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

IF NOT EXISTS(SELECT Name FROM SYSOBJECTS WHERE  XTYPE = 'U' AND NAME = 'DIME_DadosQuadro83')
--CRIANDO TABELA DADOS QUADRO 83
CREATE TABLE [dbo].[DIME_DadosQuadro83](
	[NumeroItem] [varchar](3) NOT NULL,
	[IdDeclaracao] [int] NOT NULL,
	[Valor] [numeric](15, 2) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[NumeroItem] ASC,
	[IdDeclaracao] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[DIME_DadosQuadro83]  WITH CHECK ADD FOREIGN KEY([IdDeclaracao])
REFERENCES [dbo].[DIME_Declaracao] ([IdDeclaracao])

ALTER TABLE [dbo].[DIME_DadosQuadro83]  WITH CHECK ADD FOREIGN KEY([NumeroItem])
REFERENCES [dbo].[DIME_TabelaQuadro83] ([NumeroItem])

IF NOT EXISTS(SELECT Name FROM SYSOBJECTS WHERE  XTYPE = 'U' AND NAME = 'DIME_TabelaQuadro84')
--CRIANDO TABELA TABELA QUADRO 84
CREATE TABLE [dbo].[DIME_TabelaQuadro84](
	[NumeroItem] [varchar](3) NOT NULL,
	[Descricao] [varchar](200) NULL,
	[TipoItem] [varchar](2) NULL,
PRIMARY KEY CLUSTERED 
(
	[NumeroItem] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

IF NOT EXISTS(SELECT Name FROM SYSOBJECTS WHERE  XTYPE = 'U' AND NAME = 'DIME_DadosQuadro84')
--CRIANDO TABELA DADOS QUADRO 84
CREATE TABLE [dbo].[DIME_DadosQuadro84](
	[NumeroItem] [varchar](3) NOT NULL,
	[IdDeclaracao] [int] NOT NULL,
	[Valor] [numeric](15, 2) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[NumeroItem] ASC,
	[IdDeclaracao] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[DIME_DadosQuadro84]  WITH CHECK ADD FOREIGN KEY([IdDeclaracao])
REFERENCES [dbo].[DIME_Declaracao] ([IdDeclaracao])

ALTER TABLE [dbo].[DIME_DadosQuadro84]  WITH CHECK ADD FOREIGN KEY([NumeroItem])
REFERENCES [dbo].[DIME_TabelaQuadro84] ([NumeroItem])

IF NOT EXISTS(SELECT Name FROM SYSOBJECTS WHERE  XTYPE = 'U' AND NAME = 'DIME_TabelaQuadro90')
--CRIANDO TABELA TABELA QUADRO 90
CREATE TABLE [dbo].[DIME_TabelaQuadro90](
	[NumeroItem] [varchar](3) NOT NULL,
	[Descricao] [varchar](200) NULL,
	[TipoItem] [varchar](2) NULL,
PRIMARY KEY CLUSTERED 
(
	[NumeroItem] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

IF NOT EXISTS(SELECT Name FROM SYSOBJECTS WHERE  XTYPE = 'U' AND NAME = 'DIME_DadosQuadro90')
--CRIANDO TABELA DADOS QUADRO 90
CREATE TABLE [dbo].[DIME_DadosQuadro90](
	[NumeroItem] [varchar](3) NOT NULL,
	[IdDeclaracao] [int] NOT NULL,
	[Valor] [numeric](15, 2) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[NumeroItem] ASC,
	[IdDeclaracao] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[DIME_DadosQuadro90]  WITH CHECK ADD FOREIGN KEY([IdDeclaracao])
REFERENCES [dbo].[DIME_Declaracao] ([IdDeclaracao])

ALTER TABLE [dbo].[DIME_DadosQuadro90]  WITH CHECK ADD FOREIGN KEY([NumeroItem])
REFERENCES [dbo].[DIME_TabelaQuadro90] ([NumeroItem])


IF NOT EXISTS(SELECT Name FROM SYSOBJECTS WHERE  XTYPE = 'U' AND NAME = 'DIME_TabelaQuadro91')
--CRIANDO TABELA TABELA QUADRO 91
CREATE TABLE [dbo].[DIME_TabelaQuadro91](
	[NumeroItem] [varchar](3) NOT NULL,
	[Descricao] [varchar](200) NULL,
	[TipoItem] [varchar](2) NULL,
PRIMARY KEY CLUSTERED 
(
	[NumeroItem] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

IF NOT EXISTS(SELECT Name FROM SYSOBJECTS WHERE  XTYPE = 'U' AND NAME = 'DIME_DadosQuadro91')
--CRIANDO TABELA DADOS QUADRO 91
CREATE TABLE [dbo].[DIME_DadosQuadro91](
	[NumeroItem] [varchar](3) NOT NULL,
	[IdDeclaracao] [int] NOT NULL,
	[Valor] [numeric](15, 2) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[NumeroItem] ASC,
	[IdDeclaracao] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[DIME_DadosQuadro91]  WITH CHECK ADD FOREIGN KEY([IdDeclaracao])
REFERENCES [dbo].[DIME_Declaracao] ([IdDeclaracao])

ALTER TABLE [dbo].[DIME_DadosQuadro91]  WITH CHECK ADD FOREIGN KEY([NumeroItem])
REFERENCES [dbo].[DIME_TabelaQuadro91] ([NumeroItem])

IF NOT EXISTS(SELECT Name FROM SYSOBJECTS WHERE  XTYPE = 'U' AND NAME = 'DIME_TabelaQuadro92')
--CRIANDO TABELA TABELA QUADRO 92
CREATE TABLE [dbo].[DIME_TabelaQuadro92](
	[NumeroItem] [varchar](3) NOT NULL,
	[Descricao] [varchar](200) NULL,
	[TipoItem] [varchar](2) NULL,
PRIMARY KEY CLUSTERED 
(
	[NumeroItem] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

IF NOT EXISTS(SELECT Name FROM SYSOBJECTS WHERE  XTYPE = 'U' AND NAME = 'DIME_DadosQuadro92')
--CRIANDO TABELA DADOS QUADRO 92
CREATE TABLE [dbo].[DIME_DadosQuadro92](
	[NumeroItem] [varchar](3) NOT NULL,
	[IdDeclaracao] [int] NOT NULL,
	[Valor] [numeric](15, 2) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[NumeroItem] ASC,
	[IdDeclaracao] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[DIME_DadosQuadro92]  WITH CHECK ADD FOREIGN KEY([IdDeclaracao])
REFERENCES [dbo].[DIME_Declaracao] ([IdDeclaracao])

ALTER TABLE [dbo].[DIME_DadosQuadro92]  WITH CHECK ADD FOREIGN KEY([NumeroItem])
REFERENCES [dbo].[DIME_TabelaQuadro92] ([NumeroItem])


IF NOT EXISTS(SELECT Name FROM SYSOBJECTS WHERE  XTYPE = 'U' AND NAME = 'DIME_TabelaQuadro93')
--CRIANDO TABELA TABELA QUADRO 93
CREATE TABLE [dbo].[DIME_TabelaQuadro93](
	[NumeroItem] [varchar](3) NOT NULL,
	[Descricao] [varchar](200) NULL,
	[TipoItem] [varchar](2) NULL,
PRIMARY KEY CLUSTERED 
(
	[NumeroItem] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

IF NOT EXISTS(SELECT Name FROM SYSOBJECTS WHERE  XTYPE = 'U' AND NAME = 'DIME_DadosQuadro93')
--CRIANDO TABELA DADOS QUADRO 93
CREATE TABLE [dbo].[DIME_DadosQuadro93](
	[NumeroItem] [varchar](3) NOT NULL,
	[IdDeclaracao] [int] NOT NULL,
	[Valor] [numeric](15, 2) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[NumeroItem] ASC,
	[IdDeclaracao] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[DIME_DadosQuadro93]  WITH CHECK ADD FOREIGN KEY([IdDeclaracao])
REFERENCES [dbo].[DIME_Declaracao] ([IdDeclaracao])

ALTER TABLE [dbo].[DIME_DadosQuadro93]  WITH CHECK ADD FOREIGN KEY([NumeroItem])
REFERENCES [dbo].[DIME_TabelaQuadro93] ([NumeroItem])

IF NOT EXISTS(SELECT Name FROM SYSOBJECTS WHERE  XTYPE = 'U' AND NAME = 'DIME_TabelaQuadro94')
--CRIANDO TABELA TABELA QUADRO 94
CREATE TABLE [dbo].[DIME_TabelaQuadro94](
	[NumeroItem] [varchar](3) NOT NULL,
	[Descricao] [varchar](200) NULL,
	[TipoItem] [varchar](2) NULL,
PRIMARY KEY CLUSTERED 
(
	[NumeroItem] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

IF NOT EXISTS(SELECT Name FROM SYSOBJECTS WHERE  XTYPE = 'U' AND NAME = 'DIME_DadosQuadro94')
--CRIANDO TABELA DADOS QUADRO 94
CREATE TABLE [dbo].[DIME_DadosQuadro94](
	[NumeroItem] [varchar](3) NOT NULL,
	[IdDeclaracao] [int] NOT NULL,
	[Valor] [numeric](15, 2) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[NumeroItem] ASC,
	[IdDeclaracao] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[DIME_DadosQuadro94]  WITH CHECK ADD FOREIGN KEY([IdDeclaracao])
REFERENCES [dbo].[DIME_Declaracao] ([IdDeclaracao])

ALTER TABLE [dbo].[DIME_DadosQuadro94]  WITH CHECK ADD FOREIGN KEY([NumeroItem])
REFERENCES [dbo].[DIME_TabelaQuadro94] ([NumeroItem])

IF NOT EXISTS(SELECT Name FROM SYSOBJECTS WHERE  XTYPE = 'U' AND NAME = 'DIME_PeriodoApuracao')
CREATE TABLE [dbo].[DIME_PeriodoApuracao](
	[IdPeriodo] [int] IDENTITY(1,1) NOT NULL,
	[BD] [varchar](50) NOT NULL,
	[Filial] [varchar](5) NOT NULL,	
	[TipoReg] [varchar](20) NOT NULL,	
	[IdDeclaracao] [int] NOT NULL,
	[IdContribuinte] [int] NOT NULL,
	[Periodo] [varchar](8) NOT NULL,
	[Usuario] [varchar](30) NOT NULL

PRIMARY KEY CLUSTERED 
(
	[IdPeriodo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[DIME_PeriodoApuracao]  WITH CHECK ADD FOREIGN KEY([IdContribuinte])
REFERENCES [dbo].[DIME_Contribuinte] ([IdContribuinte])

IF NOT EXISTS(SELECT Name FROM SYSOBJECTS WHERE  XTYPE = 'U' AND NAME = 'DIME_DebitosApuracao')
--CRIANDO TABELA DEBITOS APURAÇÃO
CREATE TABLE [dbo].[DIME_DebitosApuracao](
	[IdPeriodo] [int] NOT NULL,
	[NumeroItem] [varchar](3) NOT NULL,
	[Vl_Ajuste] [numeric](15, 2) NULL
) ON [PRIMARY]

ALTER TABLE [dbo].[DIME_DebitosApuracao]  WITH CHECK ADD FOREIGN KEY([IdPeriodo])
REFERENCES [dbo].[DIME_PeriodoApuracao] ([IdPeriodo])

ALTER TABLE [dbo].[DIME_DebitosApuracao]  WITH CHECK ADD FOREIGN KEY([NumeroItem])
REFERENCES [dbo].[DIME_TabelaQuadro04] ([NumeroItem])


IF NOT EXISTS(SELECT Name FROM SYSOBJECTS WHERE  XTYPE = 'U' AND NAME = 'DIME_EntradasP1')
CREATE TABLE [dbo].[DIME_EntradasP1](
	[IdPeriodo] [int] NOT NULL,
	[BD] [varchar](50) NULL,
	[Filial] [varchar](5) NULL,
	[TPDoc] [varchar](5) NULL,
	[DataLanc] [Date] NULL,
	[DataDoc] [Date] NULL,
	[Serial] [varchar](30) NULL,
	[Especie] [varchar](30) NULL,
	[Estado] [varchar](5) NULL,
	[CFOP] [varchar](5) NULL,
	[Vl_Contabil] [numeric](15, 2) NULL,
	[Vl_Base_Calc] [numeric](15, 2) NULL,
	[Aliq] [numeric](15, 2) NULL,
	[Vl_Imp_Cred] [numeric](15, 2) NULL,
	[Vl_Isentas_Nao_Trib] [numeric](15, 2) NULL,
	[Vl_Outras] [numeric](15, 2) NULL,
	[Vl_Base_ST] [numeric](15, 2) NULL,
	[Vl_ST] [numeric](15, 2) NULL,
	[Vl_Base_Calc_IR] [numeric](15, 2) NULL,
	[Vl_IR] [numeric](15, 2) NULL,
	[Vl_Difal] [numeric](15, 2) NULL
) ON [PRIMARY]

ALTER TABLE [dbo].[DIME_EntradasP1]  WITH CHECK ADD FOREIGN KEY([IdPeriodo])
REFERENCES [dbo].[DIME_PeriodoApuracao] ([IdPeriodo])

IF NOT EXISTS(SELECT Name FROM SYSOBJECTS WHERE  XTYPE = 'U' AND NAME = 'DIME_SaidasP2')
CREATE TABLE [dbo].[DIME_SaidasP2](
	[IdPeriodo] [int] NOT NULL,
	[BD] [varchar](50) NULL,	
	[Filial] [varchar](5) NULL,
	[TPDoc] [varchar](5) NULL,
	[Especie] [varchar](30) NULL,	
	[Serial] [varchar](30) NULL,
	[CFOP] [varchar](5) NULL,
	[Vl_Contabil] [numeric](15, 2) NULL,
	[Vl_Base_Calc] [numeric](15, 2) NULL,
	[Aliq] [numeric](15, 2) NULL,
	[Vl_Imp_Debit] [numeric](15, 2) NULL,
	[Vl_Isentas_Nao_Trib] [numeric](15, 2) NULL,
	[Vl_Outras] [numeric](15, 2) NULL,
	[Vl_Base_ST] [numeric](15, 2) NULL,
	[Vl_ST] [numeric](15, 2) NULL,
	[Vl_Base_Calc_IR] [numeric](15, 2) NULL,
	[Vl_IR] [numeric](15, 2) NULL,
	[Dia] [int] NULL
) ON [PRIMARY]

ALTER TABLE [dbo].[DIME_SaidasP2]  WITH CHECK ADD FOREIGN KEY([IdPeriodo])
REFERENCES [dbo].[DIME_PeriodoApuracao] ([IdPeriodo])
", Support.Conectar());
                #endregion
                command.ExecuteNonQuery();
                //Conexao.ExecuteSqlScalar(sql);

                #region POPULANDO TABELAS
                command = new SqlCommand(@"
DECLARE @LINHAS int = case when (select count(*) from [SPS_Fiscal].[dbo].[DIME_TabelaCodAtividade]) <> 0 
Then (select count(*) from [SPS_Fiscal].[dbo].[DIME_TabelaCodAtividade]) else 0 end
IF @LINHAS = 0
begin
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaCodAtividade] ([Codigo]  ,[Descricao]) VALUES (	1	,	'Prestador de serviços de transportes'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaCodAtividade] ([Codigo]  ,[Descricao]) VALUES (	2	,	'Prestador de serviços de telecomunicações'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaCodAtividade] ([Codigo]  ,[Descricao]) VALUES (	3	,	'Fornecedor de energia elétrica ao consumidor independente, inclusive da parcela relativa à demanda contratada'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaCodAtividade] ([Codigo]  ,[Descricao]) VALUES (	4	,	'Distribuidor de energia elétrica a consumidor pessoa física ou jurídica inclusive os fornecimentos a consumidor independente e demanda contratada'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaCodAtividade] ([Codigo]  ,[Descricao]) VALUES (	5	,	'Fornecedor de gás natural'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaCodAtividade] ([Codigo]  ,[Descricao]) VALUES (	6	,	'Empresa que opere com o marketing direto'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaCodAtividade] ([Codigo]  ,[Descricao]) VALUES (	7	,	'Depósito ou centro de distribuição ou efetuar a entrega de mercadoria vendida por outro estabelecimento do mesmo titular'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaCodAtividade] ([Codigo]  ,[Descricao]) VALUES (	8	,	'Fornecedor de alimentos preparados'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaCodAtividade] ([Codigo]  ,[Descricao]) VALUES (	501	,	'For Detentor de TTD de obrigação acessória autorizando a remessa de mercadorias ao varejo ou pronta entrega através de postos de abastecimento, situados no Estado'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaCodAtividade] ([Codigo]  ,[Descricao]) VALUES (	502	,	'For Detentor de TTD de obrigação acessória autorizando que nas operações de venda utilize demonstrativo auxiliar especifico para escrituração do Livro de Saídas'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaCodAtividade] ([Codigo]  ,[Descricao]) VALUES (	503	,	'For Detentor de TTD de obrigação acessória autorizando que o estabelecimento gerador de energia elétrica possua inscrição única englobando várias PCHs'	)
end


set @LINHAS = case when (select count(*) from [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios]) <> 0 
Then (select count(*) from [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios]) else 0 end
IF @LINHAS = 0
begin
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	8869	,	'	ALTO BELA VISTA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	8885	,	'	BALNEÁRIO ARROIO DO SILVA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	8907	,	'	BALNEÁRIO GAIVOTA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	8923	,	'	BANDEIRANTE	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	8940	,	'	BARRA BONITA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	8966	,	'	BELA VISTA DO TOLDO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	8982	,	'	BOCAINA DO SUL	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	9008	,	'	BOM JESUS	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	9024	,	'	BOM JESUS DO OESTE	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	9040	,	'	BRUNÓPOLIS	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	9067	,	'	CAPÃO ALTO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	9083	,	'	CHAPADÃO DO LAGEADO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	9105	,	'	CUNHATAÍ	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	9121	,	'	ENTRE RIOS	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	9148	,	'	ERMO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	9164	,	'	FLOR DO SERTÃO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	9180	,	'	FREI ROGÉRIO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	9202	,	'	IBIAM	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	9229	,	'	IOMERÊ	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	9245	,	'	JUPIÁ	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	9261	,	'	LUZERNA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	9288	,	'	PAIAL	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	9300	,	'	PAINEL	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	9326	,	'	PALMEIRA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	9342	,	'	PRINCESA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	9369	,	'	SALTINHO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	9385	,	'	SANTA TEREZINHA DO PROGRESSO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	9407	,	'	SANTIAGO DO SUL	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	9423	,	'	SÃO BERNARDINO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	9440	,	'	SÃO PEDRO DE ALCÂNTARA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	9466	,	'	TIGRINHOS	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	9482	,	'	TREVISO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	9504	,	'	ZORTÉA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	9733	,	'	FORQUILHINHA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	11924	,	'	BALNEÁRIO RINCÃO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	11940	,	'	PESCARIA BRAVA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	55379	,	'	BOMBINHAS	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	55395	,	'	MORRO GRANDE	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	55417	,	'	PASSO DE TORRES	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	55433	,	'	COCAL DO SUL	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	55450	,	'	CAPIVARI DE BAIXO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	55476	,	'	SANGÃO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	55492	,	'	BALNEÁRIO BARRA DO SUL	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	55514	,	'	SÃO JOÃO DO ITAPERIU	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	55530	,	'	CALMON	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	55557	,	'	SANTA TEREZINHA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	55573	,	'	BRAÇO DO TROMBUDO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	55590	,	'	MIRIM DOCE	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	55611	,	'	MONTE CARLO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	55638	,	'	VARGEM	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	55654	,	'	VARGEM BONITA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	55670	,	'	CERRO NEGRO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	55697	,	'	PONTE ALTA DO NORTE	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	55719	,	'	RIO RUFINO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	55735	,	'	SÃO CRISTÓVÃO DO SUL	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	55751	,	'	MACIEIRA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	55778	,	'	ÁGUAS FRIAS	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	55794	,	'	CORDILHEIRA ALTA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	55816	,	'	FORMOSA DO SUL	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	55832	,	'	GUATAMBU	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	55859	,	'	IRATI	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	55875	,	'	JARDINÓPOLIS	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	55891	,	'	NOVA ITABERABA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	55913	,	'	NOVO HORIZONTE	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	55930	,	'	PLANALTO ALEGRE	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	55956	,	'	SUL BRASIL	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	55972	,	'	ARABUTÃ	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	55999	,	'	ARVOREDO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	57355	,	'	CORONEL MARTINS	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	57371	,	'	IPUAÇU	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	57398	,	'	LAJEADO GRANDE	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	57410	,	'	OURO VERDE	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	57436	,	'	PASSOS MAIA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	57452	,	'	BELMONTE	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	57479	,	'	PARAÍSO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	57495	,	'	RIQUEZA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	57517	,	'	SANTA HELENA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	57533	,	'	SÃO JOÃO DO OESTE	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	57550	,	'	SÃO MIGUEL DA BOA VISTA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80012	,	'	ABELARDO LUZ	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80039	,	'	AGROLÂNDIA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80055	,	'	AGRONÔMICA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80071	,	'	ÁGUA DOCE	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80098	,	'	ÁGUAS DE CHAPECÓ	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80110	,	'	ÁGUAS MORNAS	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80136	,	'	ALFREDO WAGNER	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80152	,	'	ANCHIETA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80179	,	'	ANGELINA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80195	,	'	ANITA GARIBALDI	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80217	,	'	ANITÁPOLIS	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80233	,	'	ANTÔNIO CARLOS	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80250	,	'	ARAQUARI	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80276	,	'	ARARANGUÁ	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80292	,	'	ARMAZÉM	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80314	,	'	ARROIO TRINTA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80330	,	'	ASCURRA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80357	,	'	ATALANTA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80373	,	'	AURORA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80390	,	'	BALNEÁRIO CAMBORIÚ	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80411	,	'	BARRA VELHA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80438	,	'	BENEDITO NOVO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80454	,	'	BIGUAÇU	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80470	,	'	BLUMENAU	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80497	,	'	BOM RETIRO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80519	,	'	BOTUVERÁ	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80535	,	'	BRAÇO DO NORTE	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80551	,	'	BRUSQUE	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80578	,	'	CAÇADOR	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80594	,	'	CAIBI	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80616	,	'	CAMBORIÚ	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80632	,	'	CAMPO ALEGRE	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80659	,	'	CAMPO BELO DO SUL	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80675	,	'	CAMPO ERÊ	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80691	,	'	CAMPOS NOVOS	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80713	,	'	CANELINHA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80730	,	'	CANOINHAS	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80756	,	'	CAPINZAL	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80772	,	'	CATANDUVAS	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80799	,	'	CAXAMBU DO SUL	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80810	,	'	CHAPECÓ	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80837	,	'	CONCÓRDIA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80853	,	'	CORONEL FREITAS	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80870	,	'	CORUPÁ	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80896	,	'	CRICIÚMA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80918	,	'	CUNHA PORÃ	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80934	,	'	CURITIBANOS	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80950	,	'	DESCANSO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80977	,	'	DIONÍSIO CERQUEIRA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	80993	,	'	DONA EMMA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81019	,	'	ERVAL VELHO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81035	,	'	FAXINAL DOS GUEDES	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81051	,	'	FLORIANÓPOLIS	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81078	,	'	FRAIBURGO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81094	,	'	GALVÃO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81116	,	'	GOVERNADOR CELSO RAMOS	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81132	,	'	GAROPABA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81159	,	'	GARUVA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81175	,	'	GASPAR	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81191	,	'	GRÃO PARÁ	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81213	,	'	GRAVATAL	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81230	,	'	GUABIRUBA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81256	,	'	GUARACIABA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81272	,	'	GUARAMIRIM	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81299	,	'	GUARUJÁ DO SUL	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81310	,	'	HERVAL DO OESTE	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81337	,	'	IBICARÉ	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81353	,	'	IBIRAMA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81370	,	'	IÇARA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81396	,	'	ILHOTA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81418	,	'	IMARUÍ	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81434	,	'	IMBITUBA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81450	,	'	IMBUIA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81477	,	'	INDAIAL	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81493	,	'	IPIRA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81515	,	'	IPUMIRIM	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81531	,	'	IRANI	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81558	,	'	IRINEÓPOLIS	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81574	,	'	ITÁ	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81590	,	'	ITAIÓPOLIS	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81612	,	'	ITAJAÍ	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81639	,	'	ITAPEMA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81655	,	'	ITAPIRANGA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81671	,	'	ITUPORANGA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81698	,	'	JABORÁ	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81710	,	'	JACINTO MACHADO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81736	,	'	JAGUARUNA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81752	,	'	JARAGUÁ DO SUL	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81779	,	'	JOAÇABA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81795	,	'	JOINVILLE	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81817	,	'	LACERDÓPOLIS	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81833	,	'	LAGES	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81850	,	'	LAGUNA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81876	,	'	LAURENTINO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81892	,	'	LAURO MULLER	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81914	,	'	LEBON RÉGIS	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81930	,	'	LEOBERTO LEAL	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81957	,	'	LONTRAS	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81973	,	'	LUIZ ALVES	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	81990	,	'	MAFRA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82015	,	'	MAJOR GERCINO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82031	,	'	MAJOR VIEIRA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82058	,	'	MARAVILHA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82074	,	'	MASSARANDUBA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82090	,	'	MATOS COSTA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82112	,	'	MELEIRO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82139	,	'	MODELO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82155	,	'	MONDAÍ	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82171	,	'	MONTE CASTELO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82198	,	'	MORRO DA FUMAÇA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82210	,	'	NAVEGANTES	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82236	,	'	NOVA ERECHIM	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82252	,	'	NOVA TRENTO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82279	,	'	NOVA VENEZA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82295	,	'	ORLEANS	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82317	,	'	OURO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82333	,	'	PALHOÇA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82350	,	'	PALMA SOLA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82376	,	'	PALMITOS	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82392	,	'	PAPANDUVA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82414	,	'	PAULO LOPES	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82430	,	'	PEDRAS GRANDES	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82457	,	'	PENHA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82473	,	'	PERITIBA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82490	,	'	PETROLÂNDIA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82511	,	'	BALNEÁRIO PIÇARRAS	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82538	,	'	PINHALZINHO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82554	,	'	PINHEIRO PRETO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82570	,	'	PIRATUBA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82597	,	'	POMERODE	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82619	,	'	PONTE ALTA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82635	,	'	PONTE SERRADA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82651	,	'	PORTO BELO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82678	,	'	PORTO UNIÃO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82694	,	'	POUSO REDONDO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82716	,	'	PRAIA GRANDE	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82732	,	'	PRESIDENTE CASTELO BRANCO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82759	,	'	PRESIDENTE GETÚLIO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82775	,	'	PRESIDENTE NEREU	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82791	,	'	QUILOMBO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82813	,	'	RANCHO QUEIMADO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82830	,	'	RIO DAS ANTAS	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82856	,	'	RIO DO CAMPO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82872	,	'	RIO DO OESTE	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82899	,	'	RIO DOS CEDROS	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82910	,	'	RIO DO SUL	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82937	,	'	RIO FORTUNA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82953	,	'	RIO NEGRINHO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82970	,	'	RODEIO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	82996	,	'	ROMELÂNDIA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83011	,	'	SALETE	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83038	,	'	SALTO VELOSO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83054	,	'	SANTA CECÍLIA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83070	,	'	SANTA ROSA DE LIMA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83097	,	'	SANTO AMARO DA IMPERATRIZ	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83119	,	'	SÃO BENTO DO SUL	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83135	,	'	SÃO BONIFÁCIO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83151	,	'	SÃO CARLOS	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83178	,	'	SÃO DOMINGOS	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83194	,	'	SÃO FRANCISCO DO SUL	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83216	,	'	SÃO JOÃO BATISTA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83232	,	'	SÃO JOÃO DO SUL	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83259	,	'	SÃO JOAQUIM	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83275	,	'	SÃO JOSÉ	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83291	,	'	SÃO JOSÉ DO CEDRO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83313	,	'	SÃO JOSÉ DO CERRITO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83330	,	'	SÃO LOURENÇO DO OESTE	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83356	,	'	SÃO LUDGERO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83372	,	'	SÃO MARTINHO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83399	,	'	SÃO MIGUEL DO OESTE	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83410	,	'	SAUDADES	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83437	,	'	SCHROEDER	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83453	,	'	SEARA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83470	,	'	SIDERÓPOLIS	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83496	,	'	SOMBRIO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83518	,	'	TAIÓ	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83534	,	'	TANGARÁ	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83550	,	'	TIJUCAS	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83577	,	'	TIMBÓ	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83593	,	'	TRÊS BARRAS	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83615	,	'	TREZE DE MAIO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83631	,	'	TREZE TÍLIAS	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83658	,	'	TROMBUDO CENTRAL	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83674	,	'	TUBARÃO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83690	,	'	TURVO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83712	,	'	URUBICI	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83739	,	'	URUSSANGA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83755	,	'	VARGEÃO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83771	,	'	VIDAL RAMOS	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83798	,	'	VIDEIRA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83810	,	'	WITMARSUM	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83836	,	'	XANXERÊ	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83852	,	'	XAVANTINA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83879	,	'	XAXIM	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83895	,	'	BOM JARDIM DA SERRA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83917	,	'	MARACAJÁ	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83933	,	'	TIMBÉ DO SUL	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83950	,	'	CORREIA PINTO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	83976	,	'	OTACÍLIO COSTA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	99392	,	'	ABDON BATISTA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	99414	,	'	APIÚNA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	99430	,	'	CELSO RAMOS	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	99457	,	'	DOUTOR PEDRINHO	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	99511	,	'	IPORÃ DO OESTE	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	99538	,	'	IRACEMINHA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	99570	,	'	JOSÉ BOITEUX	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	99619	,	'	LINDÓIA DO SUL	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	99635	,	'	MAREMA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	99678	,	'	SANTA ROSA DO SUL	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	99716	,	'	TIMBÓ GRANDE	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	99732	,	'	UNIÃO DO OESTE	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	99759	,	'	URUPEMA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	99775	,	'	VITOR MEIRELES	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	99856	,	'	ITAPOÁ	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	99899	,	'	SERRA ALTA	'	)
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaMunicipios] ([Codigo] ,[NomeMunicipio]) VALUES (	99910	,	'	TUNÁPOLIS	'	)
end

set @LINHAS = case when (select count(*) from [SPS_Fiscal].[dbo].[DIME_TabelaQuadro03]) <> 0 
Then (select count(*) from [SPS_Fiscal].[dbo].[DIME_TabelaQuadro03]) else 0 end
IF @LINHAS = 0
begin
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaQuadro03]
           ([NumeroItem]
           ,[Descricao]
           ,[TipoItem])

SELECT '010','Valor contábil','1' UNION ALL
SELECT '020','Base de cálculo','1' UNION ALL
SELECT '030','Imposto creditado','1' UNION ALL
SELECT '040','Operações isentas ou não tributadas','1' UNION ALL
SELECT '050','Outras operações sem crédito de imposto','1' UNION ALL
SELECT '053','Base de Cálculo Imposto Retido','1' UNION ALL
SELECT '054','Imposto Retido','1' UNION ALL
SELECT '057','Imposto Diferencial Alíquota','1' UNION ALL
SELECT '060','Valor Contábil','2' UNION ALL
SELECT '070','Base de Cálculo','2' UNION ALL
SELECT '080','Imposto debitado','2' UNION ALL
SELECT '090','Operações isentas ou não tributadas','2' UNION ALL
SELECT '100','Outras operações sem débito de imposto','2' UNION ALL
SELECT '103','Base de Cálculo Imposto Retido','2' UNION ALL
SELECT '104','Imposto Retido','2' 
end

/*
-- [DIME_TabelaQuadro04]
*/
set @LINHAS = case when (select count(*) from [SPS_Fiscal].[dbo].[DIME_TabelaQuadro04]) <> 0 
Then (select count(*) from [SPS_Fiscal].[dbo].[DIME_TabelaQuadro04]) else 0 end
IF @LINHAS = 0
begin
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaQuadro04]
           ([NumeroItem]
           ,[Descricao]
           ,[TipoItem])

SELECT '010','(+) Débito pelas saídas', '1' UNION ALL
SELECT '020','(+) Débito por diferencial de alíquota de ativo permanente','1' UNION ALL
SELECT '030','(+) Débito por diferencial de alíquota do material de uso ou consumo','1' UNION ALL
SELECT '040','(+) Débito de máquinas / equipamentos importados para ativo permanente','1' UNION ALL
SELECT '045','(+) Débito da Diferença de Alíquota de Operação ou Prestação a Consumidor Final de Outro Estado','1' UNION ALL
SELECT '050','(+) Estorno de crédito','2' UNION ALL
SELECT '060','(+) Outros estornos de crédito','2' UNION ALL
SELECT '065','(+) Estorno de Crédito da Entrada em Decorrência da Utilização de Crédito Presumido','2' UNION ALL
SELECT '070','(+) Outros débitos','3' UNION ALL
SELECT '990','(=) Subtotal de Débitos => (transportar para o item 010 do quadro 09 Cálculo do Imposto a Pagar ou Saldo Credor)','3'
end

/*
-- [DIME_TabelaQuadro05]
*/
set @LINHAS = case when (select count(*) from [SPS_Fiscal].[dbo].[DIME_TabelaQuadro05]) <> 0 
Then (select count(*) from [SPS_Fiscal].[dbo].[DIME_TabelaQuadro05]) else 0 end
IF @LINHAS = 0
begin
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaQuadro05]
           ([NumeroItem]
           ,[Descricao]
           ,[TipoItem])
SELECT '010','(+) Saldo credor do mês anterior','1' UNION ALL
SELECT '020','(+) Crédito pelas entradas','2' UNION ALL
SELECT '030','(+) Crédito de ativo permanente','2' UNION ALL
SELECT '040','(+) Crédito por diferencial de alíquota material de uso / consumo','2' UNION ALL
SELECT '045','(+) Crédito da Diferença de Alíquota de Operação ou Prestação a Consumidor Final de Outro Estado','2' UNION ALL
SELECT '050','(+) Crédito de ICMS retido por substituição tributária','2' UNION ALL
SELECT '990','(=) Subtotal de créditos => (transportar para o item 050 do quadro 09 - Cálculo do Imposto a Pagar ou Saldo Credor)','9'
end

/*
-- [DIME_TabelaQuadro09]
*/
set @LINHAS = case when (select count(*) from [SPS_Fiscal].[dbo].[DIME_TabelaQuadro09]) <> 0 
Then (select count(*) from [SPS_Fiscal].[dbo].[DIME_TabelaQuadro09]) else 0 end
IF @LINHAS = 0
begin
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaQuadro09]
           ([NumeroItem]
           ,[Descricao]
           ,[TipoItem])
SELECT '010','(+) Subtotal de débitos','1' UNION ALL
SELECT '011','(+) Complemento de débito por mudança de regime de apuração','1' UNION ALL
SELECT '020','(+) Saldos devedores recebidos de estabelecimentos consolidados','1' UNION ALL
SELECT '030','(+) Débito por reserva de crédito acumulado','1' UNION ALL
SELECT '036','(+) Segregação do Crédito Presumido Utilizado em Substituição aos Créditos pelas Entradas','1' UNION ALL
SELECT '037','(+) Segregação do Crédito Decorrente do Pagamento Antecipado do ICMS Devido na Saída Subsequente à Importação, com Utilização de Crédito Presumido','1' UNION ALL
SELECT '038','(+) Segregação de outros créditos permitidos para compensar com o débito pela utilização do crédito presumido','1' UNION ALL
SELECT '040','(=) Total de débitos','1' UNION ALL
SELECT '050','(+) Subtotal de créditos','2' UNION ALL
SELECT '060','(+) Saldos credores recebidos de estabelecimentos consolidados','2' UNION ALL
SELECT '070','(+) Créditos recebidos por transferência de outros contribuintes','2' UNION ALL
SELECT '075','(+) Créditos declarados no DCIP','2' UNION ALL
SELECT '076','(+) Segregação dos Débitos Relativos às Saídas com Crédito Presumido em Substituição aos Créditos pelas Entradas','2' UNION ALL
SELECT '080','(=) Total de créditos','2 ' UNION ALL
SELECT '090','(+) Imposto do 1º decêndio','3' UNION ALL
SELECT '100','(+) Imposto do 2º decêndio','3' UNION ALL
SELECT '105','(+) Antecipações Combustíveis líquidos e gasosos','3' UNION ALL
SELECT '110','(=) Total de ajustes da apuração decendial e antecipações','3' UNION ALL
SELECT '120','(=) Saldo devedor (Total de Débitos - (Total de Créditos + Total de ajustes da apuração decendial e antecipações))','4' UNION ALL
SELECT '130','(-) Saldo devedor transferido ao estabelecimento consolidador','4' UNION ALL
SELECT '140','((=) Saldo Credor (Total de Créditos + Total de ajustes da apuração decendial e antecipações) - (Total de Débitos))','5' UNION ALL
SELECT '150','(-) Saldo credor transferido ao estabelecimento consolidador','5' UNION ALL
SELECT '160','Saldo credor transferível relativo à exportação','6' UNION ALL
SELECT '170','Saldo credor transferível relativo a saídas isentas','6' UNION ALL
SELECT '180','Saldo credor transferível relativo a saídas diferidas','6' UNION ALL
SELECT '190','Saldo credor relativo a outros créditos','6' UNION ALL
SELECT '998','(=) Saldo Credor para o mês seguinte','5' UNION ALL
SELECT '999','(=) Imposto a recolher','4'
end

/*
-- [DIME_TabelaQuadro11]
*/
set @LINHAS = case when (select count(*) from [SPS_Fiscal].[dbo].[DIME_TabelaQuadro11]) <> 0 
Then (select count(*) from [SPS_Fiscal].[dbo].[DIME_TabelaQuadro11]) else 0 end
IF @LINHAS = 0
begin
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaQuadro11]
           ([NumeroItem]
           ,[Descricao]
           ,[TipoItem])
SELECT '010','Valor dos produtos (não preencher a partir do período de referência agosto de 2013)','0' UNION ALL
SELECT '020','Valor do IPI (não preencher a partir do período de referência agosto de 2013)','0' UNION ALL
SELECT '030','Despesas acessórias (não preencher a partir do período de referência agosto de 2013)','0' UNION ALL
SELECT '040','Base de cálculo do ICMS próprio (não preencher a partir do período de referência agosto de 2013)','0' UNION ALL
SELECT '050','ICMS próprio (não preencher a partir do período de referência agosto de 2013)','0' UNION ALL
SELECT '060','Base cálculo do imposto retido','0' UNION ALL
SELECT '065','Imposto Retido apurado por mercadoria e recolhido por operação','0' UNION ALL
SELECT '070','(+) Imposto retido com apuração mensal','1' UNION ALL
SELECT '073','(+) Imposto Retido pelo AEHC com regime especial de apuração mensal','1' UNION ALL
SELECT '075','(+) Saldos devedores recebidos de estabelecimentos consolidados','1' UNION ALL
SELECT '080','Total de débitos','1' UNION ALL
SELECT '090','(+) Saldo credor do período anterior sobre a substituição tributária','2' UNION ALL
SELECT '100','(+) Devolução de mercadorias e desfazimento de venda (não preencher a partir do período de referência agosto de 2013)','2' UNION ALL
SELECT '105','(+) Créditos declarados no DCIP','2' UNION ALL
SELECT '110','(+) Ressarcimento de ICMS substituição tributária (não preencher a partir do período de referência agosto de 2013)','2' UNION ALL
SELECT '120','(+) Outros créditos (não preencher a partir do período de referência agosto de 2013)','2' UNION ALL
SELECT '125','(+) Saldos credores recebidos de estabelecimentos consolidados','2' UNION ALL
SELECT '130','(=) Total de créditos','2' UNION ALL
SELECT '140','Não se aplica','3' UNION ALL
SELECT '150','Não se aplica','3' UNION ALL
SELECT '155','(+) Antecipações Combustíveis líquidos e gasosos','3' UNION ALL
SELECT '160','(=) Total de ajustes das Antecipações Combustíveis','3' UNION ALL
SELECT '170','(=) Saldo devedor (Total de Débitos - (Total de Créditos + Total de ajustes das antecipações combustíveis))','4' UNION ALL
SELECT '180','(-) Saldo devedor transferido ao estabelecimento consolidador','4' UNION ALL
SELECT '190','((=) Saldo Credor (Total de Créditos + Total de ajustes das antecipações combustíveis) - (Total de Débitos))','5' UNION ALL
SELECT '200','(-) Saldo credor transferido ao estabelecimento consolidador','5' UNION ALL
SELECT '998','(=) Saldo Credor para o mês seguinte','5' UNION ALL
SELECT '999','(=) Imposto a recolher sobre a substituição tributária','4' 
end

/*
-- [DIME_TabelaQuadro80]
*/
set @LINHAS = case when (select count(*) from [SPS_Fiscal].[dbo].[DIME_TabelaQuadro80]) <> 0 
Then (select count(*) from [SPS_Fiscal].[dbo].[DIME_TabelaQuadro80]) else 0 end
IF @LINHAS = 0
begin
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaQuadro80]
           ([NumeroItem]
           ,[Descricao]
           ,[TipoItem])
SELECT '010','(+) Estoque no início do exercício','1' UNION ALL
SELECT '020','(+) Estoque no fim do exercício','1' UNION ALL
SELECT '030','(+) Receita bruta de vendas e serviços','1'
end

/*
-- [DIME_TabelaQuadro81]
*/
set @LINHAS = case when (select count(*) from [SPS_Fiscal].[dbo].[DIME_TabelaQuadro81]) <> 0 
Then (select count(*) from [SPS_Fiscal].[dbo].[DIME_TabelaQuadro81]) else 0 end
IF @LINHAS = 0
begin
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaQuadro81]
           ([NumeroItem]
           ,[Descricao]
           ,[TipoItem])
SELECT '110','(=) Circulante','1' UNION ALL
SELECT '111','(+) Disponibilidades','1' UNION ALL
SELECT '113','(+) Contas a receber do circulante','1' UNION ALL
SELECT '121','(+) Estoque de mercadorias e matéria-prima','1' UNION ALL
SELECT '123','(+) Outros estoques','1' UNION ALL
SELECT '128','(+) Outras contas do ativo circulante','1' UNION ALL
SELECT '130','(=) Realizável a longo prazo','1' UNION ALL
SELECT '131','(+) Contas a receber do realizável','1' UNION ALL
SELECT '148','(+) Outras contas do realizável','1' UNION ALL
SELECT '150','(=) Permanente','1' UNION ALL
SELECT '151','(+) Investimentos','1' UNION ALL
SELECT '155','(+) Imobilizado (líquido)','1' UNION ALL
SELECT '157','(+) Diferido (líquido)','1' UNION ALL
SELECT '159','(+) Intangível','1' UNION ALL
SELECT '199','(=) Total geral do ativo','1'
end

/*
-- [DIME_TabelaQuadro82]
*/
set @LINHAS = case when (select count(*) from [SPS_Fiscal].[dbo].[DIME_TabelaQuadro82]) <> 0 
Then (select count(*) from [SPS_Fiscal].[dbo].[DIME_TabelaQuadro82]) else 0 end
IF @LINHAS = 0
begin
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaQuadro82]
           ([NumeroItem]
           ,[Descricao]
           ,[TipoItem])
SELECT '210','(=) Circulante','1' UNION ALL
SELECT '211','(+) Fornecedores','1' UNION ALL
SELECT '213','(+) Empréstimos e financiamentos','1' UNION ALL
SELECT '215','(+) Outras contas do passivo circulante','1' UNION ALL
SELECT '230','(=) Exigível a longo prazo','1' UNION ALL
SELECT '240','(=) Resultado de exercícios futuros','1' UNION ALL
SELECT '269','(=) Passivo a Descoberto','1' UNION ALL
SELECT '270','(=) Patrimônio líquido','1' UNION ALL
SELECT '271','(+) capital Social','1' UNION ALL
SELECT '278','(+) Outras contas do patrimônio líquido','1' UNION ALL
SELECT '279','(-) Outras contas do patrimônio líquido (de valor)','1' UNION ALL
SELECT '299','(=) Total geral do passivo','1'
end

/*
-- [DIME_TabelaQuadro83]
*/
set @LINHAS = case when (select count(*) from [SPS_Fiscal].[dbo].[DIME_TabelaQuadro83]) <> 0 
Then (select count(*) from [SPS_Fiscal].[dbo].[DIME_TabelaQuadro83]) else 0 end
IF @LINHAS = 0
begin
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaQuadro83]
           ([NumeroItem]
           ,[Descricao]
           ,[TipoItem])
SELECT '310','(+) Receita bruta de vendas/serviços','1' UNION ALL
SELECT '311','(-) Deduções da receita bruta','1' UNION ALL
SELECT '320','(=) Receita líquida vendas/serviços','1' UNION ALL
SELECT '323','(-) Custo da mercadoria ou produtos vendida(os) ou dos serviços prestados','1' UNION ALL
SELECT '330','(=) Lucro bruto','1' UNION ALL
SELECT '331','(=) Prejuízo bruto','1' UNION ALL
SELECT '333','(+) Outras receita operacionais','1' UNION ALL
SELECT '335','(-) Despezas operacionais','1' UNION ALL
SELECT '340','(=) Lucro operacional','1' UNION ALL
SELECT '341','(=) Prejuízo operacional','1' UNION ALL
SELECT '343','(+) Receitas não operacionais','1' UNION ALL
SELECT '345','(-) Despesas não operacionais','1' UNION ALL
SELECT '350','(=) Resultado antes do I.R. e da contribuição social','1' UNION ALL
SELECT '351','(=) Resultado negativo antes do I.R. e da contribuição social','1' UNION ALL
SELECT '353','(-) Provisão para o I.R. e para a contribuição social','1' UNION ALL
SELECT '354','(+) Provisão para o I.R. e para contribuição social (Acrescentando Art. 4º da Port. 274/15)','1' UNION ALL
SELECT '360','(=) Resultado após I.R. e a contribuição social','1' UNION ALL
SELECT '361','(=) Resultado negativo após o I.R e a contribuição social','1' UNION ALL
SELECT '363','(-) Participações e contribuições','1' UNION ALL
SELECT '398','(=) Prejuízo do exercício','1' UNION ALL
SELECT '399','(=) Lucro do exercício','1' 
end


/*
-- [DIME_TabelaQuadro84]
*/
set @LINHAS = case when (select count(*) from [SPS_Fiscal].[dbo].[DIME_TabelaQuadro84]) <> 0 
Then (select count(*) from [SPS_Fiscal].[dbo].[DIME_TabelaQuadro84]) else 0 end
IF @LINHAS = 0
begin
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaQuadro84]
           ([NumeroItem]
           ,[Descricao]
           ,[TipoItem])
SELECT '411','(=) Pró-labore','1' UNION ALL
SELECT '412','(+) Comissões, salários, ordenados','1' UNION ALL
SELECT '421','(+) Encargos sociais','1' UNION ALL
SELECT '422','(+) Tributos federais','1' UNION ALL
SELECT '423','(+) Tributos estaduais','1' UNION ALL
SELECT '424','(+) tributos municipais','1' UNION ALL
SELECT '431','(+) Combustíveis e lubrificantes','1' UNION ALL
SELECT '432','(+) Água e telefone','1' UNION ALL
SELECT '433','(+) Energia elétrica','1' UNION ALL
SELECT '441','(+) Aluguéis','1' UNION ALL
SELECT '442','(+) Seguros','1' UNION ALL
SELECT '443','(+) Fretes e carretos','1' UNION ALL
SELECT '451','(+) Serviços profissionais','1' UNION ALL
SELECT '461','(+) Despesas financeiras','1' UNION ALL
SELECT '498','(+) Outras despesas','1' UNION ALL
SELECT '499','(=) Total','1' 
end

/*
-- [DIME_TabelaQuadro90]
*/
set @LINHAS = case when (select count(*) from [SPS_Fiscal].[dbo].[DIME_TabelaQuadro90]) <> 0 
Then (select count(*) from [SPS_Fiscal].[dbo].[DIME_TabelaQuadro90]) else 0 end
IF @LINHAS = 0
begin
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaQuadro90]
           ([NumeroItem]
           ,[Descricao]
           ,[TipoItem])
SELECT '010','(+) Estoque no início do exercício','1' UNION ALL
SELECT '020','(+) Estoque no fim do exercício','1' UNION ALL
SELECT '030','(+) Receita bruta de vendas e serviços','1'
end

/*
-- [DIME_TabelaQuadro91]
*/
set @LINHAS = case when (select count(*) from [SPS_Fiscal].[dbo].[DIME_TabelaQuadro91]) <> 0 
Then (select count(*) from [SPS_Fiscal].[dbo].[DIME_TabelaQuadro91]) else 0 end
IF @LINHAS = 0
begin

INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaQuadro91]
           ([NumeroItem]
           ,[Descricao]
           ,[TipoItem])
SELECT '110','(=) Circulante','1' UNION ALL
SELECT '111','(+) Disponibilidades','1' UNION ALL
SELECT '113','(+) Contas a receber do circulante','1' UNION ALL
SELECT '121','(+) Estoque de mercadorias e matéria-prima','1' UNION ALL
SELECT '123','(+) Outros estoques','1' UNION ALL
SELECT '128','(+) Outras contas do ativo circulante','1' UNION ALL
SELECT '130','(=) Realizável a longo prazo','1' UNION ALL
SELECT '131','(+) Contas a receber do realizável','1' UNION ALL
SELECT '148','(+) Outras contas do realizável','1' UNION ALL
SELECT '150','(=) Permanente','1' UNION ALL
SELECT '151','(+) Investimentos','1' UNION ALL
SELECT '155','(+) Imobilizado (líquido)','1' UNION ALL
SELECT '157','(+) Diferido (líquido)','1' UNION ALL
SELECT '159','(+) Intangível','1' UNION ALL
SELECT '199','(=) Total geral do ativo','1'
end

/*
-- [DIME_TabelaQuadro92]
*/
set @LINHAS = case when (select count(*) from [SPS_Fiscal].[dbo].[DIME_TabelaQuadro92]) <> 0 
Then (select count(*) from [SPS_Fiscal].[dbo].[DIME_TabelaQuadro92]) else 0 end
IF @LINHAS = 0
begin
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaQuadro92]
           ([NumeroItem]
           ,[Descricao]
           ,[TipoItem])
SELECT '210','(=) Circulante','1' UNION ALL
SELECT '211','(+) Fornecedores','1' UNION ALL
SELECT '213','(+) Empréstimos e financiamentos','1' UNION ALL
SELECT '215','(+) Outras contas do passivo circulante','1' UNION ALL
SELECT '230','(=) Exigível a longo prazo','1' UNION ALL
SELECT '240','(=) Resultado de exercícios futuros','1' UNION ALL
SELECT '269','(=) Passivo a Descoberto','1' UNION ALL
SELECT '270','(=) Patrimônio líquido','1' UNION ALL
SELECT '271','(+) capital Social','1' UNION ALL
SELECT '278','(+) Outras contas do patrimônio líquido','1' UNION ALL
SELECT '279','(-) Outras contas do patrimônio líquido (de valor)','1' UNION ALL
SELECT '299','(=) Total geral do passivo','1'
end

/*
-- [DIME_TabelaQuadro93]
*/
set @LINHAS = case when (select count(*) from [SPS_Fiscal].[dbo].[DIME_TabelaQuadro93]) <> 0 
Then (select count(*) from [SPS_Fiscal].[dbo].[DIME_TabelaQuadro93]) else 0 end
IF @LINHAS = 0
begin
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaQuadro93]
           ([NumeroItem]
           ,[Descricao]
           ,[TipoItem])
SELECT '310','(+) Receita bruta de vendas/serviços','1' UNION ALL
SELECT '311','(-) Deduções da receita bruta','1' UNION ALL
SELECT '320','(=) Receita líquida vendas/serviços','1' UNION ALL
SELECT '323','(-) Custo da mercadoria ou produtos vendida(os) ou dos serviços prestados','1' UNION ALL
SELECT '330','(=) Lucro bruto','1' UNION ALL
SELECT '331','(=) Prejuízo bruto','1' UNION ALL
SELECT '333','(+) Outras receita operacionais','1' UNION ALL
SELECT '335','(-) Despesas operacionais','1' UNION ALL
SELECT '340','(=) Lucro operacional','1' UNION ALL
SELECT '341','(=) Prejuízo operacional','1' UNION ALL
SELECT '343','(+) Receitas não operacionais','1' UNION ALL
SELECT '345','(-) Despesas não operacionais','1' UNION ALL
SELECT '350','(=) Resultado antes do I.R. e da contribuição social','1' UNION ALL
SELECT '351','(=) Resultado negativo antes do I.R. e da contribuição social','1' UNION ALL
SELECT '353','(-) Provisão para o I.R. e para a contribuição social','1' UNION ALL
SELECT '354','(+) Provisão para o I.R. e para contribuição social (Acrescentando Art. 4º da Port. 274/15)','1' UNION ALL
SELECT '360','(=) Resultado após I.R. e a contribuição social','1' UNION ALL
SELECT '361','(=) Resultado negativo após o I.R e a contribuição social','1' UNION ALL
SELECT '363','(-) Participações e contribuições','1' UNION ALL
SELECT '398','(=) Prejuízo do exercício','1' UNION ALL
SELECT '399','(=) Lucro do exercício','1' 
end

/*
-- [DIME_TabelaQuadro94]
*/
set @LINHAS = case when (select count(*) from [SPS_Fiscal].[dbo].[DIME_TabelaQuadro94]) <> 0 
Then (select count(*) from [SPS_Fiscal].[dbo].[DIME_TabelaQuadro94]) else 0 end
IF @LINHAS = 0
begin
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TabelaQuadro94]
           ([NumeroItem]
           ,[Descricao]
           ,[TipoItem])
SELECT '411','(=) Pró-labore','1' UNION ALL
SELECT '412','(+) Comissões, salários, ordenados','1' UNION ALL
SELECT '421','(+) Encargos sociais','1' UNION ALL
SELECT '422','(+) Tributos federais','1' UNION ALL
SELECT '423','(+) Tributos estaduais','1' UNION ALL
SELECT '424','(+) tributos municipais','1' UNION ALL
SELECT '431','(+) Combustíveis e lubrificantes','1' UNION ALL
SELECT '432','(+) Água e telefone','1' UNION ALL
SELECT '433','(+) Energia elétrica','1' UNION ALL
SELECT '441','(+) Aluguéis','1' UNION ALL
SELECT '442','(+) Seguros','1' UNION ALL
SELECT '443','(+) Fretes e carretos','1' UNION ALL
SELECT '451','(+) Serviços profissionais','1' UNION ALL
SELECT '461','(+) Despesas financeiras','1' UNION ALL
SELECT '498','(+) Outras despesas','1' UNION ALL
SELECT '499','(=) Total','1' 
end

set @LINHAS = case when (select count(*) from [SPS_Fiscal].[dbo].[DIME_TipoRegistro]) <> 0 
Then (select count(*) from [SPS_Fiscal].[dbo].[DIME_TipoRegistro]) else 0 end
IF @LINHAS = 0
begin
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TipoRegistro]
           ([Quadro]
           ,[TipoRegistro]
           ,[Descricao])
     
SELECT '01','22','Valores Fiscais Entradas' UNION ALL
SELECT '02','23','Valores Fiscais Saídas' UNION ALL
SELECT '03','24','Resumo dos Valores Fiscais' UNION ALL
SELECT '04','25','Resumo da Apuração dos Débitos' UNION ALL
SELECT '05','26','Resumo da Apuração dos Créditos' UNION ALL
SELECT '09','30','Cálculo do Imposto a Pagar ou Saldo Credor' UNION ALL
SELECT '11','32','Informações sobre Substituição Tributária' UNION ALL
SELECT '12','33','Discriminação dos Pagamentos do Imposto e dos Débitos Específicos' UNION ALL
SELECT '46','46','Créditos por Regimes e Autorizações Especiais' UNION ALL
SELECT '48','48','Informações para Rateio do Valor Adicionado' UNION ALL
SELECT '49','49','Entradas por Unidade da Federação' UNION ALL
SELECT '50','50','Saídas por Unidade da Federação' UNION ALL
SELECT '80','80','Resumo do Livro Registro de Inventário e Receita Bruta' UNION ALL
SELECT '81','81','Ativo' UNION ALL
SELECT '82','82','Passivo' UNION ALL
SELECT '83','83','Demonstração de Resultado' UNION ALL
SELECT '84','84','Detalhamento das Despesas' UNION ALL
SELECT '90','90','Resumo do Livro Registro de Inventário - Encerramento de Atividade' UNION ALL
SELECT '91','91','Ativo' UNION ALL
SELECT '92','92','Passivo' UNION ALL
SELECT '93','93','Demonstração de Resultado' UNION ALL
SELECT '94','94','Detalhamento de Despesas' 
end

 IF (select count(*) from [SPS_Fiscal].[dbo].[DIME_TipoRegistro] where quadro = 80) = 0
begin
INSERT INTO [SPS_Fiscal].[dbo].[DIME_TipoRegistro]
           ([Quadro]
           ,[TipoRegistro]
           ,[Descricao])
SELECT '80','80','Resumo do Livro Registro de Inventário e Receita Bruta' UNION ALL
SELECT '81','81','Ativo' UNION ALL
SELECT '82','82','Passivo' UNION ALL
SELECT '83','83','Demonstração de Resultado' UNION ALL
SELECT '84','84','Detalhamento das Despesas' UNION ALL
SELECT '90','90','Resumo do Livro Registro de Inventário - Encerramento de Atividade' UNION ALL
SELECT '91','91','Ativo' UNION ALL
SELECT '92','92','Passivo' UNION ALL
SELECT '93','93','Demonstração de Resultado' UNION ALL
SELECT '94','94','Detalhamento de Despesas'
end
", Support.Conectar());
                #endregion
                //SqlCommand command = new SqlCommand(@"INSERT INTO [DIME_Contabilista]([CPF],[NomeContabilista]) VALUES (@CPF, @NomeContabilista)", Support.Conectar());
                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static void CriaProc()
        {
            SqlCommand command = new SqlCommand();
            #region CRIANDO PROCEDURES

            command = new System.Data.SqlClient.SqlCommand(@"IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'SP_SPSQuadros')
                                                                            DROP PROCEDURE [dbo].[SP_SPSQuadros]

                                                                                    IF EXISTS(SELECT * FROM sys.objects WHERE type = 'P' AND name = 'SP_SPSLivros')
                                                                            DROP PROCEDURE [dbo].[SP_SPSLivros]", Support.Conectar());
            command.ExecuteNonQuery();

            command = new SqlCommand(@"SELECT Name FROM SYSOBJECTS WHERE NAME = 'SP_SPSLivros'", Support.Conectar());
            int nome = Convert.ToInt32(command.ExecuteScalar());
            if (nome == 0)
            {
                #region QUERY DE CRIAÇÃO DA PROCEDURE
                command = new SqlCommand(@"CREATE PROCEDURE [dbo].[SP_SPSLivros] ( 
	@dataDe datetime, 
	@dataAte datetime, 
	@BPLID NVARCHAR(4) , 
	@IdPeriodo NVARCHAR(MAX),
	@BD NVARCHAR(50) )
--[SPS_Fiscal].[dbo].[SP_SPSLivros] '20170201','20170228',3,2, 'SBO_GPO_KIKOS'

--SELECT * FROM [SPS_Fiscal].[dbo].[DIME_EntradasP1] where idPeriodo = 2 ORDER BY DataLanc, DataDoc, Convert(Int, Serial)
--SELECT * FROM [SPS_Fiscal].[dbo].[DIME_SaidasP2] where idPeriodo = 2

as
begin

DECLARE @Cod int = 1
Declare @Tab nvarchar(3)
Declare @Array Table (Codigo Char(2), Nome nvarchar(3), Tipo nvarchar(30))

SET NOCOUNT ON;

-- Inserindo Colunas para o laço

Insert @Array Values('1','PCH','NF Entrada')
Insert @Array Values('2','PDN','Receb Merc')
Insert @Array Values('3','RDN','Devolucao')
Insert @Array Values('4','RIN','Dev. Saida')
Insert @Array Values('5','INV','NF Saida')
Insert @Array Values('6','DLN','Enrega')
Insert @Array Values('7','RPD','Dev. Mercadoria')
Insert @Array Values('8','RPC','Dev. Entrada')

/******************************************************************************************************
	INICIO DA APURAÇÂO ICMS
******************************************************************************************************/
/*
------------------
	ENTRADAS
------------------
*/
Declare @Entrada01 nvarchar(max)
Declare @Entrada02 nvarchar(max)

PRINT ('ENTRADA')

CREATE TABLE #ENTRADA ( IDPeriodo NVARCHAR(MAX), BD NVARCHAR(50) NULL, Filial NVARCHAR(3) NULL,TPDoc NVARCHAR(3) NULL,DatadeEntrada date NULL, Especie NVARCHAR(5) NULL, SerieSubSerie nVARCHAR(15) NULL, 
						Serial nVARCHAR(30) NULL, DataDoc date NULL, Emitente nVARCHAR(254) NULL,IE nVARCHAR(30) NULL,
						CNPJ nVARCHAR(30) NULL,State nVARCHAR(3) NULL,ValorContabil numeric (18,2) NULL, CFOPCode NVARCHAR(5) NULL,
						BaseCalculo numeric (18,2) NULL,Aliq numeric (19,6) NULL, TaxSum numeric (18,2) NULL, BaseIsenta numeric (18,2) NULL,
						BaseOutras numeric (18,2) NULL, BaseST numeric (18,2) NULL, ValorST numeric (18,2) NULL)

WHILE  @Cod < 5
Begin
SET @Tab = (Select Nome From @Array where codigo=@Cod)  

SET @Entrada01 = '
INSERT INTO #ENTRADA
SELECT '''+@IdPeriodo+''' AS IDPeriodo, '''+@BD+''' AS DB, '''+@BPLId+''' AS Filial, '''+@Tab+''' AS TPDoc, T0.DocDate AS ''DatadeEntrada'',  
	CASE WHEN T0.Model = 39 THEN ''NFe'' WHEN T0.Model = 44 Then ''CT-e'' WHEN T0.Model = 51 Then ''08'' WHEN T0.Model = 19 Then ''22'' ELSE ''NFEE'' END AS Especie, 
	isNULL(T0.SeriesStr,'''') AS ''SerieSubSerie'', 
	T0.Serial, 
	T0.TaxDate AS ''DataDoc'', 
	T0.CardName AS ''Emitente'',
	isNULL(T2.TaxId1,'''') AS ''IE'',
	isNULL(T2.TaxId0,'''') AS ''CNPJ'',
	T2.State, 
	isNULL(T1.LineTotal,0.0) ValorContabil, 
	T1.CFOPCode, 
	isNULL(T4.BaseSum,0.0) BaseCalculo, 
	isNULL(T4.TaxRate,0.0) AS Aliq,
	isNULL(T4.TaxSum,0.0) TaxSum, 
	CASE WHEN T4.U_Isento = 100 THEN isNULL(T4.U_OthAmtL,0.0) ELSE 0.0 END BaseIsenta, 
	CASE WHEN T4.U_Outros = 100 THEN isNULL(T4.U_OthAmtL,0.0) ELSE 0.0 END BaseOutras,
	BaseST = isNULL((SELECT SUM(V4.BaseSum) FROM '+@BD+'.[dbo].['+@Tab+'4] V4 WHERE T0.DocEntry = V4.DocEntry  AND V4.RelateType = 1 AND T1.LineNum = V4.LineNum AND V4.staType in (SELECT TT.AbsId FROM '+@BD+'.[dbo].[OSTT] TT INNER JOIN '+@BD+'.[dbo].[ONFT] FT ON TT.NfTaxId = FT.AbsId WHERE Code = ''ICMS-ST'')), 0.0),
	ValorST = isNULL((SELECT SUM(V4.TaxSum) FROM '+@BD+'.[dbo].['+@Tab+'4] V4 WHERE T0.DocEntry = V4.DocEntry  AND V4.RelateType = 1 AND T1.LineNum = V4.LineNum AND V4.staType in (SELECT TT.AbsId FROM '+@BD+'.[dbo].[OSTT] TT INNER JOIN '+@BD+'.[dbo].[ONFT] FT ON TT.NfTaxId = FT.AbsId WHERE Code = ''ICMS-ST'')), 0.0)
FROM '+@BD+'.[dbo].[O'+@Tab+'] T0 
	INNER JOIN '+@BD+'.[dbo].['+@Tab+'1] T1 ON T0.Docentry = T1.DocEntry 
	INNER JOIN '+@BD+'.[dbo].['+@Tab+'12] T2 ON T0.DocEntry = T2.DocEntry
	INNER JOIN '+@BD+'.[dbo].['+@Tab+'4] T4 ON T0.DocEntry = T4.DocEntry AND T4. RelateType = 1 AND T1.LineNum = T4.LineNum 
		AND T4.staType in (SELECT TT.AbsId FROM '+@BD+'.[dbo].[OSTT] TT INNER JOIN '+@BD+'.[dbo].[ONFT] FT ON TT.NfTaxId = FT.AbsId WHERE Code = ''ICMS'')
WHERE Model in (39, 6, 44, 51, 19) AND BPLID = '+@BPLId+' AND CANCELED = ''N'' 
	AND T0.DocDate between convert(datetime,'''+convert(nvarchar,@datade,102)+''') and convert(datetime,'''+convert(nvarchar,@DataAte,102)+''')
'
/** ADIANTAMENTO **/
SET @Entrada02 = ' INSERT INTO #ENTRADA
SELECT '''+@IdPeriodo+''' AS IDPeriodo, '''+@BD+''' AS DB, '''+@BPLId+''' AS Filial, '''+@Tab+''' AS TPDoc, T0.DocDate AS ''DatadeEntrada'',  
	CASE WHEN T0.Model = 39 THEN ''NFe'' WHEN T0.Model = 44 Then ''CT-e'' WHEN T0.Model = 51 Then ''08'' WHEN T0.Model = 19 Then ''22'' ELSE ''NFEE'' END AS Especie, 
	isNULL(T0.SeriesStr,'''') AS ''SerieSubSerie'', 
	T0.Serial, 
	T0.TaxDate AS ''DataDoc'', 
	T0.CardName AS ''Emitente'',
	isNULL(T2.TaxId1,'''') AS ''IE'',
	isNULL(T2.TaxId0,'''') AS ''CNPJ'',
	isNULL(T2.State,''''), 
	isNULL(T1.DistribSum,0.0) ValorContabil, 
	T1.CFOPCode, 
	isNULL(T4.BaseSum,0.0) BaseCalculo, 
	isNULL(T4.TaxRate,0.0) AS Aliq,
	isNULL(T4.TaxSum,0.0) TaxSum, 
	CASE WHEN T4.U_Isento = 100 THEN isNULL(T4.U_OthAmtL,0.0) ELSE 0.0 END BaseIsenta, 
	CASE WHEN T4.U_Outros = 100 THEN isNULL(T4.U_OthAmtL,0.0) ELSE 0.0 END BaseOutras,
	BaseST = isNULL((SELECT SUM(V4.BaseSum) FROM '+@BD+'.[dbo].['+@Tab+'4] V4 WHERE T0.DocEntry = V4.DocEntry  AND V4.RelateType = 1 AND T1.LineNum = V4.LineNum AND V4.staType in (SELECT TT.AbsId FROM '+@BD+'.[dbo].[OSTT] TT INNER JOIN '+@BD+'.[dbo].[ONFT] FT ON TT.NfTaxId = FT.AbsId WHERE Code = ''ICMS-ST'')), 0.0),
	ValorST = isNULL((SELECT SUM(V4.TaxSum) FROM '+@BD+'.[dbo].['+@Tab+'4] V4 WHERE T0.DocEntry = V4.DocEntry  AND V4.RelateType = 1 AND T1.LineNum = V4.LineNum AND V4.staType in (SELECT TT.AbsId FROM '+@BD+'.[dbo].[OSTT] TT INNER JOIN '+@BD+'.[dbo].[ONFT] FT ON TT.NfTaxId = FT.AbsId WHERE Code = ''ICMS-ST'')), 0.0)
FROM '+@BD+'.[dbo].[O'+@Tab+'] T0 
	INNER JOIN '+@BD+'.[dbo].['+@Tab+'1] T1 ON T0.Docentry = T1.DocEntry 
	INNER JOIN '+@BD+'.[dbo].['+@Tab+'12] T2 ON T0.DocEntry = T2.DocEntry
	INNER JOIN '+@BD+'.[dbo].['+@Tab+'4] T4 ON T0.DocEntry = T4.DocEntry AND T4. RelateType = 13 AND T1.LineNum = T4.LineNum 
		AND T4.staType in (SELECT TT.AbsId FROM '+@BD+'.[dbo].[OSTT] TT INNER JOIN '+@BD+'.[dbo].[ONFT] FT ON TT.NfTaxId = FT.AbsId WHERE Code = ''ICMS'')
WHERE Model in (39, 6, 44, 51, 19) AND BPLID = '+@BPLId+' AND CANCELED = ''N'' 
	AND T0.DocDate between convert(datetime,'''+convert(nvarchar,@datade,102)+''') and convert(datetime,'''+convert(nvarchar,@DataAte,102)+''')
'

--print @Entrada01
--print @Entrada02
exec sp_executesql @Entrada01
exec sp_executesql @Entrada02

SET @Cod = @Cod + 1

END

/*
------------------
	SAIDAS
------------------
*/

PRINT ('SAIDA')

Declare @SAIDA01 nvarchar(max)
Declare @SAIDA02 nvarchar(max)

CREATE TABLE #SAIDA ( IDPeriodo NVARCHAR(MAX), BD NVARCHAR(50) NULL, Filial NVARCHAR(3) NULL, TPDoc NVARCHAR(3) NULL,Especie NVARCHAR(5) NULL, SerieSubSerie nVARCHAR(15) NULL, 
						Serial nVARCHAR(30) NULL, DataDoc nVARCHAR(3) NULL, UFDest nVARCHAR(3) NULL,ValorContabil numeric (18,2) NULL, CFOPCode NVARCHAR(5) NULL,
						BaseCalculo numeric (18,2) NULL,Aliq numeric (19,6) NULL, TaxSum numeric (18,2) NULL, BaseIsenta numeric (18,2) NULL,
						BaseOutras numeric (18,2) NULL, BaseST numeric (18,2) NULL, ValorST numeric (18,2) NULL)

SET @Cod = 5
WHILE @Cod < 9
Begin

SET @Tab = (Select Nome From @Array where codigo=@Cod)  

SET @SAIDA01 = '
INSERT INTO #SAIDA
SELECT  '''+@IdPeriodo+''' AS IDPeriodo,  '''+@BD+''' AS DB, '''+@BPLId+''' AS Filial, '''+@Tab+''' AS TPDoc, 
	CASE WHEN T0.Model = 39 THEN ''NFe'' WHEN T0.Model = 44 Then ''CT-e'' ELSE ''NFEE'' END AS Especie, 
	isNULL(T0.SeriesStr, '''') AS ''SerieSubSerie'', 
	T0.Serial, 
	DATEPART(DAY, T0.TaxDate) AS ''DataDoc'', 
	isNULL(T2.State, '''') AS UFDest, 
	isNULL(T1.LineTotal,0.0) ValorContabil, 
	T1.CFOPCode, 
	isNULL(T4.BaseSum,0.0) BaseCalculo, 
	isNULL(T4.TaxRate, 0.0) AS Aliq,
	isNULL(T4.TaxSum,0.0) TaxSum, 
	CASE WHEN T4.U_Isento = 100 THEN isNULL(T4.U_OthAmtL,0.0) ELSE 0.0 END BaseIsenta, 
	CASE WHEN T4.U_Outros = 100 THEN isNULL(T4.U_OthAmtL,0.0) ELSE 0.0 END BaseOutras,
	BaseST = isNULL((SELECT SUM(V4.BaseSum) FROM '+@BD+'.[dbo].['+@Tab+'4] V4 WHERE T0.DocEntry = V4.DocEntry  AND V4.RelateType = 1 AND T1.LineNum = V4.LineNum AND V4.staType in (SELECT TT.AbsId FROM '+@BD+'.[dbo].[OSTT] TT INNER JOIN '+@BD+'.[dbo].[ONFT] FT ON TT.NfTaxId = FT.AbsId WHERE Code = ''ICMS-ST'')), 0.0),
	ValorST = isNULL((SELECT SUM(V4.TaxSum) FROM '+@BD+'.[dbo].['+@Tab+'4] V4 WHERE T0.DocEntry = V4.DocEntry  AND V4.RelateType = 1 AND T1.LineNum = V4.LineNum AND V4.staType in (SELECT TT.AbsId FROM '+@BD+'.[dbo].[OSTT] TT INNER JOIN '+@BD+'.[dbo].[ONFT] FT ON TT.NfTaxId = FT.AbsId WHERE Code = ''ICMS-ST'')), 0.0)
FROM '+@BD+'.[dbo].[O'+@Tab+'] T0 
	INNER JOIN '+@BD+'.[dbo].['+@Tab+'1] T1 ON T0.Docentry = T1.DocEntry 
	INNER JOIN '+@BD+'.[dbo].['+@Tab+'12] T2 ON T0.DocEntry = T2.DocEntry
	INNER JOIN '+@BD+'.[dbo].['+@Tab+'4] T4 ON T0.DocEntry = T4.DocEntry AND T4. RelateType = 1 AND T1.LineNum = T4.LineNum 
		AND T4.staType in (SELECT TT.AbsId FROM '+@BD+'.[dbo].[OSTT] TT INNER JOIN '+@BD+'.[dbo].[ONFT] FT ON TT.NfTaxId = FT.AbsId WHERE Code = ''ICMS'')
WHERE Model in (39, 44) AND BPLID = '+@BPLId+' AND CANCELED = ''N'' 
	AND T0.DocDate between convert(datetime,'''+convert(nvarchar,@datade,102)+''') and convert(datetime,'''+convert(nvarchar,@DataAte,102)+''')
'

/** ADIANTAMENTO **/

SET @SAIDA02 = 'INSERT INTO #SAIDA
SELECT  '''+@IdPeriodo+''' AS IDPeriodo,  '''+@BD+''' AS DB, '''+@BPLId+''' AS Filial, '''+@Tab+''' AS TPDoc, 
	CASE WHEN T0.Model = 39 THEN ''NFe'' WHEN T0.Model = 44 Then ''CT-e'' ELSE ''NFEE'' END AS Especie, 
	isNULL(T0.SeriesStr,'''') AS ''SerieSubSerie'', 
	T0.Serial, 
	DATEPART(DAY, T0.TaxDate) AS ''DataDoc'', 
	isNULL(T2.State,'''') AS UFDest, 
	isNULL(T1.DistribSum, 0.0) ValorContabil, 
	T1.CFOPCode, 
	isNULL(T4.BaseSum, 0.0) BaseCalculo, 
	isNULL(T4.TaxRate,0.0) AS Aliq,
	isNULL(T4.TaxSum, 0.0) TaxSum, 
	CASE WHEN T4.U_Isento = 100 THEN isNULL(T4.U_OthAmtL, 0.0) ELSE 0.0 END BaseIsenta, 
	CASE WHEN T4.U_Outros = 100 THEN isNULL(T4.U_OthAmtL, 0.0) ELSE 0.0 END BaseOutras,
	BaseST = isNULL((SELECT SUM(V4.BaseSum) FROM '+@BD+'.[dbo].['+@Tab+'4] V4 WHERE T0.DocEntry = V4.DocEntry  AND V4.RelateType = 1 AND T1.LineNum = V4.LineNum AND V4.staType in (SELECT TT.AbsId FROM '+@BD+'.[dbo].[OSTT] TT INNER JOIN '+@BD+'.[dbo].[ONFT] FT ON TT.NfTaxId = FT.AbsId WHERE Code = ''ICMS-ST'')), 0.0),
	ValorST = isNULL((SELECT SUM(V4.TaxSum) FROM '+@BD+'.[dbo].['+@Tab+'4] V4 WHERE T0.DocEntry = V4.DocEntry  AND V4.RelateType = 1 AND T1.LineNum = V4.LineNum AND V4.staType in (SELECT TT.AbsId FROM '+@BD+'.[dbo].[OSTT] TT INNER JOIN '+@BD+'.[dbo].[ONFT] FT ON TT.NfTaxId = FT.AbsId WHERE Code = ''ICMS-ST'')), 0.0)
FROM '+@BD+'.[dbo].[O'+@Tab+'] T0 
	INNER JOIN '+@BD+'.[dbo].['+@Tab+'1] T1 ON T0.Docentry = T1.DocEntry 
	INNER JOIN '+@BD+'.[dbo].['+@Tab+'12] T2 ON T0.DocEntry = T2.DocEntry
	INNER JOIN '+@BD+'.[dbo].['+@Tab+'4] T4 ON T0.DocEntry = T4.DocEntry AND T4. RelateType = 13 AND T1.LineNum = T4.LineNum 
		AND T4.staType in (SELECT TT.AbsId FROM '+@BD+'.[dbo].[OSTT] TT INNER JOIN '+@BD+'.[dbo].[ONFT] FT ON TT.NfTaxId = FT.AbsId WHERE Code = ''ICMS'')
WHERE Model in (39, 44) AND BPLID = '+@BPLId+' AND CANCELED = ''N'' 
	AND T0.DocDate between convert(datetime,'''+convert(nvarchar,@datade,102)+''') and convert(datetime,'''+convert(nvarchar,@DataAte,102)+''')

'
--PRINT @Saida01
--PRINT @Saida02
exec sp_executesql @Saida01
exec sp_executesql @Saida02

SET @Cod = @Cod + 1

END


DELETE FROM [SPS_Fiscal].[dbo].[DIME_EntradasP1] WHERE [IdPeriodo] = @IdPeriodo 

INSERT INTO [SPS_Fiscal].[dbo].[DIME_EntradasP1]
            ([IdPeriodo]
            ,[BD]
		    ,[Filial]
            ,[TPDoc]
            ,[DataLanc]
            ,[DataDoc]
            ,[Serial]
            ,[Especie]
            ,[Estado]
            ,[CFOP]
            ,[Vl_Contabil]
            ,[Vl_Base_Calc]
            ,[Aliq]
            ,[Vl_Imp_Cred]
            ,[Vl_Isentas_Nao_Trib]
            ,[Vl_Outras]
            ,[Vl_Base_ST]
            ,[Vl_ST]
            ,[Vl_Base_Calc_IR]
            ,[Vl_IR]
            ,[Vl_Difal])

SELECT	 [IdPeriodo]
            ,[BD]
		    ,[Filial]
		    ,[TPDoc]
		    ,[DatadeEntrada]
		    ,[DataDoc]
		    ,[Serial]
            ,[Especie]
            ,[State]
            ,[CFOPCode]
            ,[ValorContabil]
            ,[BaseCalculo]
            ,[Aliq]
            ,[TaxSum]
            ,[BaseIsenta]
            ,[BaseOutras]
            ,[BaseST]
            ,[ValorST]
            ,0.0
            ,0.0
            ,0.0
FROM #ENTRADA
--GROUP BY [IdPeriodo],[BD],[Filial],[TPDoc],[DatadeEntrada],[DataDoc],[Serial],[Especie],[State],[CFOPCode],[Aliq]

DELETE FROM [SPS_Fiscal].[dbo].[DIME_SaidasP2] WHERE [IdPeriodo] = @IdPeriodo 
INSERT INTO [SPS_Fiscal].[dbo].[DIME_SaidasP2]
            ([IdPeriodo]
            ,[BD]
		    ,[Filial]
            ,[TPDoc]
            ,[Especie]
            ,[Serial]
            ,[CFOP]
            ,[Vl_Contabil]
            ,[Vl_Base_Calc]
            ,[Aliq]
            ,[Vl_Imp_Debit]
            ,[Vl_Isentas_Nao_Trib]
            ,[Vl_Outras]
            ,[Vl_Base_ST]
            ,[Vl_ST]
            ,[Vl_Base_Calc_IR]
            ,[Vl_IR]
			,[Dia])

SELECT	 [IdPeriodo]
		    ,[BD]
		    ,[Filial]
		    ,[TPDoc]
		    ,[Especie]
            ,[Serial]
            ,[CFOPCode]
            ,[ValorContabil] + [ValorST]
            ,[BaseCalculo]
            ,[Aliq]
            ,[TaxSum]
            ,[BAseIsenta]
            ,[BaseOutras]
            ,[BaseST]
            ,[ValorST]
            ,0.0
            ,0.0
			,DataDoc

FROM #SAIDA
--GROUP BY [IdPeriodo],[BD],[Filial],[TPDoc],[Serial],[Especie],[CFOPCode],[Aliq]

DROP TABLE #ENTRADA
DROP TABLE #SAIDA
END", Support.Conectar());
                #endregion
                command.ExecuteNonQuery();
            }

            command = new SqlCommand(@"SELECT Name FROM SYSOBJECTS WHERE NAME = 'SP_SPSQuadros'", Support.Conectar());
            nome = Convert.ToInt32(command.ExecuteScalar());
            if (nome == 0)
            {
                #region QUERY DE CRIAÇÃO DE PROCEDURE
                command = new SqlCommand(@"CREATE PROCEDURE [dbo].[SP_SPSQuadros] ( 
		@BPLID NVARCHAR(3) ,
		@DataDe Date ,
		@DataAte Date ,
		@IdPeriodo int ,
		@IdDeclaracao int)

-- EXEC [dbo].[SP_SPSQuadros] '5', '20170201',  '20170228',  2, 5
--SELECT * FROM [SPS_Fiscal].[dbo].[DIME_EntradasP1] WHERE IdPeriodo = @IdPeriodo
--SELECT * FROM [SPS_Fiscal].[dbo].[DIME_DadosQuadro01] WHERE IdDeclaracao = @IdDeclaracao
--SELECT * FROM [SPS_Fiscal].[dbo].[DIME_SaidasP2] WHERE IdPeriodo = @IdPeriodo
--SELECT * FROM [SPS_Fiscal].[dbo].[DIME_DadosQuadro02] WHERE IdDeclaracao = @IdDeclaracao
--SELECT * FROM [SPS_Fiscal].[dbo].[DIME_DadosQuadro03] WHERE IdDeclaracao = @IdDeclaracao
--SELECT * FROM [SPS_Fiscal].[dbo].[DIME_DadosQuadro04] WHERE IdDeclaracao = @IdDeclaracao
--SELECT * FROM [SPS_Fiscal].[dbo].[DIME_DadosQuadro05] WHERE IdDeclaracao = @IdDeclaracao
--SELECT * FROM [SPS_Fiscal].[dbo].[DIME_DadosQuadro09] WHERE IdDeclaracao = @IdDeclaracao
--SELECT * FROM [SPS_Fiscal].[dbo].[DIME_DadosQuadro11] WHERE IdDeclaracao = @IdDeclaracao

--------------------------------------------------------------------------------------------------------------------
-- INSERT DA APURAÇÃO
--------------------------------------------------------------------------------------------------------------------
as
begin
-------------------------------------------------------------------------------------------------------------
PRINT ('[DIME_DadosQuadro01]')
DELETE FROM [SPS_Fiscal].[dbo].[DIME_DadosQuadro01] WHERE [IdDeclaracao] = @IdDeclaracao
/*
-- [DIME_DadosQuadro01]
*/
-------------------------------------------------------------------------------------------------------------
INSERT INTO [SPS_Fiscal].[dbo].[DIME_DadosQuadro01]
           ([IdDeclaracao]
           ,[CFOP]
           ,[Vl_Contabil]
           ,[Vl_Base_Calc]
           ,[Vl_Imp_Cred]
           ,[Vl_Isentas_Nao_Trib]
           ,[Vl_Outras]
           ,[Vl_Base_Calc_IR]
           ,[Vl_IR]
           ,[Vl_Difal])
SELECT
	@IdDeclaracao,
	CFOP, 
	SUM(Vl_Contabil) ValorContabil,  
	SUM(Vl_Base_Calc) BaseCalculo, 
	Sum(Vl_Imp_Cred) Imposto, 
	isNULL(Sum(Vl_Isentas_Nao_Trib), 0.0) BaseIsenta, 
	isNULL(Sum(Vl_Outras), 0.0) BaseOutras,
	isNULL(Sum(Vl_Base_ST), 0.0) BaseST,
	isNULL(Sum(Vl_ST),0.0) ValorST,
	0.0
FROM [SPS_Fiscal].[dbo].[DIME_EntradasP1]
WHERE [IdPeriodo] = @IdPeriodo
GROUP BY CFOP



-------------------------------------------------------------------------------------------------------------
PRINT ('[DIME_DadosQuadro02]')
DELETE FROM [SPS_Fiscal].[dbo].[DIME_DadosQuadro02] WHERE IdDeclaracao = @IdDeclaracao
/*
-- [DIME_DadosQuadro02]
*/
-------------------------------------------------------------------------------------------------------------
INSERT INTO [SPS_Fiscal].[dbo].[DIME_DadosQuadro02]
           ([IdDeclaracao]
           ,[CFOP]
           ,[Vl_Contabil]
           ,[Vl_Base_Calc]
           ,[Vl_Imp_Debit]
           ,[Vl_Isentas_Nao_Trib]
           ,[Vl_Outras]
           ,[Vl_Base_Calc_IR]
           ,[Vl_IR])
SELECT @IdDeclaracao,
CFOP, 
SUM(Vl_Contabil) ValorContabil,  
SUM(Vl_Base_Calc) BaseCalculo, 
Sum(Vl_Imp_Debit) Imposto, 
isNULL(Sum(Vl_Isentas_Nao_Trib), 0.0) BaseIsenta, 
isNULL(Sum(Vl_Outras), 0.0) BaseOutras,
isNULL(Sum([Vl_Base_Calc_IR]), 0.0) BaseST,
isNULL(Sum([Vl_IR]), 0.0) ValorST
FROM [SPS_Fiscal].[dbo].[DIME_SaidasP2]
WHERE [IdPeriodo] = @IdPeriodo
GROUP BY CFOP

-------------------------------------------------------------------------------------------------------------
PRINT ('[DIME_DadosQuadro03]')
DELETE FROM [SPS_Fiscal].[dbo].[DIME_DadosQuadro03] WHERE IdDeclaracao = @IdDeclaracao
/*
-- [DIME_DadosQuadro03]
*/
-------------------------------------------------------------------------------------------------------------
INSERT INTO [SPS_Fiscal].[dbo].[DIME_DadosQuadro03]
           ([NumeroItem]
           ,[IdDeclaracao]
           ,[Valor])

SELECT   '010', @IdDeclaracao, SUM(Vl_Contabil) ValorContabil FROM [SPS_Fiscal].[dbo].[DIME_EntradasP1] WHERE [IdPeriodo] = @IdPeriodo UNION ALL
SELECT   '020', @IdDeclaracao, SUM(Vl_Base_Calc) BaseCalculo FROM [SPS_Fiscal].[dbo].[DIME_EntradasP1] WHERE [IdPeriodo] = @IdPeriodo UNION ALL
SELECT   '030', @IdDeclaracao, Sum(Vl_Imp_Cred) Imposto FROM [SPS_Fiscal].[dbo].[DIME_EntradasP1] WHERE [IdPeriodo] = @IdPeriodo UNION ALL
SELECT   '040', @IdDeclaracao, Sum(Vl_Isentas_Nao_Trib) BaseIsenta FROM [SPS_Fiscal].[dbo].[DIME_EntradasP1] WHERE [IdPeriodo] = @IdPeriodo UNION ALL
SELECT   '050', @IdDeclaracao, Sum(Vl_Outras) BaseOutras FROM [SPS_Fiscal].[dbo].[DIME_EntradasP1] WHERE [IdPeriodo] = @IdPeriodo UNION ALL
SELECT   '053', @IdDeclaracao, Sum(Vl_Base_ST) BaseST FROM [SPS_Fiscal].[dbo].[DIME_EntradasP1] WHERE [IdPeriodo] = @IdPeriodo UNION ALL
SELECT   '054', @IdDeclaracao, Sum([Vl_ST]) ValorST FROM [SPS_Fiscal].[dbo].[DIME_EntradasP1] WHERE [IdPeriodo] = @IdPeriodo UNION ALL
SELECT   '057', @IdDeclaracao, 0.0  UNION ALL
SELECT   '060', @IdDeclaracao, SUM(Vl_Contabil) ValorContabil FROM [SPS_Fiscal].[dbo].[DIME_SaidasP2] WHERE [IdPeriodo] = @IdPeriodo UNION ALL
SELECT   '070', @IdDeclaracao, SUM(Vl_Base_Calc) BaseCalculo FROM [SPS_Fiscal].[dbo].[DIME_SaidasP2] WHERE [IdPeriodo] = @IdPeriodo UNION ALL
SELECT   '080', @IdDeclaracao, Sum(Vl_Imp_Debit) Imposto FROM [SPS_Fiscal].[dbo].[DIME_SaidasP2] WHERE [IdPeriodo] = @IdPeriodo UNION ALL
SELECT   '090', @IdDeclaracao, Sum(Vl_Isentas_Nao_Trib) BaseIsenta FROM [SPS_Fiscal].[dbo].[DIME_SaidasP2] WHERE [IdPeriodo] = @IdPeriodo UNION ALL
SELECT   '100', @IdDeclaracao, Sum(Vl_Outras) BaseOutras FROM [SPS_Fiscal].[dbo].[DIME_SaidasP2] WHERE [IdPeriodo] = @IdPeriodo UNION ALL
SELECT   '103', @IdDeclaracao, Sum(Vl_Base_Calc_IR) BaseIR FROM [SPS_Fiscal].[dbo].[DIME_SaidasP2] WHERE [IdPeriodo] = @IdPeriodo UNION ALL
SELECT   '104', @IdDeclaracao, Sum(Vl_IR) ValorIR FROM [SPS_Fiscal].[dbo].[DIME_SaidasP2] WHERE [IdPeriodo] = @IdPeriodo 

-------------------------------------------------------------------------------------------------------------
PRINT('[DIME_DadosQuadro04]')
declare @valor decimal(18,2) = (select valor from [SPS_Fiscal].[dbo].[DIME_DadosQuadro04] WHERE IdDeclaracao = @IdDeclaracao AND NumeroItem = 070)
declare @total decimal(18,2) = (SELECT SUM(valor) FROM [dbo].[DIME_DadosQuadro04] WHERE IdDeclaracao = @IdDeclaracao AND [NumeroItem] NOT IN (990))
DELETE FROM [SPS_Fiscal].[dbo].[DIME_DadosQuadro04] WHERE IdDeclaracao = @IdDeclaracao
/*
-- [DIME_DadosQuadro04]
*/
-------------------------------------------------------------------------------------------------------------
INSERT INTO [SPS_Fiscal].[dbo].[DIME_DadosQuadro04]
           ([NumeroItem]
           ,[IdDeclaracao]
           ,[Valor])

SELECT '010', @IdDeclaracao, Sum(Vl_Imp_Debit) Imposto FROM [SPS_Fiscal].[dbo].[DIME_SaidasP2] WHERE [IdPeriodo] = @IdPeriodo UNION ALL
SELECT '020', @IdDeclaracao, 0.0  UNION ALL
SELECT '030', @IdDeclaracao, 0.0  UNION ALL
SELECT '040', @IdDeclaracao, 0.0  UNION ALL
SELECT '045', @IdDeclaracao, 0.0  UNION ALL
SELECT '050', @IdDeclaracao, 0.0  UNION ALL
SELECT '060', @IdDeclaracao, 0.0  UNION ALL
SELECT '065', @IdDeclaracao, 0.0  UNION ALL
SELECT '070', @IdDeclaracao, ISNULL(@valor, 0.0)  UNION ALL
SELECT '990', @IdDeclaracao, @total

-------------------------------------------------------------------------------------------------------------
PRINT ('[DIME_DadosQuadro05]')
declare @Q5_30 decimal(18,2) = (select valor from [SPS_Fiscal].[dbo].[DIME_DadosQuadro05] WHERE IdDeclaracao = @IdDeclaracao AND NumeroItem = '030')
declare @Q5_40 decimal(18,2) = (select valor from [SPS_Fiscal].[dbo].[DIME_DadosQuadro05] WHERE IdDeclaracao = @IdDeclaracao AND NumeroItem = '040')
declare @Q5_45 decimal(18,2) = (select valor from [SPS_Fiscal].[dbo].[DIME_DadosQuadro05] WHERE IdDeclaracao = @IdDeclaracao AND NumeroItem = '045')
declare @Q5_50 decimal(18,2) = (select valor from [SPS_Fiscal].[dbo].[DIME_DadosQuadro05] WHERE IdDeclaracao = @IdDeclaracao AND NumeroItem = '050')

DELETE FROM [SPS_Fiscal].[dbo].[DIME_DadosQuadro05] WHERE IdDeclaracao = @IdDeclaracao
/*
-- [DIME_DadosQuadro05]
*/
-------------------------------------------------------------------------------------------------------------
INSERT INTO [SPS_Fiscal].[dbo].[DIME_DadosQuadro05]
           ([NumeroItem]
           ,[IdDeclaracao]
           ,[Valor])
SELECT '010', @IdDeclaracao, 0.0 UNION ALL
SELECT '020', @IdDeclaracao, Sum(Vl_Imp_Cred) Imposto FROM [SPS_Fiscal].[dbo].[DIME_EntradasP1] WHERE [IdPeriodo] = @IdPeriodo UNION ALL
SELECT '030', @IdDeclaracao, isNULL(@Q5_30,0.0) UNION ALL
SELECT '040', @IdDeclaracao, isNULL(@Q5_40,0.0) UNION ALL
SELECT '045', @IdDeclaracao, isNULL(@Q5_45,0.0) UNION ALL
SELECT '050', @IdDeclaracao, isNULL(@Q5_50,0.0) UNION ALL
SELECT '990', @IdDeclaracao, Sum(Vl_Imp_Cred) Imposto FROM [SPS_Fiscal].[dbo].[DIME_EntradasP1] WHERE [IdPeriodo] = @IdPeriodo 

-------------------------------------------------------------------------------------------------------------
PRINT ('[DIME_DadosQuadro09]')
declare @Q9_11 decimal(18,2) = (select valor from [SPS_Fiscal].[dbo].[DIME_DadosQuadro09] WHERE IdDeclaracao = @IdDeclaracao AND NumeroItem = '011')
declare @Q9_20 decimal(18,2) = (select valor from [SPS_Fiscal].[dbo].[DIME_DadosQuadro09] WHERE IdDeclaracao = @IdDeclaracao AND NumeroItem = '020')
declare @Q9_30 decimal(18,2) = (select valor from [SPS_Fiscal].[dbo].[DIME_DadosQuadro09] WHERE IdDeclaracao = @IdDeclaracao AND NumeroItem = '030')
declare @Q9_60 decimal(18,2) = (select valor from [SPS_Fiscal].[dbo].[DIME_DadosQuadro09] WHERE IdDeclaracao = @IdDeclaracao AND NumeroItem = '060')
DECLARE @Q9_70 decimal(18,2) = (select SUM(Vl_Cred_Apur) FROM [SPS_Fiscal].[dbo].[DIME_DadosQuadro46] where Ind_Origem = '1' AND IdDeclaracao = @IdDeclaracao)
DECLARE @Q9_75 decimal(18,2) = (select SUM(Vl_Cred_Apur) FROM [SPS_Fiscal].[dbo].[DIME_DadosQuadro46] where Ind_Origem = '14' AND IdDeclaracao = @IdDeclaracao)
declare @Q9_90 decimal(18,2) = (select valor from [SPS_Fiscal].[dbo].[DIME_DadosQuadro09] WHERE IdDeclaracao = @IdDeclaracao AND NumeroItem = '090')
declare @Q9_100 decimal(18,2) = (select valor from [SPS_Fiscal].[dbo].[DIME_DadosQuadro09] WHERE IdDeclaracao = @IdDeclaracao AND NumeroItem = '100')
declare @Q9_105 decimal(18,2) = (select valor from [SPS_Fiscal].[dbo].[DIME_DadosQuadro09] WHERE IdDeclaracao = @IdDeclaracao AND NumeroItem = '105')
declare @Q9_130 decimal(18,2) = (select valor from [SPS_Fiscal].[dbo].[DIME_DadosQuadro09] WHERE IdDeclaracao = @IdDeclaracao AND NumeroItem = '130')
declare @Q9_150 decimal(18,2) = (select valor from [SPS_Fiscal].[dbo].[DIME_DadosQuadro09] WHERE IdDeclaracao = @IdDeclaracao AND NumeroItem = '150')
declare @Q9_160 decimal(18,2) = (select valor from [SPS_Fiscal].[dbo].[DIME_DadosQuadro09] WHERE IdDeclaracao = @IdDeclaracao AND NumeroItem = '160')
declare @Q9_170 decimal(18,2) = (select valor from [SPS_Fiscal].[dbo].[DIME_DadosQuadro09] WHERE IdDeclaracao = @IdDeclaracao AND NumeroItem = '170')
declare @Q9_180 decimal(18,2) = (select valor from [SPS_Fiscal].[dbo].[DIME_DadosQuadro09] WHERE IdDeclaracao = @IdDeclaracao AND NumeroItem = '180')
declare @Q9_190 decimal(18,2) = (select valor from [SPS_Fiscal].[dbo].[DIME_DadosQuadro09] WHERE IdDeclaracao = @IdDeclaracao AND NumeroItem = '190')
DELETE FROM [SPS_Fiscal].[dbo].[DIME_DadosQuadro09] WHERE IdDeclaracao = @IdDeclaracao
/*
-- [DIME_DadosQuadro09]
*/
-------------------------------------------------------------------------------------------------------------
INSERT INTO [SPS_Fiscal].[dbo].[DIME_DadosQuadro09]
           ([NumeroItem]
           ,[IdDeclaracao]
           ,[Valor])
SELECT '010',@IdDeclaracao, @total UNION ALL
SELECT '011',@IdDeclaracao, isNULL(@Q9_11,0.0) UNION ALL
SELECT '020',@IdDeclaracao, isNULL(@Q9_20,0.0) UNION ALL
SELECT '030',@IdDeclaracao, isNULL(@Q9_30,0.0) UNION ALL
SELECT '036',@IdDeclaracao, 0.0 UNION ALL
SELECT '037',@IdDeclaracao, 0.0 UNION ALL
SELECT '038',@IdDeclaracao, 0.0 UNION ALL
SELECT '040',@IdDeclaracao, Sum(Vl_Imp_Debit) Imposto FROM [SPS_Fiscal].[dbo].[DIME_SaidasP2] WHERE [IdPeriodo] = @IdPeriodo UNION ALL
SELECT '050',@IdDeclaracao, Sum(Vl_Imp_Cred) Imposto FROM [SPS_Fiscal].[dbo].[DIME_EntradasP1] WHERE [IdPeriodo] = @IdPeriodo UNION ALL
SELECT '060',@IdDeclaracao, isNULL(@Q9_60,0.0) UNION ALL
SELECT '070',@IdDeclaracao, isNULL(@Q9_70,0.0) UNION ALL
SELECT '075',@IdDeclaracao, isNULL(@Q9_75,0.0) UNION ALL
SELECT '076',@IdDeclaracao, 0.0 UNION ALL
SELECT '080',@IdDeclaracao, Sum(Vl_Imp_Cred) Imposto FROM [SPS_Fiscal].[dbo].[DIME_EntradasP1] WHERE [IdPeriodo] = @IdPeriodo UNION ALL
SELECT '090',@IdDeclaracao, isNULL(@Q9_90,0.0) UNION ALL
SELECT '100',@IdDeclaracao, isNULL(@Q9_100,0.0) UNION ALL
SELECT '105',@IdDeclaracao, isNULL(@Q9_105,0.0) UNION ALL
SELECT '110',@IdDeclaracao, 0.0 UNION ALL
SELECT '120',@IdDeclaracao, (	
								(SELECT Sum(Vl_Imp_Debit) FROM [SPS_Fiscal].[dbo].[DIME_SaidasP2] WHERE [IdPeriodo] = @IdPeriodo) 
								- (SELECT Sum(Vl_Imp_Cred) FROM [SPS_Fiscal].[dbo].[DIME_EntradasP1] WHERE [IdPeriodo] = @IdPeriodo)
							) UNION ALL
SELECT '130',@IdDeclaracao, isNULL(@Q9_130,0.0) UNION ALL
SELECT '140',@IdDeclaracao, 0.0 UNION ALL
SELECT '150',@IdDeclaracao, isNULL(@Q9_150,0.0) UNION ALL
SELECT '160',@IdDeclaracao, isNULL(@Q9_160,0.0) UNION ALL
SELECT '170',@IdDeclaracao, isNULL(@Q9_170,0.0) UNION ALL
SELECT '180',@IdDeclaracao, isNULL(@Q9_180,0.0) UNION ALL
SELECT '190',@IdDeclaracao, isNULL(@Q9_190,0.0) UNION ALL
SELECT '998',@IdDeclaracao, 0.0 UNION ALL
SELECT '999',@IdDeclaracao, (	
								(SELECT Sum(Vl_Imp_Debit) FROM [SPS_Fiscal].[dbo].[DIME_SaidasP2] WHERE [IdPeriodo] = @IdPeriodo) 
								- (SELECT Sum(Vl_Imp_Cred) FROM [SPS_Fiscal].[dbo].[DIME_EntradasP1] WHERE [IdPeriodo] = @IdPeriodo)
							)

-------------------------------------------------------------------------------------------------------------
PRINT ('[DIME_DadosQuadro11]')
DELETE FROM [SPS_Fiscal].[dbo].[DIME_DadosQuadro11] WHERE IdDeclaracao = @IdDeclaracao
/*
-- [DIME_DadosQuadro11]
*/
-------------------------------------------------------------------------------------------------------------
INSERT INTO [SPS_Fiscal].[dbo].[DIME_DadosQuadro11]
           ([NumeroItem]
           ,[IdDeclaracao]
           ,[Valor])
SELECT '010',@IdDeclaracao, 0.0 UNION ALL
SELECT '020',@IdDeclaracao, 0.0 UNION ALL
SELECT '030',@IdDeclaracao, 0.0 UNION ALL
SELECT '040',@IdDeclaracao, 0.0 UNION ALL
SELECT '050',@IdDeclaracao, 0.0 UNION ALL
SELECT '060',@IdDeclaracao, 0.0 UNION ALL
SELECT '065',@IdDeclaracao, 0.0 UNION ALL
SELECT '070',@IdDeclaracao, 0.0 UNION ALL
SELECT '073',@IdDeclaracao, 0.0 UNION ALL
SELECT '075',@IdDeclaracao, 0.0 UNION ALL
SELECT '080',@IdDeclaracao, 0.0 UNION ALL
SELECT '090',@IdDeclaracao, 0.0 UNION ALL
SELECT '100',@IdDeclaracao, 0.0 UNION ALL
SELECT '105',@IdDeclaracao, 0.0 UNION ALL
SELECT '110',@IdDeclaracao, 0.0 UNION ALL
SELECT '120',@IdDeclaracao, 0.0 UNION ALL
SELECT '125',@IdDeclaracao, 0.0 UNION ALL
SELECT '130',@IdDeclaracao, 0.0 UNION ALL
SELECT '140',@IdDeclaracao, 0.0 UNION ALL
SELECT '150',@IdDeclaracao, 0.0 UNION ALL
SELECT '155',@IdDeclaracao, 0.0 UNION ALL
SELECT '160',@IdDeclaracao, 0.0 UNION ALL
SELECT '170',@IdDeclaracao, 0.0 UNION ALL
SELECT '180',@IdDeclaracao, 0.0 UNION ALL
SELECT '190',@IdDeclaracao, 0.0 UNION ALL
SELECT '200',@IdDeclaracao, 0.0 UNION ALL
SELECT '998',@IdDeclaracao, 0.0 UNION ALL
SELECT '999',@IdDeclaracao, 0.0 

-------------------------------------------------------------------------------------------------------------

END", Support.Conectar());
                #endregion
                command.ExecuteNonQuery();
            }
            #endregion
        }

        public static void AtualizaDBVersao2()
        {

        }
    }
}
