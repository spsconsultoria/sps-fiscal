﻿using SAPbobsCOM;
using SAPbouiCOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SPS_Fiscal.Addon
{
    class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            try
            {
                #region VALIDAR PARÂMETROS
                //Validar Parâmetros
                if (!(args.Length > 0))
                {
                    System.Windows.Forms.MessageBox.Show("Parâmetro inválido");
                    System.Windows.Forms.Application.Exit();
                    Environment.Exit(0);
                }
                #endregion

                #region ABRIR CONEXÃO B1
                //Abrir conexão B1
                new Conexao();
                #endregion

                CriarMenu();

                CriaBanco();

                #region Eventos
                Conexao.uiApplication.MenuEvent += Events.MenuEvent;
                Conexao.uiApplication.ItemEvent += Events.ItemEvent;
                Conexao.uiApplication.AppEvent += Events.AppEvent;
                Conexao.uiApplication.ProgressBarEvent += Events.ProgressBarEvent;
                Conexao.uiApplication.FormDataEvent += Events.FormDataEvent;
                #endregion

                #region Tratamento para o add-on não perder conexão com a UI
                var addon = new Program();
                var threadCheckConnection = new System.Threading.Thread(Program.CheckConnection) { IsBackground = true };
                threadCheckConnection.Start();
                #endregion

                Conexao.uiApplication.SetStatusBarMessage("Add-on SPS_Fiscal iniciado.", BoMessageTime.bmt_Short, false);
                System.Windows.Forms.Application.Run();
            }
            catch (Exception ex)
            {
                if (ex.Message.IndexOf("(-10)  [131-183] -(-10)  [131-183] -") != -1)
                    System.Windows.Forms.MessageBox.Show(ex.Message);
            }
        }

        public static void CriarMenu()
        {
            try
            {
                CreationUserMenu menu;

                menu = new CreationUserMenu("SPS Fiscal", "1536", "mnuidime", SAPbouiCOM.BoMenuType.mt_POPUP, 7, "");
                menu.Add();
                menu = new CreationUserMenu("Conexão Banco de Dados", "mnuidime", "frmConBanco", SAPbouiCOM.BoMenuType.mt_STRING, 1, "");
                menu.Add();
                //menu = new CreationUserMenu("Criar Tabelas", "mnuidime", "frmTabelas", SAPbouiCOM.BoMenuType.mt_STRING, 2, "");
                //menu.Add();
                menu = new CreationUserMenu("DIME - SC", "mnuidime", "frmAdmDecDime", SAPbouiCOM.BoMenuType.mt_STRING, 3, "");
                menu.Add();
                
            }
            catch { throw; }

        }

        private static void CheckConnection()
        {
            while (true)
            {
                try
                {
                    Conexao.uiApplication.RemoveWindowsMessage(BoWindowsMessageType.bo_WM_TIMER, true);
                    bool testConnection;
                    SAPbouiCOM.Company app = Conexao.uiApplication.Company;
                    testConnection = (bool)(app == null);
                }
                catch
                {
                    Environment.Exit(0);
                }
                System.Threading.Thread.Sleep(10000);
            }
        }

        private static void CriaBanco()
        {
            bool CriaCampo = false;
            if (!ExisteTB("SPS_CON"))
            {
                AddUserTable("SPS_CON", "Tabela de Conexão com DB", SAPbobsCOM.BoUTBTableType.bott_NoObject);
                CriaCampo = true;
            }
            if (CriaCampo == true)
            {
                AddUserField("@SPS_CON", "User", "usuário SQL", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 20, null, null, null);
                AddUserField("@SPS_CON", "Pass", "Senha SQL", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 20, null, null, null);
                AddUserField("@SPS_CON", "Server", "Servidor SQL", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 20, null, null, null);
            }
        }

        public static void AddUserField(string NomeTabela, string NomeCampo, string DescCampo, SAPbobsCOM.BoFieldTypes Tipo, SAPbobsCOM.BoFldSubTypes SubTipo, Int16 Tamanho, string[,] valoresValidos, string valorDefault, string linkedTable)
        {
            int lErrCode;
            string sErrMsg = "";

            try
            {
                string sSquery = "SELECT [name] FROM syscolumns WHERE [name] = 'U_" + NomeCampo + " ' and id = (SELECT id FROM sysobjects WHERE type = 'U'AND [NAME] = '" + NomeTabela.Replace("[", "").Replace("]", "") + "')";
                object oResult = Conexao.ExecuteSqlScalar(sSquery);
                if (oResult != null) return;

                SAPbobsCOM.UserFieldsMD oUserField;
                oUserField = (SAPbobsCOM.UserFieldsMD)Conexao.diCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserFields);
                oUserField.TableName = NomeTabela.Replace("@", "").Replace("[", "").Replace("]", "").Trim();
                oUserField.Name = NomeCampo;
                oUserField.Description = DescCampo;
                oUserField.Type = Tipo;
                oUserField.SubType = SubTipo;
                oUserField.DefaultValue = valorDefault;
                if (!string.IsNullOrEmpty(linkedTable)) oUserField.LinkedTable = linkedTable;

                //adicionar valores válidos
                if (valoresValidos != null)
                {
                    Int32 qtd = valoresValidos.GetLength(0);
                    if (qtd > 0)
                    {
                        for (int i = 0; i < qtd; i++)
                        {
                            oUserField.ValidValues.Value = valoresValidos[i, 0];
                            oUserField.ValidValues.Description = valoresValidos[i, 1];
                            oUserField.ValidValues.Add();
                        }
                    }
                }

                if (Tamanho != 0)
                    oUserField.EditSize = Tamanho;

                try
                {
                    oUserField.Add();
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                    GC.Collect();
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(oUserField);
                    Conexao.diCompany.GetLastError(out lErrCode, out sErrMsg);
                    if (lErrCode != 0)
                    {
                        throw new Exception(sErrMsg);
                    }
                    oUserField = null;
                }
                catch (Exception)
                {
                    throw;
                }
                oUserField = null;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public static bool ExisteTB(string TBName)
        {
            SAPbobsCOM.UserTablesMD oUserTable;
            oUserTable = (SAPbobsCOM.UserTablesMD)Conexao.diCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserTables);
            bool ret = oUserTable.GetByKey(TBName);
            int errCode; string errMsg;
            Conexao.diCompany.GetLastError(out errCode, out errMsg);

            TBName = null;
            errMsg = null;
            System.Runtime.InteropServices.Marshal.ReleaseComObject(oUserTable);
            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();

            return (ret);
        }

        public static void AddUserTable(string NomeTB, string Desc, SAPbobsCOM.BoUTBTableType oTableType)
        {
            int lErrCode;
            string sErrMsg = "";


            SAPbobsCOM.UserTablesMD oUserTable;

            oUserTable = (SAPbobsCOM.UserTablesMD)Conexao.diCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserTables);

            try
            {
                oUserTable.TableName = NomeTB.Replace("@", "").Replace("[", "").Replace("]", "").Trim();
                oUserTable.TableDescription = Desc;
                oUserTable.TableType = oTableType;
                try
                {
                    oUserTable.Add();
                    Conexao.diCompany.GetLastError(out lErrCode, out sErrMsg);
                    if (lErrCode != 0)
                    {
                        throw new Exception(sErrMsg);
                    }

                    System.Runtime.InteropServices.Marshal.ReleaseComObject(oUserTable);
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                }
                catch (Exception)
                {
                    throw;
                }

            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
