﻿using SAPbouiCOM;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPS_Fiscal.Addon
{
    class frmfrmResApurCred:IForm
    {
        private ItemEvent _eventoItem;
        public frmfrmResApurCred(ItemEvent evento)
        {
            this._eventoItem = evento;
        }
        public bool ItemEvent()
        {
            try
            {
                switch (_eventoItem.EventType)
                {
                    case BoEventTypes.et_ALL_EVENTS:
                        break;
                    case BoEventTypes.et_B1I_SERVICE_COMPLETE:
                        break;
                    case BoEventTypes.et_CHOOSE_FROM_LIST:
                        break;
                    case BoEventTypes.et_CLICK:
                        break;
                    case BoEventTypes.et_COMBO_SELECT:
                        break;
                    case BoEventTypes.et_DATASOURCE_LOAD:
                        break;
                    case BoEventTypes.et_DOUBLE_CLICK:
                        break;
                    case BoEventTypes.et_Drag:
                        break;
                    case BoEventTypes.et_EDIT_REPORT:
                        break;
                    case BoEventTypes.et_FORMAT_SEARCH_COMPLETED:
                        break;
                    case BoEventTypes.et_FORM_ACTIVATE:
                        break;
                    case BoEventTypes.et_FORM_CLOSE:
                        break;
                    case BoEventTypes.et_FORM_DATA_ADD:
                        break;
                    case BoEventTypes.et_FORM_DATA_DELETE:
                        break;
                    case BoEventTypes.et_FORM_DATA_LOAD:
                        break;
                    case BoEventTypes.et_FORM_DATA_UPDATE:
                        break;
                    case BoEventTypes.et_FORM_DEACTIVATE:
                        break;
                    case BoEventTypes.et_FORM_DRAW:
                        break;
                    case BoEventTypes.et_FORM_KEY_DOWN:
                        break;
                    case BoEventTypes.et_FORM_LOAD:
                        break;
                    case BoEventTypes.et_FORM_MENU_HILIGHT:
                        break;
                    case BoEventTypes.et_FORM_RESIZE:
                        break;
                    case BoEventTypes.et_FORM_UNLOAD:
                        break;
                    case BoEventTypes.et_FORM_VISIBLE:
                        break;
                    case BoEventTypes.et_GOT_FOCUS:
                        break;
                    case BoEventTypes.et_GRID_SORT:
                        break;
                    case BoEventTypes.et_ITEM_PRESSED:
                        #region BOTÃO CADASTRAR
                        if (!_eventoItem.Before_Action && _eventoItem.ItemUID == "btnOk")
                        {
                            SAPbouiCOM.Form oForm = Conexao.uiApplication.Forms.Item(_eventoItem.FormUID);

                            Grid oGrid = (Grid)oForm.Items.Item("grd2").Specific;

                            if (oGrid.Rows.Count > 1)
                            {
                                for (int i = 0; i < oGrid.Rows.Count; i++)
                                {
                                    string NumeroItem = oForm.DataSources.DataTables.Item("grd2").GetValue("NumeroItem", i);

                                    try
                                    {
                                        string sql = string.Format(@"UPDATE [dbo].[DIME_DadosQuadro05] SET [Valor] = @Valor
                                                 WHERE [NumeroItem] = '{0}'", NumeroItem);
                                        SqlCommand command = new SqlCommand(sql, Support.Conectar());
                                        command.Parameters.AddWithValue("@Valor", Convert.ToDouble(oForm.DataSources.DataTables.Item("grd2").GetValue("Valor", i)));
                                        command.ExecuteNonQuery();
                                    }
                                    catch (Exception ex)
                                    {
                                        throw new Exception(ex.Message);
                                    }
                                }
                                oForm.Close();
                            }
                        }
                        #endregion
                        break;
                    case BoEventTypes.et_KEY_DOWN:
                        break;
                    case BoEventTypes.et_LOST_FOCUS:
                        break;
                    case BoEventTypes.et_MATRIX_COLLAPSE_PRESSED:
                        break;
                    case BoEventTypes.et_MATRIX_LINK_PRESSED:
                        break;
                    case BoEventTypes.et_MATRIX_LOAD:
                        break;
                    case BoEventTypes.et_MENU_CLICK:
                        break;
                    case BoEventTypes.et_PICKER_CLICKED:
                        break;
                    case BoEventTypes.et_PRINT:
                        break;
                    case BoEventTypes.et_PRINT_DATA:
                        break;
                    case BoEventTypes.et_PRINT_LAYOUT_KEY:
                        break;
                    case BoEventTypes.et_RIGHT_CLICK:
                        break;
                    case BoEventTypes.et_UDO_FORM_BUILD:
                        break;
                    case BoEventTypes.et_UDO_FORM_OPEN:
                        break;
                    case BoEventTypes.et_VALIDATE:
                        break;
                    default:
                        break;
                }
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region CARREGA GRID
        public static void CarregaGrid(ref SAPbouiCOM.Form oForm)
        {
            try
            {
                string sql = string.Format(@"
SELECT '1 - Transporte o saldo credor do mês anterior' AS 'Item'
,T0.[NumeroItem]
,T0.[Descricao]
,T1.[Valor]
INTO #TEMP
FROM [SPS_Fiscal].[dbo].[DIME_TabelaQuadro05] T0
INNER JOIN [SPS_Fiscal].[dbo].[DIME_DadosQuadro05] T1 ON T0.NumeroItem = T1.NumeroItem
WHERE T0.TipoItem = 1 AND T1.IdDeclaracao = {0}

UNION ALL

SELECT '2 - Créditos Gerais' AS 'Item'
,T0.[NumeroItem]
,T0.[Descricao]
,T1.[Valor]
FROM [SPS_Fiscal].[dbo].[DIME_TabelaQuadro05] T0
INNER JOIN [SPS_Fiscal].[dbo].[DIME_DadosQuadro05] T1 ON T0.NumeroItem = T1.NumeroItem
WHERE T0.TipoItem = 2 AND T1.IdDeclaracao = {0}

UNION ALL

SELECT '3 - Totalização' AS 'Item'
,T0.[NumeroItem]
,T0.[Descricao]
,T1.[Valor]
FROM [SPS_Fiscal].[dbo].[DIME_TabelaQuadro05] T0
INNER JOIN [SPS_Fiscal].[dbo].[DIME_DadosQuadro05] T1 ON T0.NumeroItem = T1.NumeroItem
WHERE T0.TipoItem = 3 AND T1.IdDeclaracao = {0}

declare @valor as decimal(18,2) = (select sum(Valor) from #TEMP)
select Item, NumeroItem AS 'Número', Descricao, Valor from #TEMP

UNION ALL

SELECT '4 - Totalização' AS 'Item'
, '990' AS 'Número'
, '(=) Subtotal de crédito => (transportar para o item 050 do quadro 09 - Cálculo do Imposto a Pagar ou Saldo Credor)' as 'Descricao'
, @valor as 'Valor'
order by NumeroItem asc
drop table #TEMP", frmfrmCadDeclarDime.IdDeclaracao);
                oForm.DataSources.DataTables.Item("grd").ExecuteQuery(sql);
                ((Grid)oForm.Items.Item("grd").Specific).CollapseLevel = 1;
                ((Grid)oForm.Items.Item("grd").Specific).AutoResizeColumns();
                ((Grid)oForm.Items.Item("grd").Specific).SelectionMode = BoMatrixSelect.ms_Auto;
                
                sql = string.Format(@"SELECT T0.[NumeroItem]
                                            ,T0.[Descricao]
	                                        ,T1.[Valor]
                                        FROM [SPS_Fiscal].[dbo].[DIME_TabelaQuadro05] T0
                                        INNER JOIN [SPS_Fiscal].[dbo].[DIME_DadosQuadro05] T1 ON T0.NumeroItem = T1.NumeroItem
                                        WHERE T0.NumeroItem in (030,040,045,050) AND T1.IdDeclaracao = {0}", frmfrmCadDeclarDime.IdDeclaracao);
                oForm.DataSources.DataTables.Item("grd2").ExecuteQuery(sql);
                ((Grid)oForm.Items.Item("grd2").Specific).AutoResizeColumns();
                ((Grid)oForm.Items.Item("grd2").Specific).Columns.Item("NumeroItem").Editable = false;
                ((Grid)oForm.Items.Item("grd2").Specific).Columns.Item("Descricao").Editable = false;
                ((Grid)oForm.Items.Item("grd2").Specific).Columns.Item("Valor").Editable = true;
                ((Grid)oForm.Items.Item("grd2").Specific).SelectionMode = BoMatrixSelect.ms_Auto;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region OUTROS EVENTOS
        public bool MenuEvent()
        {
            throw new NotImplementedException();
        }

        public bool FormDataEvent()
        {
            throw new NotImplementedException();
        }

        public bool AppEvent()
        {
            throw new NotImplementedException();
        }

        public bool PrintEvent()
        {
            throw new NotImplementedException();
        }

        public bool ProgressBarEvent()
        {
            throw new NotImplementedException();
        }

        public bool ReportDataEvent()
        {
            throw new NotImplementedException();
        }

        public bool RightClickEvent()
        {
            throw new NotImplementedException();
        }

        public bool StatusBarEvent()
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
