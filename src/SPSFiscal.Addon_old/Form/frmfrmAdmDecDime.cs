﻿using SAPbouiCOM;
using SAPbobsCOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Data.SqlClient;

namespace SPS_Fiscal.Addon
{
    class frmfrmAdmDecDime : IForm
    {

        private ItemEvent _eventoItem;
        public static SAPbouiCOM.Form oFormPrincipal;
        public frmfrmAdmDecDime(ItemEvent evento)
        {
            this._eventoItem = evento;
        }

        public bool ItemEvent()
        {
            try
            {
                switch (_eventoItem.EventType)
                {
                    case BoEventTypes.et_ALL_EVENTS:
                        break;
                    case BoEventTypes.et_B1I_SERVICE_COMPLETE:
                        break;
                    case BoEventTypes.et_CHOOSE_FROM_LIST:
                        break;
                    case BoEventTypes.et_CLICK:
                        break;
                    case BoEventTypes.et_COMBO_SELECT:
                        break;
                    case BoEventTypes.et_DATASOURCE_LOAD:
                        break;
                    case BoEventTypes.et_DOUBLE_CLICK:
                        break;
                    case BoEventTypes.et_Drag:
                        break;
                    case BoEventTypes.et_EDIT_REPORT:
                        break;
                    case BoEventTypes.et_FORMAT_SEARCH_COMPLETED:
                        break;
                    case BoEventTypes.et_FORM_ACTIVATE:
                        break;
                    case BoEventTypes.et_FORM_CLOSE:
                        break;
                    case BoEventTypes.et_FORM_DATA_ADD:
                        break;
                    case BoEventTypes.et_FORM_DATA_DELETE:
                        break;
                    case BoEventTypes.et_FORM_DATA_LOAD:
                        break;
                    case BoEventTypes.et_FORM_DATA_UPDATE:
                        break;
                    case BoEventTypes.et_FORM_DEACTIVATE:
                        break;
                    case BoEventTypes.et_FORM_DRAW:
                        break;
                    case BoEventTypes.et_FORM_KEY_DOWN:
                        break;
                    case BoEventTypes.et_FORM_LOAD:
                        break;
                    case BoEventTypes.et_FORM_MENU_HILIGHT:
                        break;
                    case BoEventTypes.et_FORM_RESIZE:
                        break;
                    case BoEventTypes.et_FORM_UNLOAD:
                        break;
                    case BoEventTypes.et_FORM_VISIBLE:
                        break;
                    case BoEventTypes.et_GOT_FOCUS:
                        break;
                    case BoEventTypes.et_GRID_SORT:
                        break;
                    case BoEventTypes.et_ITEM_PRESSED:
                        #region BOTÃO CONFIGURAÇÕES
                        if (!_eventoItem.Before_Action && _eventoItem.ItemUID == "btnConfig")
                        {
                            oFormPrincipal = Conexao.uiApplication.Forms.Item(_eventoItem.FormUID);

                            #region ABRIR TELA
                            SAPbouiCOM.Form oForm = null;
                            XmlDocument xmlDoc;
                            string appPath = System.Windows.Forms.Application.StartupPath;
                            if (!appPath.EndsWith(@"\")) appPath += @"\";
                            //carregar XML
                            xmlDoc = new XmlDocument();
                            xmlDoc.Load(appPath + @"\srf\frmConfigDime.srf");
                            string strXML = xmlDoc.InnerXml;

                            SAPbouiCOM.FormCreationParams oCreationParams = ((SAPbouiCOM.FormCreationParams)(Conexao.uiApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_FormCreationParams)));
                            oCreationParams.XmlData = strXML;

                            oForm = Conexao.uiApplication.Forms.AddEx(oCreationParams);

                            #endregion

                            frmfrmConfigDime.carregaGridContabilista(ref oForm);
                            frmfrmConfigDime.carregaGridContribuinte(ref oForm);
                        }
                        #endregion

                        #region BOTÃO NOVA DECLARAÇÃO
                        if (!_eventoItem.Before_Action && _eventoItem.ItemUID == "btnNDecl")
                        {
                            oFormPrincipal = Conexao.uiApplication.Forms.Item(_eventoItem.FormUID);

                            #region ABRIR TELA
                            SAPbouiCOM.Form oForm = null;
                            XmlDocument xmlDoc;
                            string appPath = System.Windows.Forms.Application.StartupPath;
                            if (!appPath.EndsWith(@"\")) appPath += @"\";
                            //carregar XML
                            xmlDoc = new XmlDocument();
                            xmlDoc.Load(appPath + @"\srf\frmCadDeclarDime.srf");
                            string strXML = xmlDoc.InnerXml;

                            SAPbouiCOM.FormCreationParams oCreationParams = ((SAPbouiCOM.FormCreationParams)(Conexao.uiApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_FormCreationParams)));
                            oCreationParams.XmlData = strXML;

                            oForm = Conexao.uiApplication.Forms.AddEx(oCreationParams);

                            #endregion

                            oForm.Freeze(true);
                            oForm.Items.Item("Item_1").Visible = false;
                            oForm.Items.Item("Item_41").Visible = false;
                            oForm.Items.Item("btnGerar").Visible = false;
                            oForm.Items.Item("btnRecar").Visible = false;

                            oForm.DataSources.UserDataSources.Item("porteEmp").Value = "1";
                            oForm.DataSources.UserDataSources.Item("apurCent").Value = "1";
                            oForm.DataSources.UserDataSources.Item("credPres").Value = "1";
                            oForm.DataSources.UserDataSources.Item("credFisc").Value = "1";

                            frmfrmCadDeclarDime.IdDeclaracao = "";
                            Support.CarregaContribuinte(ref oForm, false);
                            Support.CarregaComboPeriodo(ref oForm);
                            oForm.Freeze(false);

                        }
                        #endregion

                        #region BOTÃO CONSULTAR
                        if (!_eventoItem.Before_Action && _eventoItem.ItemUID == "btnConsult")
                        {
                            SAPbouiCOM.Form oForm = Conexao.uiApplication.Forms.Item(_eventoItem.FormUID);

                            if (string.IsNullOrEmpty(((ComboBox)oForm.Items.Item("contri").Specific).Selected.Description) ||
                                string.IsNullOrEmpty(oForm.DataSources.UserDataSources.Item("dtDe").Value) ||
                                string.IsNullOrEmpty(oForm.DataSources.UserDataSources.Item("dtAte").Value))
                            {
                                Conexao.uiApplication.SetStatusBarMessage("Preenchimento obrigatório de todos os campos para consulta", BoMessageTime.bmt_Short, true);
                                return true;
                            }

                            oForm.Freeze(true);
                            CarregaDados(ref oForm);
                            oForm.Freeze(false);
                            Conexao.uiApplication.SetStatusBarMessage("Consulta finalizada!", BoMessageTime.bmt_Short, false);
                        }
                        #endregion

                        #region BOTÃO EXCLUIR
                        if (!_eventoItem.Before_Action && _eventoItem.ItemUID == "btnRDecl")
                        {
                            SAPbouiCOM.Form oForm = Conexao.uiApplication.Forms.Item(_eventoItem.FormUID);

                            for (int i = 0; i < ((Grid)oForm.Items.Item("grd").Specific).Rows.Count; i++)
                            {
                                if (((Grid)oForm.Items.Item("grd").Specific).Rows.IsSelected(i))
                                {
                                    if (Conexao.uiApplication.MessageBox("Deseja realmente excluir?", 1, "Sim", "Não") == 1)
                                    {
                                        string Code = oForm.DataSources.DataTables.Item("grd").GetValue("#", i).ToString();
                                        Delete(ref oForm, Code);
                                    }
                                }
                            }
                            Conexao.uiApplication.SetStatusBarMessage("Declaração excluido com sucesso!", BoMessageTime.bmt_Short, false);
                            oForm.Freeze(true);
                            CarregaDados(ref oForm);
                            oForm.Freeze(false);
                        }
                        #endregion
                        break;
                    case BoEventTypes.et_KEY_DOWN:
                        break;
                    case BoEventTypes.et_LOST_FOCUS:
                        break;
                    case BoEventTypes.et_MATRIX_COLLAPSE_PRESSED:
                        break;
                    case BoEventTypes.et_MATRIX_LINK_PRESSED:
                        #region CLICK LINK GRID
                        if (!_eventoItem.BeforeAction && _eventoItem.ColUID == "#")
                        {
                            oFormPrincipal = Conexao.uiApplication.Forms.Item(_eventoItem.FormUID);

                            #region ABRIR TELA
                            SAPbouiCOM.Form oForm = null;
                            XmlDocument xmlDoc;
                            string appPath = System.Windows.Forms.Application.StartupPath;
                            if (!appPath.EndsWith(@"\")) appPath += @"\";
                            //carregar XML
                            xmlDoc = new XmlDocument();
                            xmlDoc.Load(appPath + @"\srf\frmCadDeclarDime.srf");
                            string strXML = xmlDoc.InnerXml;

                            SAPbouiCOM.FormCreationParams oCreationParams = ((SAPbouiCOM.FormCreationParams)(Conexao.uiApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_FormCreationParams)));
                            oCreationParams.XmlData = strXML;

                            oForm = Conexao.uiApplication.Forms.AddEx(oCreationParams);

                            #endregion

                            oForm.Freeze(true);

                            oForm.Items.Item("IE").Click();
                            oForm.Items.Item("contri").Enabled = false;
                            oForm.Items.Item("periodo").Enabled = false;

                            string IdDeclaracao = oFormPrincipal.DataSources.DataTables.Item("grd").GetValue("#", _eventoItem.Row).ToString();
                            frmfrmCadDeclarDime.CarregaGridQuadros(ref oForm, IdDeclaracao);
                            frmfrmCadDeclarDime.CarregaGridResumoAnual(ref oForm, IdDeclaracao);
                            frmfrmCadDeclarDime.IdDeclaracao = IdDeclaracao;
                            frmfrmCadDeclarDime.Periodo = oFormPrincipal.DataSources.DataTables.Item("grd").GetValue("Periodo da Declaração", _eventoItem.Row).ToString();

                            if (frmfrmCadDeclarDime.Periodo.Substring(5) == "06")
                                oForm.Items.Item("Item_1").Visible = true;
                            else
                                oForm.Items.Item("Item_1").Visible = false;

                            oForm.DataSources.UserDataSources.Item("contri").Value = oFormPrincipal.DataSources.DataTables.Item("grd").GetValue("Nome do Contribuinte", _eventoItem.Row).ToString();
                            oForm.DataSources.UserDataSources.Item("cnpj").Value = oFormPrincipal.DataSources.DataTables.Item("grd").GetValue("CNPJ do Contribuinte", _eventoItem.Row).ToString();
                            oForm.DataSources.UserDataSources.Item("IE").Value = oFormPrincipal.DataSources.DataTables.Item("grd").GetValue("NroInscricao", _eventoItem.Row).ToString();
                            oForm.DataSources.UserDataSources.Item("periodo").Value = oFormPrincipal.DataSources.DataTables.Item("grd").GetValue("Periodo da Declaração", _eventoItem.Row).ToString();
                            oForm.DataSources.UserDataSources.Item("tpDeclar").Value = oFormPrincipal.DataSources.DataTables.Item("grd").GetValue("Tipo da Declaração", _eventoItem.Row).ToString();
                            oForm.DataSources.UserDataSources.Item("regApur").Value = oFormPrincipal.DataSources.DataTables.Item("grd").GetValue("RegimeApuracao", _eventoItem.Row).ToString();
                            oForm.DataSources.UserDataSources.Item("ApurConso").Value = oFormPrincipal.DataSources.DataTables.Item("grd").GetValue("ApuracaoConsolidada", _eventoItem.Row).ToString();
                            oForm.DataSources.UserDataSources.Item("subsTrib").Value = oFormPrincipal.DataSources.DataTables.Item("grd").GetValue("SubstitutoTributario", _eventoItem.Row).ToString();
                            oForm.DataSources.UserDataSources.Item("temEscr").Value = oFormPrincipal.DataSources.DataTables.Item("grd").GetValue("TemEscritaContabil", _eventoItem.Row).ToString();
                            oForm.DataSources.UserDataSources.Item("qtdTrab").Value = oFormPrincipal.DataSources.DataTables.Item("grd").GetValue("QtdeTrabAtividade", _eventoItem.Row).ToString();
                            oForm.DataSources.UserDataSources.Item("nomeContab").Value = oFormPrincipal.DataSources.DataTables.Item("grd").GetValue("NomeContabilista", _eventoItem.Row).ToString();
                            oForm.DataSources.UserDataSources.Item("cpfContab").Value = oFormPrincipal.DataSources.DataTables.Item("grd").GetValue("ContabilistaCPF", _eventoItem.Row).ToString();
                            oForm.DataSources.UserDataSources.Item("porteEmp").Value = oFormPrincipal.DataSources.DataTables.Item("grd").GetValue("PorteEmpresa", _eventoItem.Row).ToString();
                            oForm.DataSources.UserDataSources.Item("apurCent").Value = oFormPrincipal.DataSources.DataTables.Item("grd").GetValue("ApuracaoCentralizada", _eventoItem.Row).ToString();
                            oForm.DataSources.UserDataSources.Item("transfCred").Value = oFormPrincipal.DataSources.DataTables.Item("grd").GetValue("TransCredPeriodo", _eventoItem.Row).ToString();
                            oForm.DataSources.UserDataSources.Item("credPres").Value = oFormPrincipal.DataSources.DataTables.Item("grd").GetValue("TemCreditosPresumido", _eventoItem.Row).ToString();
                            oForm.DataSources.UserDataSources.Item("credFisc").Value = oFormPrincipal.DataSources.DataTables.Item("grd").GetValue("TemCredIncentFiscais", _eventoItem.Row).ToString();

                            Support.CarregaContribuinte(ref oForm, false);
                            Support.CarregaComboPeriodo(ref oForm);
                            oForm.Freeze(false);
                        }
                        #endregion
                        break;
                    case BoEventTypes.et_MATRIX_LOAD:
                        break;
                    case BoEventTypes.et_MENU_CLICK:
                        break;
                    case BoEventTypes.et_PICKER_CLICKED:
                        break;
                    case BoEventTypes.et_PRINT:
                        break;
                    case BoEventTypes.et_PRINT_DATA:
                        break;
                    case BoEventTypes.et_PRINT_LAYOUT_KEY:
                        break;
                    case BoEventTypes.et_RIGHT_CLICK:
                        break;
                    case BoEventTypes.et_UDO_FORM_BUILD:
                        break;
                    case BoEventTypes.et_UDO_FORM_OPEN:
                        break;
                    case BoEventTypes.et_VALIDATE:
                        break;
                    default:
                        break;
                }
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region DELETE
        private void Delete(ref SAPbouiCOM.Form oForm, string Code)
        {
            try
            {
                string sql = string.Format("UPDATE [DIME_Declaracao] SET [Excluido] = @Excluido WHERE [IdDeclaracao] = {0}", Code);
                SqlCommand command = new SqlCommand(sql, Support.Conectar());
                command.Parameters.AddWithValue("@Excluido", "S");
                command.ExecuteNonQuery();
            }
            catch (SqlException erro)
            {
                throw new Exception(erro.Message);
            }
            finally
            {
                Support.Conectar().Close();
            }
        }
        #endregion

        #region CARREGAGRID ADM
        private static void CarregaDados(ref SAPbouiCOM.Form oForm)
        {
            string contribuinte = ((ComboBox)oForm.Items.Item("contri").Specific).Selected.Description;
            string dtInicio = oForm.DataSources.UserDataSources.Item("dtDe").Value;
            string dtFim = oForm.DataSources.UserDataSources.Item("dtAte").Value;
            Grid oGrid = (Grid)oForm.Items.Item("grd").Specific;
            try
            {
                string sql = string.Format(@"SELECT T0.[IdDeclaracao] as '#'
                                                        ,[DtInicio]
                                                        ,[DtTermino]
                                                        ,[NomeContabilista]
                                                        ,[ContabilistaCPF]
                                                        ,[NomeContribuinte] as 'Nome do Contribuinte'
	                                                    --,T1.PeriodoReferencia AS 'Periodo da Declaração'
                                                        ,[NroInscricao]
                                                        ,[ContribuinteCNPJ] as 'CNPJ do Contribuinte'
                                                        ,T0.[PeriodoReferencia]  AS 'Periodo da Declaração'
                                                        ,[TipoDeclaracao] as 'Tipo da Declaração'
                                                        ,[RegimeApuracao]
                                                        ,[PorteEmpresa]
                                                        ,[ApuracaoConsolidada]
                                                        ,[ApuracaoCentralizada]
                                                        ,[TransCredPeriodo]
                                                        ,[TemCreditosPresumido]
                                                        ,[TemCredIncentFiscais]
                                                        ,[Movimento]
                                                        ,[SubstitutoTributario]
                                                        ,[TemEscritaContabil]
                                                        ,[QtdeTrabAtividade]
                                                        ,[Excluido]
                                                    FROM [SPS_Fiscal].[dbo].[DIME_Declaracao] T0
                                                    --INNER JOIN [SPS_Fiscal].[dbo].[DIME_PeriodoApuracao] T1 ON T1.IdDeclaracao = T0.IdDeclaracao
                                                    WHERE [NomeContribuinte] = '{0}' AND [Excluido] = 'N' AND 
                                                    [DtInicio] BETWEEN '{1}' AND  '{2}' ", contribuinte, Convert.ToDateTime(dtInicio).ToString("yyyyMMdd"), Convert.ToDateTime(dtFim).ToString("yyyyMMdd"));
                oForm.DataSources.DataTables.Item("grd").ExecuteQuery(sql);
                oGrid.Columns.Item("DtInicio").Visible = false;
                oGrid.Columns.Item("DtTermino").Visible = false;
                oGrid.Columns.Item("NomeContabilista").Visible = false;
                oGrid.Columns.Item("ContabilistaCPF").Visible = false;
                oGrid.Columns.Item("NroInscricao").Visible = false;
                //oGrid.Columns.Item("PeriodoReferencia").Visible = false;
                oGrid.Columns.Item("RegimeApuracao").Visible = false;
                oGrid.Columns.Item("PorteEmpresa").Visible = false;
                oGrid.Columns.Item("ApuracaoConsolidada").Visible = false;
                oGrid.Columns.Item("ApuracaoCentralizada").Visible = false;
                oGrid.Columns.Item("TransCredPeriodo").Visible = false;
                oGrid.Columns.Item("TemCreditosPresumido").Visible = false;
                oGrid.Columns.Item("TemCredIncentFiscais").Visible = false;
                oGrid.Columns.Item("Movimento").Visible = false;
                oGrid.Columns.Item("SubstitutoTributario").Visible = false;
                oGrid.Columns.Item("TemEscritaContabil").Visible = false;
                oGrid.Columns.Item("QtdeTrabAtividade").Visible = false;
                oGrid.Columns.Item("Excluido").Visible = false;
                (((SAPbouiCOM.EditTextColumn)(((Grid)oForm.Items.Item("grd").Specific).Columns.Item("#")))).LinkedObjectType = "BOY_IPT";
                ((Grid)oForm.Items.Item("grd").Specific).AutoResizeColumns();
                ((Grid)oForm.Items.Item("grd").Specific).SelectionMode = BoMatrixSelect.ms_Auto;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region OUTROS EVENTOS
        public bool MenuEvent()
        {
            throw new NotImplementedException();
        }

        public bool FormDataEvent()
        {
            throw new NotImplementedException();
        }

        public bool AppEvent()
        {
            throw new NotImplementedException();
        }

        public bool PrintEvent()
        {
            throw new NotImplementedException();
        }

        public bool ProgressBarEvent()
        {
            throw new NotImplementedException();
        }

        public bool ReportDataEvent()
        {
            throw new NotImplementedException();
        }

        public bool RightClickEvent()
        {
            throw new NotImplementedException();
        }

        public bool StatusBarEvent()
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
