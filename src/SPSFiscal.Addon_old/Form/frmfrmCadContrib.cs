﻿using SAPbouiCOM;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPS_Fiscal.Addon
{
    class frmfrmCadContrib : IForm
    {
        private ItemEvent _eventoItem;
        public frmfrmCadContrib(ItemEvent evento)
        {
            this._eventoItem = evento;
        }

        public static string Code;

        public bool ItemEvent()
        {
            try
            {
                switch (_eventoItem.EventType)
                {
                    case BoEventTypes.et_ALL_EVENTS:
                        break;
                    case BoEventTypes.et_B1I_SERVICE_COMPLETE:
                        break;
                    case BoEventTypes.et_CHOOSE_FROM_LIST:
                        break;
                    case BoEventTypes.et_CLICK:
                        break;
                    case BoEventTypes.et_COMBO_SELECT:
                        #region COMBO FILIAL
                        if (!_eventoItem.Before_Action && _eventoItem.ItemUID == "cbFilial")
                        {
                            SAPbouiCOM.Form oForm = Conexao.uiApplication.Forms.Item(_eventoItem.FormUID);

                            CarregaCNPJ(ref oForm);
                        }
                        #endregion
                        break;
                    case BoEventTypes.et_DATASOURCE_LOAD:
                        break;
                    case BoEventTypes.et_DOUBLE_CLICK:
                        break;
                    case BoEventTypes.et_Drag:
                        break;
                    case BoEventTypes.et_EDIT_REPORT:
                        break;
                    case BoEventTypes.et_FORMAT_SEARCH_COMPLETED:
                        break;
                    case BoEventTypes.et_FORM_ACTIVATE:
                        break;
                    case BoEventTypes.et_FORM_CLOSE:
                        break;
                    case BoEventTypes.et_FORM_DATA_ADD:
                        break;
                    case BoEventTypes.et_FORM_DATA_DELETE:
                        break;
                    case BoEventTypes.et_FORM_DATA_LOAD:
                        break;
                    case BoEventTypes.et_FORM_DATA_UPDATE:
                        break;
                    case BoEventTypes.et_FORM_DEACTIVATE:
                        break;
                    case BoEventTypes.et_FORM_DRAW:
                        break;
                    case BoEventTypes.et_FORM_KEY_DOWN:
                        break;
                    case BoEventTypes.et_FORM_LOAD:
                        break;
                    case BoEventTypes.et_FORM_MENU_HILIGHT:
                        break;
                    case BoEventTypes.et_FORM_RESIZE:
                        break;
                    case BoEventTypes.et_FORM_UNLOAD:
                        break;
                    case BoEventTypes.et_FORM_VISIBLE:
                        break;
                    case BoEventTypes.et_GOT_FOCUS:
                        break;
                    case BoEventTypes.et_GRID_SORT:
                        break;
                    case BoEventTypes.et_ITEM_PRESSED:
                        #region BOTÃO SALVAR
                        if (!_eventoItem.Before_Action && _eventoItem.ItemUID == "btnSave")
                        {
                            SAPbouiCOM.Form oForm = Conexao.uiApplication.Forms.Item(_eventoItem.FormUID);
                            try
                            {
                                if (string.IsNullOrEmpty(Code))
                                {
                                    string sql = string.Format("SELECT [ErpIdEmpresa] FROM [SPS_Fiscal].[dbo].[DIME_Contribuinte] WHERE [ErpIdEmpresa] = '{0}' AND IdContabilista = '{1}' AND [Excluido] = 'N'", oForm.DataSources.UserDataSources.Item("cbFilial").Value, oForm.DataSources.UserDataSources.Item("contab").Value);
                                    string filial = Convert.ToString(Conexao.ExecuteSqlScalar(sql));

                                    if (string.IsNullOrEmpty(filial))
                                    {
                                        Save(ref oForm, oForm.DataSources.UserDataSources.Item("cbFilial").Value);
                                    }
                                    else
                                    {
                                        Conexao.uiApplication.SetStatusBarMessage("Filial já cadastrada", BoMessageTime.bmt_Short, true);
                                        return true;

                                    }
                                }
                                else
                                {
                                    frmfrmConfigDime.oFormPrincipal.Freeze(true);
                                    Update(ref oForm);
                                    frmfrmConfigDime.oFormPrincipal.Freeze(true);
                                }
                            }
                            catch (Exception ex)
                            {
                                throw new Exception(ex.Message);
                            }
                            if (string.IsNullOrEmpty(Code))
                                Conexao.uiApplication.SetStatusBarMessage("Contribuinte Salvo com Sucesso!", BoMessageTime.bmt_Short, false);
                            else
                                Conexao.uiApplication.SetStatusBarMessage("Contribuinte Alterado com Sucesso!", BoMessageTime.bmt_Short, false);
                            frmfrmConfigDime.carregaGridContribuinte(ref frmfrmConfigDime.oFormPrincipal);
                            oForm.Close();
                        }
                        #endregion

                        #region BOTÃO EXCLUIR
                        if (!_eventoItem.Before_Action && _eventoItem.ItemUID == "btnDelete")
                        {
                            SAPbouiCOM.Form oForm = Conexao.uiApplication.Forms.Item(_eventoItem.FormUID);

                            if (Conexao.uiApplication.MessageBox("Deseja realmente excluir?", 1, "Sim", "Não") == 1)
                            {
                                try
                                {
                                    Delete(ref oForm);
                                    Conexao.uiApplication.SetStatusBarMessage("Contribuinte Excluido com Sucesso!", BoMessageTime.bmt_Short, false);
                                    frmfrmConfigDime.carregaGridContribuinte(ref frmfrmConfigDime.oFormPrincipal);
                                    oForm.Close();
                                }
                                catch (Exception ex)
                                {
                                    throw new Exception(ex.Message);
                                }
                            }
                        }
                        #endregion
                        break;
                    case BoEventTypes.et_KEY_DOWN:
                        break;
                    case BoEventTypes.et_LOST_FOCUS:
                        break;
                    case BoEventTypes.et_MATRIX_COLLAPSE_PRESSED:
                        break;
                    case BoEventTypes.et_MATRIX_LINK_PRESSED:
                        break;
                    case BoEventTypes.et_MATRIX_LOAD:
                        break;
                    case BoEventTypes.et_MENU_CLICK:
                        break;
                    case BoEventTypes.et_PICKER_CLICKED:
                        break;
                    case BoEventTypes.et_PRINT:
                        break;
                    case BoEventTypes.et_PRINT_DATA:
                        break;
                    case BoEventTypes.et_PRINT_LAYOUT_KEY:
                        break;
                    case BoEventTypes.et_RIGHT_CLICK:
                        break;
                    case BoEventTypes.et_UDO_FORM_BUILD:
                        break;
                    case BoEventTypes.et_UDO_FORM_OPEN:
                        break;
                    case BoEventTypes.et_VALIDATE:
                        break;
                    default:
                        break;
                }
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region CARREGA CNPJ
        public static void CarregaCNPJ(ref SAPbouiCOM.Form oForm)
        {
            string sql = string.Format("select TaxIdNum, TaxIdNum2, bplName from OBPL where BPLId = '{0}'", oForm.DataSources.UserDataSources.Item("cbFilial").Value);
            System.Data.DataTable dt = Conexao.ExecuteSqlDataTable(sql);
            oForm.DataSources.UserDataSources.Item("cnpj").Value = dt.Rows[0]["TaxIdNum"].ToString();
            oForm.DataSources.UserDataSources.Item("razao").Value = dt.Rows[0]["bplName"].ToString();
            oForm.DataSources.UserDataSources.Item("IE").Value = dt.Rows[0]["TaxIdNum2"].ToString();
        }
        #endregion

        #region SALVAR
        private void Save(ref SAPbouiCOM.Form oForm, string bplid)
        {
            string DB = Conexao.diCompany.CompanyDB.ToString();
            try
            {
                int qtdTrab = 0;

                if (oForm.DataSources.UserDataSources.Item("qtdTrab").Value == "")
                {
                    qtdTrab = 0;
                }
                else
                {
                    qtdTrab = Convert.ToInt32(oForm.DataSources.UserDataSources.Item("qtdTrab").Value);
                }

                SqlCommand command = new SqlCommand(@"INSERT INTO [DIME_Contribuinte]([IdContabilista]
                                                                                  ,[NomeRazaoSocial]
                                                                                  ,[CNPJ]
                                                                                  ,[NroInscricao]
                                                                                  ,[TipoDeclaracao]
                                                                                  ,[RegimeApuracao]
                                                                                  ,[ApuracaoConsolidada]
                                                                                  ,[SubstitutoTributario]
                                                                                  ,[TemEscritaContabil]
                                                                                  ,[QtdeTrabAtividade]  
                                                                                  ,[ErpIdEmpresa]
                                                                                  ,[ErpBaseDados]
                                                                                  ,[Excluido]  
                                                                            ) VALUES (@IdContabilista
                                                                                  ,@NomeRazaoSocial
                                                                                  ,@CNPJ
                                                                                  ,@NroInscricao
                                                                                  ,@TipoDeclaracao
                                                                                  ,@RegimeApuracao
                                                                                  ,@ApuracaoConsolidada
                                                                                  ,@SubstitutoTributario
                                                                                  ,@TemEscritaContabil
                                                                                  ,@QtdeTrabAtividade
                                                                                  ,@ErpIdEmpresa
                                                                                  ,@ErpBaseDados
                                                                                  ,'N')", Support.Conectar());
                command.Parameters.AddWithValue("@IdContabilista", oForm.DataSources.UserDataSources.Item("contab").Value);
                command.Parameters.AddWithValue("@NomeRazaoSocial", oForm.DataSources.UserDataSources.Item("razao").Value.Substring(0, 50));
                command.Parameters.AddWithValue("@CNPJ", oForm.DataSources.UserDataSources.Item("cnpj").Value);
                command.Parameters.AddWithValue("@NroInscricao", oForm.DataSources.UserDataSources.Item("IE").Value);
                command.Parameters.AddWithValue("@TipoDeclaracao", oForm.DataSources.UserDataSources.Item("tipoDec").Value);
                command.Parameters.AddWithValue("@RegimeApuracao", oForm.DataSources.UserDataSources.Item("regimeApur").Value);
                command.Parameters.AddWithValue("@ApuracaoConsolidada", oForm.DataSources.UserDataSources.Item("ApurCon").Value);
                command.Parameters.AddWithValue("@SubstitutoTributario", oForm.DataSources.UserDataSources.Item("subTrib").Value);
                command.Parameters.AddWithValue("@TemEscritaContabil", oForm.DataSources.UserDataSources.Item("TemEscr").Value);
                command.Parameters.AddWithValue("@QtdeTrabAtividade", qtdTrab);
                command.Parameters.AddWithValue("@ErpIdEmpresa", bplid);
                command.Parameters.AddWithValue("@ErpBaseDados", DB);
                command.ExecuteNonQuery();
            }
            catch (SqlException erro)
            {
                throw new Exception(erro.Message);
            }
            finally
            {
                Support.Conectar().Close();
            }

        }
        #endregion

        #region ALTERAR
        private void Update(ref SAPbouiCOM.Form oForm)
        {
            try
            {
                string sql = string.Format(@"UPDATE [DIME_Contribuinte] SET [IdContabilista] = @IdContabilista
                                                                            ,[NomeRazaoSocial] = @NomeRazaoSocial
                                                                            ,[CNPJ] = @CNPJ
                                                                            ,[NroInscricao] = @NroInscricao
                                                                            ,[TipoDeclaracao] = @TipoDeclaracao
                                                                            ,[RegimeApuracao] = @RegimeApuracao
                                                                            ,[ApuracaoConsolidada] = @ApuracaoConsolidada
                                                                            ,[SubstitutoTributario] = @SubstitutoTributario
                                                                            ,[TemEscritaContabil] = @TemEscritaContabil
                                                                            ,[QtdeTrabAtividade] = @QtdeTrabAtividade
                                                                            WHERE [IdContabilista] = {0}", Code);
                SqlCommand command = new SqlCommand(sql, Support.Conectar());
                command.Parameters.AddWithValue("@IdContabilista", oForm.DataSources.UserDataSources.Item("contab").Value);
                command.Parameters.AddWithValue("@NomeRazaoSocial", oForm.DataSources.UserDataSources.Item("razao").Value);
                command.Parameters.AddWithValue("@CNPJ", oForm.DataSources.UserDataSources.Item("cnpj").Value);
                command.Parameters.AddWithValue("@NroInscricao", oForm.DataSources.UserDataSources.Item("IE").Value);
                command.Parameters.AddWithValue("@TipoDeclaracao", oForm.DataSources.UserDataSources.Item("tipoDec").Value);
                command.Parameters.AddWithValue("@RegimeApuracao", oForm.DataSources.UserDataSources.Item("regimeApur").Value);
                command.Parameters.AddWithValue("@ApuracaoConsolidada", oForm.DataSources.UserDataSources.Item("ApurCon").Value);
                command.Parameters.AddWithValue("@SubstitutoTributario", oForm.DataSources.UserDataSources.Item("subTrib").Value);
                command.Parameters.AddWithValue("@TemEscritaContabil", oForm.DataSources.UserDataSources.Item("TemEscr").Value);
                command.Parameters.AddWithValue("@QtdeTrabAtividade", oForm.DataSources.UserDataSources.Item("qtdTrab").Value);
                command.ExecuteNonQuery();
                command.ExecuteNonQuery();
            }
            catch (SqlException erro)
            {
                throw new Exception(erro.Message);
            }
            finally
            {
                Support.Conectar().Close();
            }
        }
        #endregion

        #region DELETE
        private void Delete(ref SAPbouiCOM.Form oForm)
        {
            try
            {
                string sql = string.Format("UPDATE [DIME_Contribuinte] SET [Excluido] = 'S' WHERE [IdContribuinte] = {0}", Code);
                SqlCommand command = new SqlCommand(sql, Support.Conectar());
                command.ExecuteNonQuery();
            }
            catch (SqlException erro)
            {
                throw new Exception(erro.Message);
            }
            finally
            {
                Support.Conectar().Close();
            }
        }
        #endregion

        #region OUTROS EVENTOS
        public bool MenuEvent()
        {
            throw new NotImplementedException();
        }

        public bool FormDataEvent()
        {
            throw new NotImplementedException();
        }

        public bool AppEvent()
        {
            throw new NotImplementedException();
        }

        public bool PrintEvent()
        {
            throw new NotImplementedException();
        }

        public bool ProgressBarEvent()
        {
            throw new NotImplementedException();
        }

        public bool ReportDataEvent()
        {
            throw new NotImplementedException();
        }

        public bool RightClickEvent()
        {
            throw new NotImplementedException();
        }

        public bool StatusBarEvent()
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
