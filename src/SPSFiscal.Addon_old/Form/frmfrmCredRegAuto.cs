﻿using SAPbouiCOM;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace SPS_Fiscal.Addon
{
    class frmfrmCredRegAuto:IForm
    {
        public static SAPbouiCOM.Form oFormPrincipal;
        private ItemEvent _eventoItem;
        public frmfrmCredRegAuto(ItemEvent evento)
        {
            this._eventoItem = evento;
        }

        public bool ItemEvent()
        {
            try
            {
                switch (_eventoItem.EventType)
                {
                    case BoEventTypes.et_ALL_EVENTS:
                        break;
                    case BoEventTypes.et_B1I_SERVICE_COMPLETE:
                        break;
                    case BoEventTypes.et_CHOOSE_FROM_LIST:
                        break;
                    case BoEventTypes.et_CLICK:
                        break;
                    case BoEventTypes.et_COMBO_SELECT:
                        break;
                    case BoEventTypes.et_DATASOURCE_LOAD:
                        break;
                    case BoEventTypes.et_DOUBLE_CLICK:
                        break;
                    case BoEventTypes.et_Drag:
                        break;
                    case BoEventTypes.et_EDIT_REPORT:
                        break;
                    case BoEventTypes.et_FORMAT_SEARCH_COMPLETED:
                        break;
                    case BoEventTypes.et_FORM_ACTIVATE:
                        break;
                    case BoEventTypes.et_FORM_CLOSE:
                        break;
                    case BoEventTypes.et_FORM_DATA_ADD:
                        break;
                    case BoEventTypes.et_FORM_DATA_DELETE:
                        break;
                    case BoEventTypes.et_FORM_DATA_LOAD:
                        break;
                    case BoEventTypes.et_FORM_DATA_UPDATE:
                        break;
                    case BoEventTypes.et_FORM_DEACTIVATE:
                        break;
                    case BoEventTypes.et_FORM_DRAW:
                        break;
                    case BoEventTypes.et_FORM_KEY_DOWN:
                        break;
                    case BoEventTypes.et_FORM_LOAD:
                        break;
                    case BoEventTypes.et_FORM_MENU_HILIGHT:
                        break;
                    case BoEventTypes.et_FORM_RESIZE:
                        break;
                    case BoEventTypes.et_FORM_UNLOAD:
                        break;
                    case BoEventTypes.et_FORM_VISIBLE:
                        break;
                    case BoEventTypes.et_GOT_FOCUS:
                        break;
                    case BoEventTypes.et_GRID_SORT:
                        break;
                    case BoEventTypes.et_ITEM_PRESSED:
                        #region BOTÃO CADASTRAR
                        if (!_eventoItem.Before_Action && _eventoItem.ItemUID == "btnCad")
                        {
                            oFormPrincipal = Conexao.uiApplication.Forms.Item(_eventoItem.FormUID);

                            #region ABRIR TELA
                            SAPbouiCOM.Form oForm = null;
                            XmlDocument xmlDoc;
                            string appPath = System.Windows.Forms.Application.StartupPath;
                            if (!appPath.EndsWith(@"\")) appPath += @"\";
                            //carregar XML
                            xmlDoc = new XmlDocument();
                            xmlDoc.Load(appPath + @"\srf\frmLancCred.srf");
                            string strXML = xmlDoc.InnerXml;

                            SAPbouiCOM.FormCreationParams oCreationParams = ((SAPbouiCOM.FormCreationParams)(Conexao.uiApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_FormCreationParams)));
                            oCreationParams.XmlData = strXML;

                            oForm = Conexao.uiApplication.Forms.AddEx(oCreationParams);
                            #endregion

                            frmfrmLancCred.Id = string.Empty;
                        }
                        #endregion

                        #region BOTÃO EXCLUIR
                        if (!_eventoItem.Before_Action && _eventoItem.ItemUID == "btnDelete")
                        {
                            SAPbouiCOM.Form oForm = Conexao.uiApplication.Forms.Item(_eventoItem.FormUID);

                            Grid oGrid = (Grid)oForm.Items.Item("grd").Specific;

                            string code = string.Empty;

                            bool confirmacao = false;
                            for (int i = 0; i < oGrid.Rows.Count; i++)
                            {
                                if (oGrid.Rows.IsSelected(i))
                                {
                                    code = oForm.DataSources.DataTables.Item("grd").GetValue("#", i).ToString();
                                    if (Conexao.uiApplication.MessageBox("Deseja realmente excluir?", 1, "Sim", "Não") == 1 && confirmacao == false)
                                    {
                                        Delete(ref oForm, code);
                                        confirmacao = true;
                                    }
                                    else
                                    {
                                        return true;
                                    }
                                }
                            }
                            Conexao.uiApplication.SetStatusBarMessage("Exclusão realizada com sucesso!", BoMessageTime.bmt_Short, false);

                            oForm.Freeze(true);
                            CarregaGrid(ref oForm);
                            oForm.Freeze(false);
                        }
                        #endregion
                        break;
                    case BoEventTypes.et_KEY_DOWN:
                        break;
                    case BoEventTypes.et_LOST_FOCUS:
                        break;
                    case BoEventTypes.et_MATRIX_COLLAPSE_PRESSED:
                        break;
                    case BoEventTypes.et_MATRIX_LINK_PRESSED:
                        #region LINK QUADRO
                        if (!_eventoItem.BeforeAction && _eventoItem.ColUID == "#")
                        {
                            oFormPrincipal = Conexao.uiApplication.Forms.Item(_eventoItem.FormUID);

                            Grid oGrid = (Grid)oFormPrincipal.Items.Item("grd").Specific;

                            #region ABRIR TELA
                            SAPbouiCOM.Form oForm = null;
                            XmlDocument xmlDoc;
                            string appPath = System.Windows.Forms.Application.StartupPath;
                            if (!appPath.EndsWith(@"\")) appPath += @"\";
                            //carregar XML
                            xmlDoc = new XmlDocument();
                            xmlDoc.Load(appPath + @"\srf\frmLancCred.srf");
                            string strXML = xmlDoc.InnerXml;

                            SAPbouiCOM.FormCreationParams oCreationParams = ((SAPbouiCOM.FormCreationParams)(Conexao.uiApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_FormCreationParams)));
                            oCreationParams.XmlData = strXML;

                            oForm = Conexao.uiApplication.Forms.AddEx(oCreationParams);
                            #endregion

                            frmfrmLancCred.Id = oFormPrincipal.DataSources.DataTables.Item("grd").GetValue("#", _eventoItem.Row).ToString();

                            oForm.Freeze(true);
                            oForm.DataSources.UserDataSources.Item("ident").Value = oFormPrincipal.DataSources.DataTables.Item("grd").GetValue("Identif. do Regime ou da Autoriz. Especial", _eventoItem.Row);
                            oForm.DataSources.UserDataSources.Item("valor").Value = oFormPrincipal.DataSources.DataTables.Item("grd").GetValue("Valor", _eventoItem.Row).ToString();
                            oForm.DataSources.UserDataSources.Item("origem").Value = oFormPrincipal.DataSources.DataTables.Item("grd").GetValue("Origem de Crédito", _eventoItem.Row);
                            oForm.Freeze(false);
                        }
                        #endregion
                        break;
                    case BoEventTypes.et_MATRIX_LOAD:
                        break;
                    case BoEventTypes.et_MENU_CLICK:
                        break;
                    case BoEventTypes.et_PICKER_CLICKED:
                        break;
                    case BoEventTypes.et_PRINT:
                        break;
                    case BoEventTypes.et_PRINT_DATA:
                        break;
                    case BoEventTypes.et_PRINT_LAYOUT_KEY:
                        break;
                    case BoEventTypes.et_RIGHT_CLICK:
                        break;
                    case BoEventTypes.et_UDO_FORM_BUILD:
                        break;
                    case BoEventTypes.et_UDO_FORM_OPEN:
                        break;
                    case BoEventTypes.et_VALIDATE:
                        break;
                    default:
                        break;
                }
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region CARREGA GRID
        public static void CarregaGrid(ref SAPbouiCOM.Form oForm)
        {
            try
            {
               string sql = string.Format(@"SELECT [IdLancamento] as '#'
                                                  ,[IdDeclaracao]
                                                  ,[Seq_Reg] as 'Seq.'
                                                  ,[Cod_Identif] as 'Identif. do Regime ou da Autoriz. Especial'
                                                  ,[Vl_Cred_Apur] as 'Valor'
                                                  ,CASE [Ind_Origem] 
		                                                WHEN 1 
		                                                THEN 'AUC - Cred.Receb.transf/Compens.Sald.Dev.Prop.' 
		                                                WHEN 14
		                                                THEN 'Demonstr. de Créd. Informado Previamente - DCIP'
		                                                WHEN 16
		                                                THEN 'DCIP Créd. Imposto Retido'
		                                                WHEN 17
		                                                THEN 'DCIP Créd. Transferível Relativo à Exportação'
		                                                WHEN 18
		                                                THEN 'DCIP Créd. Transferível à Saída Insenta'
		                                                WHEN 19
		                                                THEN 'DCIP Créd. Transferível Relativo à Saída Isenta (Diferidas)'
	                                                END as 'Origem de Crédito'
                                                  FROM [SPS_Fiscal].[dbo].[DIME_DadosQuadro46]
                                                  WHERE IdDeclaracao = {0}", frmfrmCadDeclarDime.IdDeclaracao);
                oForm.DataSources.DataTables.Item("grd").ExecuteQuery(sql);
                ((Grid)oForm.Items.Item("grd").Specific).Columns.Item(1).Visible = false;
                (((SAPbouiCOM.EditTextColumn)(((Grid)oForm.Items.Item("grd").Specific).Columns.Item("#")))).LinkedObjectType = "BOY_IPT";
                ((Grid)oForm.Items.Item("grd").Specific).AutoResizeColumns();
                ((Grid)oForm.Items.Item("grd").Specific).SelectionMode = BoMatrixSelect.ms_Auto;
               
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region DELETE
        public static void Delete(ref SAPbouiCOM.Form oForm, string code)
        {
            try
            {
                SqlCommand command = new SqlCommand(string.Format("DELETE FROM [dbo].[DIME_DadosQuadro46] WHERE [IdLancamento] = '{0}'", code), Support.Conectar());
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region OUTROS EVENTOS
        public bool MenuEvent()
        {
            throw new NotImplementedException();
        }

        public bool FormDataEvent()
        {
            throw new NotImplementedException();
        }

        public bool AppEvent()
        {
            throw new NotImplementedException();
        }

        public bool PrintEvent()
        {
            throw new NotImplementedException();
        }

        public bool ProgressBarEvent()
        {
            throw new NotImplementedException();
        }

        public bool ReportDataEvent()
        {
            throw new NotImplementedException();
        }

        public bool RightClickEvent()
        {
            throw new NotImplementedException();
        }

        public bool StatusBarEvent()
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
