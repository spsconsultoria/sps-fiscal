﻿using SAPbouiCOM;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPS_Fiscal.Addon
{
    class frmfrmCadContab : IForm
    {
        private ItemEvent _eventoItem;
        public frmfrmCadContab(ItemEvent evento)
        {
            this._eventoItem = evento;
        }

        public static string Code;

        public bool ItemEvent()
        {
            try
            {
                switch (_eventoItem.EventType)
                {
                    case BoEventTypes.et_ALL_EVENTS:
                        break;
                    case BoEventTypes.et_B1I_SERVICE_COMPLETE:
                        break;
                    case BoEventTypes.et_CHOOSE_FROM_LIST:
                        break;
                    case BoEventTypes.et_CLICK:
                        break;
                    case BoEventTypes.et_COMBO_SELECT:
                        break;
                    case BoEventTypes.et_DATASOURCE_LOAD:
                        break;
                    case BoEventTypes.et_DOUBLE_CLICK:
                        break;
                    case BoEventTypes.et_Drag:
                        break;
                    case BoEventTypes.et_EDIT_REPORT:
                        break;
                    case BoEventTypes.et_FORMAT_SEARCH_COMPLETED:
                        break;
                    case BoEventTypes.et_FORM_ACTIVATE:
                        break;
                    case BoEventTypes.et_FORM_CLOSE:
                        break;
                    case BoEventTypes.et_FORM_DATA_ADD:
                        break;
                    case BoEventTypes.et_FORM_DATA_DELETE:
                        break;
                    case BoEventTypes.et_FORM_DATA_LOAD:
                        break;
                    case BoEventTypes.et_FORM_DATA_UPDATE:
                        break;
                    case BoEventTypes.et_FORM_DEACTIVATE:
                        break;
                    case BoEventTypes.et_FORM_DRAW:
                        break;
                    case BoEventTypes.et_FORM_KEY_DOWN:
                        break;
                    case BoEventTypes.et_FORM_LOAD:
                        break;
                    case BoEventTypes.et_FORM_MENU_HILIGHT:
                        break;
                    case BoEventTypes.et_FORM_RESIZE:
                        break;
                    case BoEventTypes.et_FORM_UNLOAD:
                        break;
                    case BoEventTypes.et_FORM_VISIBLE:
                        break;
                    case BoEventTypes.et_GOT_FOCUS:
                        break;
                    case BoEventTypes.et_GRID_SORT:
                        break;
                    case BoEventTypes.et_ITEM_PRESSED:
                        #region BOTÃO SALVAR
                        if (!_eventoItem.Before_Action && _eventoItem.ItemUID == "btnSave")
                        {
                            SAPbouiCOM.Form oForm = Conexao.uiApplication.Forms.Item(_eventoItem.FormUID);

                            try
                            {
                                if (string.IsNullOrEmpty(Code))
                                    Save(ref oForm);
                                else
                                    Update(ref oForm);
                            }
                            catch (Exception ex)
                            {
                                throw new Exception(ex.Message);
                            }
                            if (string.IsNullOrEmpty(Code))
                                Conexao.uiApplication.SetStatusBarMessage("Contabilista Salvo com Sucesso!", BoMessageTime.bmt_Short, false);
                            else
                                Conexao.uiApplication.SetStatusBarMessage("Contabilista Alterado com Sucesso!", BoMessageTime.bmt_Short, false);
                            frmfrmConfigDime.carregaGridContabilista(ref frmfrmConfigDime.oFormPrincipal);
                            oForm.Close();
                        }
                        #endregion

                        #region BOTÃO EXCLUIR
                        if (!_eventoItem.Before_Action && _eventoItem.ItemUID == "btnDelete")
                        {
                            SAPbouiCOM.Form oForm = Conexao.uiApplication.Forms.Item(_eventoItem.FormUID);

                            if (Conexao.uiApplication.MessageBox("Deseja realmente excluir?", 1, "Sim", "Não") == 1)
                            {
                                try
                                {
                                    Delete(ref oForm);
                                    Conexao.uiApplication.SetStatusBarMessage("Contabilista Excluido com Sucesso!", BoMessageTime.bmt_Short, false);
                                    frmfrmConfigDime.carregaGridContabilista(ref frmfrmConfigDime.oFormPrincipal);
                                    oForm.Close();
                                }
                                catch (Exception ex)
                                {
                                    throw new Exception(ex.Message);
                                }
                            }
                        }
                        #endregion
                        break;
                    case BoEventTypes.et_KEY_DOWN:
                        break;
                    case BoEventTypes.et_LOST_FOCUS:
                        break;
                    case BoEventTypes.et_MATRIX_COLLAPSE_PRESSED:
                        break;
                    case BoEventTypes.et_MATRIX_LINK_PRESSED:
                        break;
                    case BoEventTypes.et_MATRIX_LOAD:
                        break;
                    case BoEventTypes.et_MENU_CLICK:
                        break;
                    case BoEventTypes.et_PICKER_CLICKED:
                        break;
                    case BoEventTypes.et_PRINT:
                        break;
                    case BoEventTypes.et_PRINT_DATA:
                        break;
                    case BoEventTypes.et_PRINT_LAYOUT_KEY:
                        break;
                    case BoEventTypes.et_RIGHT_CLICK:
                        break;
                    case BoEventTypes.et_UDO_FORM_BUILD:
                        break;
                    case BoEventTypes.et_UDO_FORM_OPEN:
                        break;
                    case BoEventTypes.et_VALIDATE:
                        break;
                    default:
                        break;
                }

                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region OUTROS EVENTOS
        public bool MenuEvent()
        {
            throw new NotImplementedException();
        }

        public bool FormDataEvent()
        {
            throw new NotImplementedException();
        }

        public bool AppEvent()
        {
            throw new NotImplementedException();
        }

        public bool PrintEvent()
        {
            throw new NotImplementedException();
        }

        public bool ProgressBarEvent()
        {
            throw new NotImplementedException();
        }

        public bool ReportDataEvent()
        {
            throw new NotImplementedException();
        }

        public bool RightClickEvent()
        {
            throw new NotImplementedException();
        }

        public bool StatusBarEvent()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region SALVAR
        private void Save(ref SAPbouiCOM.Form oForm)
        {
            try
            {
                SqlCommand command = new SqlCommand(@"INSERT INTO [DIME_Contabilista]([CPF],[NomeContabilista],[Excluido]) VALUES (@CPF, @NomeContabilista, @Excluido)", Support.Conectar());
                command.Parameters.AddWithValue("@CPF", oForm.DataSources.UserDataSources.Item("cpf").Value);
                command.Parameters.AddWithValue("@NomeContabilista", oForm.DataSources.UserDataSources.Item("Nome").Value);
                command.Parameters.AddWithValue("@Excluido", "N");
                command.ExecuteNonQuery();
            }
            catch (SqlException erro)
            {
                throw new Exception(erro.Message);
            }
            finally
            {
                Support.Conectar().Close();
            }

        }
        #endregion

        #region ALTERAR
        private void Update(ref SAPbouiCOM.Form oForm)
        {
            try
            {
                string sql = string.Format("UPDATE [DIME_Contabilista] SET [CPF] = @CPF,[NomeContabilista] = @NomeContabilista  WHERE [IdContabilista] = {0}", Code);
                SqlCommand command = new SqlCommand(sql, Support.Conectar());
                command.Parameters.AddWithValue("@CPF", oForm.DataSources.UserDataSources.Item("cpf").Value);
                command.Parameters.AddWithValue("@NomeContabilista", oForm.DataSources.UserDataSources.Item("Nome").Value);
                command.ExecuteNonQuery();
            }
            catch (SqlException erro)
            {
                throw new Exception(erro.Message);
            }
            finally
            {
                Support.Conectar().Close();
            }
        }
        #endregion

        #region DELETE
        private void Delete(ref SAPbouiCOM.Form oForm)
        {
            try
            {
                string sql = string.Format("UPDATE [DIME_Contabilista] SET [Excluido] = @Excluido WHERE [IdContabilista] = {0}", Code);
                SqlCommand command = new SqlCommand(sql, Support.Conectar());
                command.Parameters.AddWithValue("@Excluido", "S");
                command.ExecuteNonQuery();
            }
            catch (SqlException erro)
            {
                throw new Exception(erro.Message);
            }
            finally
            {
                Support.Conectar().Close();
            }
        }
        #endregion
    }
}
