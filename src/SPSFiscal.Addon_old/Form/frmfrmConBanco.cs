﻿using SAPbouiCOM;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPS_Fiscal.Addon
{
    class frmfrmConBanco : IForm
    {
        public static bool Novo;

        private ItemEvent _eventoItem;
        public frmfrmConBanco(ItemEvent evento)
        {
            this._eventoItem = evento;
        }

        public bool ItemEvent()
        {
            try
            {
                switch (_eventoItem.EventType)
                {
                    case BoEventTypes.et_ALL_EVENTS:
                        break;
                    case BoEventTypes.et_B1I_SERVICE_COMPLETE:
                        break;
                    case BoEventTypes.et_CHOOSE_FROM_LIST:
                        break;
                    case BoEventTypes.et_CLICK:
                        break;
                    case BoEventTypes.et_COMBO_SELECT:
                        break;
                    case BoEventTypes.et_DATASOURCE_LOAD:
                        break;
                    case BoEventTypes.et_DOUBLE_CLICK:
                        break;
                    case BoEventTypes.et_Drag:
                        break;
                    case BoEventTypes.et_EDIT_REPORT:
                        break;
                    case BoEventTypes.et_FORMAT_SEARCH_COMPLETED:
                        break;
                    case BoEventTypes.et_FORM_ACTIVATE:
                        break;
                    case BoEventTypes.et_FORM_CLOSE:
                        break;
                    case BoEventTypes.et_FORM_DATA_ADD:
                        break;
                    case BoEventTypes.et_FORM_DATA_DELETE:
                        break;
                    case BoEventTypes.et_FORM_DATA_LOAD:
                        break;
                    case BoEventTypes.et_FORM_DATA_UPDATE:
                        break;
                    case BoEventTypes.et_FORM_DEACTIVATE:
                        break;
                    case BoEventTypes.et_FORM_DRAW:
                        break;
                    case BoEventTypes.et_FORM_KEY_DOWN:
                        break;
                    case BoEventTypes.et_FORM_LOAD:
                        break;
                    case BoEventTypes.et_FORM_MENU_HILIGHT:
                        break;
                    case BoEventTypes.et_FORM_RESIZE:
                        break;
                    case BoEventTypes.et_FORM_UNLOAD:
                        break;
                    case BoEventTypes.et_FORM_VISIBLE:
                        break;
                    case BoEventTypes.et_GOT_FOCUS:
                        break;
                    case BoEventTypes.et_GRID_SORT:
                        break;
                    case BoEventTypes.et_ITEM_PRESSED:
                        if (!_eventoItem.Before_Action && _eventoItem.ItemUID == "btnOk")
                        {
                            SAPbouiCOM.Form oForm = Conexao.uiApplication.Forms.Item(_eventoItem.FormUID);
                            SAPbobsCOM.UserTable UDT;

                            UDT = Conexao.diCompany.UserTables.Item("SPS_Con");
                            int iRet = -1;
                            if (Novo == true)
                            {
                                UDT.Code = "01";
                                UDT.Name = UDT.Code;
                                UDT.UserFields.Fields.Item("U_User").Value = oForm.DataSources.UserDataSources.Item("User").Value;
                                UDT.UserFields.Fields.Item("U_Pass").Value = oForm.DataSources.UserDataSources.Item("Pass").Value;
                                UDT.UserFields.Fields.Item("U_Server").Value = oForm.DataSources.UserDataSources.Item("Server").Value;
                                iRet = UDT.Add();

                                if (iRet != 0)
                                {
                                    Conexao.uiApplication.SetStatusBarMessage("Erro ao Salvar", BoMessageTime.bmt_Short, true);
                                }
                                else
                                {
                                    Conexao.uiApplication.SetStatusBarMessage("Salvo com sucesso!", BoMessageTime.bmt_Short, false);
                                    oForm.Close();
                                }
                            }
                            else
                            {
                                UDT.GetByKey("01");
                                UDT.UserFields.Fields.Item("U_User").Value = oForm.DataSources.UserDataSources.Item("User").Value;
                                UDT.UserFields.Fields.Item("U_Pass").Value = oForm.DataSources.UserDataSources.Item("Pass").Value;
                                UDT.UserFields.Fields.Item("U_Server").Value = oForm.DataSources.UserDataSources.Item("Server").Value;
                                iRet = UDT.Update();

                                if (iRet != 0)
                                {
                                    Conexao.uiApplication.SetStatusBarMessage("Erro ao alterar", BoMessageTime.bmt_Short, true);
                                }
                                else
                                {
                                    Conexao.uiApplication.SetStatusBarMessage("Alterado com sucesso!", BoMessageTime.bmt_Short, false);
                                    oForm.Close();
                                }
                            }
                        }

                        if (!_eventoItem.Before_Action && _eventoItem.ItemUID == "btnTabela")
                        {
                            int Versao = Convert.ToInt32(System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString().Substring(0, 1));
                            try
                            {
                                string sql = string.Format(@"SELECT name FROM sys.databases WHERE name = 'SPS_Fiscal'");
                                string resultado = Convert.ToString(Conexao.ExecuteSqlScalar(sql));
                                
                                SqlCommand command = new SqlCommand();

                                //if (string.IsNullOrEmpty(resultado))
                                //{
                                        Conexao.uiApplication.SetStatusBarMessage("Criando Banco de Dados SPS_Fiscal", SAPbouiCOM.BoMessageTime.bmt_Long, false);
                                        DB.CriaDB();
                                        Conexao.uiApplication.SetStatusBarMessage("Banco de Dados com sucesso", SAPbouiCOM.BoMessageTime.bmt_Short, false);                                  
                                //}

                                //if (Versao >= 2)
                                //{

                                //}

                                Conexao.uiApplication.SetStatusBarMessage("Criando Procedures", SAPbouiCOM.BoMessageTime.bmt_Short, false);
                                DB.CriaProc();
                                Conexao.uiApplication.SetStatusBarMessage("Procedures Criada com sucesso", SAPbouiCOM.BoMessageTime.bmt_Short, false);

                            }
                            catch (Exception ex)
                            {
                                throw new Exception(ex.Message);
                            }
                        }
                        break;
                    case BoEventTypes.et_KEY_DOWN:
                        break;
                    case BoEventTypes.et_LOST_FOCUS:
                        break;
                    case BoEventTypes.et_MATRIX_COLLAPSE_PRESSED:
                        break;
                    case BoEventTypes.et_MATRIX_LINK_PRESSED:
                        break;
                    case BoEventTypes.et_MATRIX_LOAD:
                        break;
                    case BoEventTypes.et_MENU_CLICK:
                        break;
                    case BoEventTypes.et_PICKER_CLICKED:
                        break;
                    case BoEventTypes.et_PRINT:
                        break;
                    case BoEventTypes.et_PRINT_DATA:
                        break;
                    case BoEventTypes.et_PRINT_LAYOUT_KEY:
                        break;
                    case BoEventTypes.et_RIGHT_CLICK:
                        break;
                    case BoEventTypes.et_UDO_FORM_BUILD:
                        break;
                    case BoEventTypes.et_UDO_FORM_OPEN:
                        break;
                    case BoEventTypes.et_VALIDATE:
                        break;
                    default:
                        break;
                }
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region OUTROS EVENTOS
        public bool MenuEvent()
        {
            throw new NotImplementedException();
        }

        public bool FormDataEvent()
        {
            throw new NotImplementedException();
        }

        public bool AppEvent()
        {
            throw new NotImplementedException();
        }

        public bool PrintEvent()
        {
            throw new NotImplementedException();
        }

        public bool ProgressBarEvent()
        {
            throw new NotImplementedException();
        }

        public bool ReportDataEvent()
        {
            throw new NotImplementedException();
        }

        public bool RightClickEvent()
        {
            throw new NotImplementedException();
        }

        public bool StatusBarEvent()
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
