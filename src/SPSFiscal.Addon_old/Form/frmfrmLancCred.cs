﻿using SAPbouiCOM;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPS_Fiscal.Addon
{
    class frmfrmLancCred : IForm
    {
        public static string Id;
        private ItemEvent _eventoItem;
        public frmfrmLancCred(ItemEvent evento)
        {
            this._eventoItem = evento;
        }

        public bool ItemEvent()
        {
            try
            {
                switch (_eventoItem.EventType)
                {
                    case BoEventTypes.et_ALL_EVENTS:
                        break;
                    case BoEventTypes.et_B1I_SERVICE_COMPLETE:
                        break;
                    case BoEventTypes.et_CHOOSE_FROM_LIST:
                        break;
                    case BoEventTypes.et_CLICK:
                        break;
                    case BoEventTypes.et_COMBO_SELECT:
                        break;
                    case BoEventTypes.et_DATASOURCE_LOAD:
                        break;
                    case BoEventTypes.et_DOUBLE_CLICK:
                        break;
                    case BoEventTypes.et_Drag:
                        break;
                    case BoEventTypes.et_EDIT_REPORT:
                        break;
                    case BoEventTypes.et_FORMAT_SEARCH_COMPLETED:
                        break;
                    case BoEventTypes.et_FORM_ACTIVATE:
                        break;
                    case BoEventTypes.et_FORM_CLOSE:
                        break;
                    case BoEventTypes.et_FORM_DATA_ADD:
                        break;
                    case BoEventTypes.et_FORM_DATA_DELETE:
                        break;
                    case BoEventTypes.et_FORM_DATA_LOAD:
                        break;
                    case BoEventTypes.et_FORM_DATA_UPDATE:
                        break;
                    case BoEventTypes.et_FORM_DEACTIVATE:
                        break;
                    case BoEventTypes.et_FORM_DRAW:
                        break;
                    case BoEventTypes.et_FORM_KEY_DOWN:
                        break;
                    case BoEventTypes.et_FORM_LOAD:
                        break;
                    case BoEventTypes.et_FORM_MENU_HILIGHT:
                        break;
                    case BoEventTypes.et_FORM_RESIZE:
                        break;
                    case BoEventTypes.et_FORM_UNLOAD:
                        break;
                    case BoEventTypes.et_FORM_VISIBLE:
                        break;
                    case BoEventTypes.et_GOT_FOCUS:
                        break;
                    case BoEventTypes.et_GRID_SORT:
                        break;
                    case BoEventTypes.et_ITEM_PRESSED:
                        #region BOTÃO CADASTRAR
                        if (!_eventoItem.Before_Action && _eventoItem.ItemUID == "btnSave")
                        {
                            SAPbouiCOM.Form oForm = Conexao.uiApplication.Forms.Item(_eventoItem.FormUID);

                            if (string.IsNullOrEmpty(Id))
                            {
                                string sql = string.Format(@"SELECT 'true' FROM [SPS_Fiscal].[dbo].[DIME_DadosQuadro46] WHERE Ind_Origem = '{0}'", oForm.DataSources.UserDataSources.Item("origem").Value);
                               bool existe = Convert.ToBoolean(Conexao.ExecuteSqlScalar(sql));

                                if (existe)
                                    Conexao.uiApplication.SetStatusBarMessage("Lançamento já realizado para a origem selecionada", BoMessageTime.bmt_Short, true);
                                else
                                {
                                    Save(ref oForm, existe);
                                    Conexao.uiApplication.SetStatusBarMessage("Lançamento salvo com sucesso", BoMessageTime.bmt_Short, false);
                                }
                            }
                            else
                            {
                                Update(ref oForm);
                                Conexao.uiApplication.SetStatusBarMessage("Lançamento alterado com sucesso", BoMessageTime.bmt_Short, false);
                            }
                        }
                        #endregion
                        break;
                    case BoEventTypes.et_KEY_DOWN:
                        break;
                    case BoEventTypes.et_LOST_FOCUS:
                        break;
                    case BoEventTypes.et_MATRIX_COLLAPSE_PRESSED:
                        break;
                    case BoEventTypes.et_MATRIX_LINK_PRESSED:
                        break;
                    case BoEventTypes.et_MATRIX_LOAD:
                        break;
                    case BoEventTypes.et_MENU_CLICK:
                        break;
                    case BoEventTypes.et_PICKER_CLICKED:
                        break;
                    case BoEventTypes.et_PRINT:
                        break;
                    case BoEventTypes.et_PRINT_DATA:
                        break;
                    case BoEventTypes.et_PRINT_LAYOUT_KEY:
                        break;
                    case BoEventTypes.et_RIGHT_CLICK:
                        break;
                    case BoEventTypes.et_UDO_FORM_BUILD:
                        break;
                    case BoEventTypes.et_UDO_FORM_OPEN:
                        break;
                    case BoEventTypes.et_VALIDATE:
                        break;
                    default:
                        break;
                }
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region SAVE
        private static void Save(ref SAPbouiCOM.Form oForm, bool existe)
        {
            try
            {
                    SqlCommand command = new SqlCommand(@"DECLARE @CODIGO AS INT SET @CODIGO = (SELECT MAX(IdLancamento+1)
                                                      FROM [DIME_DadosQuadro46]) IF(@CODIGO IS NULL) BEGIN SET @CODIGO ='1' END
                                                      INSERT INTO [dbo].[DIME_DadosQuadro46]
                                                                       ([IdDeclaracao]
                                                                       ,[Seq_Reg]  
                                                                       ,[Cod_Identif]
                                                                       ,[Vl_Cred_Apur]
                                                                       ,[Ind_Origem])
                                                                 VALUES
                                                                       (@IdDeclaracao
                                                                       ,@CODIGO
                                                                       ,@Cod_Identif
                                                                       ,@Vl_Cred_Apur
                                                                       ,@Ind_Origem)", Support.Conectar());
                    command.Parameters.AddWithValue("@IdDeclaracao", frmfrmCadDeclarDime.IdDeclaracao);
                    command.Parameters.AddWithValue("@Cod_Identif", oForm.DataSources.UserDataSources.Item("ident").Value);
                    command.Parameters.AddWithValue("@Vl_Cred_Apur", Convert.ToDouble(oForm.DataSources.UserDataSources.Item("valor").Value));
                    command.Parameters.AddWithValue("@Ind_Origem", oForm.DataSources.UserDataSources.Item("origem").Value);
                    command.ExecuteNonQuery();

                    if (oForm.DataSources.UserDataSources.Item("origem").Value == "1")
                    {
                        string sql = string.Format(@"select SUM(Vl_Cred_Apur) FROM [SPS_Fiscal].[dbo].[DIME_DadosQuadro46] where Ind_Origem = '1' AND IdDeclaracao = '{0}'", frmfrmCadDeclarDime.IdDeclaracao);
                        double valor = Convert.ToDouble(Conexao.ExecuteSqlScalar(sql));
                        sql = string.Format(@"UPDATE [SPS_Fiscal].[dbo].[DIME_DadosQuadro09] SET Valor = Convert(Decimal(18,2),'{0}') WHERE NumeroItem = '070' AND IdDeclaracao = '{1}'", valor.ToString().Replace(",", "."), frmfrmCadDeclarDime.IdDeclaracao);
                        Conexao.ExecuteSqlScalar(sql);

                        sql = string.Format(@"select SUM(Valor) FROM [SPS_Fiscal].[dbo].[DIME_DadosQuadro09]  where NumeroItem in (050,060,070,075,076) AND IdDeclaracao = '{0}'", frmfrmCadDeclarDime.IdDeclaracao);
                        valor = Convert.ToDouble(Conexao.ExecuteSqlScalar(sql));
                        sql = string.Format(@"UPDATE [SPS_Fiscal].[dbo].[DIME_DadosQuadro09] SET Valor = Convert(Decimal(18,2),'{0}') WHERE NumeroItem = '080' AND IdDeclaracao = '{1}'", valor.ToString().Replace(",", "."), frmfrmCadDeclarDime.IdDeclaracao);
                        Conexao.ExecuteSqlScalar(sql);
                    }

                    if (oForm.DataSources.UserDataSources.Item("origem").Value == "14")
                    {
                        string sql = string.Format(@"select SUM(Vl_Cred_Apur) FROM [SPS_Fiscal].[dbo].[DIME_DadosQuadro46] where Ind_Origem = '14' AND IdDeclaracao = '{0}'", frmfrmCadDeclarDime.IdDeclaracao);
                        double valor = Convert.ToDouble(Conexao.ExecuteSqlScalar(sql));
                        sql = string.Format(@"UPDATE [SPS_Fiscal].[dbo].[DIME_DadosQuadro09] SET Valor = Convert(Decimal(18,2),'{0}') WHERE NumeroItem = '075'  AND IdDeclaracao = '{1}'", valor.ToString().Replace(",", "."), frmfrmCadDeclarDime.IdDeclaracao);
                        Conexao.ExecuteSqlScalar(sql);

                        sql = string.Format(@"select SUM(Valor) FROM [SPS_Fiscal].[dbo].[DIME_DadosQuadro09]  where NumeroItem in (050,060,070,075,076) AND IdDeclaracao = '{0}'", frmfrmCadDeclarDime.IdDeclaracao);
                        valor = Convert.ToDouble(Conexao.ExecuteSqlScalar(sql));
                        sql = string.Format(@"UPDATE [SPS_Fiscal].[dbo].[DIME_DadosQuadro09] SET Valor = Convert(Decimal(18,2),'{0}') WHERE NumeroItem = '080' AND IdDeclaracao = '{1}'", valor.ToString().Replace(",", "."), frmfrmCadDeclarDime.IdDeclaracao);
                        Conexao.ExecuteSqlScalar(sql);

                    oForm.Close();

                    frmfrmCredRegAuto.oFormPrincipal.Freeze(true);
                    frmfrmCredRegAuto.CarregaGrid(ref frmfrmCredRegAuto.oFormPrincipal);
                    frmfrmCredRegAuto.oFormPrincipal.Freeze(false);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region UPDATE
        private static void Update(ref SAPbouiCOM.Form oForm)
        {
            try
            {
                string sql = string.Format(@"UPDATE [dbo].[DIME_DadosQuadro46]
                                                       SET [Cod_Identif] = @Cod_Identif
                                                          ,[Vl_Cred_Apur] = @Vl_Cred_Apur
                                                          ,[Ind_Origem] = @Ind_Origem
                                                 WHERE [IdLancamento] = '{0}'", Id);
                SqlCommand command = new SqlCommand(sql, Support.Conectar());
                command.Parameters.AddWithValue("@Cod_Identif", oForm.DataSources.UserDataSources.Item("ident").Value);
                command.Parameters.AddWithValue("@Vl_Cred_Apur", Convert.ToDouble(oForm.DataSources.UserDataSources.Item("valor").Value));
                command.Parameters.AddWithValue("@Ind_Origem", oForm.DataSources.UserDataSources.Item("origem").Value);
                command.ExecuteNonQuery();

                oForm.Close();

                frmfrmCredRegAuto.oFormPrincipal.Freeze(true);
                frmfrmCredRegAuto.CarregaGrid(ref frmfrmCredRegAuto.oFormPrincipal);
                frmfrmCredRegAuto.oFormPrincipal.Freeze(false);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region OUTROS EVENTOS
        public bool MenuEvent()
        {
            throw new NotImplementedException();
        }

        public bool FormDataEvent()
        {
            throw new NotImplementedException();
        }

        public bool AppEvent()
        {
            throw new NotImplementedException();
        }

        public bool PrintEvent()
        {
            throw new NotImplementedException();
        }

        public bool ProgressBarEvent()
        {
            throw new NotImplementedException();
        }

        public bool ReportDataEvent()
        {
            throw new NotImplementedException();
        }

        public bool RightClickEvent()
        {
            throw new NotImplementedException();
        }

        public bool StatusBarEvent()
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
