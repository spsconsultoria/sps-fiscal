﻿using SAPbouiCOM;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPS_Fiscal.Addon
{
    class frmfrmCalcImpPagar : IForm
    {
        private ItemEvent _eventoItem;
        public frmfrmCalcImpPagar(ItemEvent evento)
        {
            this._eventoItem = evento;
        }
        public bool ItemEvent()
        {
            try
            {
                switch (_eventoItem.EventType)
                {
                    case BoEventTypes.et_ALL_EVENTS:
                        break;
                    case BoEventTypes.et_B1I_SERVICE_COMPLETE:
                        break;
                    case BoEventTypes.et_CHOOSE_FROM_LIST:
                        break;
                    case BoEventTypes.et_CLICK:
                        break;
                    case BoEventTypes.et_COMBO_SELECT:
                        break;
                    case BoEventTypes.et_DATASOURCE_LOAD:
                        break;
                    case BoEventTypes.et_DOUBLE_CLICK:
                        break;
                    case BoEventTypes.et_Drag:
                        break;
                    case BoEventTypes.et_EDIT_REPORT:
                        break;
                    case BoEventTypes.et_FORMAT_SEARCH_COMPLETED:
                        break;
                    case BoEventTypes.et_FORM_ACTIVATE:
                        break;
                    case BoEventTypes.et_FORM_CLOSE:
                        break;
                    case BoEventTypes.et_FORM_DATA_ADD:
                        break;
                    case BoEventTypes.et_FORM_DATA_DELETE:
                        break;
                    case BoEventTypes.et_FORM_DATA_LOAD:
                        break;
                    case BoEventTypes.et_FORM_DATA_UPDATE:
                        break;
                    case BoEventTypes.et_FORM_DEACTIVATE:
                        break;
                    case BoEventTypes.et_FORM_DRAW:
                        break;
                    case BoEventTypes.et_FORM_KEY_DOWN:
                        break;
                    case BoEventTypes.et_FORM_LOAD:
                        break;
                    case BoEventTypes.et_FORM_MENU_HILIGHT:
                        break;
                    case BoEventTypes.et_FORM_RESIZE:
                        break;
                    case BoEventTypes.et_FORM_UNLOAD:
                        break;
                    case BoEventTypes.et_FORM_VISIBLE:
                        break;
                    case BoEventTypes.et_GOT_FOCUS:
                        break;
                    case BoEventTypes.et_GRID_SORT:
                        break;
                    case BoEventTypes.et_ITEM_PRESSED:
                        #region BOTÃO CADASTRAR
                        if (!_eventoItem.Before_Action && _eventoItem.ItemUID == "1")
                        {
                            SAPbouiCOM.Form oForm = Conexao.uiApplication.Forms.Item(_eventoItem.FormUID);

                            Grid oGrid = (Grid)oForm.Items.Item("grd2").Specific;

                            Button oButton = (Button)oForm.Items.Item("1").Specific;

                            int linhasGrid = oGrid.Rows.Count;

                            for (int i = 0; i < linhasGrid; i++)
                            {
                                string NumeroItem = oForm.DataSources.DataTables.Item("grd2").GetValue("NumeroItem", i);

                                try
                                {
                                    string sql = string.Format(@"UPDATE [dbo].[DIME_DadosQuadro09]
                                                    SET [Valor] = @Valor
                                                 WHERE [NumeroItem] = '{0}'", NumeroItem);
                                    SqlCommand command = new SqlCommand(sql, Support.Conectar());
                                    command.Parameters.AddWithValue("@Valor", Convert.ToDouble(oForm.DataSources.DataTables.Item("grd2").GetValue("Valor", i)));
                                    command.ExecuteNonQuery();
                                }
                                catch (Exception ex)
                                {
                                    throw new Exception(ex.Message);
                                }
                            }
                            CarregaGrid(ref oForm);
                            Conexao.uiApplication.SetStatusBarMessage("Informações salvas com sucesso.", BoMessageTime.bmt_Short, false);
                        }
                        #endregion
                        break;
                    case BoEventTypes.et_KEY_DOWN:
                        break;
                    case BoEventTypes.et_LOST_FOCUS:
                        break;
                    case BoEventTypes.et_MATRIX_COLLAPSE_PRESSED:
                        break;
                    case BoEventTypes.et_MATRIX_LINK_PRESSED:
                        break;
                    case BoEventTypes.et_MATRIX_LOAD:
                        break;
                    case BoEventTypes.et_MENU_CLICK:
                        break;
                    case BoEventTypes.et_PICKER_CLICKED:
                        break;
                    case BoEventTypes.et_PRINT:
                        break;
                    case BoEventTypes.et_PRINT_DATA:
                        break;
                    case BoEventTypes.et_PRINT_LAYOUT_KEY:
                        break;
                    case BoEventTypes.et_RIGHT_CLICK:
                        break;
                    case BoEventTypes.et_UDO_FORM_BUILD:
                        break;
                    case BoEventTypes.et_UDO_FORM_OPEN:
                        break;
                    case BoEventTypes.et_VALIDATE:
                        break;
                    default:
                        break;
                }
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region UPDATE
        private static void Update(ref SAPbouiCOM.Form oForm, int i)
        {
            try
            {
                string sql = string.Format(@"UPDATE [dbo].[DIME_DadosQuadro05] SET [Valor] = @Valor
                                                 WHERE [IdLancamento] = '{0}'", frmfrmCadDeclarDime.IdDeclaracao);
                SqlCommand command = new SqlCommand(sql, Support.Conectar());
                command.Parameters.AddWithValue("@Valor", Convert.ToDouble(oForm.DataSources.DataTables.Item("grd2").GetValue("Valor", i)));
                command.ExecuteNonQuery();
                oForm.Close();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region CARREGA GRID
        public static void CarregaGrid(ref SAPbouiCOM.Form oForm)
        {
            try
            {
                string sql = string.Format(@"SELECT '1 - Totalização de Débitos' as 'Item'
	  ,T0.[NumeroItem] as 'Número'
      ,T0.[Descricao]
	  ,T1.[Valor]
INTO #TEMP
  FROM [SPS_Fiscal].[dbo].[DIME_TabelaQuadro09] T0
  INNER JOIN [SPS_Fiscal].[dbo].[DIME_DadosQuadro09] T1 ON T0.NumeroItem = T1.NumeroItem
  WHERE T0.TipoItem = '1' AND T1.IdDeclaracao = {0}

  UNION ALL

SELECT '2 - Totalização de Créditos' as 'Item'
	  ,T0.[NumeroItem] as 'Número'
      ,T0.[Descricao]
	  ,T1.[Valor]
  FROM [SPS_Fiscal].[dbo].[DIME_TabelaQuadro09] T0
  INNER JOIN [SPS_Fiscal].[dbo].[DIME_DadosQuadro09] T1 ON T0.NumeroItem = T1.NumeroItem
  WHERE T0.TipoItem = '2' AND T1.IdDeclaracao = {0}

  UNION ALL

SELECT '3 - Ajustes da apuração decendial e Antecipações' as 'Item'
	  ,T0.[NumeroItem] as 'Número'
      ,T0.[Descricao]
	  ,T1.[Valor]
  FROM [SPS_Fiscal].[dbo].[DIME_TabelaQuadro09] T0
  INNER JOIN [SPS_Fiscal].[dbo].[DIME_DadosQuadro09] T1 ON T0.NumeroItem = T1.NumeroItem
  WHERE T0.TipoItem = '3' AND T1.IdDeclaracao = {0}

  UNION ALL

SELECT '4 - Apuração do Saldo Devedor' as 'Item'
	  ,T0.[NumeroItem] as 'Número'
      ,T0.[Descricao]
	  ,T1.[Valor]
  FROM [SPS_Fiscal].[dbo].[DIME_TabelaQuadro09] T0
  INNER JOIN [SPS_Fiscal].[dbo].[DIME_DadosQuadro09] T1 ON T0.NumeroItem = T1.NumeroItem
  WHERE T0.TipoItem = '4' AND T1.IdDeclaracao = {0}

  UNION ALL

SELECT '5 - Apuração do Saldo Credor' as 'Item' 
	  ,T0.[NumeroItem] as 'Número'
      ,T0.[Descricao]
	  ,T1.[Valor]
  FROM [SPS_Fiscal].[dbo].[DIME_TabelaQuadro09] T0
  INNER JOIN [SPS_Fiscal].[dbo].[DIME_DadosQuadro09] T1 ON T0.NumeroItem = T1.NumeroItem
  WHERE T0.TipoItem = '5' AND T1.IdDeclaracao = {0}

  UNION ALL

SELECT '6 - Discriminação do saldo credor para o mês seguiinte' as 'Item' 
	  ,T0.[NumeroItem] as 'Número'
      ,T0.[Descricao]
	  ,T1.[Valor]
  FROM [SPS_Fiscal].[dbo].[DIME_TabelaQuadro09] T0
  INNER JOIN [SPS_Fiscal].[dbo].[DIME_DadosQuadro09] T1 ON T0.NumeroItem = T1.NumeroItem
  WHERE T0.TipoItem = '6' AND T1.IdDeclaracao = {0}
ORDER BY Número ASC

declare @TotalDeb as decimal(18,2) = (select SUM(Valor) FROM #TEMP  WHERE Número IN (010,011,020,030) group by Item)
declare @TotalCred as decimal(18,2) = (select SUM(Valor) FROM #TEMP  WHERE Número IN (050,060,070,075) group by Item)
declare @AjusteApur as decimal(18,2) = (select SUM(Valor) FROM #TEMP  WHERE Número IN (090,100,105,110) group by Item)
declare @ApurDev as decimal(18,2) = (select SUM(Valor) FROM #TEMP  WHERE Número IN (120,130) group by Item)
declare @Discriminacao as decimal(18,2) = (select SUM(Valor) FROM #TEMP  WHERE Número IN (160,170,180) group by Item)

select item
,Número
,Descricao
,CASE Número 
WHEN '040' THEN @TotalDeb
WHEN '080' THEN @TotalCred
WHEN '110' THEN @AjusteApur
WHEN '999' THEN @ApurDev
WHEN '190' THEN @Discriminacao
ELSE Valor 
END AS 'Valor'
from #TEMP
drop table #TEMP", frmfrmCadDeclarDime.IdDeclaracao);
                oForm.DataSources.DataTables.Item("grd").ExecuteQuery(sql);
                ((Grid)oForm.Items.Item("grd").Specific).CollapseLevel = 1;
                ((Grid)oForm.Items.Item("grd").Specific).AutoResizeColumns();
                ((Grid)oForm.Items.Item("grd").Specific).SelectionMode = BoMatrixSelect.ms_Auto;

                sql = string.Format(@"SELECT T0.[NumeroItem]
                                                ,T0.[Descricao]
	                                            ,T1.[Valor]
                                            FROM [SPS_Fiscal].[dbo].[DIME_TabelaQuadro09] T0
                                            INNER JOIN [SPS_Fiscal].[dbo].[DIME_DadosQuadro09] T1 ON T0.NumeroItem = T1.NumeroItem
                                            WHERE T0.NumeroItem in (010,011,020,030,050,060,090,100,105,130,150,160,170,180,190) AND T1.IdDeclaracao = {0}", frmfrmCadDeclarDime.IdDeclaracao);
                oForm.DataSources.DataTables.Item("grd2").ExecuteQuery(sql);
                ((Grid)oForm.Items.Item("grd2").Specific).Columns.Item("NumeroItem").Editable = false;
                ((Grid)oForm.Items.Item("grd2").Specific).Columns.Item("Descricao").Editable = false;
                ((Grid)oForm.Items.Item("grd2").Specific).Columns.Item("Valor").Editable = true;
                ((Grid)oForm.Items.Item("grd2").Specific).AutoResizeColumns();
                ((Grid)oForm.Items.Item("grd2").Specific).SelectionMode = BoMatrixSelect.ms_Auto;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region UPDATE
        private static void Update2(ref SAPbouiCOM.Form oForm, string NumeroItem, int i)
        {

        }
        #endregion

        #region OUTROS EVENTOS
        public bool MenuEvent()
        {
            throw new NotImplementedException();
        }

        public bool FormDataEvent()
        {
            throw new NotImplementedException();
        }

        public bool AppEvent()
        {
            throw new NotImplementedException();
        }

        public bool PrintEvent()
        {
            throw new NotImplementedException();
        }

        public bool ProgressBarEvent()
        {
            throw new NotImplementedException();
        }

        public bool ReportDataEvent()
        {
            throw new NotImplementedException();
        }

        public bool RightClickEvent()
        {
            throw new NotImplementedException();
        }

        public bool StatusBarEvent()
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
