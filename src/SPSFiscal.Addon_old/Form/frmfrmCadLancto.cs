﻿using SAPbouiCOM;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPS_Fiscal.Addon
{
    class frmfrmCadLancto : IForm
    {
        private ItemEvent _eventoItem;
        public static string Id;
        public frmfrmCadLancto(ItemEvent evento)
        {
            this._eventoItem = evento;
        }
        public bool ItemEvent()
        {
            try
            {
                switch (_eventoItem.EventType)
                {
                    case BoEventTypes.et_ALL_EVENTS:
                        break;
                    case BoEventTypes.et_B1I_SERVICE_COMPLETE:
                        break;
                    case BoEventTypes.et_CHOOSE_FROM_LIST:
                        break;
                    case BoEventTypes.et_CLICK:
                        break;
                    case BoEventTypes.et_COMBO_SELECT:
                        break;
                    case BoEventTypes.et_DATASOURCE_LOAD:
                        break;
                    case BoEventTypes.et_DOUBLE_CLICK:
                        break;
                    case BoEventTypes.et_Drag:
                        break;
                    case BoEventTypes.et_EDIT_REPORT:
                        break;
                    case BoEventTypes.et_FORMAT_SEARCH_COMPLETED:
                        break;
                    case BoEventTypes.et_FORM_ACTIVATE:
                        break;
                    case BoEventTypes.et_FORM_CLOSE:
                        break;
                    case BoEventTypes.et_FORM_DATA_ADD:
                        break;
                    case BoEventTypes.et_FORM_DATA_DELETE:
                        break;
                    case BoEventTypes.et_FORM_DATA_LOAD:
                        break;
                    case BoEventTypes.et_FORM_DATA_UPDATE:
                        break;
                    case BoEventTypes.et_FORM_DEACTIVATE:
                        break;
                    case BoEventTypes.et_FORM_DRAW:
                        break;
                    case BoEventTypes.et_FORM_KEY_DOWN:
                        break;
                    case BoEventTypes.et_FORM_LOAD:
                        break;
                    case BoEventTypes.et_FORM_MENU_HILIGHT:
                        break;
                    case BoEventTypes.et_FORM_RESIZE:
                        break;
                    case BoEventTypes.et_FORM_UNLOAD:
                        break;
                    case BoEventTypes.et_FORM_VISIBLE:
                        break;
                    case BoEventTypes.et_GOT_FOCUS:
                        break;
                    case BoEventTypes.et_GRID_SORT:
                        break;
                    case BoEventTypes.et_ITEM_PRESSED:
                        #region BOTÃO CADASTRAR
                        if (!_eventoItem.Before_Action && _eventoItem.ItemUID == "btnSave")
                        {
                            SAPbouiCOM.Form oForm = Conexao.uiApplication.Forms.Item(_eventoItem.FormUID);

                            if (string.IsNullOrEmpty(Id))
                            {
                                Save(ref oForm);
                                Conexao.uiApplication.SetStatusBarMessage("Lançamento salvo com sucesso", BoMessageTime.bmt_Short, false);
                            }
                            else
                            {
                                Update(ref oForm);
                                Conexao.uiApplication.SetStatusBarMessage("Lançamento altrado com sucesso", BoMessageTime.bmt_Short, false);
                            }
                        }
                        #endregion
                        break;
                    case BoEventTypes.et_KEY_DOWN:
                        break;
                    case BoEventTypes.et_LOST_FOCUS:
                        break;
                    case BoEventTypes.et_MATRIX_COLLAPSE_PRESSED:
                        break;
                    case BoEventTypes.et_MATRIX_LINK_PRESSED:
                        break;
                    case BoEventTypes.et_MATRIX_LOAD:
                        break;
                    case BoEventTypes.et_MENU_CLICK:
                        break;
                    case BoEventTypes.et_PICKER_CLICKED:
                        break;
                    case BoEventTypes.et_PRINT:
                        break;
                    case BoEventTypes.et_PRINT_DATA:
                        break;
                    case BoEventTypes.et_PRINT_LAYOUT_KEY:
                        break;
                    case BoEventTypes.et_RIGHT_CLICK:
                        break;
                    case BoEventTypes.et_UDO_FORM_BUILD:
                        break;
                    case BoEventTypes.et_UDO_FORM_OPEN:
                        break;
                    case BoEventTypes.et_VALIDATE:
                        break;
                    default:
                        break;
                }
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region SAVE
        private static void Save(ref SAPbouiCOM.Form oForm)
        {
            try
            {
                SqlCommand command = new SqlCommand(@"INSERT INTO [dbo].[DIME_DadosQuadro12]
                                                                   ([IdDeclaracao]
                                                                   ,[Ind_Org_Recol]
                                                                   ,[Cod_Receita]
                                                                   ,[Dt_Vcto_Recol]
                                                                   ,[Vl_Recol]
                                                                   ,[Cod_Class_Vcto]
                                                                   ,[Nro_Acordo])
                                                             VALUES
                                                                   (@IdDeclaracao
                                                                   ,@Ind_Org_Recol
                                                                   ,@Cod_Receita
                                                                   ,@Dt_Vcto_Recol
                                                                   ,@Vl_Recol
                                                                   ,@Cod_Class_Vcto
                                                                   ,@Nro_Acordo)", Support.Conectar());
                command.Parameters.AddWithValue("@IdDeclaracao", frmfrmCadDeclarDime.IdDeclaracao);
                command.Parameters.AddWithValue("@Ind_Org_Recol", oForm.DataSources.UserDataSources.Item("Origem").Value);
                command.Parameters.AddWithValue("@Cod_Receita", oForm.DataSources.UserDataSources.Item("codRec").Value);
                command.Parameters.AddWithValue("@Dt_Vcto_Recol", Convert.ToDateTime(oForm.DataSources.UserDataSources.Item("dtVenc").Value));
                command.Parameters.AddWithValue("@Vl_Recol", Convert.ToDouble(oForm.DataSources.UserDataSources.Item("valor").Value));
                command.Parameters.AddWithValue("@Cod_Class_Vcto", oForm.DataSources.UserDataSources.Item("ClassVenc").Value);
                command.Parameters.AddWithValue("@Nro_Acordo", oForm.DataSources.UserDataSources.Item("numAcordo").Value);
                command.ExecuteNonQuery();

                oForm.Close();

                frmfrmDiscrPagtoImp.oFormPrincipal.Freeze(true);
                frmfrmDiscrPagtoImp.CarregaGrid(ref frmfrmDiscrPagtoImp.oFormPrincipal);
                frmfrmDiscrPagtoImp.oFormPrincipal.Freeze(false);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region UPDATE
        private static void Update(ref SAPbouiCOM.Form oForm)
        {
            try
            {
                string sql = string.Format(@"UPDATE [dbo].[DIME_DadosQuadro12]
                                                   SET [Ind_Org_Recol] = @Ind_Org_Recol
                                                      ,[Cod_Receita] = @Cod_Receita
                                                      ,[Dt_Vcto_Recol] = @Dt_Vcto_Recol
                                                      ,[Vl_Recol] = @Vl_Recol
                                                      ,[Cod_Class_Vcto] = @Cod_Class_Vcto
                                                      ,[Nro_Acordo] = @Nro_Acordo
                                                 WHERE [IdLancamento] = '{0}'", Id);
                SqlCommand command = new SqlCommand(sql, Support.Conectar());
                command.Parameters.AddWithValue("@Ind_Org_Recol", oForm.DataSources.UserDataSources.Item("Origem").Value);
                command.Parameters.AddWithValue("@Cod_Receita", oForm.DataSources.UserDataSources.Item("codRec").Value);
                command.Parameters.AddWithValue("@Dt_Vcto_Recol", oForm.DataSources.UserDataSources.Item("dtVenc").Value);
                command.Parameters.AddWithValue("@Vl_Recol", Convert.ToDouble(oForm.DataSources.UserDataSources.Item("valor").Value));
                command.Parameters.AddWithValue("@Cod_Class_Vcto", oForm.DataSources.UserDataSources.Item("ClassVenc").Value);
                command.Parameters.AddWithValue("@Nro_Acordo", oForm.DataSources.UserDataSources.Item("numAcordo").Value);
                command.ExecuteNonQuery();

                oForm.Close();

                frmfrmDiscrPagtoImp.oFormPrincipal.Freeze(true);
                frmfrmDiscrPagtoImp.CarregaGrid(ref frmfrmDiscrPagtoImp.oFormPrincipal);
                frmfrmDiscrPagtoImp.oFormPrincipal.Freeze(false);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region OUTROS EVENTOS
        public bool MenuEvent()
        {
            throw new NotImplementedException();
        }

        public bool FormDataEvent()
        {
            throw new NotImplementedException();
        }

        public bool AppEvent()
        {
            throw new NotImplementedException();
        }

        public bool PrintEvent()
        {
            throw new NotImplementedException();
        }

        public bool ProgressBarEvent()
        {
            throw new NotImplementedException();
        }

        public bool ReportDataEvent()
        {
            throw new NotImplementedException();
        }

        public bool RightClickEvent()
        {
            throw new NotImplementedException();
        }

        public bool StatusBarEvent()
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
