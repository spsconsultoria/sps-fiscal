﻿using SAPbouiCOM;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPS_Fiscal.Addon
{
    class frmfrmValFiscEnt : IForm
    {
        public static int Id;
        private ItemEvent _eventoItem;
        public frmfrmValFiscEnt(ItemEvent evento)
        {
            this._eventoItem = evento;
        }

        public bool ItemEvent()
        {
            try
            {
                switch (_eventoItem.EventType)
                {
                    case BoEventTypes.et_ALL_EVENTS:
                        break;
                    case BoEventTypes.et_B1I_SERVICE_COMPLETE:
                        break;
                    case BoEventTypes.et_CHOOSE_FROM_LIST:
                        break;
                    case BoEventTypes.et_CLICK:
                        break;
                    case BoEventTypes.et_COMBO_SELECT:
                        break;
                    case BoEventTypes.et_DATASOURCE_LOAD:
                        break;
                    case BoEventTypes.et_DOUBLE_CLICK:
                        break;
                    case BoEventTypes.et_Drag:
                        break;
                    case BoEventTypes.et_EDIT_REPORT:
                        break;
                    case BoEventTypes.et_FORMAT_SEARCH_COMPLETED:
                        break;
                    case BoEventTypes.et_FORM_ACTIVATE:
                        break;
                    case BoEventTypes.et_FORM_CLOSE:
                        break;
                    case BoEventTypes.et_FORM_DATA_ADD:
                        break;
                    case BoEventTypes.et_FORM_DATA_DELETE:
                        break;
                    case BoEventTypes.et_FORM_DATA_LOAD:
                        break;
                    case BoEventTypes.et_FORM_DATA_UPDATE:
                        break;
                    case BoEventTypes.et_FORM_DEACTIVATE:
                        break;
                    case BoEventTypes.et_FORM_DRAW:
                        break;
                    case BoEventTypes.et_FORM_KEY_DOWN:
                        break;
                    case BoEventTypes.et_FORM_LOAD:
                        break;
                    case BoEventTypes.et_FORM_MENU_HILIGHT:
                        break;
                    case BoEventTypes.et_FORM_RESIZE:
                        break;
                    case BoEventTypes.et_FORM_UNLOAD:
                        break;
                    case BoEventTypes.et_FORM_VISIBLE:
                        break;
                    case BoEventTypes.et_GOT_FOCUS:
                        break;
                    case BoEventTypes.et_GRID_SORT:
                        break;
                    case BoEventTypes.et_ITEM_PRESSED:
                        #region BOTÃO CADASTRAR
                        if (!_eventoItem.Before_Action && _eventoItem.ItemUID == "btnOk")
                        {
                            SAPbouiCOM.Form oForm = Conexao.uiApplication.Forms.Item(_eventoItem.FormUID);

                            Grid oGrid = (Grid)oForm.Items.Item("grd").Specific;

                            if (oGrid.Rows.Count > 1)
                            {
                                for (int i = 0; i < oGrid.Rows.Count; i++)
                                {
                                    Update(ref oForm, i);
                                }
                            }

                            oForm.Close();
                        }
                        #endregion
                        break;
                    case BoEventTypes.et_KEY_DOWN:
                        break;
                    case BoEventTypes.et_LOST_FOCUS:
                        break;
                    case BoEventTypes.et_MATRIX_COLLAPSE_PRESSED:
                        break;
                    case BoEventTypes.et_MATRIX_LINK_PRESSED:
                        break;
                    case BoEventTypes.et_MATRIX_LOAD:
                        break;
                    case BoEventTypes.et_MENU_CLICK:
                        break;
                    case BoEventTypes.et_PICKER_CLICKED:
                        break;
                    case BoEventTypes.et_PRINT:
                        break;
                    case BoEventTypes.et_PRINT_DATA:
                        break;
                    case BoEventTypes.et_PRINT_LAYOUT_KEY:
                        break;
                    case BoEventTypes.et_RIGHT_CLICK:
                        break;
                    case BoEventTypes.et_UDO_FORM_BUILD:
                        break;
                    case BoEventTypes.et_UDO_FORM_OPEN:
                        break;
                    case BoEventTypes.et_VALIDATE:
                        break;
                    default:
                        break;
                }
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region UPDATE
        private static void Update(ref SAPbouiCOM.Form oForm, int i)
        {
            try
            {
                string sql = string.Format(@"UPDATE [dbo].[DIME_DadosQuadro01]
                                                    SET [Vl_Difal] = @Vl_Difal
                                                 WHERE [IdLancamento] = '{0}'", frmfrmCadDeclarDime.IdDeclaracao);
                SqlCommand command = new SqlCommand(sql, Support.Conectar());
                command.Parameters.AddWithValue("@Vl_Difal", Convert.ToDouble(oForm.DataSources.DataTables.Item("grd").GetValue("Débito DIFAL",i)));
                command.ExecuteNonQuery();
                oForm.Close();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region OUTROS EVENTOS
        public bool MenuEvent()
        {
            throw new NotImplementedException();
        }

        public bool FormDataEvent()
        {
            throw new NotImplementedException();
        }

        public bool AppEvent()
        {
            throw new NotImplementedException();
        }

        public bool PrintEvent()
        {
            throw new NotImplementedException();
        }

        public bool ProgressBarEvent()
        {
            throw new NotImplementedException();
        }

        public bool ReportDataEvent()
        {
            throw new NotImplementedException();
        }

        public bool RightClickEvent()
        {
            throw new NotImplementedException();
        }

        public bool StatusBarEvent()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region CARREGA GRID 22
        public static void CarregaGrid(ref SAPbouiCOM.Form oForm)
        {
            try
            {
                string sql = string.Format(@"SELECT [IdLancamento]
                                                  ,[IdDeclaracao]
                                                  ,[CFOP]
                                                  ,[Vl_Contabil] as 'Valor Contábil'
                                                  ,[Vl_Base_Calc] as 'Base Cálculo'
                                                  ,[Vl_Imp_Cred] as 'imposto Creditado'
                                                  ,[Vl_Isentas_Nao_Trib] as 'Isentas/Não tributadas' 
                                                  ,[Vl_Outras] as 'Outras'
                                                  ,[Vl_Base_Calc_IR] as 'BC.Imposto Retido'
                                                  ,[Vl_IR] as 'Imposto Retido'
                                                  ,[Vl_Difal] as 'Débito DIFAL'
                                              FROM [SPS_Fiscal].[dbo].[DIME_DadosQuadro01]
                                              WHERE IdDeclaracao = {0}", frmfrmCadDeclarDime.IdDeclaracao);
                oForm.DataSources.DataTables.Item("grd").ExecuteQuery(sql);
                ((Grid)oForm.Items.Item("grd").Specific).Columns.Item(0).Visible = false;
                ((Grid)oForm.Items.Item("grd").Specific).Columns.Item(1).Visible = false;
                ((Grid)oForm.Items.Item("grd").Specific).Columns.Item("CFOP").Editable = false;
                ((Grid)oForm.Items.Item("grd").Specific).Columns.Item("Valor Contábil").Editable = false;
                ((Grid)oForm.Items.Item("grd").Specific).Columns.Item("Base Cálculo").Editable = false;
                ((Grid)oForm.Items.Item("grd").Specific).Columns.Item("imposto Creditado").Editable = false;
                ((Grid)oForm.Items.Item("grd").Specific).Columns.Item("Isentas/Não tributadas").Editable = false;
                ((Grid)oForm.Items.Item("grd").Specific).Columns.Item("Outras").Editable = false;
                ((Grid)oForm.Items.Item("grd").Specific).Columns.Item("BC.Imposto Retido").Editable = false;
                ((Grid)oForm.Items.Item("grd").Specific).Columns.Item("Imposto Retido").Editable = false;
                ((Grid)oForm.Items.Item("grd").Specific).Columns.Item("Débito DIFAL").Editable = true;
                ((Grid)oForm.Items.Item("grd").Specific).AutoResizeColumns();
                ((Grid)oForm.Items.Item("grd").Specific).SelectionMode = BoMatrixSelect.ms_Auto;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion
    }
}
