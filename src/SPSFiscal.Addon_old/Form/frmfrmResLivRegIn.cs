﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SAPbouiCOM;

namespace SPS_Fiscal.Addon
{
    class frmfrmResLivRegIn : IForm
    {
        private ItemEvent _eventoItem;
        public static string quadro;
        public frmfrmResLivRegIn(ItemEvent evento)
        {
            this._eventoItem = evento;
        }

        public bool ItemEvent()
        {
            try
            {
                switch (_eventoItem.EventType)
                {
                    case BoEventTypes.et_ALL_EVENTS:
                        break;
                    case BoEventTypes.et_ITEM_PRESSED:
                        #region BOTÃO CADASTRAR

                        if (!_eventoItem.Before_Action && _eventoItem.ItemUID == "1")
                        {
                            SAPbouiCOM.Form oForm = Conexao.uiApplication.Forms.Item(_eventoItem.FormUID);

                            if (quadro == "80")
                            {
                                SaveQuadro80(ref oForm);
                                CarregaGridQuadro80(ref oForm);
                            }
                            else if (quadro == "81")
                            {
                                SaveQuadro81(ref oForm);
                                CarregaGridQuadro81(ref oForm);
                            }
                            else if (quadro == "82")
                            {
                                SaveQuadro82(ref oForm);
                                CarregaGridQuadro82(ref oForm);
                            }
                            else if (quadro == "83")
                            {
                                SaveQuadro83(ref oForm);
                                CarregaGridQuadro83(ref oForm);
                            }
                            else if (quadro == "84")
                            {
                                SaveQuadro84(ref oForm);
                                CarregaGridQuadro84(ref oForm);
                            }
                            else if (quadro == "90")
                            {
                                SaveQuadro90(ref oForm);
                                CarregaGridQuadro90(ref oForm);
                            }
                            else if (quadro == "91")
                            {
                                SaveQuadro91(ref oForm);
                                CarregaGridQuadro91(ref oForm);
                            }
                            else if (quadro == "92")
                            {
                                SaveQuadro92(ref oForm);
                                CarregaGridQuadro92(ref oForm);
                            }
                            else if (quadro == "93")
                            {
                                SaveQuadro93(ref oForm);
                                CarregaGridQuadro93(ref oForm);
                            }
                            else
                            {
                                SaveQuadro94(ref oForm);
                                CarregaGridQuadro94(ref oForm);
                            }
                            Conexao.uiApplication.SetStatusBarMessage("Informações salvas com sucesso.", BoMessageTime.bmt_Short, false);
                        }
                        #endregion
                        break;
                    case BoEventTypes.et_KEY_DOWN:
                        break;
                    case BoEventTypes.et_GOT_FOCUS:
                        break;
                    case BoEventTypes.et_LOST_FOCUS:
                        break;
                    case BoEventTypes.et_COMBO_SELECT:
                        break;
                    case BoEventTypes.et_CLICK:
                        break;
                    case BoEventTypes.et_DOUBLE_CLICK:
                        break;
                    case BoEventTypes.et_MATRIX_LINK_PRESSED:
                        break;
                    case BoEventTypes.et_MATRIX_COLLAPSE_PRESSED:
                        break;
                    case BoEventTypes.et_VALIDATE:
                        break;
                    case BoEventTypes.et_MATRIX_LOAD:
                        break;
                    case BoEventTypes.et_DATASOURCE_LOAD:
                        break;
                    case BoEventTypes.et_FORM_LOAD:
                        break;
                    case BoEventTypes.et_FORM_UNLOAD:
                        break;
                    case BoEventTypes.et_FORM_ACTIVATE:
                        break;
                    case BoEventTypes.et_FORM_DEACTIVATE:
                        break;
                    case BoEventTypes.et_FORM_CLOSE:
                        break;
                    case BoEventTypes.et_FORM_RESIZE:
                        break;
                    case BoEventTypes.et_FORM_KEY_DOWN:
                        break;
                    case BoEventTypes.et_FORM_MENU_HILIGHT:
                        break;
                    case BoEventTypes.et_PRINT:
                        break;
                    case BoEventTypes.et_PRINT_DATA:
                        break;
                    case BoEventTypes.et_EDIT_REPORT:
                        break;
                    case BoEventTypes.et_CHOOSE_FROM_LIST:
                        break;
                    case BoEventTypes.et_RIGHT_CLICK:
                        break;
                    case BoEventTypes.et_MENU_CLICK:
                        break;
                    case BoEventTypes.et_FORM_DATA_ADD:
                        break;
                    case BoEventTypes.et_FORM_DATA_UPDATE:
                        break;
                    case BoEventTypes.et_FORM_DATA_DELETE:
                        break;
                    case BoEventTypes.et_FORM_DATA_LOAD:
                        break;
                    case BoEventTypes.et_PICKER_CLICKED:
                        break;
                    case BoEventTypes.et_GRID_SORT:
                        break;
                    case BoEventTypes.et_Drag:
                        break;
                    case BoEventTypes.et_FORM_DRAW:
                        break;
                    case BoEventTypes.et_UDO_FORM_BUILD:
                        break;
                    case BoEventTypes.et_UDO_FORM_OPEN:
                        break;
                    case BoEventTypes.et_B1I_SERVICE_COMPLETE:
                        break;
                    case BoEventTypes.et_FORMAT_SEARCH_COMPLETED:
                        break;
                    case BoEventTypes.et_PRINT_LAYOUT_KEY:
                        break;
                    case BoEventTypes.et_FORM_VISIBLE:
                        break;
                    case BoEventTypes.et_ITEM_WEBMESSAGE:
                        break;
                    default:
                        break;
                }

                return true;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        #region CARREGA GRID
        public static void CarregaGridQuadro80(ref SAPbouiCOM.Form oForm)
        {
            try
            {
                string sql = string.Format(@"DECLARE @LINHAS int = case when (select count(*) from [SPS_Fiscal].[dbo].[DIME_DadosQuadro80] where IdDeclaracao = {0}) <> 0 
                                            Then (select count(*) from [SPS_Fiscal].[dbo].[DIME_DadosQuadro80] where IdDeclaracao = {0}) else 0 end

                                            IF @LINHAS = 0
                                            INSERT INTO [SPS_Fiscal].[dbo].[DIME_DadosQuadro80] (NumeroItem, IdDeclaracao, Valor)
                                            select [NumeroItem], {0},0.0 from [SPS_Fiscal].[dbo].[DIME_TabelaQuadro80]

                                            SELECT T0.[NumeroItem]
                                                ,T0.[Descricao]
	                                            ,T1.[Valor]
                                            FROM [SPS_Fiscal].[dbo].[DIME_TabelaQuadro80] T0
                                            left JOIN [SPS_Fiscal].[dbo].[DIME_DadosQuadro80] T1 ON T0.NumeroItem = T1.NumeroItem
                                            WHERE T0.NumeroItem in (010,020,030) AND T1.IdDeclaracao = {0}", frmfrmCadDeclarDime.IdDeclaracao);
                oForm.DataSources.DataTables.Item("grd").ExecuteQuery(sql);
                ((Grid)oForm.Items.Item("grd").Specific).AutoResizeColumns();
                ((Grid)oForm.Items.Item("grd").Specific).Columns.Item("NumeroItem").Editable = false;
                ((Grid)oForm.Items.Item("grd").Specific).Columns.Item("Descricao").Editable = false;
                ((Grid)oForm.Items.Item("grd").Specific).Columns.Item("Valor").Editable = true;
                ((Grid)oForm.Items.Item("grd").Specific).SelectionMode = BoMatrixSelect.ms_Auto;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public static void CarregaGridQuadro81(ref SAPbouiCOM.Form oForm)
        {
            try
            {
                string sql = string.Format(@"DECLARE @LINHAS int = case when (select count(*) from [SPS_Fiscal].[dbo].[DIME_DadosQuadro81] where IdDeclaracao = {0}) <> 0 
                                            Then (select count(*) from [SPS_Fiscal].[dbo].[DIME_DadosQuadro81] where IdDeclaracao = {0}) else 0 end

                                            IF @LINHAS = 0
                                            INSERT INTO [SPS_Fiscal].[dbo].[DIME_DadosQuadro81] (NumeroItem, IdDeclaracao, Valor)
                                            select [NumeroItem], {0},0.0 from [SPS_Fiscal].[dbo].[DIME_TabelaQuadro81]

                                            SELECT T0.[NumeroItem]
                                                ,T0.[Descricao]
	                                            ,T1.[Valor]
                                            FROM [SPS_Fiscal].[dbo].[DIME_TabelaQuadro81] T0
                                            left JOIN [SPS_Fiscal].[dbo].[DIME_DadosQuadro81] T1 ON T0.NumeroItem = T1.NumeroItem
                                            WHERE T1.IdDeclaracao = {0}", frmfrmCadDeclarDime.IdDeclaracao);
                oForm.DataSources.DataTables.Item("grd").ExecuteQuery(sql);
                ((Grid)oForm.Items.Item("grd").Specific).AutoResizeColumns();
                ((Grid)oForm.Items.Item("grd").Specific).Columns.Item("NumeroItem").Editable = false;
                ((Grid)oForm.Items.Item("grd").Specific).Columns.Item("Descricao").Editable = false;
                ((Grid)oForm.Items.Item("grd").Specific).Columns.Item("Valor").Editable = true;
                ((Grid)oForm.Items.Item("grd").Specific).SelectionMode = BoMatrixSelect.ms_Auto;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public static void CarregaGridQuadro82(ref SAPbouiCOM.Form oForm)
        {
            try
            {
                string sql = string.Format(@"DECLARE @LINHAS int = case when (select count(*) from [SPS_Fiscal].[dbo].[DIME_DadosQuadro82] where IdDeclaracao = {0}) <> 0 
                                            Then (select count(*) from [SPS_Fiscal].[dbo].[DIME_DadosQuadro82] where IdDeclaracao = {0}) else 0 end

                                            IF @LINHAS = 0
                                            INSERT INTO [SPS_Fiscal].[dbo].[DIME_DadosQuadro82] (NumeroItem, IdDeclaracao, Valor)
                                            select [NumeroItem], {0},0.0 from [SPS_Fiscal].[dbo].[DIME_TabelaQuadro82]

                                            SELECT T0.[NumeroItem]
                                                ,T0.[Descricao]
	                                            ,T1.[Valor]
                                            FROM [SPS_Fiscal].[dbo].[DIME_TabelaQuadro82] T0
                                            left JOIN [SPS_Fiscal].[dbo].[DIME_DadosQuadro82] T1 ON T0.NumeroItem = T1.NumeroItem
                                            WHERE T1.IdDeclaracao = {0}", frmfrmCadDeclarDime.IdDeclaracao);
                oForm.DataSources.DataTables.Item("grd").ExecuteQuery(sql);
                ((Grid)oForm.Items.Item("grd").Specific).AutoResizeColumns();
                ((Grid)oForm.Items.Item("grd").Specific).Columns.Item("NumeroItem").Editable = false;
                ((Grid)oForm.Items.Item("grd").Specific).Columns.Item("Descricao").Editable = false;
                ((Grid)oForm.Items.Item("grd").Specific).Columns.Item("Valor").Editable = true;
                ((Grid)oForm.Items.Item("grd").Specific).SelectionMode = BoMatrixSelect.ms_Auto;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public static void CarregaGridQuadro83(ref SAPbouiCOM.Form oForm)
        {
            try
            {
                string sql = string.Format(@"DECLARE @LINHAS int = case when (select count(*) from [SPS_Fiscal].[dbo].[DIME_DadosQuadro83] where IdDeclaracao = {0}) <> 0 
                                            Then (select count(*) from [SPS_Fiscal].[dbo].[DIME_DadosQuadro83] where IdDeclaracao = {0}) else 0 end

                                            IF @LINHAS = 0
                                            INSERT INTO [SPS_Fiscal].[dbo].[DIME_DadosQuadro83] (NumeroItem, IdDeclaracao, Valor)
                                            select [NumeroItem], {0},0.0 from [SPS_Fiscal].[dbo].[DIME_TabelaQuadro83]

                                            SELECT T0.[NumeroItem]
                                                ,T0.[Descricao]
	                                            ,T1.[Valor]
                                            FROM [SPS_Fiscal].[dbo].[DIME_TabelaQuadro83] T0
                                            left JOIN [SPS_Fiscal].[dbo].[DIME_DadosQuadro83] T1 ON T0.NumeroItem = T1.NumeroItem
                                            WHERE T1.IdDeclaracao = {0}", frmfrmCadDeclarDime.IdDeclaracao);
                oForm.DataSources.DataTables.Item("grd").ExecuteQuery(sql);
                ((Grid)oForm.Items.Item("grd").Specific).AutoResizeColumns();
                ((Grid)oForm.Items.Item("grd").Specific).Columns.Item("NumeroItem").Editable = false;
                ((Grid)oForm.Items.Item("grd").Specific).Columns.Item("Descricao").Editable = false;
                ((Grid)oForm.Items.Item("grd").Specific).Columns.Item("Valor").Editable = true;
                ((Grid)oForm.Items.Item("grd").Specific).SelectionMode = BoMatrixSelect.ms_Auto;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public static void CarregaGridQuadro84(ref SAPbouiCOM.Form oForm)
        {
            try
            {
                string sql = string.Format(@"DECLARE @LINHAS int = case when (select count(*) from [SPS_Fiscal].[dbo].[DIME_DadosQuadro84] where IdDeclaracao = {0}) <> 0 
                                            Then (select count(*) from [SPS_Fiscal].[dbo].[DIME_DadosQuadro84] where IdDeclaracao = {0}) else 0 end

                                            IF @LINHAS = 0
                                            INSERT INTO [SPS_Fiscal].[dbo].[DIME_DadosQuadro84] (NumeroItem, IdDeclaracao, Valor)
                                            select [NumeroItem], {0},0.0 from [SPS_Fiscal].[dbo].[DIME_TabelaQuadro84]

                                            SELECT T0.[NumeroItem]
                                                ,T0.[Descricao]
	                                            ,T1.[Valor]
                                            FROM [SPS_Fiscal].[dbo].[DIME_TabelaQuadro84] T0
                                            left JOIN [SPS_Fiscal].[dbo].[DIME_DadosQuadro84] T1 ON T0.NumeroItem = T1.NumeroItem
                                            WHERE T1.IdDeclaracao = {0}", frmfrmCadDeclarDime.IdDeclaracao);
                oForm.DataSources.DataTables.Item("grd").ExecuteQuery(sql);
                ((Grid)oForm.Items.Item("grd").Specific).AutoResizeColumns();
                ((Grid)oForm.Items.Item("grd").Specific).Columns.Item("NumeroItem").Editable = false;
                ((Grid)oForm.Items.Item("grd").Specific).Columns.Item("Descricao").Editable = false;
                ((Grid)oForm.Items.Item("grd").Specific).Columns.Item("Valor").Editable = true;
                ((Grid)oForm.Items.Item("grd").Specific).SelectionMode = BoMatrixSelect.ms_Auto;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public static void CarregaGridQuadro90(ref SAPbouiCOM.Form oForm)
        {
            try
            {
                string sql = string.Format(@"DECLARE @LINHAS int = case when (select count(*) from [SPS_Fiscal].[dbo].[DIME_DadosQuadro90] where IdDeclaracao = {0}) <> 0 
                                            Then (select count(*) from [SPS_Fiscal].[dbo].[DIME_DadosQuadro90] where IdDeclaracao = {0}) else 0 end

                                            IF @LINHAS = 0
                                            INSERT INTO [SPS_Fiscal].[dbo].[DIME_DadosQuadro90] (NumeroItem, IdDeclaracao, Valor)
                                            select [NumeroItem], {0},0.0 from [SPS_Fiscal].[dbo].[DIME_TabelaQuadro90]

                                            SELECT T0.[NumeroItem]
                                                ,T0.[Descricao]
	                                            ,T1.[Valor]
                                            FROM [SPS_Fiscal].[dbo].[DIME_TabelaQuadro90] T0
                                            left JOIN [SPS_Fiscal].[dbo].[DIME_DadosQuadro90] T1 ON T0.NumeroItem = T1.NumeroItem
                                            WHERE T1.IdDeclaracao = {0}", frmfrmCadDeclarDime.IdDeclaracao);
                oForm.DataSources.DataTables.Item("grd").ExecuteQuery(sql);
                ((Grid)oForm.Items.Item("grd").Specific).AutoResizeColumns();
                ((Grid)oForm.Items.Item("grd").Specific).Columns.Item("NumeroItem").Editable = false;
                ((Grid)oForm.Items.Item("grd").Specific).Columns.Item("Descricao").Editable = false;
                ((Grid)oForm.Items.Item("grd").Specific).Columns.Item("Valor").Editable = true;
                ((Grid)oForm.Items.Item("grd").Specific).SelectionMode = BoMatrixSelect.ms_Auto;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public static void CarregaGridQuadro91(ref SAPbouiCOM.Form oForm)
        {
            try
            {
                string sql = string.Format(@"DECLARE @LINHAS int = case when (select count(*) from [SPS_Fiscal].[dbo].[DIME_DadosQuadro91] where IdDeclaracao = {0}) <> 0 
                                            Then (select count(*) from [SPS_Fiscal].[dbo].[DIME_DadosQuadro91] where IdDeclaracao = {0}) else 0 end

                                            IF @LINHAS = 0
                                            INSERT INTO [SPS_Fiscal].[dbo].[DIME_DadosQuadro91] (NumeroItem, IdDeclaracao, Valor)
                                            select [NumeroItem], {0},0.0 from [SPS_Fiscal].[dbo].[DIME_TabelaQuadro91]

                                            SELECT T0.[NumeroItem]
                                                ,T0.[Descricao]
	                                            ,T1.[Valor]
                                            FROM [SPS_Fiscal].[dbo].[DIME_TabelaQuadro91] T0
                                            left JOIN [SPS_Fiscal].[dbo].[DIME_DadosQuadro91] T1 ON T0.NumeroItem = T1.NumeroItem
                                            WHERE T1.IdDeclaracao = {0}", frmfrmCadDeclarDime.IdDeclaracao);
                oForm.DataSources.DataTables.Item("grd").ExecuteQuery(sql);
                ((Grid)oForm.Items.Item("grd").Specific).AutoResizeColumns();
                ((Grid)oForm.Items.Item("grd").Specific).Columns.Item("NumeroItem").Editable = false;
                ((Grid)oForm.Items.Item("grd").Specific).Columns.Item("Descricao").Editable = false;
                ((Grid)oForm.Items.Item("grd").Specific).Columns.Item("Valor").Editable = true;
                ((Grid)oForm.Items.Item("grd").Specific).SelectionMode = BoMatrixSelect.ms_Auto;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public static void CarregaGridQuadro92(ref SAPbouiCOM.Form oForm)
        {
            try
            {
                string sql = string.Format(@"DECLARE @LINHAS int = case when (select count(*) from [SPS_Fiscal].[dbo].[DIME_DadosQuadro92] where IdDeclaracao = {0}) <> 0 
                                            Then (select count(*) from [SPS_Fiscal].[dbo].[DIME_DadosQuadro92] where IdDeclaracao = {0}) else 0 end

                                            IF @LINHAS = 0
                                            INSERT INTO [SPS_Fiscal].[dbo].[DIME_DadosQuadro92] (NumeroItem, IdDeclaracao, Valor)
                                            select [NumeroItem], {0},0.0 from [SPS_Fiscal].[dbo].[DIME_TabelaQuadro92]

                                            SELECT T0.[NumeroItem]
                                                ,T0.[Descricao]
	                                            ,T1.[Valor]
                                            FROM [SPS_Fiscal].[dbo].[DIME_TabelaQuadro92] T0
                                            left JOIN [SPS_Fiscal].[dbo].[DIME_DadosQuadro92] T1 ON T0.NumeroItem = T1.NumeroItem
                                            WHERE T1.IdDeclaracao = {0}", frmfrmCadDeclarDime.IdDeclaracao);
                oForm.DataSources.DataTables.Item("grd").ExecuteQuery(sql);
                ((Grid)oForm.Items.Item("grd").Specific).AutoResizeColumns();
                ((Grid)oForm.Items.Item("grd").Specific).Columns.Item("NumeroItem").Editable = false;
                ((Grid)oForm.Items.Item("grd").Specific).Columns.Item("Descricao").Editable = false;
                ((Grid)oForm.Items.Item("grd").Specific).Columns.Item("Valor").Editable = true;
                ((Grid)oForm.Items.Item("grd").Specific).SelectionMode = BoMatrixSelect.ms_Auto;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public static void CarregaGridQuadro93(ref SAPbouiCOM.Form oForm)
        {
            try
            {
                string sql = string.Format(@"DECLARE @LINHAS int = case when (select count(*) from [SPS_Fiscal].[dbo].[DIME_DadosQuadro93] where IdDeclaracao = {0}) <> 0 
                                            Then (select count(*) from [SPS_Fiscal].[dbo].[DIME_DadosQuadro93] where IdDeclaracao = {0}) else 0 end

                                            IF @LINHAS = 0
                                            INSERT INTO [SPS_Fiscal].[dbo].[DIME_DadosQuadro93] (NumeroItem, IdDeclaracao, Valor)
                                            select [NumeroItem], {0},0.0 from [SPS_Fiscal].[dbo].[DIME_TabelaQuadro93]

                                            SELECT T0.[NumeroItem]
                                                ,T0.[Descricao]
	                                            ,T1.[Valor]
                                            FROM [SPS_Fiscal].[dbo].[DIME_TabelaQuadro93] T0
                                            left JOIN [SPS_Fiscal].[dbo].[DIME_DadosQuadro93] T1 ON T0.NumeroItem = T1.NumeroItem
                                            WHERE T1.IdDeclaracao = {0}", frmfrmCadDeclarDime.IdDeclaracao);
                oForm.DataSources.DataTables.Item("grd").ExecuteQuery(sql);
                ((Grid)oForm.Items.Item("grd").Specific).AutoResizeColumns();
                ((Grid)oForm.Items.Item("grd").Specific).Columns.Item("NumeroItem").Editable = false;
                ((Grid)oForm.Items.Item("grd").Specific).Columns.Item("Descricao").Editable = false;
                ((Grid)oForm.Items.Item("grd").Specific).Columns.Item("Valor").Editable = true;
                ((Grid)oForm.Items.Item("grd").Specific).SelectionMode = BoMatrixSelect.ms_Auto;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public static void CarregaGridQuadro94(ref SAPbouiCOM.Form oForm)
        {
            try
            {
                string sql = string.Format(@"DECLARE @LINHAS int = case when (select count(*) from [SPS_Fiscal].[dbo].[DIME_DadosQuadro94] where IdDeclaracao = {0}) <> 0 
                                            Then (select count(*) from [SPS_Fiscal].[dbo].[DIME_DadosQuadro94] where IdDeclaracao = {0}) else 0 end

                                            IF @LINHAS = 0
                                            INSERT INTO [SPS_Fiscal].[dbo].[DIME_DadosQuadro94] (NumeroItem, IdDeclaracao, Valor)
                                            select [NumeroItem], {0},0.0 from [SPS_Fiscal].[dbo].[DIME_TabelaQuadro94]

                                            SELECT T0.[NumeroItem]
                                                ,T0.[Descricao]
	                                            ,T1.[Valor]
                                            FROM [SPS_Fiscal].[dbo].[DIME_TabelaQuadro94] T0
                                            left JOIN [SPS_Fiscal].[dbo].[DIME_DadosQuadro94] T1 ON T0.NumeroItem = T1.NumeroItem
                                            WHERE T1.IdDeclaracao = {0}", frmfrmCadDeclarDime.IdDeclaracao);
                oForm.DataSources.DataTables.Item("grd").ExecuteQuery(sql);
                ((Grid)oForm.Items.Item("grd").Specific).AutoResizeColumns();
                ((Grid)oForm.Items.Item("grd").Specific).Columns.Item("NumeroItem").Editable = false;
                ((Grid)oForm.Items.Item("grd").Specific).Columns.Item("Descricao").Editable = false;
                ((Grid)oForm.Items.Item("grd").Specific).Columns.Item("Valor").Editable = true;
                ((Grid)oForm.Items.Item("grd").Specific).SelectionMode = BoMatrixSelect.ms_Auto;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region SAVE

        private void SaveQuadro80(ref Form oForm)
        {
            Grid oGrid = (Grid)oForm.Items.Item("grd").Specific;

            try
            {
                for (int i = 0; i < oGrid.Rows.Count; i++)
                {
                    string sql = string.Format(@"UPDATE [dbo].[DIME_DadosQuadro80] SET [Valor] = @Valor
                                                 WHERE [NumeroItem] = '{0}'", oForm.DataSources.DataTables.Item("grd").GetValue("NumeroItem", i));
                    SqlCommand command = new SqlCommand(sql, Support.Conectar());
                    command.Parameters.AddWithValue("@Valor", Convert.ToDouble(oForm.DataSources.DataTables.Item("grd").GetValue("Valor", i)));
                    command.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        private void SaveQuadro81(ref Form oForm)
        {
            Grid oGrid = (Grid)oForm.Items.Item("grd").Specific;

            try
            {
                for (int i = 0; i < oGrid.Rows.Count; i++)
                {
                    string sql = string.Format(@"UPDATE [dbo].[DIME_DadosQuadro81] SET [Valor] = @Valor
                                                 WHERE [NumeroItem] = '{0}'", oForm.DataSources.DataTables.Item("grd").GetValue("NumeroItem", i));
                    SqlCommand command = new SqlCommand(sql, Support.Conectar());
                    command.Parameters.AddWithValue("@Valor", Convert.ToDouble(oForm.DataSources.DataTables.Item("grd").GetValue("Valor", i)));
                    command.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        private void SaveQuadro82(ref Form oForm)
        {
            Grid oGrid = (Grid)oForm.Items.Item("grd").Specific;

            try
            {
                for (int i = 0; i < oGrid.Rows.Count; i++)
                {
                    string sql = string.Format(@"UPDATE [dbo].[DIME_DadosQuadro82] SET [Valor] = @Valor
                                                 WHERE [NumeroItem] = '{0}'", oForm.DataSources.DataTables.Item("grd").GetValue("NumeroItem", i));
                    SqlCommand command = new SqlCommand(sql, Support.Conectar());
                    command.Parameters.AddWithValue("@Valor", Convert.ToDouble(oForm.DataSources.DataTables.Item("grd").GetValue("Valor", i)));
                    command.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        private void SaveQuadro83(ref Form oForm)
        {
            Grid oGrid = (Grid)oForm.Items.Item("grd").Specific;

            try
            {
                for (int i = 0; i < oGrid.Rows.Count; i++)
                {
                    string sql = string.Format(@"UPDATE [dbo].[DIME_DadosQuadro83] SET [Valor] = @Valor
                                                 WHERE [NumeroItem] = '{0}'", oForm.DataSources.DataTables.Item("grd").GetValue("NumeroItem", i));
                    SqlCommand command = new SqlCommand(sql, Support.Conectar());
                    command.Parameters.AddWithValue("@Valor", Convert.ToDouble(oForm.DataSources.DataTables.Item("grd").GetValue("Valor", i)));
                    command.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        private void SaveQuadro84(ref Form oForm)
        {
            Grid oGrid = (Grid)oForm.Items.Item("grd").Specific;

            try
            {
                for (int i = 0; i < oGrid.Rows.Count; i++)
                {
                    string sql = string.Format(@"UPDATE [dbo].[DIME_DadosQuadro84] SET [Valor] = @Valor
                                                 WHERE [NumeroItem] = '{0}'", oForm.DataSources.DataTables.Item("grd").GetValue("NumeroItem", i));
                    SqlCommand command = new SqlCommand(sql, Support.Conectar());
                    command.Parameters.AddWithValue("@Valor", Convert.ToDouble(oForm.DataSources.DataTables.Item("grd").GetValue("Valor", i)));
                    command.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        private void SaveQuadro90(ref Form oForm)
        {
            Grid oGrid = (Grid)oForm.Items.Item("grd").Specific;

            try
            {
                for (int i = 0; i < oGrid.Rows.Count; i++)
                {
                    string sql = string.Format(@"UPDATE [dbo].[DIME_DadosQuadro90] SET [Valor] = @Valor
                                                 WHERE [NumeroItem] = '{0}'", oForm.DataSources.DataTables.Item("grd").GetValue("NumeroItem", i));
                    SqlCommand command = new SqlCommand(sql, Support.Conectar());
                    command.Parameters.AddWithValue("@Valor", Convert.ToDouble(oForm.DataSources.DataTables.Item("grd").GetValue("Valor", i)));
                    command.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        private void SaveQuadro91(ref Form oForm)
        {
            Grid oGrid = (Grid)oForm.Items.Item("grd").Specific;

            try
            {
                for (int i = 0; i < oGrid.Rows.Count; i++)
                {
                    string sql = string.Format(@"UPDATE [dbo].[DIME_DadosQuadro91] SET [Valor] = @Valor
                                                 WHERE [NumeroItem] = '{0}'", oForm.DataSources.DataTables.Item("grd").GetValue("NumeroItem", i));
                    SqlCommand command = new SqlCommand(sql, Support.Conectar());
                    command.Parameters.AddWithValue("@Valor", Convert.ToDouble(oForm.DataSources.DataTables.Item("grd").GetValue("Valor", i)));
                    command.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        private void SaveQuadro92(ref Form oForm)
        {
            Grid oGrid = (Grid)oForm.Items.Item("grd").Specific;

            try
            {
                for (int i = 0; i < oGrid.Rows.Count; i++)
                {
                    string sql = string.Format(@"UPDATE [dbo].[DIME_DadosQuadro92] SET [Valor] = @Valor
                                                 WHERE [NumeroItem] = '{0}'", oForm.DataSources.DataTables.Item("grd").GetValue("NumeroItem", i));
                    SqlCommand command = new SqlCommand(sql, Support.Conectar());
                    command.Parameters.AddWithValue("@Valor", Convert.ToDouble(oForm.DataSources.DataTables.Item("grd").GetValue("Valor", i)));
                    command.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        private void SaveQuadro93(ref Form oForm)
        {
            Grid oGrid = (Grid)oForm.Items.Item("grd").Specific;

            try
            {
                for (int i = 0; i < oGrid.Rows.Count; i++)
                {
                    string sql = string.Format(@"UPDATE [dbo].[DIME_DadosQuadro93] SET [Valor] = @Valor
                                                 WHERE [NumeroItem] = '{0}'", oForm.DataSources.DataTables.Item("grd").GetValue("NumeroItem", i));
                    SqlCommand command = new SqlCommand(sql, Support.Conectar());
                    command.Parameters.AddWithValue("@Valor", Convert.ToDouble(oForm.DataSources.DataTables.Item("grd").GetValue("Valor", i)));
                    command.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        private void SaveQuadro94(ref Form oForm)
        {
            Grid oGrid = (Grid)oForm.Items.Item("grd").Specific;

            try
            {
                for (int i = 0; i < oGrid.Rows.Count; i++)
                {
                    string sql = string.Format(@"UPDATE [dbo].[DIME_DadosQuadro94] SET [Valor] = @Valor
                                                 WHERE [NumeroItem] = '{0}'", oForm.DataSources.DataTables.Item("grd").GetValue("NumeroItem", i));
                    SqlCommand command = new SqlCommand(sql, Support.Conectar());
                    command.Parameters.AddWithValue("@Valor", Convert.ToDouble(oForm.DataSources.DataTables.Item("grd").GetValue("Valor", i)));
                    command.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region OUTROS EVENTOS

        public bool MenuEvent()
        {
            throw new NotImplementedException();
        }

        public bool FormDataEvent()
        {
            throw new NotImplementedException();
        }

        public bool AppEvent()
        {
            throw new NotImplementedException();
        }

        public bool PrintEvent()
        {
            throw new NotImplementedException();
        }

        public bool ProgressBarEvent()
        {
            throw new NotImplementedException();
        }

        public bool ReportDataEvent()
        {
            throw new NotImplementedException();
        }

        public bool RightClickEvent()
        {
            throw new NotImplementedException();
        }

        public bool StatusBarEvent()
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
