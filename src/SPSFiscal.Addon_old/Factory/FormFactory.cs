using System;
using System.Reflection;
using System.Collections.Generic;
using System.Text;
using SAPbouiCOM;

namespace SPS_Fiscal.Addon
{
    public class FormFactory
    {
        public static IForm CreateForm(Type form, ref ItemEvent evento)
        {
            ConstructorInfo constructor = form.GetConstructor(new Type[] { typeof(ItemEvent) });
            IForm newForm = null;

            try
            {
                newForm = (IForm)constructor.Invoke(new object[] { evento });
            }
            catch (NullReferenceException) { }
            catch (Exception ex) { throw ex; }

            return newForm;
        }
        public static IForm CreateForm(Type form, ref MenuEvent evento)
        {
            ConstructorInfo constructor = form.GetConstructor(new Type[] { typeof(MenuEvent) });
            IForm newForm = null;

            try
            {
                newForm = (IForm)constructor.Invoke(new object[] { evento });
            }
            catch (NullReferenceException) { }
            catch (Exception ex) { throw ex; }

            return newForm;
        }
        public static IForm CreateForm(Type form, ref BusinessObjectInfo evento)
        {
            ConstructorInfo constructor = form.GetConstructor(new Type[] { typeof(BusinessObjectInfo) });
            IForm newForm = null;

            try
            {
                newForm = (IForm)constructor.Invoke(new object[] { evento });
            }
            catch (NullReferenceException) { }
            catch (Exception ex) { throw ex; }

            return newForm;
        }

        public static IForm CreateForm(Type form, ref BoStatusBarMessageType evento)
        {
            ConstructorInfo constructor = form.GetConstructor(new Type[] { typeof(BoStatusBarMessageType) });
            IForm newForm = null;

            try
            {
                newForm = (IForm)constructor.Invoke(new object[] { evento });
            }
            catch (NullReferenceException) { }
            catch (Exception ex) { throw ex; }

            return newForm;
        }
        public static IForm CreateForm(Type form, ref ContextMenuInfo evento)
        {
            ConstructorInfo constructor = form.GetConstructor(new Type[] { typeof(ContextMenuInfo) });
            IForm newForm = null;

            try
            {
                newForm = (IForm)constructor.Invoke(new object[] { evento });
            }
            catch (NullReferenceException) { }
            catch (Exception ex) { throw ex; }

            return newForm;
        }
        public static IForm CreateForm(Type form, ref ReportDataInfo evento)
        {
            ConstructorInfo constructor = form.GetConstructor(new Type[] { typeof(ReportDataInfo) });
            //return (IForm)constructor.Invoke(new object[] { evento });
            IForm newForm = null;

            try
            {
                newForm = (IForm)constructor.Invoke(new object[] { evento });
            }
            catch (NullReferenceException) { }
            catch (Exception ex) { throw ex; }

            return newForm;
        }
        public static IForm CreateForm(Type form, ref ProgressBarEvent evento)
        {
            ConstructorInfo constructor = form.GetConstructor(new Type[] { typeof(ProgressBarEvent) });
            IForm newForm = null;

            try
            {
                newForm = (IForm)constructor.Invoke(new object[] { evento });
            }
            catch (NullReferenceException) { }
            catch (Exception ex) { throw ex; }

            return newForm;
        }
        public static IForm CreateForm(Type form, ref PrintEventInfo evento)
        {
            ConstructorInfo constructor = form.GetConstructor(new Type[] { typeof(PrintEventInfo) });
            IForm newForm = null;

            try
            {
                newForm = (IForm)constructor.Invoke(new object[] { evento });
            }
            catch (NullReferenceException) { }
            catch (Exception ex) { throw ex; }

            return newForm;
        }
        public static IForm CreateForm(Type form, ref BoAppEventTypes evento)
        {
            ConstructorInfo constructor = form.GetConstructor(new Type[] { typeof(BoAppEventTypes) });
            IForm newForm = null;

            try
            {
                newForm = (IForm)constructor.Invoke(new object[] { evento });
            }
            catch (NullReferenceException) { }
            catch (Exception ex) { throw ex; }

            return newForm;
        }
    }
}
