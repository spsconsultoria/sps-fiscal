using System;
using System.Collections.Generic;
using System.Text;

namespace SPS_Fiscal.Addon
{
    public interface IForm
    {
        bool ItemEvent();
        bool MenuEvent();
        bool FormDataEvent();
        bool AppEvent();
        bool PrintEvent();
        bool ProgressBarEvent();
        bool ReportDataEvent();
        bool RightClickEvent();
        bool StatusBarEvent();

    }
}
