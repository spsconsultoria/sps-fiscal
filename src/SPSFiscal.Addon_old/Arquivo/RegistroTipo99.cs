﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPS_Fiscal.Addon
{
    public class RegistroTipo99
    {
        private string linha;
        public string QtdReg { get; set; }

        public string LinhaArquivo()
        {
            linha = string.Empty;

            linha += "99";
            linha += "".PadRight(2, ' ');
            linha += QtdReg.PadLeft(5,'0');
            linha += "00001";

            return linha;
        }
    }
}
