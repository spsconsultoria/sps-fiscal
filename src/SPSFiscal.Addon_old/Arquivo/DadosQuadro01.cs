﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPS_Fiscal.Addon
{
    public class DadosQuadro01
    {
        private string linha;

        public string CFOP { get; set; }
        public string ValorContabil { get; set; }
        public string BaseCalculo { get; set; }
        public string ImpostoCreditado { get; set; }
        public string InsentaNaoTributaria { get; set; }
        public string Outras { get; set; }
        public string BaseCalculoImpostoRetido { get; set; }
        public string ImpostoRetido { get; set; }
        public string DirefencaAliquoa { get; set; }
        public string LinhaArquivo()
        {
            linha = string.Empty;
            linha += "22";
            linha += "01";
            linha += CFOP.PadLeft(5, '0');
            linha += ValorContabil.Replace(".","").PadLeft(17, '0');
            linha += BaseCalculo.Replace(".", "").PadLeft(17, '0');
            linha += ImpostoCreditado.Replace(".", "").PadLeft(17, '0');
            linha += InsentaNaoTributaria.Replace(".", "").PadLeft(17, '0');
            linha += Outras.Replace(".", "").PadLeft(17, '0');
            linha += BaseCalculoImpostoRetido.Replace(".", "").PadLeft(17, '0');
            linha += ImpostoRetido.Replace(".", "").PadLeft(17, '0');
            linha += DirefencaAliquoa.Replace(".", "").PadLeft(17, '0');

            return linha;
        }
    }
}
