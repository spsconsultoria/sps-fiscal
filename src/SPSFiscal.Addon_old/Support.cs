﻿using SAPbouiCOM;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SPS_Fiscal.Addon
{
    public class Support
    {
        #region CARREGA COMBO FILIAL
        public static void AdicionarComboFiliaisOBPL(ref SAPbouiCOM.Form oForm)
        {
            try
            {
                oForm.DataSources.UserDataSources.Add("cbFilial", BoDataType.dt_SHORT_TEXT);
                SAPbouiCOM.ComboBox oCombo = (SAPbouiCOM.ComboBox)oForm.Items.Item("cbFilial").Specific;
                oCombo.DataBind.SetBound(true, "", "cbFilial");
                string sQuery = string.Format(@"  select T2.BPLId,t2.BPLName from OUSR T0 
                    inner join USR6 T1 on T0.USER_CODE = t1.UserCode
                    inner join OBPL T2 on T1.BPLId = T2.BPLId
                    where T0.USER_CODE = '{0}'
                    order by t2.BPLName", Conexao.uiApplication.Company.UserName);
                System.Data.DataTable oDT = Conexao.ExecuteSqlDataTable(sQuery);

                for (int i = 0; i < oDT.Rows.Count; i++)
                {
                    oCombo.ValidValues.Add(oDT.Rows[i][0].ToString(), oDT.Rows[i][1].ToString());
                }

                if (oDT.Rows.Count == 0)
                {
                    sQuery = string.Format(@"select top 1 T1.U_BPLID, CompnyName from oadm T0, [@LFT_PARAMGERAISECD] T1", Conexao.uiApplication.Company.UserName);
                    oDT = Conexao.ExecuteSqlDataTable(sQuery);

                    oCombo.ValidValues.Add(oDT.Rows[0][0].ToString(), oDT.Rows[0][1].ToString());
                }

            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion

        #region CARREGA CONTABILISTA
        public static void CarregaContabilista(ref SAPbouiCOM.Form oForm)
        {
            try
            {
                SqlCommand command = new SqlCommand(@"SELECT [IdContabilista], [NomeContabilista] FROM [DIME_Contabilista]", Support.Conectar());
                SqlDataAdapter da = new SqlDataAdapter(command);
                System.Data.DataTable dt = new System.Data.DataTable();
                da.Fill(dt);

                SAPbouiCOM.ComboBox oCombo = (SAPbouiCOM.ComboBox)oForm.Items.Item("contab").Specific;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    oCombo.ValidValues.Add(dt.Rows[i][0].ToString(), dt.Rows[i][1].ToString());
                }
            }
            catch (SqlException erro)
            {
                throw new Exception(erro.Message);
            }
            finally
            {
                Support.Conectar().Close();
            }
        }
        #endregion

        #region CARREGA COMBO PERIODO
        public static void CarregaComboPeriodo(ref SAPbouiCOM.Form oForm)
        {
            try
            {
                string SQL = "SELECT code, Name FROM OFPR order by code desc";
                System.Data.DataTable dt = Conexao.ExecuteSqlDataTable(SQL);

                SAPbouiCOM.ComboBox oCombo = (SAPbouiCOM.ComboBox)oForm.Items.Item("periodo").Specific;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    oCombo.ValidValues.Add(dt.Rows[i][0].ToString(), dt.Rows[i][1].ToString());
                }
            }
            catch (SqlException erro)
            {
                throw new Exception(erro.Message);
            }
            finally
            {
                Support.Conectar().Close();
            }
        }
        #endregion

        #region CARREGA CONTRIBUINTE
        public static void CarregaContribuinte(ref SAPbouiCOM.Form oForm, bool Update)
        {
            try
            {
                SqlCommand command = new SqlCommand(@"SELECT [IdContribuinte], [NomeRazaoSocial] FROM [SPS_Fiscal].[dbo].[DIME_Contribuinte] WHERE [Excluido] = 'N'", Support.Conectar());
                SqlDataAdapter da = new SqlDataAdapter(command);
                System.Data.DataTable dt = new System.Data.DataTable();
                da.Fill(dt);

                SAPbouiCOM.ComboBox oCombo = (SAPbouiCOM.ComboBox)oForm.Items.Item("contri").Specific;

                if (Update)
                {
                    while (oCombo.ValidValues.Count > 0)
                    {
                        oCombo.ValidValues.Remove(0, BoSearchKey.psk_Index);
                    }
                }

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    oCombo.ValidValues.Add(dt.Rows[i][0].ToString(), dt.Rows[i][1].ToString());
                }
            }
            catch (SqlException erro)
            {
                throw new Exception(erro.Message);
            }
            finally
            {
                Support.Conectar().Close();
            }
        }
        #endregion

        #region CONEXAO VIA APP.CONFIG
        public static SqlConnection Conectar()
        {
            string UserDB = string.Empty;
            string PassDB = string.Empty;
            string Server = string.Empty;

            string sql = "select * from [@SPS_CON]";
            System.Data.DataTable dt = Conexao.ExecuteSqlDataTable(sql);

            if (dt.Rows.Count > 0)
            {
                UserDB = dt.Rows[0][2].ToString();
                PassDB = dt.Rows[0][3].ToString();
                Server = dt.Rows[0][4].ToString();
            }
            else
            {
                throw new Exception("configurações de conexão com o banco de dados não cadastradas");
            }

            string conn = string.Format(@"Data Source={0},1433;Initial Catalog=SPS_Fiscal;User Id={1};Password={2}", Server, UserDB, PassDB);
            SqlConnection conexao = new SqlConnection(conn);
            try
            {
                if (conexao.State == ConnectionState.Closed)
                {
                    conexao.Open();
                }
                return conexao;
            }
            catch (SqlException erro)
            {
                throw new Exception("Erro ao conectar com o banco de dados SQL Server: " + erro.Message);
            }
        }
        #endregion

        #region Abrir Arquivo

        static public CultureInfo pt_BR = new CultureInfo("pt-BR");

        [DllImport("user32.dll")]

        public static extern IntPtr GetForegroundWindow();

        static public String GetFileNameViaOFD(String filter, String initialDirectory, String dialogTitle, bool CheckFileExists)
        {
            GetFileNameClass oGetFileName = new GetFileNameClass();
            oGetFileName.Filter = filter;
            oGetFileName.InitialDirectory = initialDirectory;
            oGetFileName.DialogTitle = dialogTitle;
            oGetFileName.CheckFileExists = CheckFileExists;
            Thread threadGetFileName = new Thread(oGetFileName.GetFileName);
            threadGetFileName.SetApartmentState(ApartmentState.STA);

            try
            {
                threadGetFileName.Start();

                // Wait for thread to get started.
                while (!threadGetFileName.IsAlive)
                {
                }

                // Wait a milisec more.
                Thread.Sleep(1);

                // Wait for thread to end.
                threadGetFileName.Join();

                // Use file name as you will here
                return oGetFileName.FileName;

            }
            catch (Exception ex_GetFileNameViaOFD)
            {
                // Simply rethrow the exception.
                throw (ex_GetFileNameViaOFD);
            }
        }

        private class GetFileNameClass
        {
            private readonly SaveFileDialog _oFileDialog;

            // Properties
            public String DialogTitle
            {
                set { _oFileDialog.Title = value; }
            }

            public String FileName
            {
                get { return _oFileDialog.FileName; }
            }

            public String Filter
            {
                set { _oFileDialog.Filter = value; }
            }

            public String InitialDirectory
            {
                set { _oFileDialog.InitialDirectory = value; }
            }
            public bool CheckFileExists
            {
                set { _oFileDialog.CheckFileExists = value; }
            }


            // Constructor
            public GetFileNameClass()
            {
                _oFileDialog = new SaveFileDialog();
            }

            // Methods

            public void GetFileName()
            {
                IntPtr ptr = GetForegroundWindow();
                WindowWrapper oWindow = new WindowWrapper(ptr);
                if (_oFileDialog.ShowDialog(oWindow) != DialogResult.OK)
                {
                    _oFileDialog.FileName = string.Empty;
                }
            }
            private class WindowWrapper : IWin32Window
            {
                private readonly IntPtr _hwnd;

                // Property
                public virtual IntPtr Handle
                {
                    get { return _hwnd; }
                }

                // Constructor
                public WindowWrapper(IntPtr handle)
                {
                    _hwnd = handle;
                }
            }
        }
        #endregion
    }
}
