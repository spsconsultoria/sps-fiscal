﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPSFiscal.Model
{
    public class DimeTabelaQuadro90Model
    {
        public string NumeroItem { get; set; }
        public string Descricao { get; set; }
        public string TipoItem { get; set; }
        public virtual List<DimeDadosQuadro90Model> DadosQuadro90 { get; set; }
    }
}
