﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPSFiscal.Model
{
    public class DimeTabelaCodAtividadeModel
    {
        public string Codigo { get; set; }
        public string Descricao { get; set; }
    }
}
