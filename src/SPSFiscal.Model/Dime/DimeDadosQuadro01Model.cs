﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPSFiscal.Model
{
    public class DimeDadosQuadro01Model
    {
        public int IdLancamento { get; set; }
        public virtual DimeDeclaracaoModel Declaracao { get; set; }
        public int IdDeclaracao { get; set; }
        public string CFOP { get; set; }
        public decimal Vl_Contabil { get; set; }
        public decimal Vl_Base_Calc { get; set; }
        public decimal Vl_Imp_Cred { get; set; }
        public decimal Vl_Isentas_Nao_Trib { get; set; }
        public decimal Vl_Outras { get; set; }
        public decimal Vl_Base_Calc_IR { get; set; }
        public decimal Vl_IR { get; set; }
        public decimal Vl_Difal { get; set; }
    }
}
