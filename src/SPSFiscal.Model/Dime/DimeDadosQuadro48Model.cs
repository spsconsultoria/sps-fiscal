﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPSFiscal.Model
{
    public class DimeDadosQuadro48Model
    {
        public int IdLancamento { get; set; }
        public virtual DimeDeclaracaoModel Declaracao { get; set; }
        public int IdDeclaracao { get; set; }
        public string Cod_Municipio { get; set; }
        public decimal Vl_Perc_Ad { get; set; }
        public string Cod_Tipo_Atv { get; set; }
    }
}
