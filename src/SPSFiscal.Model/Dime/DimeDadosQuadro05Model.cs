﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPSFiscal.Model
{
    public class DimeDadosQuadro05Model
    {
        public int IdDadosQuadro5 { get; set; }
        public virtual DimeTabelaQuadro05Model TabelaQuadro05 { get; set; }
        public string NumeroItem { get; set; }
        public virtual DimeDeclaracaoModel Declaracao { get; set; }
        public int IdDeclaracao { get; set; }
        public decimal Valor { get; set; }
    }
}
