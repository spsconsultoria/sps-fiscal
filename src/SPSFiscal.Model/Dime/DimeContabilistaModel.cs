﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPSFiscal.Model
{
    public class DimeContabilistaModel
    {
        public int IdContabilista { get; set; }
        public string CPF { get; set; }
        public string NomeContabilista { get; set; }
        public string Excluido { get; set; }

        public virtual List<DimeContribuinteModel> Contribuinte { get; set; }
    }
}
