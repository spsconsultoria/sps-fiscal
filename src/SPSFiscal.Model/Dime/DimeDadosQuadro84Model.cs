﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPSFiscal.Model
{
    public class DimeDadosQuadro84Model
    {
        public int IdDadosQuadro84 { get; set; }
        public virtual DimeTabelaQuadro84Model TabelaQuadro84 { get; set; }
        public string NumeroItem { get; set; }
        public virtual DimeDeclaracaoModel Declaracao { get; set; }
        public int IdDeclaracao { get; set; }
        public decimal Valor { get; set; }
    }
}
