﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPSFiscal.Model.Dime
{
    public class DimePeriodoApuracaoModel
    {
        public int IdPeriodo { get; set; }
        public string DB { get; set; }
        public string Filial { get; set; }
        public string TipoReg { get; set; }
        public int IdDeclaracao { get; set; }
        public virtual  DimeContribuinteModel Contribuinte { get; set; }
        public int IdContribuinte { get; set; }
        public string Periodo { get; set; }
        public string Usuario { get; set; }
        public virtual List<DimeDebitosApuracaoModel> DebitosApuracao { get; set; }
        public virtual List<DimeEntradasP1Model> EntradaP1 { get; set; }
        public virtual List<DimeSaidasP2Model> SaidasP2 { get; set; }
    }
}
