﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SPSFiscal.Model.Dime;

namespace SPSFiscal.Model
{
    public class DimeContribuinteModel
    {
        public int IdContribuinte { get; set; }
        public virtual DimeContabilistaModel Contabilista { get; set; }
        public int IdContabilista { get; set; }
        public string NomeRazaoSocial { get; set; }
        public string CNPJ { get; set; }
        public string NroInscricao { get; set; }
        public string TipoDeclaracao { get; set; }
        public string RegimeApuracao { get; set; }
        public string PorteEmpresa { get; set; }
        public string ApuracaoConsolidada { get; set; }
        public string ApuracaoCentralizada { get; set; }
        public string TemCreditosPresumido { get; set; }
        public string TemCredIncentFiscais { get; set; }
        public string SubstitutoTributario { get; set; }
        public string TemEscritaContabil { get; set; }
        public int QtdeTrabAtividade { get; set; }
        public string ErpIdEmpresa { get; set; }
        public string ErpBaseDados { get; set; }
        public string Excluido { get; set; }

        public virtual List<DimeContribuinteDeclaracaoModel> ContribuinteDeclaracao { get; set; }
        public virtual List<DimePeriodoApuracaoModel> PeriodoApuracao { get; set; }
        public virtual List<DimeDeclaracaoModel> Declaracao { get; set; }

    }
}
