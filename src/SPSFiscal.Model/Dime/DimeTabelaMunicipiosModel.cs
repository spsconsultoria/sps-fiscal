﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPSFiscal.Model
{
    public class DimeTabelaMunicipiosModel
    {
        public string Codigo { get; set; }
        public string NomeMunicipio { get; set; }
    }
}
