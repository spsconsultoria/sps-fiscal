﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SPSFiscal.Model.Dime;

namespace SPSFiscal.Model
{
    public class DimeTabelaQuadro04Model
    {
        public string NumeroItem { get; set; }
        public string Descricao { get; set; }
        public string TipoItem { get; set; }
        public virtual List<DimeDadosQuadro04Model> DadosQuadro04 { get; set; }
        public virtual List<DimeDebitosApuracaoModel> DebitosApuracao { get; set; }
    }
}
