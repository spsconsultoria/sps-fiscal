﻿namespace SPSFiscal.Model
{
    public class DimeDadosQuadro49Model
    {
        public int IdLancamento { get; set; }
        public virtual DimeDeclaracaoModel Declaracao { get; set; }
        public int IdDeclaracao { get; set; }
        public string Sigla_Estado { get; set; }
        public decimal Vl_Contabil { get; set; }
        public decimal Vl_Base_Calc { get; set; }
        public decimal Vl_Outras { get; set; }
        public decimal Vl_ST { get; set; }
        public decimal Vl_ST_Outros { get; set; }

    }
}
