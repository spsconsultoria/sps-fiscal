﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPSFiscal.Model
{
    public class DimeTabelaQuadro81Model
    {
        public string NumeroItem { get; set; }
        public string Descricao { get; set; }
        public string TipoItem { get; set; }
        public virtual List<DimeDadosQuadro81Model> DadosQuadro81 { get; set; }
    }
}
