﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPSFiscal.Model
{
    public class DimeDadosQuadro46Model
    {
        public int IdLancamento { get; set; }
        public virtual DimeDeclaracaoModel Declaracao { get; set; }
        public int IdDeclaracao { get; set; }
        public int Seq_Reg { get; set; }
        public string Cod_Identif { get; set; }
        public decimal Vl_Cred_Apur { get; set; }
        public string Ind_Origem { get; set; }
    }
}
