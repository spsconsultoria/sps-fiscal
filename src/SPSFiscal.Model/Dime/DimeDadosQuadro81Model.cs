﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPSFiscal.Model
{
    public class DimeDadosQuadro81Model
    {
        public int IdDadosQuadro81 { get; set; }
        public string NumeroItem { get; set; }
        public virtual DimeDeclaracaoModel Declaracao { get; set; }

        public int IdDeclaracao { get; set; }
        public decimal Valor { get; set; }
    }
}
