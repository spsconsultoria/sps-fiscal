﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPSFiscal.Model
{
    public class DimeDadosQuadro04Model
    {
        public int IdDadosQuadro4 { get; set; }
        public virtual DimeTabelaQuadro04Model TabelaQuadro04 { get; set; }
        public string NumeroItem { get; set; }
        public virtual DimeDeclaracaoModel Declaracao { get; set; }
        public int IdDeclaracao { get; set; }
        public decimal Valor { get; set; }
    }
}
