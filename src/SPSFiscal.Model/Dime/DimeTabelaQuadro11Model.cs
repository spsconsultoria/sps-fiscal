﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPSFiscal.Model
{
    public class DimeTabelaQuadro11Model
    {
        public string NumeroItem { get; set; }
        public string Descricao { get; set; }
        public string TipoItem { get; set; }
        public virtual List<DimeDadosQuadro11Model> DadosQuadro11 { get; set; }
    }
}
