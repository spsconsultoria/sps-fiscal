﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPSFiscal.Model
{
    public class DimeRegistroDeclaracaoModel
    {
        public int IdRegistroDeclaracao { get; set; }
        public int IdDeclaracao { get; set; }
        public int TipoRegistro { get; set; }
        public virtual DimeDeclaracaoModel Declaracao { get; set; }
        public virtual DimeTipoRegistroModel TipoRegistroV { get; set; }
    }
}
