﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SPSFiscal.Model.Dime;

namespace SPSFiscal.Model
{
    public class DimeEntradasP1Model
    {
        public virtual DimePeriodoApuracaoModel PeriodoApuracao { get; set; }
        
        public int IdEntrada { get; set; }
        public int IdPeriodo { get; set; }
        public string BD { get; set; }
        public string Filial { get; set; }
        public string TPDoc { get; set; }
        public DateTime DataLanc { get; set; }
        public DateTime DataDoc { get; set; }
        public string Serial { get; set; }
        public string Especie { get; set; }
        public string Estado { get; set; }
        public string CFOP { get; set; }
        public decimal Vl_Contabil { get; set; }
        public decimal Vl_Base_Calc { get; set; }
        public decimal Aliq { get; set; }
        public decimal Vl_Imp_Cred { get; set; }
        public decimal Vl_Isentas_Nao_Trib { get; set; }
        public decimal Vl_Outras { get; set; }
        public decimal Vl_Base_ST { get; set; }
        public decimal Vl_ST { get; set; }
        public decimal Vl_Base_Calc_IR { get; set; }
        public decimal Vl_IR { get; set; }
        public decimal Vl_Difal { get; set; }
    }
}
