﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPSFiscal.Model
{
    public class DimeDadosQuadro50Model
    {
        public int IdLancamento { get; set; }
        public virtual DimeDeclaracaoModel Declaracao { get; set; }
        public int IdDeclaracao { get; set; }
        public string Sigla_Estado { get; set; }
        public decimal Vl_Contabil_Nao_Contrib { get; set; }
        public decimal Vl_Contabil_Contrib { get; set; }
        public decimal Vl_Base_Calc_Nao_Contrib { get; set; }
        public decimal Vl_Base_Calc_Contrib { get; set; }
        public decimal Vl_Outras { get; set; }
        public decimal Vl_ST { get; set; }
    }
}
