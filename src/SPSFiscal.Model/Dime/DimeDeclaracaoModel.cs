﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPSFiscal.Model
{
    public class DimeDeclaracaoModel
    {
        
        public int IdDeclaracao { get; set; }
        public int IdContribuinte { get; set; }
        public DateTime DtInicio { get; set; }
        public DateTime DtTermino { get; set; }
        public string NomeContabilista { get; set; }
        public string ContabilistaCPF { get; set; }
        public string NomeContribuinte { get; set; }
        public string NroInscricao { get; set; }
        public string ContribuinteCNPJ { get; set; }
        public string PeriodoReferencia { get; set; }
        public string TipoDeclaracao { get; set; }
        public string RegimeApuracao { get; set; }
        public string PorteEmpresa { get; set; }
        public string ApuracaoConsolidada { get; set; }
        public string ApuracaoCentralizada { get; set; }
        public string TransCredPeriodo { get; set; }
        public string TemCreditosPresumido { get; set; }
        public string TemCredIncentFiscais { get; set; }
        public string Movimento { get; set; }
        public string SubstitutoTributario { get; set; }
        public string TemEscritaContabil { get; set; }
        public int QtdeTrabAtividade { get; set; }
        public string ErpBaseDados { get; set; }
        public string Excluido { get; set; }
        public virtual List<DimeRegistroDeclaracaoModel> RegistroDeclaracao { get; set; }
        public virtual List<DimeDadosQuadro01Model> DadosQuadro01 { get; set; }
        public virtual List<DimeDadosQuadro02Model> DadosQuadro02 { get; set; }
        public virtual List<DimeDadosQuadro03Model> DadosQuadro03 { get; set; }
        public virtual List<DimeDadosQuadro04Model> DadosQuadro04 { get; set; }
        public virtual List<DimeDadosQuadro05Model> DadosQuadro05 { get; set; }
        public virtual List<DimeDadosQuadro09Model> DadosQuadro09 { get; set; }
        public virtual List<DimeDadosQuadro11Model> DadosQuadro11 { get; set; }
        public virtual List<DimeDadosQuadro12Model> DadosQuadro12 { get; set; }
        public virtual List<DimeDadosQuadro14Model> DadosQuadro14 { get; set; }
        public virtual List<DimeDadosQuadro46Model> DadosQuadro46 { get; set; }
        public virtual List<DimeDadosQuadro48Model> DadosQuadro48 { get; set; }
        public virtual List<DimeDadosQuadro49Model> DadosQuadro49 { get; set; }
        public virtual List<DimeDadosQuadro50Model> DadosQuadro50 { get; set; }
        public virtual List<DimeDadosQuadro80Model> DadosQuadro80 { get; set; }
        public virtual List<DimeDadosQuadro81Model> DadosQuadro81 { get; set; }
        public virtual List<DimeDadosQuadro82Model> DadosQuadro82 { get; set; }
        public virtual List<DimeDadosQuadro83Model> DadosQuadro83 { get; set; }
        public virtual List<DimeDadosQuadro84Model> DadosQuadro84 { get; set; }
        public virtual List<DimeDadosQuadro90Model> DadosQuadro90 { get; set; }
        public virtual List<DimeDadosQuadro91Model> DadosQuadro91 { get; set; }
        public virtual List<DimeDadosQuadro92Model> DadosQuadro92 { get; set; }
        public virtual List<DimeDadosQuadro93Model> DadosQuadro93 { get; set; }
        public virtual List<DimeDadosQuadro94Model> DadosQuadro94 { get; set; }
        public virtual List<DimeContribuinteModel> Contribuinte { get; set; }
    }
}
