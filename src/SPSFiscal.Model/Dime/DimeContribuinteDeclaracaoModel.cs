﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPSFiscal.Model
{
    public class DimeContribuinteDeclaracaoModel
    {
        public int IdDeclaracao { get; set; }
        public virtual DimeContribuinteModel Contribuinte{ get; set; }
        public int IdContribuinte { get; set; }
        public string PeriodoReferencia { get; set; }
        public DateTime DtCriacao { get; set; }
        public string HrCriacao { get; set; }
    }
}
