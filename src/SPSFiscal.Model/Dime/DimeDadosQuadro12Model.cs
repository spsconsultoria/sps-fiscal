﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPSFiscal.Model
{
    public class DimeDadosQuadro12Model
    {
        public int IdLancamento { get; set; }
        public virtual DimeDeclaracaoModel Declaracao { get; set; }
        public int IdDeclaracao { get; set; }
        public string Ind_Org_Recol { get; set; }
        public string Cod_Receita { get; set; }
        public DateTime Dt_Vcto_Recol { get; set; }
        public decimal Vl_Recol { get; set; }
        public string Cod_Class_Vcto { get; set; }
        public string Nro_Acordo { get; set; }
    }
}
