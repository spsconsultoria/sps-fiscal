﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPSFiscal.Model
{
    public class DimeDadosQuadro03Model
    {
        public int IdDadosQuadro3 { get; set; }
        public virtual DimeTabelaQuadro03Model TabelaQuadro03 { get; set; }
        public string NumeroItem { get; set; }
        public int IdDeclaracao { get; set; }
        public virtual DimeDeclaracaoModel Declaracao { get; set; }
        public decimal Valor { get; set; }
    }
}
