﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SPSFiscal.Model.Dime;

namespace SPSFiscal.Model
{
    public class DimeDebitosApuracaoModel
    {
        public virtual DimePeriodoApuracaoModel PeriodoApuracao { get; set; }
        public int IdPeriodo { get; set; }
        public virtual DimeTabelaQuadro04Model TabelaDados04 { get; set; }
        public string NumeroItem { get; set; }
        public decimal Vl_Ajuste { get; set; }
    }
}
