﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPSFiscal.Model
{
    public class DimeDadosQuadro11Model
    {
        public int IdDadosQuadro11 { get; set; }
        public virtual DimeTabelaQuadro11Model TabelaQuadro11 { get; set; }
        public int ID { get; set; }
        public string NumeroItem { get; set; }
        public virtual DimeDeclaracaoModel Declaracao { get; set; }
        public int IdDeclaracao { get; set; }
        public decimal Valor { get; set; }
    }
}
