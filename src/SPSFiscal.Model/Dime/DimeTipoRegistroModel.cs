﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPSFiscal.Model
{
    public class DimeTipoRegistroModel
    {
        public string Quadro { get; set; }
        public int TipoRegistro { get; set; }
        public string Descricao { get; set; }
        public virtual List<DimeRegistroDeclaracaoModel> RegistroDeclaracao { get; set; }

    }
}
